function validarFormValoracion(idform) {
    $(idform).validate({
        rules: {
            orden: {
                required: true,
                number: true
            },
            inicial: {
                required: true,
                number: true,
                range: [0, 5.00001],
                menor: idform + " input[name=final]"
            },
            final: {
                required: true,
                number: true,
                range: [0, 5.00001],
                mayor: idform + " input[name=inicial]"
            },
            titulo: {
                required: true
            }
        },
        messages: {
            orden: {
                required: "ingrese orden"
            },
            inicial: {
                required: "ingrese inicial",
                range: "ingrese valor entre 0 y 5",
                menor: "inicial debe ser menor que final"

            },
            final: {
                required: "ingrese final",
                range: "ingrese valor entre 0 y 5",
                mayor: "final debe ser mayor que inicial"
            },
            titulo: {
                required: "ingrese titulo"
            }
        }
    });
}

function saveChanges(idform) {
    var formula = $("#" + idform);
    if (!formula.valid()) {
        return;
    }
    var metodo = formula.find("input[name=metodo]").val();
    var data = ajax("POST", metodo, "CValoracion.php", formula.serialize());
    if ($(data).attr("id") == "bad") {
        formula.find(".respuesta").html(data);
    } else {
        formula.find(".respuesta").html(data);
    }
}

function validarTodosLosForm() {
    $(".formhorizontal").each(function() {
        validarFormValoracion("#" + $(this).attr("id"));
    });
}

$(document).ready(function() {

    jQuery.validator.addMethod("menor", function(value, element, param) {
        var fin = parseFloat($(param).val());
        var ini = parseFloat(value);
        if (ini >= fin) {
            return false;
        }
        return true;
    });

    jQuery.validator.addMethod("mayor", function(value, element, param) {
        var ini = parseFloat($(param).val());
        var fin = parseFloat(value);
        if (fin <= ini) {
            return false;
        }
        return true;
    });

    $("#BotonAno").change(function() {
        var ano = $(this).find("option:selected").val();
        redirec(host + "/vista/html/valoracion.php?&ano=" + ano);
    });

    $(".daletebutton").click(function() {
        var orden = ($(this).attr("id").slice(6));
        var ano = $("#BotonAno").val();
        if (confirm("Realmente desea eliminar esta valoracion?")) {
            var data = ajax("POST", "deleteValoracion", "CValoracion.php", "year=" + ano + "&orden=" + orden);
            if ($(data).attr("id") == "ok") {
                redirec(host + "/vista/html/valoracion.php?&ano=" + ano);
            } else {
                $("#" + orden + " .respuesta").html(data);
            }
        }
    });

    $("#new").click(function() {
        var year = $("#BotonAno").val();
        var id = 0;
        $(".formhorizontal").each(function() {
            id = $(this).attr("id");
        });
        id = parseInt(id) + 1;
        $("#configurarValoraciones").append("<div class=\"divformhorizontal\">" +
                "<form class=\"formhorizontal\" id=\"" + id + "\" role=\"form\">" +
                "<div style=\"display: none\">" +
                "<input name=\"year\" value=\"" + year + "\">" +
                "<input name=\"metodo\" value=\"saveValoracion\">" +
                "</div>" +
                "<div>" +
                "<label>Orden</label><input name=\"orden\"  value=\"" + id + "\"class=\"form-control\" type=\"number\">" +
                "</div>" +
                "<div>" +
                "<label>Inicial</label><input name=\"inicial\" class=\"form-control\" type=\"number\" >" +
                "</div>" +
                "<div>" +
                "<label>Final</label><input name=\"final\" class=\"form-control\" type=\"number\" >" +
                "</div>" +
                "<div>" +
                "<label>Titulo</label><input name=\"titulo\" class=\"form-control titu\" type=\"text\" >" +
                "</div>" +
                "<div>" +
                "<button id=\"button4\" class=\"btn btn-primary\" type=\"submit\">Guardar</button>" +
                "<p class=\"respuesta\"></p>" +
                "</div>" +
                "</form>" +
                "</div>");
        validarFormValoracion("#" + id);
        $(".formhorizontal").submit(function() {
            saveChanges($(this).attr("id"));
            return false;
        });
    });

    $(".formhorizontal").submit(function() {
        saveChanges($(this).attr("id"));
        return false;
    });

    validarTodosLosForm();

});




