function validarFormPerido(idform) {
    $(idform).validate({
        rules: {
            fechaInical: {
                required: true,
            },
            fechafinal: {
                required: true,
            }
        },
        messages: {
            fechaInical: {
                required: "Ingrese fecha inicial",
            },
            fechafinal: {
                required: "Ingrese fecha final",
            }
        }
    });
}

function saveChanges(idform) {
    var formula = $("#" + idform);
    if (!formula.valid())
        return;
    var metodo = formula.find("[name='metodo']").val();
    var year = formula.find("[name='year']").val();
    var periodo = formula.find("[name='periodo']").val();
    var fechafinal = formula.find("[name='fechafinal']").datepicker('getDate');
    var fechaInical = formula.find("[name='fechaInical']").datepicker('getDate');
    var values = "year=" + year + "&periodo=" + periodo + "&fechafinal=" + fechafinal + "&fechaInical=" + fechaInical;
    var data = ajax("POST", metodo, "CPlazo.php", values);
    formula.find(".respuesta").html(data);
}

function validarTodosLosForm() {
    var n = parseInt($("#configurarPlazos").attr("title"));
    for (var i = 0; i < n; i++) {
        validarFormPerido("#formularioPeriodo" + (i + 1));
    }
}

$(document).ready(function() {

    $(".formsperido input").datepicker({
        dateFormat: "DD d MM yy",
        changeMonth: true
    });

    $("#BotonAno").change(function() {
        var ano = $(this).find("option:selected").val();
        redirec(host + "/vista/html/Configurar.php?&ano=" + ano);
    });

    $(".formperiodo").submit(function() {
        saveChanges($(this).attr("id"));
        return false;
    });

    validarTodosLosForm();

});




