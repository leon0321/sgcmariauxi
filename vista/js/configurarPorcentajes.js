function validarFormPorcentaje(idform) {
    $(idform).validate({
        rules: {
            porcentaje: {
                required: true,
                number: true,
                range: [0, 100]
            }
        },
        messages: {
            porcentaje: {
                required: "Ingrese porcentaje",
                number: "Este Campo debe ser numerico"
            }
        }
    });
}

function saveChanges(idform) {
    var formula = $("#" + idform);
    if (!formula.valid())
        return;
    var metodo = formula.find("[name='metodo']").val();
    var year = formula.find("[name='year']").val();
    var periodo = formula.find("[name='periodo']").val();
    var porcentaje = formula.find("[name='porcentaje']").val();
    var values = "year=" + year + "&periodo=" + periodo + "&porcentaje=" + porcentaje;
    var data = ajax("POST", metodo, "CPorcentaje.php", values);
    formula.find(".respuesta").html(data);
}

$(document).ready(function() {

    $(".formsperido input").datepicker({
        dateFormat: "DD d MM yy",
        changeMonth: true
    });

    $("#BotonAno").change(function() {
        var ano = $(this).find("option:selected").val();
        redirec(host + "/vista/html/Configurarporcentajes.php?&ano=" + ano);
    });

    $(".formPorcentaje").submit(function() {
        saveChanges($(this).attr("id"));
        return false;
    });

    validarFormPorcentaje("#formularioPorcentaje1");
    validarFormPorcentaje("#formularioPorcentaje2");
    validarFormPorcentaje("#formularioPorcentaje3");
    validarFormPorcentaje("#formularioPorcentaje4");

});




