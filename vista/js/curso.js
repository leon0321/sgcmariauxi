function  validarFormCurso(c) {
    $(c).validate({
        rules: {
            letra: {
                required: true,
                accept: "[A-Z]+",
                maxlength: 1

            },
            grado: {
                required: true,
                number: true,
                range: [0, 11]
            },
            director: {
                required: true
            },
            profe: {
                required: true
            },
            fecha: {
                required: true,
                number: true,
                range: [2000, 2050]
            }
        },
        messages: {
            letra: {
                required: "Ingrese letra Mayuscula",
                accept: "Ingrese letra Mayuscula"

            },
            grado: {
                required: "Ingrese grado",
                number: "Debe ser numero"
            },
            director: {
                required: "Ingrese director de curso"
            },
            profe: {
                required: "Seleccione director de grupo"
            },
            fecha: {
                required: "ingrese año",
                number: "no es valido",
            }
        }
    });
}


function guardarCurso() {
    if (!$("#formularioCurso").valid()) {
        return;
    }
    guardar("#formularioCurso", "guadarCurso", "curso.php", "CCursos.php");
}

function actualizar(formmulario, metodo, idEtiqueta, controlador) {
    if (!$(formmulario).valid()) {
        return;
    }
    var url = host + "/controlador/" + controlador + "?metodo=" + metodo;
    $.ajax({
        type: "POST",
        url: url,
        data: $(formmulario).serialize(), // serializes the form's elements.
        success: function(data)
        {
            alert(data);
            if ($(data).attr("id") == "error") {
                alert($(data).text());
            } else {
                $(idEtiqueta).html(data);
                $("#divformuAsignatura").dialog("close");
                return;
            }
        }
    });
}

function cambiarDirector(idprofe, idcurso, idEtiqueta) {
    var controlador = "CCursos.php";
    idprofe = $("#selectDirector").val();
    var metodo = "cambiarDirector"
    var url = host + "/controlador/" + controlador + "?metodo=" + metodo;
    $.ajax({
        type: "POST",
        url: url,
        data: "curso=" + idcurso + "&profe=" + idprofe, // serializes the form's elements.
        success: function(data)
        {
            if ($(data).attr("id") == "error") {
                alert(data);
            } else {
                $(idEtiqueta + " .nombreDirector").html(data);
                $("#divCambiarDirector").dialog("close");
            }
        }
    });
}

function editCurso(idcurso) {
    formulario("printFormCurso&idcurso=" + idcurso, "CCurso.php", "#divformCurso");
    validarFormCurso("#formularioCurso");
    $("#divformCurso").dialog({
        width: 340,
        modal: true,
        buttons: {
            "Guardar": function() {
                actualizar("#formularioCurso", "editarAsignatura", "#trcurso" + idcurso, "CCurso.php");
            },
            "Cacelar": function() {
                $(this).dialog("close");
            }
        }
    });
    $("#divformuAsignatura").dialog("open");
}

$(document).ready(function() {

    jQuery.validator.addMethod("accept", function(value, element, param) {
        return value.match(new RegExp("^" + param + "$"));
    });

    $('.table').dataTable({
        "scrollY": "420px",
        "scrollX": true,
        "scrollCollapse": true,
        "paging": false
    });

    $("#BotonAno").change(function() {
        redirec(host + "/vista/html/curso.php?ano=" + $(this).val());
    });

    $("#buttonNewCurso").click(function() {
        validarFormCurso("#formularioCurso");
        $("#divformCurso").dialog({
            width: 340,
            modal: true,
            buttons: {
                "Guardar": function() {
                    guardarCurso();
                },
                "Cacelar": function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#divformCurso").dialog("open");
    });

    $(".buttonCambiarDirector").live("click", function() {
        var curso = $(this).attr("id");
        $("#trcurso" + curso).css("background-color", "#D4D1F1");
        $("#divCambiarDirector").dialog({
            width: 400,
            modal: true,
            buttons: {
                "Guardar": function() {
                    cambiarDirector($("#selectDirector").val(), curso, "#trcurso" + curso);
                    $("#trcurso" + curso).css("background-color", "");
                },
                "Cacelar": function() {
                    $(this).dialog("close");
                    $("#trcurso" + curso).css("background-color", "");
                }
            }
        });

    });



});




