function  validarFormLectivo(c) {
    $(c).validate({
        rules: {
            inicio: {
                required: true,
                dpDate: true,
                dpCompareDate: ['before', c + ' input[name=fin]'],
                rangeYear: [(c + " input[name=year]"), (c + " input[name=year]")]
            },
            year: {
                required: true,
                number: true,
                range: [2000, 2040]
            },
            fin: {
                required: true,
                dpDate: true,
                dpCompareDate: ['after', c + ' input[name=inicio]'],
                rangeYear: [(c + " input[name=year]"), (c + " input[name=year]")]
            }
        },
        messages: {
            inicio: {
                required: "Ingrese fecha inicial",
                dpCompareDate: "Esta fucha es mayor que la final",
                rangeYear: "Fecha no coincide con año lectivo"
            },
            fin: {
                required: "ingrese fecha final",
                dpCompareDate: "Esta fucha es menor que la inicial",
                rangeYear: "Fecha no coincide con año lectivo"
            }
        }
    });
}

function guardarLectivo() {
    var form = $("#formularioLectivo");
    if (!form.valid()) {
        return;
    }
    var year = form.find("[name='year']").val();
    var rector = form.find("[name='rector']").val();
    var nperiodos = form.find("[name='nperiodos']").val();
    var inicio = form.find("[name='inicio']").datepicker('getDate');
    var fin = form.find("[name='fin']").datepicker('getDate');
    var values = "year=" + year + "&nperiodos=" + nperiodos + "&inicio=" + inicio + "&fin=" + fin + "&rector=" + rector;
    var data = ajax("POST", "guardarLectivo", "CLectivo.php", values);
    if ($(data).attr("id") == "bad") {
        alert((data));
    } else {
        redirec(host + "/vista/html/lectivo.php");
    }

}

$(document).ready(function() {

    jQuery.validator.addMethod("rangeYear", function(value, element, param) {
        var year = parseInt($(element).datepicker('getDate').getUTCFullYear());
        var ini = parseInt($(param[0]).val());
        var end = parseInt($(param[1]).val());
        if (year < ini || year > end) {
            return false;
        }
        return true;
    });

    $(".fecha").datepicker({
        dateFormat: "DD d MM yy",
        changeMonth: true,
        changeYear: true
    });

    $('.table').dataTable({
        "scrollCollapse": true,
        "paging": false
    });

    $("#buttonNewLectivo").click(function() {
        validarFormLectivo("#formularioLectivo");
        $("#divformlectivo").dialog({
            width: 430,
            modal: true,
            buttons: {
                "Guardar": function() {
                    guardarLectivo();
                },
                "Cacelar": function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#divformlectivo").dialog("open");
    });

});





