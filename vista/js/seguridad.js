/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function cambiarPass() {
    $(".pass").html("");
    $("#respuesta").html("");
    if (!$("#formRecuperarContrasena").valid())
        return;
    var us = $("#u").val();
    var pass = $("#pass").val();
    var pass1 = $("#pass1").val();
    var pass2 = $("#pass2").val();
    if (pass1 != pass2) {
        $(".pass").html("<b>Las contraseñas nuevas no coinciden<b>");
        return false;
    }
    var data = ajax("POST", "cambiarContrasena", "Clogin.php", $("#formRecuperarContrasena").serialize());
    if ($(data).attr("id") == "ok") {
        $("#divformRecuperar").html("<h2 class=\"form - signin - heading\">" + $(data).text() + "</h2>");
        $("#divformRecuperar").append("<button id=\"buttonAceptar\" class=\"btn btn - lg btn - primary btn - block\" type=\"Enviar\" style=\"margin - top: 20px\"; >Aceptar</button>");
    } else {
        $("#respuesta").html("<b>" + $(data).text() + "</b>")
    }
}

$(document).ready(function() {

    $("#buttonAceptar").live("click", function() {
        redirec(host + "/vista/html/InsertarNotas.php");
    });

    $("#formRecuperarContrasena").submit(function() {
        cambiarPass();
        return false;
    });

    $("#formRecuperarContrasena").validate({
        rules: {
            username: {
                required: true,
            },
            pass: {
                required: true,
                minlength: 6,
            },
            newpass1: {
                required: true,
                minlength: 6,
            },
            newpass2: {
                required: true,
                minlength: 6,
            }
        },
        messages: {
            username: {
                required: "Ingrese codigo",
            },
            pass: {
                required: "Ingrese contraseña",
            },
            newpass1: {
                required: "Ingrese contraseña",
            },
            newpass2: {
                required: "Ingrese contraseña",
            }
        }
    });
});