function  validarFormEstudiante(c) {
    $(c).validate({
        rules: {
            nombre: {
                required: true,
                minlength: 3
            },
            apellido: {
                required: true,
                minlength: 3
            },
            telefono: {
                number: true,
                minlength: 7
            },
            codigo: {
                required: true,
                minlength: 6,
                maxlength: 30
            },
            curso: {
                required: true
            }
        },
        messages: {
            nombre: {
                required: "Ingresar Nombre"
            },
            apellido: {
                required: "ingresar apellido"
            },
            telefono: {
                number: "ingresar solo numeros"
            },
            codigo: {
                required: "Ingresar Codigo"
            },
            curso: {
                required: "Seleccione curso"
            }
        }
    });
}

function guardarCambios(idestu) {
    var form = $("#formularioEstudiante");
    if (!form.valid()) {
        return;
    }
    $(".ui-dialog-title").append('<img class="loading" src="../image/loading.gif"/>');
    ajaxAsync("POST", "updateEstudiante", "CEstudiante.php", form.serialize() + "&id=" + idestu, function(data) {
        var code = $(data).attr("id");
        if (code == "error") {
            msn($(data).text());
        } else {
            $("#trestu" + idestu).html(gettdfromform(form));
            $("#trestu" + idestu).attr("id", "trestu" + form.find("input[name=codigo]"));
            $("#divFormEstu").dialog("close");
        }
        $(".ui-dialog-title .loading").remove();
    });

}

function gettdfromform(form) {
    var id = form.find("[name='codigo']").val();
    var name = form.find("[name='nombre']").val();
    var tel = form.find("[name='telefono']").val();
    var emal = form.find("[name='correo']").val();
    var grado = form.find("[name='grado']").val();
    var lastname = form.find("[name='apellido']").val();
    var html = "<td class='name' ><a>" + lastname + " " + name + "</a></td> <td>" + id + "</td><td>" + grado + "</td><td>" + tel + "</td><td>" + emal + "</td>"
    return html;
}

function editarEstu(idestu) {
    ajaxAsync("post", "printFormEstudiante", "CEstudiante.php", "estu=" + idestu, function(data) {
        $("#divFormEstu").html(data);
        validarFormEstudiante("#formularioEstudiante");
        $("#divFormEstu").dialog({
            width: 360,
            modal: true,
            buttons: {
                "Guardar": function() {
                    guardarCambios(idestu);
                },
                "Cacelar": function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#divFormEstu").dialog("open");
        $("td img.loading").remove();
    });
}


function guardarEstudiante() {
    guardar("#formularioEstudiante", "guadarEstudiante", "estudiante.php", "CEstudiante.php");
}

$(document).ready(function() {

    $('.table').dataTable({
        "scrollY": "420px",
        "paging": false
    });

    $("form img").live("click", function() {
        var form = ($(this).parent().parent());
        var codigo = form.find("input[name=codigoActual]").val();
        if (codigo == "") {
            msn("<p id='bad'>Debe especificar un codigo antes de selecionar una imagen<p>");
            return;
        }
        $("#divImage").html("<form>" +
                "<div><img id='img' src=\"" + $(this).attr("src") + "\"/></div>" +
                "<input name='FileData' id='file_upload' type='file'/>" +
                "</form>");

        uploadButton("#file_upload", "CEstudiante.php", "changePhoto&id=" + codigo);

        $("#divImage").dialog({
            width: 450,
            modal: true
        });

        $("#divImage").dialog("open");
    });

    $("td.name").live("click", function() {
        var idestu = $(this).parent().attr("id").slice(6);
        $(this).append('<img class="loading" src="../image/loading.gif"/>');
        editarEstu(idestu);
    });

    $("#buttonNewEstudiante").click(function() {
        $("h1.page-header").prepend('<img class="loading" src="../image/loading.gif"/>');
        ajaxAsync("POST", "printFormEstudiante", "CEstudiante.php", "estu=&grado=" + $("#selectGrado").val(), function(data) {
            $("#divFormEstu").html(data);
            $("#formularioEstudiante").find("input").val("");
            validarFormEstudiante("#formularioEstudiante");
            $("#divFormEstu").dialog({
                width: 360,
                modal: true,
                buttons: {
                    "Guardar": function() {
                        if (!$("#formularioEstudiante").valid()) {
                            return;
                        }
                        $(".ui-dialog-title").append('<img class="loading" src="../image/loading.gif"/>');
                        ajaxAsync("POST", "guadarEstudiante", "CEstudiante.php", $("#formularioEstudiante").serialize(), function(data) {
                            if ($(data).attr("id") == "ok") {
                                $("#divFormEstu").dialog("close");
                                var html = gettdfromform($("#formularioEstudiante"));
                                $(".table tbody").prepend("<tr id='trestu" + $("#formularioEstudiante").find("[name='codigo']").val() + "'>" + html + "</tr>");
                            } else {
                                msn($(data).text());
                            }
                            $(".ui-dialog-title .loading").remove();
                        });
                    },
                    "Cacelar": function() {
                        $(this).dialog("close");
                    }
                }
            });
            $("#divFormEstu").dialog("open");
            $("h1 img.loading").remove();
        });
    });

    $("#formularioEstudiante .grado").live("change", function() {
        $(".ui-dialog-title").append('<img class="loading" src="../image/loading.gif"/>');
        ajaxAsync("GET", "printOPtionCurso", "CEstudiante.php", "grado=" + $(this).find("option:selected").val() + "&year=" + $("#formularioEstudiante .year").val(), function(data) {
            $("#formularioEstudiante .curso select").html(data);
            $(".ui-dialog-title .loading").remove();
        });
    });

    $("#formularioEstudiante .year").live("change", function() {
        $(".ui-dialog-title").append('<img class="loading" src="../image/loading.gif"/>');
        ajaxAsync("GET", "printOPtionCurso", "CEstudiante.php", "year=" + $(this).find("option:selected").val() + "&grado=" + $("#formularioEstudiante .grado").val(), function(data) {
            $("#formularioEstudiante .curso select").html(data);
            $(".ui-dialog-title .loading").remove();
        });
    });

    $("#selectGrado").change(function() {
        redirec(host + "/vista/html/estudiante.php?grado=" + $(this).val());
    });

});




