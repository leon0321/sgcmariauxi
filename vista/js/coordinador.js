function  validarFormCoord(c) {
    $(c).validate({
        rules: {
            nombre: {
                required: true,
                minlength: 3,
            },
            apellido: {
                required: true,
                minlength: 3,
            },
            telefono: {
                number: true,
                minlength: 7,
            },
            codigo: {
                required: true,
                minlength: 6,
            }
        },
        messages: {
            nombre: {
                required: "Ingresar Nombre",
            },
            apellido: {
                required: "ingresar apellido",
            },
            telefono: {
                number: "ingresar solo numeros",
            },
            codigo: {
                required: "Ingresar Codigo",
            }
        }
    });
}

function guardarCoordinador() {
    var form = $("#formularioCoordinador");
    if (!form.valid()) {
        return;
    }
    guardar("#formularioCoordinador", "guadarCoordinador", "coordinador.php", "CCoordinador.php");
}

function actualizarCoord(id) {
    var form = $("#formularioCoordinador");
    if (!form.valid()) {
        return;
    }

    var data = ajax("POST", "aditarCoordinador", "CCoordinador.php", form.serialize());
    if ($(data).attr("id") == "error") {
        msn($(data).text());
        return;
    }
    var nombre = form.find("input[name=nombre]").val();
    var apellido = form.find("input[name=apellido]").val();
    var codigo = form.find("input[name=codigo]").val();
    var idactual = form.find("input[name=idactual]").val();
    var correo = form.find("input[name=correo]").val();
    var telefono = form.find("input[name=telefono]").val();
    var html = "<td><a class=\"edit\" id=\"" + codigo + "\" >" + apellido + " " + nombre + "</a></td>" +
            "<td>" + codigo + "</td>" +
            "<td>" + telefono + "</td>" +
            "<td>" + correo + "</td>";
    $("#trcoord" + id).html(html);
    $("#trcoord" + id).attr("id", "trcoord" + codigo);
    $("#formuCoordinador").dialog("close");
}

function editCoord(id) {
    formulario("printFormCoordinador&profe=" + id, "CCoordinador.php", "#formuCoordinador");
    validarFormCoord("#formularioCoordinador");
    $("#formuCoordinador").dialog({
        width: 360,
        modal: true,
        buttons: {
            "Guardar": function() {
                actualizarCoord(id);
            },
            "Cacelar": function() {
                $(this).dialog("close");
            }
        }
    });
    $("#formuCoordinador").dialog("open");
}

$(document).ready(function() {

    $('.table').dataTable({
        "scrollY": "420px",
        "scrollCollapse": true,
        "paging": false
    });

    $(".edit").live("click", function() {
        editCoord($(this).attr("id"));
    });

    $("#buttonNewProfesor").click(function() {
        $("#formularioCoordinador").find("input").val("");
        validarFormCoord("#formularioCoordinador");
        $("#formuCoordinador").dialog({
            width: 360,
            modal: true,
            buttons: {
                "Guardar": function() {
                    guardarCoordinador();
                },
                "Cacelar": function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#formuCoordinador").dialog("open");
    });

});





