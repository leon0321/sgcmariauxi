function  validarFormSecretario(c) {
    $(c).validate({
        rules: {
            nombre: {
                required: true,
                minlength: 3
            },
            apellido: {
                required: true,
                minlength: 3
            },
            telefono: {
                number: true,
                minlength: 7
            },
            codigo: {
                required: true,
                minlength: 6
            }
        },
        messages: {
            nombre: {
                required: "Ingresar Nombre",
            },
            apellido: {
                required: "ingresar apellido",
            },
            telefono: {
                number: "ingresar solo numeros",
            },
            codigo: {
                required: "Ingresar Codigo",
            }
        }
    });
}

function guardarSecretario() {
    var form = $("#formularioSecretario");
    if (!form.valid()) {
        return;
    }
    guardar("#formularioSecretario", "guadarSecretario", "secretario.php", "CSecretario.php");
}

function actualizarSecretario(id) {
    var form = $("#formularioSecretario");
    if (!form.valid()) {
        return;
    }

    var data = ajax("POST", "aditarSecretario", "CSecretario.php", form.serialize());
    if ($(data).attr("id") == "error") {
        msn($(data).text());
        return;
    }
    var nombre = form.find("input[name=nombre]").val();
    var apellido = form.find("input[name=apellido]").val();
    var codigo = form.find("input[name=codigo]").val();
    var idactual = form.find("input[name=idactual]").val();
    var correo = form.find("input[name=correo]").val();
    var telefono = form.find("input[name=telefono]").val();
    var html = "<td><a class=\"edit\" id=\"" + codigo + "\" >" + apellido + " " + nombre + "</a></td>" +
            "<td>" + codigo + "</td>" +
            "<td>" + telefono + "</td>" +
            "<td>" + correo + "</td>";
    $("#trsecretario" + id).html(html);
    $("#trsecretario" + id).attr("id", "trsecretario" + codigo);
    $("#formuSecretario").dialog("close");
}

function editSecretario(id) {
    formulario("printFormSecretario&secretario=" + id, "CSecretario.php", "#formuSecretario");
    validarFormSecretario("#formularioSecretario");
    $("#formuSecretario").dialog({
        width: 360,
        modal: true,
        buttons: {
            "Guardar": function() {
                actualizarSecretario(id);
            },
            "Cacelar": function() {
                $(this).dialog("close");
            }
        }
    });
    $("#formuSecretario").dialog("open");
}

$(document).ready(function() {

    $('.table').dataTable({
        "scrollY": "420px",
        "scrollCollapse": true,
        "paging": false
    });

    $(".edit").live("click", function() {
        editSecretario($(this).attr("id"));
    });

    $("#buttonNewProfesor").click(function() {
        $("#formularioSecretario").find("input").val("");
        validarFormSecretario("#formularioSecretario");
        $("#formuSecretario").dialog({
            width: 360,
            modal: true,
            buttons: {
                "Guardar": function() {
                    guardarSecretario();
                },
                "Cacelar": function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#formuSecretario").dialog("open");
    });

});





