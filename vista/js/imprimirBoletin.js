/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function notas(curso, text) {
    var periodo = $("#Botonperiodo").val();
    var url = host + "/controlador/CCurso.php?metodo=printNotasPeriodo";
    var varibles = {
        idclase: curso,
        periodo: periodo,
        text: text
    };

    $(".active").attr("class", "");
    $("#liCurso" + curso).attr("class", "active");
    $("#divTableNotas").load(url, varibles, function() {
        $(this).show("blind");
    });
}



$(document).ready(function() {

    $('.table').dataTable({
        "scrollY": "420px",
        "scrollX": true,
        "scrollCollapse": true,
        "paging": false
    });

    $("#BotonAno").change(function() {
        var ano = $(this).find("option:selected").val();
        redirec(host + "/vista/html/ImprimirBoletines.php?&ano=" + ano);
    });

    $(".curso").click(function() {
        var curso = $(this).attr("id");
        var periodo = $("#Botonperiodo").find("option:selected").val();
        var ano = $("#BotonAno").find("option:selected").val();
        redirec(host + "/vista/html/ImprimirBoletines.php?curso=" + curso + "&periodo=" + periodo + "&ano=" + ano);
    });

});
