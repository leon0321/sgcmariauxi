/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {

    $('.table').dataTable({
        "bFilter": false,
        "scrollX": true,
        "scrollCollapse": true,
        "paging": false
    });

    $("#BotonCurso").change(function() {
        redirec(host + "/vista/html/sesionestudiante.php?curso=" + $(this).val());
    })
});
