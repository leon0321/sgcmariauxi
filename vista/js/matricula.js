function  getJsonEstuSelecionados(selector) {
    var estu = "{\"estudiantes\":[";
    var size = $(selector).size();
    if (size == 0) {
        return "";
    }
    var i = 0;
    $(selector).each(function() {
        i++;
        estu += "{\"id\":\"" + $(this).attr("id").slice(5) + "\"}" + ((i == size) ? "" : ",");
    });
    estu += "]}";
    return estu;
}

function matricular(idcurso, estus) {
    var data = ajax("POST", "MatricularEstudiantes", "CMatricula.php", "curso=" + idcurso + "&estu=" + estus);
    if ($(data).attr("id") == "bad") {
        msn(data);
    } else {
        redirec(host + "/vista/html/Matricula.php?grado=" + $(".active").attr("id") + "&ano=" + $("#BotonAno").val());
    }
}

function getNombresSelect() {
    var text = "<table><tbody>";
    var i = 1;
    $("input[type='checkbox']:checked").each(function() {
        text += "<tr><td>" + i + "</td><td>" + $(this).attr("id").slice(5) + "</td><td>" + $("#tr" + $(this).attr("id").slice(5) + " .nombre").text() + "</td></tr>";
        i++;
    })
    return text + "</tbody></table>";
}


$(document).ready(function() {

    $('.table').dataTable({
        "scrollY": "420",
        "scrollX": true,
        "scrollCollapse": true,
        "paging": false
    });

    $(".table tbody tr").click(function() {
        var c = ($(this).find("input[type=checkbox]").prop("checked"));
        if (c == false) {
            $(this).addClass("selected");
            $(this).find("input[type=checkbox]").prop("checked", true);
        } else {
            $(this).removeClass("selected");
            $(this).find("input[type=checkbox]").prop("checked", false);
        }
    });

    $(".buttonMatricular").click(function() {
        var grado = $(this).attr("id");
        var year = $("#BotonAno").val();
        var idestu = getJsonEstuSelecionados("input[type='checkbox']:checked");
        var estu = getNombresSelect();
        if (idestu == "") {
            msn("<p id='bad'>No ha selecionado estudiantes para promover</p>");
            return;
        }
        var data = ajax("POST", "printCursosDisponibles", "CMatricula.php", "grado=" + grado + "&year=" + year);
        $("#divformCursoDisponible").html(data)
        $("#divformCursoDisponible .estudiantesSeleccionados").html(estu);
        $("#divformCursoDisponible").dialog({
            modal: true,
            width: 520,
            buttons: {
                "Guardar": function() {
                    matricular($("select[name=curso]").val(), idestu);
                },
                "Cacelar": function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#divformCursoDisponible").dialog("open");
    });

    $(".grado").click(function() {
        var grado = $(this).attr("id");
        var ano = $("#BotonAno").find("option:selected").val();
        redirec(host + "/vista/html/Matricula.php?grado=" + grado + "&ano=" + ano);
    });

});
