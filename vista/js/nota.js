function editNota(idclase, idestu, periodo) {
    $("#tr" + idestu + " a").remove();
    $(".table tbody td.sorting_1").removeClass("sorting_1");
    $("#tr" + idestu).find("td").each(function() {
        var clase = $(this).attr("class");
        if (clase == "nombre") {
            var html = getvaluestd($(this)) + '<a class="botonGuardarNota" id="' + idestu + '">(Guardar)</a>';
            $(this).html(html);
        } else {
            var value = getvaluestd($(this));
            $(this).html('<input class="input' + clase + '" type="number" name="' + clase + '" value="' + value + '"/>');
        }
    });
}

function getvaluestd(td) {
    var value = "0";
    if (td.find("input").size() > 0) {
        value = td.find("input").val();
    } else if (td.text() != "") {
        value = td.text();
    }
    return value;
}

function guardarNota(values, metodo, idetiqueta, controlador) {
    var data = ajaxAsync("POST", metodo, controlador, values, function(data) {
        if ($(data).attr("id") == "bad") {
            msn(data);
        } else {
            $(".table tbody td.sorting_1").removeClass("sorting_1");
            var tr = $(idetiqueta);
            tr.find("a").remove();
            var html = '<td class="nombre" >' + getvaluestd(tr.find(".nombre")) + '<a id="a' + idetiqueta.slice(3) + '" class="editar" >(Editar)</a></td>' +
                    '<td class="faltas"  >' + getvaluestd(tr.find(".faltas")) + '</td>' +
                    '<td class="trabajoEscrito" >' + getvaluestd(tr.find(".trabajoEscrito")) + '</td>' +
                    '<td class="evaluacionEscrita" >' + getvaluestd(tr.find(".evaluacionEscrita")) + '</td>' +
                    '<td class="socializacion"  >' + getvaluestd(tr.find(".socializacion")) + '</td>' +
                    '<td class="consultaBili" >' + getvaluestd(tr.find(".consultaBili")) + '</td>' +
                    '<td class="tallerClase" >' + getvaluestd(tr.find(".tallerClase")) + '</td>' +
                    '<td class="presCuad" >' + getvaluestd(tr.find(".presCuad")) + '</td>' +
                    '<td class="tallerCasa">' + getvaluestd(tr.find(".tallerCasa")) + '</td>' +
                    '<td class="notaFormativa" >' + getvaluestd(tr.find(".notaFormativa")) + '</td>' +
                    '<td class="evaluacionPeriodo" >' + getvaluestd(tr.find(".evaluacionPeriodo")) + '</td>' +
                    '<td class="notaFinal" >' + $(data).text() + '</td>';
            tr.html(html);
        }
    });
}

function notas(idclase, periodo) {
    $("body").append('<img class="loadingbody" src="../image/loading.gif"/>');
    ajaxAsync("post", "printNotasPeriodo", "CNota.php", "idclase=" + idclase + "&periodo=" + periodo, function(data) {
        $("#divTableNotas").html(data);
        dataTableAsig(".table");
        $(".loadingbody").remove();
    });
}

function  getJsonNotas(column) {
    var notas = "{\"notas\":[";
    var clas = column;
    var sise = $("." + clas).size();
    var i = 0;
    var valid = true;
    $("." + clas).each(function() {
        if (!valid) {
            return;
        }
        var n = $(this).find("input").size();
        if (n > 0) {
            var input = $(this).find("input");
            if (validarNota(input)) {
                $("#" + column).text("guardar");
                valid = false;
                return;
            }
        }
    });

    if (!valid) {
        return "";
    }

    $("." + clas).each(function() {
        i++;
        var input = $(this).find("input,select");
        var tr = $(this).parent();
        notas += "{\"nota\":\"" + input.val() + "\", \"estu\":\"" + tr.attr("id").slice(2) + "\"}" + ((i == sise) ? "" : ",");
        if ($(this).find("input").size() > 0) {
            $(this).html(input.val());
        }
    });

    notas += "]}";
    return (!valid) ? "" : notas;
}

function validarNota(input) {
    var error = false;
    var name = input.attr("name");
    var value = input.attr("value");

    if (name == "idestu" || name == "idclase" || name == "periodo") {
    } else {
        if (value == "") {
            error = true;
            msn("debe llenar todos los campos");
        } else if (isNaN(value)) {
            error = true;
            msn("todos los campos deben ser numericos");
        } else {
            var v = parseFloat(value);
            if (v < 0) {
                error = true;
                msn("no se admiten valores negativos");
            } else if (name != "faltas" && v > 5) {
                error = true;
                msn("los campos de notas deben ser entre 0 y 5");
            }
        }
    }
    if (error) {
        input.css("background-color", "#eab4b4");
    } else {
        input.css("background-color", "white");
    }
    return error;
}


$(document).ready(function() {


    $("th a").live("click", function() {
        if ($(this).text() == "guardar") {
            if ($(this).attr("id") != "notaFinal") {
                $(this).text("editar");
            }
            var values = getJsonNotas($(this).attr("id"));
            if (values == "") {
                return;
            }
            var data = ajax("POST", "guardarNotasColumna", "CNota.php", "notas=" + values + "&column=" + $(this).attr("id") + "&" + $(".aclass").attr("href"));
        } else if ($(this).text() == "editar") {
            $(this).text("guardar");
            var clas = $(this).attr("id");
            $("." + clas).each(function() {
                var value = "";
                if ($(this).text() != "") {
                    value = $(this).text();
                } else if ($(this).find("input").size() != 0) {
                    value = $(this).find("input").val();
                }
                value = (value == "") ? "0" : value;
                $(this).html("<input class='input" + clas + "' name='" + clas + "'type='number' value='" + value + "'/>")
            });
        }
    });

    $("a.editar").live("click", function() {
        editNota($("li.active").attr("id").slice(7), $(this).attr("id").slice(1), $("#Botonperiodo").val());
    });

    $(".botonGuardarNota").live('click', function() {
        var i = 0;
        var values = "";
        var error = false;
        $(".table tbody td.sorting_1").removeClass("sorting_1");
        $("#tr" + $(this).attr("id")).find("td").each(function() {
            if (error == true) {
                return;
            }
            var name = $(this).attr("class");
            if (name != "nombre") {
                var value = getvaluestd($(this));
                if (value == "") {
                    error = true;
                    msn("debe llenar todos los campos");
                } else if (isNaN(value)) {
                    error = true;
                    msn("todos los campos deben ser numericos");
                } else {
                    var v = parseFloat(value);
                    if (v < 0) {
                        error = true;
                        msn("no se admiten valores negativos");
                    } else if (name != "f" && v > 5) {
                        error = true;
                        msn("los campos de notas deben ser entre 0 y 5");
                    }
                }
                if (error) {
                    $(this).css("background-color", "#eab4b4");
                } else {
                    $(this).css("background-color", "white");
                }
                values += name + "=" + value + "&";
                i++;
            }
        });
        if (error) {
            return;
        }
        values += "periodo=" + $("#Botonperiodo").val() + "&idclase=" + $("li.active").attr("id").slice(7) + "&idestu=" + $(this).attr("id");
        guardarNota(values, "guadarNotaPerido", "#tr" + $(this).attr("id"), "CNota.php");
    });

    $("#Botonperiodo").live("change", function() {
        var idclase = $("li.active").attr("id").slice(7);
        var periodo = $(this).val();
        notas(idclase, periodo);
    });

    $(".clases li").live("click", function() {
        var idclase = $(this).attr("id").slice(7);
        var periodo = $("#Botonperiodo").val();
        $(".clases li").removeClass("active");
        $(this).addClass("active");
        notas(idclase, periodo);
    });

    dataTableAsig(".table");

});
