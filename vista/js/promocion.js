function  validarFormNotasFinales(c) {
    $(c).validate({
        rules: {
            nf: {
                required: true,
                number: true,
                range: [0, 5]
            }
        },
        messages: {
            nf: {
                required: "Debe Ingresar una nota",
                number: "Este Campo debe ser numerico",
                range: "El valor ingresado debe estar entre cero y cinco"
            }
        }
    });
}

function promover(grado, jsonestudiantes) {
    var data = ajax("POST", "promoverGrado", "CPromocion.php", "grado=" + grado + "&jsonestudiantes=" + jsonestudiantes);
    msn($(data));
}


function  getJsonEstuSelecionados(selector) {
    var estu = "{\"estudiantes\":[";
    var size = $(selector).size();
    if (size == 0) {
        return "";
    }
    var i = 0;
    $(selector).each(function() {
        i++;
        estu += "{\"id\":\"" + $(this).attr("id").slice(5) + "\"}" + ((i == size) ? "" : ",");
    });
    estu += "]}";
    return estu;
}

function  getTr(select, nombre, id) {
    var tds = "<td><a class='btn btn-primary editarnota' id='" + id + "' >Editar</a></td>";
    tds += "<td>" + nombre + "</td>";
    $(select + " input").each(function() {
        var input = $(this);
        tds += "<td>" + input.val() + "</td>";
    });
    return tds;
}





function getNombresSelect() {
    var text = "<table><tbody>";
    var i = 1;
    $("input[type='checkbox']:checked").each(function() {
        text += "<tr><td>" + i + "</td><td>" + $(this).attr("id").slice(5) + "</td><td>" + $("#tr" + $(this).attr("id").slice(5) + " .nombre").text() + "</td></tr>";
        i++;
    })
    return text + "</tbody></table>";
}


$(document).ready(function() {

    $('.table').dataTable({
        "scrollY": "420px",
        "scrollX": true,
        "scrollCollapse": true,
        "paging": false
    });

    $(".table tbody tr").click(function() {
        var c = ($(this).find("input[type=checkbox]").prop("checked"));
        if (c == false) {
            $(this).addClass("selected");
            $(this).find("input[type=checkbox]").prop("checked", true);
        } else {
            $(this).removeClass("selected");
            $(this).find("input[type=checkbox]").prop("checked", false);
        }
    });


    $("#BotonAno").change(function() {
        var ano = $(this).find("option:selected").val();
        redirec(host + "/vista/html/Promocion.php?&ano=" + ano);
    });

    $(".curso").click(function() {
        var curso = $(this).attr("id");
        var periodo = $("#Botonperiodo").find("option:selected").val();
        var ano = $("#BotonAno").find("option:selected").val();
        redirec(host + "/vista/html/Promocion.php?curso=" + curso + "&periodo=" + periodo + "&ano=" + ano);
    });

    $(".buttonPromover").click(function() {
        var idcurso = $(this).attr("id");
        var estus = getJsonEstuSelecionados("input[type='checkbox']:checked");
        var estu = getNombresSelect();
        if (estus == "") {
            msn("<p id='bad'>No ha selecionado estudiantes para promover</p>");
            return;
        }
        var data = ajax("GET", "printGradoSuperior", "CPromocion.php", "curso=" + $(this).attr("id"));
        if ($(data).attr("id") == "bad") {
            msn($(data));
            return;
        }
        $("#divPromocionGrado .grado").text($(data).text());
        $("#divPromocionGrado .estudiantesSeleccionados").html(estu);
        $("#divPromocionGrado .json").html(estus);

        $("#divPromocionGrado").dialog({
            modal: true,
            width: 520,
            buttons: {
                "Aceptar": function() {
                    promover($("#divPromocionGrado .grado").text(),$(".json").text());
                    $(this).dialog("close");
                },
                "Cancelar": function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#divPromocionGrado").dialog("open");
    });


});
