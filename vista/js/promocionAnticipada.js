
function promoverAnti(idcurso, estus) {
    var data = ajax("POST", "promoverAnt", "CPromocion.php", "curso=" + idcurso + "&estu=" + estus);
    if ($(data).attr("id") == "bad") {
        msn($(data));
    }
}


function  getJsonEstuSelecionados(selector) {
    var estu = "{\"estudiantes\":[";
    var size = $(selector).size();
    if (size == 0) {
        return "";
    }
    var i = 0;
    $(selector).each(function() {
        i++;
        estu += "{\"id\":\"" + $(this).attr("id").slice(5) + "\"}" + ((i == size) ? "" : ",");
    });
    estu += "]}";
    return estu;
}

function getNombresSelect() {
    var text = "<table><tbody>";
    var i = 1;
    $("input[type='checkbox']:checked").each(function() {
        text += "<tr><td>" + i + "</td><td>" + $(this).attr("id").slice(5) + "</td><td>" + $("#tr" + $(this).attr("id").slice(5) + " .nombre").text() + "</td></tr>";
        i++;
    })
    return text + "</tbody></table>";
}

function  getTr(select, nombre, id) {
    var tds = "<td><a class='btn btn-primary editarnota' id='" + id + "' >Editar</a></td>";
    tds += "<td>" + nombre + "</td>";
    $(select + " input").each(function() {
        var input = $(this);
        tds += "<td>" + input.val() + "</td>";
    });
    return tds;
}

function notas(curso, text) {
    var periodo = $("#Botonperiodo").val();
    var url = host + "/controlador/Promocionanticipada.php?metodo=printNotasPeriodo";
    var varibles = {
        idclase: curso,
        periodo: periodo,
        text: text
    };
    $(".active").attr("class", "");
    $("#liCurso" + curso).attr("class", "active");
    $("#divTableNotas").load(url, varibles, function() {
        $(this).show("blind");
    });
}

function promoverAnt(idcurso, estus) {
    var data = ajax("POST", "promover", "CPromocion.php", "curso=" + idcurso + "&estu=" + estus);
    if ($(data).attr("id") == "bad") {
        msn($(data));
    }
}


$(document).ready(function() {

    $('.table').dataTable({
        "scrollY": "420px",
        "scrollX": true,
        "scrollCollapse": true,
        "paging": false
    });

    $(".table tbody tr").click(function() {
        var c = ($(this).find("input[type=checkbox]").prop("checked"));
        if (c == false) {
            $(this).addClass("selected");
            $(this).find("input[type=checkbox]").prop("checked", true);
        } else {
            $(this).removeClass("selected");
            $(this).find("input[type=checkbox]").prop("checked", false);
        }
    });

    $("#BotonAno").change(function() {
        var ano = $(this).find("option:selected").val();
        redirec(host + "/vista/html/Promocionanticipada.php?&ano=" + ano);
    });

    $(".curso").click(function() {
        var curso = $(this).attr("id");
        var periodo = $("#Botonperiodo").find("option:selected").val();
        var ano = $("#BotonAno").find("option:selected").val();
        redirec(host + "/vista/html/Promocionanticipada.php?curso=" + curso + "&periodo=" + periodo + "&ano=" + ano);
    });

    $(".buttonPromover").click(function() {
        var idcurso = $(this).attr("id");
        var idestu = getJsonEstuSelecionados("input[type='checkbox']:checked");
        var estu = getNombresSelect();
        if (idestu == "") {
            msn("<p id='bad'>No ha selecionado estudiantes para promover</p>");
            return;
        }
        var data = ajax("POST", "printCursosDisponibles", "CPromocion.php", "curso=" + idcurso);
        $("#divformCursoDisponible").html(data)
        $("#divformCursoDisponible .estudiantesSeleccionados").html(estu);
        $("#divformCursoDisponible").dialog({
            modal: true,
            width: 520,
            buttons: {
                "Guardar": function() {
                    promoverAnti($("select[name=curso]").val(), idestu);
                    $(this).dialog("close");
                },
                "Cacelar": function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#divformCursoDisponible").dialog("open");
    });
});
