function  validarFormProfesor(c) {
    $(c).validate({
        rules: {
            nombre: {
                required: true,
                minlength: 3,
            },
            apellido: {
                required: true,
                minlength: 3,
            },
            telefono: {
                number: true,
                minlength: 7,
            },
            codigo: {
                required: true,
                minlength: 6,
            }
        },
        messages: {
            nombre: {
                required: "Ingresar Nombre",
            },
            apellido: {
                required: "ingresar apellido",
            },
            telefono: {
                number: "ingresar solo numeros",
            },
            codigo: {
                required: "Ingresar Codigo",
            }
        }
    });
}

function guardarProfesor() {
    var form = $("#formularioProfesor");
    if (!form.valid()) {
        return;
    }
    $(".ui-dialog-title").append('<img class="loading" src="../image/loading.gif"/>');
    ajaxAsync("POST", "guadarProfesor", "CProfesor.php", $("#formularioProfesor").serialize(), function(data) {
        if ($(data).attr("id") == "ok") {
            var tr = '<tr id="trprofe' + $("#formularioProfesor input[name=codigo]").val() + '" class="odd" role="row"></tr>'
            $("#formuProfesor").dialog("close");
            $(".table tbody").prepend(tr);
        } else {
            msn($(data).text());
        }
        $(".ui-dialog-title img.loading").remove();
    });
}

function actualizarProfe(id) {
    var form = $("#formularioProfesor");
    if (!form.valid()) {
        return;
    }
    $(".ui-dialog-title").append('<img class="loading" src="../image/loading.gif"/>');
    var idn = $("#formularioProfesor input[name=codigo]").val();
    ajaxAsync("post", "aditarProfesor", "CProfesor.php", $("#formularioProfesor").serialize(), function(data) {
        var o = $("#trprofe" + id);
        if ($(data).attr("id") == "bad") {
            msn($(data).text());
        } else {
            o.html(getTdFromForm($("#formularioProfesor")));
            o.attr("id", "trprofe" + idn);
            $("#formuProfesor").dialog("close");
        }
        $(".ui-dialog-title img.loading").remove();
    });
}

function getTdFromForm(form) {
    var codigo = form.find("input[name=codigo]").val();
    var nombre = form.find("input[name=nombre]").val();
    var apellido = form.find("input[name=apellido]").val();
    var idacual = form.find("input[name=idacual]").val();
    var telefono = form.find("input[name=telefono]").val();
    var correo = form.find("input[name=correo]").val();
    var ptipo = form.find("select[name=ptipo] option:selected").text();
    var nivel = form.find("select[name=nivel] option:selected").text();
    var html = '<td class="sorting_1"><a onclick="editProfe(\'' + codigo + '\')">' + apellido + " " + nombre + '</a></td>' +
            '<td>' + codigo + '</td>' +
            '<td>' + telefono + '</td>' +
            '<td>' + correo + 'm</td>' +
            '<td>' + (ptipo) + '</td>' +
            '<td>' + (nivel) + '</td>';
    return html;
}

function editProfe(id) {
    $("#trprofe" + id + " td").first().append('<img class="loading" src="../image/loading.gif"/>');
    ajaxAsync("get", "printFormProfesor", "CProfesor.php", "profe=" + id, function(data) {
        $("#formuProfesor").html(data);
        validarFormProfesor("#formularioProfesor");
        $("#formuProfesor").dialog({
            width: 360,
            modal: true,
            buttons: {
                "Guardar": function() {
                    actualizarProfe(id);
                },
                "Cacelar": function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#divFormObservacion").dialog("open");
        $(".table img.loading").remove();
    });
}

$(document).ready(function() {

    $('.table').dataTable({
        "scrollY": "420px",
        "scrollX": true,
        "scrollCollapse": true,
        "paging": false
    });

    $("#buttonNewProfesor").click(function() {
        $("#formularioProfesor").find("input").val("");
        validarFormProfesor("#formularioProfesor");
        $("#formuProfesor").dialog({
            width: 360,
            modal: true,
            buttons: {
                "Guardar": function() {
                    guardarProfesor();
                },
                "Cacelar": function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#divFormObservacion").dialog("open");
    });

});





