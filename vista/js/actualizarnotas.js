function  validarFormNotasFinales(c) {
    $(c).validate({
        rules: {
            nf: {
                required: true,
                number: true,
                range: [0, 5]
            }
        },
        messages: {
            nf: {
                required: "Debe Ingresar una nota",
                number: "Este Campo debe ser numerico",
                range: "El valor ingresado debe estar entre cero y cinco"
            }
        }
    });
}

function  getJsonNotasFinales(column) {
    var notas = "{\"notas\":[";
    var clas = column;
    var size = $("." + clas + " input").size();
    var i = 0;
    $("." + clas + " input").each(function() {
        i++;
        var input = $(this);
        notas += "{\"nota\":\"" + input.val() + "\", \"clase\":\"" + input.attr("id") + "\"}" + ((i == size) ? "" : ",");
    });
    notas += "]}";
    return notas;
}

function  getTr(select, nombre, id) {
    var tds = "<td><a class='btn btn-primary editarnota' id='" + id + "' >Editar</a></td>";
    tds += "<td>" + nombre + "</td>";
    $(select + " input").each(function() {
        var input = $(this);
        tds += "<td>" + input.val() + "</td>";
    });
    return tds;
}

function saveNotasFinales() {
    if (!$("#formularioNotaPeriodo").valid()) {
        return;
    }
    var nombre = $("#nombre").text();
    var curso = $(".active").attr("id");
    var values = getJsonNotasFinales("notasfinales");
    var periodo = $("#formularioNotaPeriodo [name=periodo]").val();
    var idestu = $("#formularioNotaPeriodo [name=idestu]").val();
    var data = ajax("POST", "guardarNotasFinalesEstu", "CNota.php", "notas=" + values + "&estu=" + idestu + "&periodo=" + periodo + "&curso=" + curso);
    if ($(data).attr("id") == "ok") {
        $("#tr" + idestu).html(getTr(".notasfinales", nombre, idestu));
        $("#divformnota").dialog("close");
    } else {
        alert($(data).text());
    }


}

function notas(curso, text) {
    var periodo = $("#Botonperiodo").val();
    var url = host + "/controlador/CCurso.php?metodo=printNotasPeriodo";
    var varibles = {
        idclase: curso,
        periodo: periodo,
        text: text
    };

    $(".active").attr("class", "");
    $("#liCurso" + curso).attr("class", "active");
    $("#divTableNotas").load(url, varibles, function() {
        $(this).show("blind");
    });
}


$(document).ready(function() {

    $('.table').dataTable({
        "scrollY": "420px",
        "scrollX": true,
        "scrollCollapse": true,
        "paging": false
    });

    $("#BotonAno").change(function() {
        var ano = $(this).find("option:selected").val();
        redirec(host + "/vista/html/Actualizarnotas.php?&ano=" + ano);
    });

    $(".curso").click(function() {
        var curso = $(this).attr("id");
        var periodo = $("#Botonperiodo").find("option:selected").val();
        var ano = $("#BotonAno").find("option:selected").val();
        redirec(host + "/vista/html/Actualizarnotas.php?curso=" + curso + "&periodo=" + periodo + "&ano=" + ano);
    });

    $(".editarnota").live("click", function() {
        var ide = $(this).attr("id");
        var periodo = $("#Botonperiodo").find("option:selected").val();
        var ano = $("#BotonAno").find("option:selected").val();
        var curso = $(".active").attr("id");
        var data = ajax("POST", "printFormularioNotaPeriodo", "CNota.php", "&periodo=" + periodo + "&ano=" + ano + "&idestu=" + ide + "&curso=" + curso);
        $("#divformnota").html(data)
        $("#divformnota").dialog({
            modal: true,
            width: 340,
            buttons: {
                "Guardar": function() {
                    saveNotasFinales();
                },
                "Cacelar": function() {
                    $(this).dialog("close");
                    //$("#inputCursoObservacion").val(idcur);
                }
            }
        });
        $("#divFormObservacion").dialog("open");
        validarFormNotasFinales("#formularioNotaPeriodo");
    });

});
