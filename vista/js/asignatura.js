function  validarFormAsignatura(c) {
    $(c).validate({
        rules: {
            nombre: {
                required: true,
                minlength: 3
            },
            shortname: {
                maxlength: 10
            },
            grado: {
                required: true,
                number: true,
                range: [0, 11]
            },
            intensidad: {
                required: true,
                number: true,
                range: [1, 10]
            },
            codigo: {
                required: true
            },
            area: {
                required: true
            }
        },
        messages: {
            nombre: {
                required: "Ingrese nombre asiganatura",
            },
            shortname: {
                maxlength: "maximo 10 caracteres"
            },
            grado: {
                required: "ingrese grado"
            },
            intensidad: {
                required: "ingrese intensidad horaria"
            },
            codigo: {
                required: "ingrese codigo"
            },
            area: {
                required: "ingrese area"
            }
        }
    });
}

function guardarAsignatura() {
    if (!$("#formularioAsignatura").valid()) {
        return;
    }
    guardar("#formularioAsignatura", "guadarAsignatura", "asignatura.php", "CAsignatura.php");
}

function actualizar(formmulario, metodo, idEtiqueta, controlador) {
    if (!$(formmulario).valid()) {
        return;
    }
    var url = host + "/controlador/" + controlador + "?metodo=" + metodo;
    $.ajax({
        type: "POST",
        url: url,
        data: $(formmulario).serialize(), // serializes the form's elements.
        success: function(data)
        {
            if ($(data).attr("id") == "error") {
                alert($(data).text());
            } else {
                $(idEtiqueta).html(data);
                $("#divformuAsignatura").dialog("close");
                return;
            }
        }
    });
}

function editAsignatura(ida) {
    formulario("printFormAsignatura&ida=" + ida, "CAsignatura.php", "#divformuAsignatura");
    validarFormAsignatura("#formularioAsignatura");
    $("#divformuAsignatura").dialog({
        width: 380,
        modal: true,
        buttons: {
            "Guardar": function() {
                actualizar("#formularioAsignatura", "editarAsignatura", "#tra" + ida, "CAsignatura.php");
            },
            "Cacelar": function() {
                $(this).dialog("close");
            }
        }
    });
    $("#divformuAsignatura").dialog("open");
}

function confirmarDalete(ida) {
    var text = "Realmente desea eliminar la asigatura  \"" + $("#tra" + ida + " a.nombreAsig").text() + "\"?";
    $("#msn").html("<p>" + text + "</p>");
    $("#msn").dialog({
        width: 500,
        modal: true,
        buttons: {
            "Aceptar": function() {
                var data = ajax("POST", "deleteAsignatura", "CAsignatura.php", "ida=" + ida);
                if ($(data).attr("id") == "ok") {
                    redirec(host + "/vista/html/asignatura.php");
                } else {
                    msn(data);
                }
            },
            "Cacelar": function() {
                $("#msn").dialog("close");
            }
        }
    });
    $("#msn").dialog("open");
}

$(document).ready(function() {

    $('.table').dataTable({
        "scrollY": "420px",
        "scrollX": true,
        "scrollCollapse": true,
        "paging": false
    });





    $(".daletebutton").click(function() {
        var ida = ($(this).attr("id").slice(6));
        confirmarDalete(ida);
    });

    $("#buttonNewAsignatura").click(function() {
        formulario("printFormAsignatura", "CAsignatura.php", "#divformuAsignatura");
        validarFormAsignatura("#formularioAsignatura");
        $("#formularioAsignatura").find("input").val("");
        $("#divformuAsignatura").dialog({
            width: 380,
            modal: true,
            buttons: {
                "Guardar": function() {
                    guardarAsignatura();
                },
                "Cacelar": function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#divformuAsignatura").dialog("open");
    });



});




