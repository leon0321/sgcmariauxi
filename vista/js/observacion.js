function  validarForm(c) {
    $(c).validate({
        rules: {
            comp: {
                required: true,
                number: true,
                range: [0, 5]
            },
            faltas: {
                required: true,
                number: true,
                range: [0, 100]
            },
            estu: {
                required: true
            },
            descripcion: {
                required: true
            },
            accion: {
                required: true
            },
            nombre: {
                required: true,
                number: false
            }
        },
        messages: {
            comp: {
                required: "Debe Ingresar una nota de comportamiento",
                number: "Este Campo debe ser numerico",
                range: "El valor ingresado debe estar entre cero y cinco"
            },
            faltas: {
                required: "Debe Ingresar numero de faltas en el periodo",
                number: "Este Campo debe ser numerico",
                range: "El valor ingresado debe estar entre cero y cien"
            },
            estu: {
                required: "Debe seleccionar un Estudiante",
            },
            descripcion: {
                required: "Debe redactar una descripcion",
            },
            accion: {
                required: "Bebe Redactar una Accion correctiva",
            },
            nombre: {
                required: "Ingrese un Nombre",
                number: "Numeros no permitidos"
            }
        }
    });
}

function getTdFromForm(form) {
    var curso = form.find("input[name=curso]").val();
    var profe = form.find("input[name=profe]").val();
    var periodo = form.find("input[name=periodo]").val();
    var comp = form.find("input[name=comp]").val();
    var faltas = form.find("input[name=faltas]").val();
    var estu = form.find("select[name=estu] option:selected");
    var descripcion = form.find("[name=descripcion]").val();
    var accion = form.find("[name=accion]").val();
    var d = new Date();
    var date = d.getYear() + "-" + d.getMonth() + "-" + d.getDay() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
    var html = '<td><a class="edit">' + estu.text() + '</a></td>' +
            '<td>' + date + '</td>' +
            '<td>' + periodo + '</td>' +
            '<td>' + faltas + '</td>' +
            '<td>' + comp + '</td>' +
            '<td>' + descripcion + '</td>' +
            '<td>' + accion + '</td>';
    return html;
}

function guardarObservacion() {
    var ok = $("#formObservacion").valid();
    if (!ok) {
        return;
    }
    ajaxAsync("POST", "saveObservacion", "CObservacion.php", $("#formObservacion").serialize(), function(data) {
        if ($(data).text() != "") {
            msn(data);
            return;
        } else {
            $("#divFormObservacion").dialog("close");
            var estu = $("#formObservacion select[name=estu]").val();
            $("#tr" + estu).html(getTdFromForm($("#formObservacion")));
        }
    });
}

function saveObservacion(ide, idcurso, periodo) {
    ajaxAsync("get", "printFormObservacion", "CObservacion.php", "ide=" + ide + "&idcur=" + idcurso + "&periodo=" + periodo, function(data) {
        $("#divFormObservacion").html(data);
        $("#divFormObservacion").dialog({
            modal: true,
            width: 600,
            buttons: {
                "Guardar": function() {
                    guardarObservacion();
                },
                "Cacelar": function() {
                    $(this).dialog("close");
                }
            }
        });
        validarForm("#formObservacion");
        $("#divFormObservacion").dialog("open");
    })
}


function printObservaciones(idcurso, periodo) {
    $("body").append('<img class="loadingbody" src="../image/loading.gif"/>');
    ajaxAsync("get", "printObservaciones", "CObservacion.php", "idcurso=" + idcurso + "&periodo=" + periodo, function(data) {
        $("#divTableObservacion").html(data);
        dataTableAsig(".table");
        $("h1.page-header").text("Periodo: " + periodo.slice(4) + " Curso: " + idcurso);
        $(".loadingbody").remove();
    });
}

$(document).ready(function() {

    $("li.curso").click(function() {
        $("#formObservacion").attr("class", $(this).attr("id"));
        var periodo = $("#Botonperiodo").val();
        $("#inputCursoObservacion").val($(this).attr("id"));
        printObservaciones($(this).attr("id"), periodo);
    });

    $("#Botonperiodo").change(function() {
        $("#formObservacion").attr("class", $("li.active").attr("id"));
        var periodo = $(this).val();
        $("#inputCursoObservacion").val($("li.active").attr("id"));
        printObservaciones($("li.active").attr("id"), periodo);
    });

    $("a.edit").live("click", function() {
        var ide = $(this).attr("id").slice(1);
        var idcurso = $(".curso.active").attr("id");
        var periodo = $("#Botonperiodo").val();
        saveObservacion(ide, idcurso, periodo);
    })

    dataTableAsig(".table");
});