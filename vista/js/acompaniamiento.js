/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {

    $('.table').dataTable({
        "scrollY": "420px",
        "scrollX": true,
        "scrollCollapse": true,
        "paging": false
    });
    
    $("#BotonAno").change(function() {
        var ano = $(this).find("option:selected").val();
        redirec(host + "/vista/html/ImprimirFichaacompanamiento.php?&ano=" + ano);
    });

    $(".curso").click(function() {
        var curso = $(this).attr("id");
        var periodo = $("#Botonperiodo").find("option:selected").val();
        var ano = $("#BotonAno").find("option:selected").val();
        redirec(host + "/vista/html/ImprimirFichaacompanamiento.php?curso=" + curso + "&periodo=" + periodo + "&ano=" + ano);
    });

});