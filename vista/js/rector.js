function  validarFormRector(c) {
    $(c).validate({
        rules: {
            nombre: {
                required: true,
                minlength: 3
            },
            apellido: {
                required: true,
                minlength: 3
            },
            telefono: {
                number: true,
                minlength: 7
            },
            sexo: {
                required: true
            },
            codigo: {
                required: true,
                minlength: 6
            }
        },
        messages: {
            nombre: {
                required: "Ingresar Nombre"
            },
            apellido: {
                required: "ingresar apellido"
            },
            telefono: {
                number: "ingresar solo numeros"
            },
            sexo: {
                required: "Ingrese sexo"
            },
            codigo: {
                required: "Ingresar Codigo"
            }
        }
    });
}

function guardarRector() {
    var form = $("#formularioRector");
    if (!form.valid()) {
        return;
    }
    guardar("#formularioRector", "guadarRector", "rector.php", "CRector.php");
}

function actualizarRector(id) {
    var form = $("#formularioRector");
    if (!form.valid()) {
        return;
    }

    var data = ajax("POST", "aditarRector", "CRector.php", form.serialize());
    if ($(data).attr("id") == "error") {
        msn($(data).text());
        return;
    }
    var nombre = form.find("input[name=nombre]").val();
    var apellido = form.find("input[name=apellido]").val();
    var codigo = form.find("input[name=codigo]").val();
    var idactual = form.find("input[name=idactual]").val();
    var correo = form.find("input[name=correo]").val();
    var telefono = form.find("input[name=telefono]").val();
    var html = "<td><a class=\"edit\" id=\"" + codigo + "\" >" + apellido + " " + nombre + "</a></td>" +
            "<td>" + codigo + "</td>" +
            "<td>" + telefono + "</td>" +
            "<td>" + correo + "</td>";
    $("#trrector" + id).html(html);
    $("#trrector" + id).attr("id", "trrector" + codigo);
    $("#formuRector").dialog("close");
}

function editRector(id) {
    formulario("printFormRector&profe=" + id, "CRector.php", "#formuRector");
    validarFormRector("#formularioRector");
    $("#formuRector").dialog({
        width: 360,
        modal: true,
        buttons: {
            "Guardar": function() {
                actualizarRector(id);
            },
            "Cacelar": function() {
                $(this).dialog("close");
            }
        }
    });
    $("#formuRector").dialog("open");
}

$(document).ready(function() {

    $('.table').dataTable({
        "scrollY": "420px",
        "scrollCollapse": true,
        "paging": false
    });

    $(".edit").live("click", function() {
        editRector($(this).attr("id"));
    });

    $("#buttonNewProfesor").click(function() {
        $("#formularioRector").find("input").val("");
        validarFormRector("#formularioRector");
        $("#formuRector").dialog({
            width: 360,
            modal: true,
            buttons: {
                "Guardar": function() {
                    guardarRector();
                },
                "Cacelar": function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#formuRector").dialog("open");
    });

});





