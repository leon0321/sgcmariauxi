var host = "http://localhost/sgcmariauxi";

function redirec(url) {
    location.href = url;
}

function uploadButton(id, controlador, metodo) {
    $(id).uploadify({
        'fileTypeExts': '*.jpg',
        swf: 'uploadify.swf',
        uploader: host + "/controlador/" + controlador + "?metodo=" + metodo,
        'onUploadSuccess': function(file, data, response) {
            if ($(data).attr("id") != "ok") {
                $("#file_upload-queue").html("<div id='bad'>" + file.name + " : Error en la carga del archivo</div>");
                return;
            }

            $("form img").each(function() {
                $(this).attr("src", $(this).attr("src") + "&nocache=" + (new Date()).getTime());
            });
        },
        'onUploadError': function(file, errorCode, errorMsg, errorString) {
            alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
        }
    });
}

function ajaxAsync(type, metodo, controlador, values, function_success) {
    var url = host + "/controlador/" + controlador + "?metodo=" + metodo;
    $.ajax({
        type: type,
        url: url,
        data: values, // serializes the form's elements.
        success: function(data)
        {
            function_success(data);
        }
    });
}

function ajax(type, metodo, controlador, datos) {
    var url = host + "/controlador/" + controlador + "?metodo=" + metodo;
    var dato;
    $.ajax({
        async: false,
        type: type,
        url: url,
        data: datos, // serializes the form's elements.
        success: function(data)
        {
            dato = data;
        }
    });
    return dato;
}

function dataTableAsig(selector) {
    $(selector).dataTable({
        "scrollY": "420px",
        "scrollX": true,
        "scrollCollapse": true,
        "paging": false
    });
}

function cerrarsesion() {
    ajax("POST", "cerrar", "CSession.php", "hola=1")
    redirec(host + "/index.php");
}

function msn(html) {
    $("#msn").html(html);
    $("#msn").dialog({
        modal: true,
        width: 450,
        buttons: {
            "Aceptar": function() {
                $(this).dialog("close");
            }
        }
    });
    $("#msn").dialog("open");
}

function formulario(metodo, controlador, idEtiqueta) {
    var data = ajax("GET", metodo, controlador, "");
    $(idEtiqueta).html(data);
}

function guardar(formmulario, metodo, vista, controlador) {
    var url = host + "/controlador/" + controlador + "?metodo=" + metodo;
    var data = ajax("POST", metodo, controlador, $(formmulario).serialize());
    if ($(data).attr("id") == "ok") {
        redirec(host + "/vista/html/" + vista);
        return;
    } else {
        msn($(data).html());
    }
}