function  validarFormClase(c) {
    $(c).validate({
        rules: {
            curso: {
                required: true
            },
            asignatura: {
                required: true
            },
            profesor: {
                required: true
            },
            profe: {
                required: true
            },
            periodo: {
                required: true,
                number: true,
                range: [2000, 2040]
            },
            estado: {
                required: true
            }
        },
        messages: {
            curso: {
                required: "Seleccioe curso"
            },
            asignatura: {
                required: "Seleccione asignatura"
            },
            profesor: {
                required: "Seleccione profesor"
            },
            profe: {
                required: "Seleccione profesor"
            },
            periodo: {
                required: "Ingrese periodo",
                number: "Bebe ser numerico"
            },
            estado: {
                required: "Ingrese estado"
            }
        }
    });
}

function guardarClase() {
    if (!$("#formularioClase").valid()) {
        return;
    }
    var s = $("#formularioClase").serialize();
    var area = $("#selectAsignatura").find('option:selected').attr("class");
    var s = s + "&area=" + area;
    guardarC(s, "guadarClase", "clase.php", "CClases.php");
}

function guardarC(formmulario, metodo, vista, controlador) {
    var url = host + "/controlador/" + controlador + "?metodo=" + metodo;
    $.ajax({
        type: "POST",
        url: url,
        data: formmulario, // serializes the form's elements.
        success: function(data)
        {
            if ($(data).text() == "ok") {
                redirec(host + "/vista/html/" + vista);
                return;
            } else {
                alert(data);
            }
        }
    });
}

function cambiarProfesor(idprofe, idclase) {
    if (!$("#formularioCambiarProfe").valid()) {
        return;
    }
    var nombreprofe = $("#formularioCambiarProfe select[name=profe]").find('option:selected').text();
    var controlador = "CClases.php";
    var metodo = "cambiarProfe"
    var vista = "clase.php";
    var url = host + "/controlador/" + controlador + "?metodo=" + metodo;
    $.ajax({
        type: "POST",
        url: url,
        data: "clase=" + idclase + "&profe=" + idprofe, // serializes the form's elements.
        success: function(data)
        {
            if ($(data).text() == "ok") {
                $("#tr" + idclase + " .nombreprofe").text(nombreprofe);
                $("#divCambiarProfe").dialog("close");
            } else {
                alert(data);
            }
        }
    });
}


$(document).ready(function() {

    $('.table').dataTable({
        "scrollY": "420px",
        "scrollX": true,
        "scrollCollapse": true,
        "paging": false
    });

    $("#BotonAno").change(function() {
        redirec(host + "/vista/html/clase.php?ano=" + $(this).val());
    });

    $("#selectYear").change(function() {
        var option = $(this).find('option:selected');
        var year = option.val();
        formulario("printSelectCursos&year=" + year, "CTool.php", "#selectCurso");
    });

    $("#selectCurso").change(function() {
        var option = $(this).find('option:selected');
        var grado = option.attr("id");
        var curso = option.val();
        formulario("printSelectAsignaturasDisponibles&grado=" + grado + "&curso=" + curso, "CAsignatura.php", "#selectAsignatura");
    });

    $("#buscarClase").click(function() {
        alert("hola");

        var grado = $("#selectGrado").find('option:selected').val();
        var año = $("#inputfecha").val();

        redirec("clase.php?grado=" + grado + "&ano=" + año);
    })

    $(".buttonCambiarProfe").click(function() {
        var idclase = $(this).attr("id");
        $("#tr" + idclase).css("background-color", "#D4D1F1");
        $("#divCambiarProfe").dialog({
            width: 340,
            modal: true,
            buttons: {
                "Cambiar": function() {
                    cambiarProfesor($("#formularioCambiarProfe select[name=profe]").val(), idclase);
                    $("#tr" + idclase).css("background-color", "");
                },
                "Cacelar": function() {
                    $(this).dialog("close");
                    $("#tr" + idclase).css("background-color", "");
                }
            }
        });
        $("#divCambiarProfe").dialog("open");
    });

    validarFormClase("#formularioCambiarProfe");

    $("#buttonNewClase").click(function() {
        validarFormClase("#formularioClase");
        $("#divFormuClase").dialog({
            width: 340,
            modal: true,
            buttons: {
                "Guardar": function() {
                    guardarClase();
                },
                "Cacelar": function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#divFormuClase").dialog("open");
    });

});




