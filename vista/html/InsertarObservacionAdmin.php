<?php
include_once '../../controlador/CObservacion.php';
include_once '../../controlador/Clogin.php';
include_once '../../controlador/CCursos.php';
include_once '../../modelo/logica/Calendario.php';
include_once '../../modelo/persistencia/FormatoNotaDAO.php';
include_once '../../controlador/CTool.php';
@session_start();
Clogin::redirect("admin");
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Sesion Secretaria </title>
        <link rel="SHORTCUT ICON" href="../image/favicon.ico"/>
        <link href="../css/jquery-ui.css" rel="stylesheet">
        <link href="../css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/formulario.css" rel="stylesheet" />
        <link href="../css/sgcmauxi.css" rel="stylesheet">
        <link href="../css/tabla.css" rel="stylesheet">
    </head>

    <body>
        
        <?php CTool::showMenuAdmin(); ?>        

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        <div id="divBotonAno" class="btn-group">
                            <select id="BotonAno" class="btn btn-primary" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" >
                                <?php
                                $years = BD::getField("periodo", "idperiodo");
                                $year = (empty($_GET["ano"])) ? $years[0] : $_GET["ano"];
                                for ($i = 0; $i < count($years); $i++) {
                                    $s = ($year == $years[$i]) ? "selected" : "";
                                    echo '<option ' . $s . ' value="' . $years[$i] . '">' . $years[$i] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div id="divBotonperiodo" class="btn-group">
                            <select id="Botonperiodo" class="btn btn-primary" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" >
                                <?php
                                $periodo = (empty($_GET["periodo"])) ? ($year."1") : $_GET["periodo"];
                                $fn = FormatoNotaDAO::getFormatoNotaByYear($year);
                                for ($i = 0; $i < $fn->getNPeridos(); $i++) {
                                    $s = (($i + 1) == $periodo) ? "selected" : "";
                                    echo '<option ' . $s . ' value="' . ($year . ($i + 1)) . '" >Periodo ' . ($i + 1) . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </ul>
                    <ul class="nav nav-sidebar">
                        <ul>mis cursos</ul>
                        <?php
                        $curso = (empty($_GET["curso"])) ? "11A".$year : $_GET["curso"];
                        CCursos::printCursosSecretario($curso, $year);
                        ?>            
                    </ul>
                </div>
                <div  class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <h1 class="page-header">Periodo: <?php echo $periodo; ?> Curso: <?php echo $curso ?></h1>
                    <div id="divTableObservacion" class="table-responsive">
                        <?php
                        CObservacion::printEstudiantesAndObservacions($curso, $periodo);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="divFormObservacion" title="Observacion" style="display: none" class="formWithTextArea">
            <?php
//            CObservacion::printFormObservacion($profe = $u->getId());
            ?>
        </div>
        <div id="msn"></div>
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../js/jquery/jquery-1.11.0.min.js"></script>
        <script src="../js/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="../css/bootstrap/js/bootstrap.min.js"></script>
        <script src="../js/jquery/jquery-ui.js"></script>
        <script src="../js/jquery/jquery.validate.js"></script>
        <script src="../js/formulario.js" ></script>
        <script src="../js/jquery/jquery.dataTables.js"></script>
        <script src="../js/observacion_admin.js" ></script>
    </body>
</html>
