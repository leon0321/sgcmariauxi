<?php
include_once '../../controlador/Clogin.php';
include_once '../../controlador/CTool.php';
@session_start();
Clogin::redirect("admin");
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="SHORTCUT ICON" href="../image/favicon.ico"/>
        <title>Panel Administrativo</title>
        <link rel="SHORTCUT ICON" href="../image/favicon.ico"/>
        <link href="../css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/sgcmauxi.css" rel="stylesheet">
    </head>
    <body>
        <?php CTool::showMenuAdmin(); ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">            

                    </ul>

                </div>
                <div  id="divTableNotas" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
          <script src="../js/jquery/jquery-1.11.0.min.js"></script>
        <script src="../js/jquery/jquery-migrate-1.2.1.min.js"></script>
         <script src="../js/jquery/jquery/jquery.ui.datepicker-es.js"  ></script>
        <script src="../js/jquery/jquery.validate.js"></script>
        <script src="../css/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>