<?php
include_once '../../controlador/Clogin.php';
@session_start();
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta na    me="author" content="">
        <link rel="SHORTCUT ICON" href="../image/favicon.ico"/>
        <title>Rcuperar Contraseña </title>
        <link rel="SHORTCUT ICON" href="../image/favicon.ico"/>
        <link href="../css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/sgcmauxi.css" rel="stylesheet">
    </head>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../../index.php">SGC MARIA AUXILIADORA</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">



                        <p class="navbar-text">Bienvenido "<?php
                            $u = Clogin::getUser();
                            echo $u->getNombre() . " " . $u->getApellido()
                            ?>"</p>
                        <li><a  href="../../controlador/Clogin.php?metodo=logout">(cerrar sesion)</a></li>
                    </ul>

                </div>

            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div id="divformRecuperar" class="col-sm-4 col-sm-offset-4 col-md- col-md-offset-4 main">
                    <form id="formRecuperarContrasena" class="form-signin" role="form">
                        <h2 class="form-signin-heading">Recuperar Contraseña</h2>
                        <input id="u" name="username" type="text" class="form-control" placeholder="Identificacion" required autofocus>
                        <input id="pass" name="pass" type="password" class="form-control" placeholder="Contraseña Antigua" style="margin-top: 5px"; required>
                        <input id="pass1" name="newpass1" type="password" class="form-control" placeholder="Contraseña Nueva" style="margin-top: 5px";required>
                        <p class="pass"></p>
                        <input id="pass2" name="newpass2" type="password" class="form-control" placeholder="Repetir Contraseña" style="margin-top: 5px"; required>
                        <p class="pass"></p>
                        <button id="buttonRecuperarContrasena" class="btn btn-lg btn-primary " type="Enviar" style="margin-top: 20px;margin-left: 225px;"; >Enviar</button>
                        <a href="<?php echo Clogin::getHost() . "/index.php"; ?>" class="btn btn-primary btn-lg " role="button" style="margin-top: 20px">Cancelar</a>
                        <p id="respuesta"></p>
                    </form>
                </div >
                <div id="respuesta"></div>
            </div>
        </div>


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script type="text/javascript" src="../js/jquery/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="../js/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="../css/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/jquery/jquery.validate.js" ></script>
        <script type="text/javascript" src="../js/formulario.js"></script>
        <script type="text/javascript" src="../js/seguridad.js"></script>
    </body>
</html>