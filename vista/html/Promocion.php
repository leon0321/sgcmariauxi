<?php
include_once '../../controlador/CCursos.php';
include_once '../../controlador/CNota.php';
include_once '../../controlador/Clogin.php';
include_once '../../modelo/persistencia/FormatoNotaDAO.php';
include_once '../../controlador/CPromocion.php';
include_once '../../controlador/CTool.php';
@session_start();
Clogin::redirect("admin");
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Sesion Secretaria </title>
        <link rel="SHORTCUT ICON" href="../image/favicon.ico"/>
        <link href="../css/jquery-ui.css" rel="stylesheet">
        <link href="../css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/formulario.css" rel="stylesheet" />
        <link href="../css/sgcmauxi.css" rel="stylesheet">
        <link href="../css/tabla.css" rel="stylesheet">
    </head>

    <body>
        <?php CTool::showMenuAdmin(); ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-2 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        <div id="divBotonAno" class="btn-group">
                            <select id="BotonAno" class="btn btn-primary" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" >
                                <?php
                                $year = (empty($_GET["ano"])) ? "" : $_GET["ano"];
                                $years = BD::getField("periodo", "idperiodo");
                                for ($i = 0; $i < count($years); $i++) {
                                    $s = ($year == $years[$i]) ? "selected" : "";
                                    echo '<option ' . $s . ' value="' . $years[$i] . '">' . $years[$i] . '</option>';
                                }
                                $year = ($year == "") ? $years[0] : $year;
                                ?>
                            </select>
                        </div>
                    </ul>
                    <ul class="nav nav-sidebar">
                        <ul>mis cursos</ul>
                        <?php
                        $curso = (empty($_GET["curso"])) ? "" : $_GET["curso"];
                        CCursos::printCursosSecretario($curso, $year);
                        ?>            
                    </ul>
                </div>
                <div  id="divTableNotas" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <?php
                    $curso = (empty($_GET["curso"])) ? "" : $_GET["curso"];
                    CPromocion::printNotasEstudiantes($curso, $year);
                    ?>
                </div>
            </div>
            <div title="Promocion de grado" id="divPromocionGrado" style="display: none">
                <h3>Estudiantes Seleccionados</h3>
                <div class="estudiantesSeleccionados">
                </div>
                <textarea class="json" style="display: none"></textarea>
                <h4>Esta seguro de promover estos estudiantes al grado <b class="grado"></b>º</h4>
            </div>
        </div>
        <div id="msn" style="display: none"></div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../js/jquery/jquery-1.11.0.min.js"></script>
        <script src="../js/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="../css/bootstrap/js/bootstrap.min.js"></script>
        <script src="../js/jquery/jquery-ui.js"></script>
        <script src="../js/jquery/jquery.validate.js"></script>
        <script src="../js/formulario.js" ></script>
        <script src="../js/jquery/jquery.dataTables.js"></script>
        <script src="../js/promocion.js"></script>
    </body>
</html>