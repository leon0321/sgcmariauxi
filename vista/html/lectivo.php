<?php
include_once '../../controlador/CLectivo.php';
include_once '../../controlador/Clogin.php';
include_once '../../controlador/CTool.php';
@session_start();
Clogin::redirect("admin");
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="SHORTCUT ICON" href="../image/favicon.ico"/>
        <title>Panel Administrativo </title>
        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="../css/sgcmauxi.css" rel="stylesheet">
        <link href="../css/jquery-ui.css" rel="stylesheet"/>
        <link href="../css/tabla.css" rel="stylesheet"/>
        <link href="../css/formulario.css   " rel="stylesheet"/>
        <script src="../js/vista.js"></script>
        <script>mantenerScroll();</script>
    </head>
    <body>
        <?php CTool::showMenuAdmin(); ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-2 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">

                        <div id="Botonperiodo" class="btn-group">
                            <button id="buttonNewLectivo" type="button" class="btn btn-primary">Nuevo Año Lectivo</button>
                        </div>            
                    </ul>
                   <?php CTool::showOptiosModels("lectivo.php"); ?>
                </div>
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <h1 class="page-header">Años lectivos</h1>          
                    <div class="table-responsive">
                        <?php
                        CLectivo::printLectivos();
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div title="Nuevo año lectivo" id="divformlectivo" class="formulario" style="display: none">
            <?php CLectivo::printFormLectivo(""); ?>
        </div>
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../js/jquery/jquery-1.11.0.min.js"></script>
        <script src="../js/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="../js/jquery/jquery-ui.js"></script>
        <script src="../js/formulario.js" ></script>
        <script src="../js/jquery/jquery.dataTables.js"></script>
        <script src="../js/jquery/jquery.ui.datepicker-es.js"  ></script>
        <script src="../js/jquery/jquery.validate.js"></script>
        <script src="../js/jquery/jquery.ui.datepicker.validation.min.js"></script>
        <script src="../js/lectivo.js"></script>
        <script src="../css/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>