<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="SHORTCUT ICON" href="favicon.ico"/>

        <title>Sesion Profesor </title>

        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="../css/sgcmauxi.css" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="Index.html">SGC MARIA AUXILIADORA</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Introducir <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="InsertarNotas.html">CALIFICACIONES</a>

                                <li><a href="InsertarObservaciones.html">OBSERVACIONES</a></li>                   
                            </ul>
                        </li>
                        <li><a href="RecuperarContraseña.html">Seguridad</a></li>
                        <p class="navbar-text">Bienvenido"Nombre Apellidos"</p>
                        <li><a  href="../../controlador/Clogin.php?metodo=logout">(cerrar sesion)</a></li>
                    </ul>

                </div>

            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-2 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">

                        <div id="Botonperiodo" class="btn-group">
                            <button type="button" class="btn btn-primary">Periodo</button>
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Periodo1</a></li>
                                <li><a href="#">Periodo2</a></li>
                                <li><a href="#">Periodo3</a></li>
                                <li><a href="#">Periodo4</a></li>
                            </ul>
                        </div>            
                    </ul>
                    <ul class="nav nav-sidebar">                        
                        <li><a href="estudiante.php">Estudiantes</a></li>
                        <li><a href="profesor.php">Profesores</a></li>
                        <li><a href="curso.php">Cursos</a></li>
                        <li><a href="asignatura.php">Asiganaturas</a></li>
                        <li><a href="clase.php">Clases</a></li>
                    </ul>
                </div>
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
    </body>
</html>