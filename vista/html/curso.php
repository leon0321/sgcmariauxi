<?php
include_once '../../controlador/CCursos.php';
include_once '../../controlador/Clogin.php';
include_once '../../controlador/CTool.php';
@session_start();
Clogin::redirect("admin");
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="SHORTCUT ICON" href="../image/favicon.ico"/>

        <title>Panel Administrativo</title>

        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="../css/sgcmauxi.css" rel="stylesheet">
        <link href="../css/jquery-ui.css" rel="stylesheet"/>
        <link href="../css/formulario.css" rel="stylesheet"/>
        <link href="../css/tabla.css" rel="stylesheet"/>

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="../js/vista.js"></script>
        <script>mantenerScroll();</script>
    </head>

    <body>
        <?php CTool::showMenuAdmin(); ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-2 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">

                        <div id="Botonperiodo" class="btn-group">
                            <button id="buttonNewCurso" type="button" class="btn btn-primary">New Curso</button>
                        </div>            
                    </ul>
                    <?php CTool::showOptiosModels("curso.php"); ?>
                </div>
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <h1 class="page-header">AGREGAR O EDITAR CURSOS
                        <div id="divBotonAno" class="btn-group" class="col-sm-10  col-md-10 col-md-offset-2 main">
                            <select id="BotonAno" class="btn btn-primary" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" >
                                <?php
                                $years = BD::getField("periodo", "idperiodo");
                                $year = (empty($_GET["ano"])) ? $years[0] : $_GET["ano"];
                                echo "<option value='all'>Todos</option>";
                                for ($i = 0; $i < count($years); $i++) {
                                    $s = ($year == $years[$i]) ? "selected" : "";
                                    echo '<option ' . $s . ' value="' . $years[$i] . '">' . $years[$i] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </h1>          
                    <div class="table-responsive">
                        <?php
                        CCursos::printCursos($year);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div title="Formulario Curso" id="divformCurso" class="formulario" style="display: none">
            <?php
            CCursos::printFormCurso();
            ?>
        </div>
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../js/jquery/jquery-1.11.0.min.js"></script>
        <script src="../js/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="../js/jquery/jquery-ui.js"></script>
        <script src="../js/formulario.js" ></script>
        <script src="../js/jquery/jquery.dataTables.js"></script>
        <script src="../js/jquery/jquery.validate.js"></script>
        <script src="../js/curso.js"></script>
        <script src="../css/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>