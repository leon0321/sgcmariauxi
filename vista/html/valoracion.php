<?php
include_once realpath(dirname(__FILE__)) . '/../../controlador/CValoracion.php';
include_once '../../modelo/persistencia/BD.php';
include_once '../../controlador/Clogin.php';
include_once '../../modelo/persistencia/FormatoNotaDAO.php';
include_once '../../controlador/CTool.php';
@session_start();
Clogin::redirect("admin");
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="SHORTCUT ICON" href="../image/favicon.ico"/>
        <title>Panel Administrativo</title>
        <link href="../css/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="../css/sgcmauxi.css" rel="stylesheet"/>
        <link href="../css/formulario.css" rel="stylesheet"/>
        <link href="../css/jquery-ui.css" rel="stylesheet"/>
    </head>

    <body>
       <?php CTool::showMenuAdmin(); ?>
        <div class="container-fluid">
            <div class="row formsperido">
                <div class="col-sm-2 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">    
                        <li><a href="Configurar.php">Configurar Plazos</a></li>
                        <li><a href="Configurarporcentajes.php">Configurar Porcentajes</a> </li>        
                        <li class="active"><a href="valoracion.php">Configurar Valoracion</a> </li>        
                    </ul>

                </div>

                <div  id="divTableNotas" class="col-sm-10  col-md-10 col-md-offset-2 main">
                    <h1 class="page-header">Configurar Valoraciones
                        <div id="divBotonAno" class="btn-group" class="col-sm-10  col-md-10 col-md-offset-2 main">
                            <select id="BotonAno" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" >
                                <?php
                                $years = BD::getField("periodo", "idperiodo");
                                $year = (empty($_GET["ano"])) ? $years[0] : $_GET["ano"];
                                for ($i = 0; $i < count($years); $i++) {
                                    $s = ($year == $years[$i]) ? "selected" : "";
                                    echo '<option ' . $s . ' value="' . $years[$i] . '">' . $years[$i] . '</option>';
                                }
                                ?>
                            </select>
                            <button id="new" class="btn btn-primary">Nueva valoracion</button>
                        </div>
                    </h1>
                    <div id="configurarValoraciones">
                        <?php
                        CValoracion::printValoraciones($year);
                        ?>
                    </div>
                </div>
            </div> 
        </div>
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../js/jquery/jquery-1.11.0.min.js"></script>
        <script src="../js/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="../js/jquery/jquery-ui.js"></script>
        <script src="../js/jquery/jquery.ui.datepicker-es.js"  ></script>
        <script src="../js/jquery/jquery.validate.js"></script>
        <script src="../css/bootstrap/js/bootstrap.min.js"></script>
        <script src="../js/formulario.js"></script>
        <script src="../js/valoracion.js"></script>
    </body>
</html>