<?php
include_once '../../controlador/Clogin.php';
include_once '../../controlador/CPorcentaje.php';
include_once '../../modelo/persistencia/BD.php';
include_once '../../modelo/persistencia/FormatoNotaDAO.php';
include_once '../../controlador/CTool.php';


@session_start();
Clogin::redirect("admin");
?>
!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="SHORTCUT ICON" href="../image/favicon.ico"/>
        <title>Panel Administrativo</title>

        <link href="../css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/sgcmauxi.css" rel="stylesheet">
    </head>

    <body>
<?php CTool::showMenuAdmin(); ?>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-2 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">      
                        <div class="col-sm-2 col-md-2 sidebar">
                            <ul class="nav nav-sidebar">    
                                <li ><a href="Configurar.php">Configurar Plazos</a></li>
                                <li class="active"><a href="">Configurar Porcentajes</a> </li>        
                                <li><a href="valoracion.php">Configurar Valoracion</a> </li>        
                            </ul>
                        </div>      
                    </ul>
                </div>

                <div  id="divTableNotas" class="col-sm-10  col-md-10 col-md-offset-2 main">
                    <h1 class="page-header">Configurar poncentajes de los periodos
                        <div id="divBotonAno" class="btn-group" class="col-sm-10  col-md-10 col-md-offset-2 main">
                            <select id="BotonAno" class="btn btn-primary" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" >
                                <?php
                                $year = (empty($_GET["ano"])) ? "" : $_GET["ano"];
                                $years = BD::getField("periodo", "idperiodo");
                                $year = ($year == "") ? $years[0] : $year;
                                for ($i = 0; $i < count($years); $i++) {
                                    $s = ($year == $years[$i]) ? "selected" : "";
                                    echo '<option ' . $s . ' value="' . $years[$i] . '">' . $years[$i] . '</option>';
                                }
                                $fn = FormatoNotaDAO::getFormatoNotaByYear($year);
                                ?>
                            </select>
                        </div>
                    </h1>
                    <?php
                    for ($i = 0; $i < $fn->getNPeridos(); $i++) {
                        echo '<div class="col-md-3">';
                        CPorcentaje::prinFormPorcentaje($fn, ($i + 1));
                        echo "</div>";
                    }
                    ?>
                </div>
            </div>
        </div>
        <script src="../js/jquery/jquery-1.11.0.min.js"></script>
        <script src="../js/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="../js/jquery/jquery-ui.js"></script>
        <script src="../js/jquery/jquery/jquery.ui.datepicker-es.js"  ></script>
        <script src="../js/jquery/jquery.validate.js"></script>
        <script src="../css/bootstrap/js/bootstrap.min.js"></script>
        <script src="../js/formulario.js"></script>
        <script src="../js/configurarPorcentajes.js"></script>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

    </body>
</html>