<?php
include_once '../../controlador/CCursos.php';
include_once '../../controlador/CObservacion.php';
include_once '../../controlador/Clogin.php';
include_once '../../modelo/persistencia/FormatoNotaDAO.php';

@session_start();
Clogin::redirect("ImprimirBoletines");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="SHORTCUT ICON" href="../image/favicon.ico"/>

        <title>Sesion Secretaria </title>

        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="../css/sgcmauxi.css" rel="stylesheet">
        <link href="../css/tabla.css" rel="stylesheet" />

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="ImprimirBoletines.php">SGC MARIA AUXILIADORA</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Imprimir <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="ImprimirBoletines.php">BOLETINES</a>

                                <li><a href="ImprimirFichaacompanamiento.php">FICHA ACOMPAÑAMIENTO</a></li>                   
                            </ul>
                        </li>
                        <li><a href="RecuperarContrasena.php">Seguridad</a></li>
                        <p class="navbar-text">Bienvenido "<?php
                            $u = Clogin::getUser();
                            echo $u->getNombre() . " " . $u->getApellido()
                            ?>"</p> 
                        <li><a  href="../../controlador/Clogin.php?metodo=logout">(cerrar sesion)</a></li>
                    </ul>

                </div>

            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-2 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">            
                        <div id="divBotonAno" class="btn-group">
                            <select id="BotonAno" class="btn btn-primary" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" >
                                <?php
                                $years = BD::getField("periodo", "idperiodo");
                                $year = (empty($_GET["ano"])) ? $years[0] : $_GET["ano"];
                                for ($i = 0; $i < count($years); $i++) {
                                    $s = ($year == $years[$i]) ? "selected" : "";
                                    echo '<option ' . $s . ' value="' . $years[$i] . '">' . $years[$i] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div id="divBotonperiodo" class="btn-group">
                            <select id="Botonperiodo" class="btn btn-primary" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" >
                                <?php
                                $periodo = (empty($_GET["periodo"])) ? "" : $_GET["periodo"];
                                $fn = FormatoNotaDAO::getFormatoNotaByYear($year);
                                for ($i = 0; $i < $fn->getNPeridos(); $i++) {
                                    $s = (($i + 1) == $periodo) ? "selected" : "";
                                    echo '<option ' . $s . ' value="' . ($i + 1) . '" >Periodo ' . ($i + 1) . '</option>';
                                }
                                ?>
                            </select>
                        </div>  
                    </ul>
                    <ul class="nav nav-sidebar">
                        <ul>mis cursos</ul>
                        <?php
                        $curso = (empty($_GET["curso"])) ? "" : $_GET["curso"];
                        CCursos::printCursosSecretario($curso, $year);
                        ?>
                    </ul>
                </div>
                <div  id="divTableNotas" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <?php
                    $curso = (empty($_GET["curso"])) ? "" : $_GET["curso"];
                    CObservacion::printObservacionesSecret($curso, $year . $periodo);
                    ?>
                </div>

            </div>
        </div>
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../js/jquery/jquery-1.11.0.min.js"></script>
        <script src="../js/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="../css/bootstrap/js/bootstrap.min.js"></script>
        <script src="../js/formulario.js"></script>
        <script src="../js/jquery/jquery.dataTables.js"></script>
        <script src="../js/acompaniamiento.js"></script>
    </body>
</html>