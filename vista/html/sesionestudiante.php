<?php
include_once '../../controlador/Clogin.php';
include_once '../../modelo/persistencia/CursoDAO.php';
include_once '../../modelo/persistencia/FormatoNotaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../../controlador/CSessionEstudiante.php';
@session_start();
Clogin::redirect("estudiante");
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Sesion Estudiante </title>
        <link rel="SHORTCUT ICON" href="../image/favicon.ico"/>
        <link href="../css/jquery-ui.css" rel="stylesheet"/>
        <link href="../css/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="../css/formulario.css" rel="stylesheet" />
        <link href="../css/sgcmauxi.css" rel="stylesheet"/>
        <link href="../css/tabla.css" rel="stylesheet" />

    </head>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="sesionestudiante.php">SGC MARIA AUXILIADORA</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">


                        <li class="dropdown">
                            <a  class="dropdown-toggle" data-toggle="dropdown">
                                Consultar<b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="sesionestudiante.php">CONSULTAR NOTAS </a></li>
                                <li><a href="consultarobservaciones.php">CONSULTAR OBSERVACIONES</a></li>                   
                            </ul>
                        </li>
                        <p class="navbar-text">Bienvenido "<?php
                            $u = Clogin::getUser();
                            echo $u->getNombre() . " " . $u->getApellido();
                            ?>"
                        </p>
                        <li><a  href="../../controlador/Clogin.php?metodo=logout">(cerrar sesion)</a></li>
                    </ul>

                </div>

            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-2 col-md-2 sidebar">
                    <div id="divBotonCurso" class="btn-group">
                        <select id="BotonCurso" class="btn btn-primary" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" >
                            <?php
                            $cursos = CursoDAO::getCursosByEstudiante($u->getId());
                            $idcurso = (empty($_GET["curso"])) ? $cursos[0]->getId() : $_GET["curso"];
                            $n = 0;
                            for ($i = 0; $i < count($cursos); $i++) {
                                if (($idcurso == $cursos[$i]->getId())) {
                                    $n = $i;
                                }
                            }
                            $formatNota = FormatoNotaDAO::getFormatoNotaByYear($cursos[$n]->getFecha());
                            for ($i = 0; $i < count($cursos); $i++) {
                                echo '<option ' . (($cursos[$n]->getId() == $cursos[$i]->getId()) ? 'selected' : '') . ' value="' . $cursos[$i]->getId() . '">' . $cursos[$i]->getNombre() . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <ul class="nav nav-sidebar">
                        <?php
                        $periodo = (empty($_GET["periodo"])) ? $cursos[$n]->getFecha() . "1" : $_GET["periodo"];
                        for ($i = 0; $i < $formatNota->getNPeridos(); $i++) {
                            ?>
                            <li class="<?php echo ($periodo == $cursos[$n]->getFecha() . ($i + 1) ) ? "active" : ""; ?>"><a href="sesionestudiante.php?periodo=<?php echo $cursos[$n]->getFecha() . ($i + 1); ?>&curso=<?php echo $cursos[$n]->getId(); ?>">Periodo <?php echo ($i + 1) ?></a></li>
                            <?php
                        }
                        ?>
                        <li class="<?php echo ($periodo == $cursos[$n]->getFecha()) ? "active" : ""; ?>"><a href="sesionestudiante.php?metodo=prinNotasGeneral&periodo=<?php echo $cursos[$n]->getFecha(); ?>&curso=<?php echo $cursos[$n]->getId(); ?>">General</a></li>
                    </ul>
                </div>
            </div>
        </div>


        <div id="notasEstudianete"class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <?php
            if (empty($_GET["metodo"])) {
                CSessionEstudiante::printNotasEstudiante($u->getId(), $cursos[$n]->getId(), $periodo);
            }else{
                CSessionEstudiante::printNotasGeneral($u->getId(), $cursos[$n]->getId());
            }
            ?>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../js/jquery/jquery-1.11.0.min.js"></script>
        <script src="../js/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="../css/bootstrap/js/bootstrap.min.js"></script>
        <script src="../js/jquery/jquery-ui.js"></script>
        <script src="../js/jquery/jquery.validate.js"></script>
        <script src="../js/formulario.js" ></script>
        <script src="../js/jquery/jquery.dataTables.js"></script>
        <script src="../js/sessionEstudiante.js" ></script>
    </body>
</html>