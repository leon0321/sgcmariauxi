<?php
include_once '../../controlador/CEstudiante.php';
include_once '../../controlador/Clogin.php';
include_once '../../controlador/CTool.php';
@session_start();
Clogin::redirect("admin");
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="SHORTCUT ICON" href="../image/favicon.ico"/>

        <title>Panel Administrativo </title>

        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="../css/sgcmauxi.css" rel="stylesheet">
        <link href="../css/jquery-ui.css" rel="stylesheet"/>
        <link href="../css/jquery/uploadify.css" rel="stylesheet"/>
        <link href="../css/jquery.dataTables.css" rel="stylesheet" />
        <link href="../css/formulario.css" rel="stylesheet" />
        <link href="../css/tabla.css" rel="stylesheet"/>


        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="../js/vista.js"></script>
        <script>mantenerScroll();</script>
    </head>

    <body>
        <?php CTool::showMenuAdmin(); ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-2 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">

                        <div id="Botonperiodo" class="btn-group">
                            <button id="buttonNewEstudiante" type="button" class="btn btn-primary">Matricular nuevo estudiante</button>
                        </div>            
                    </ul>
                    <?php CTool::showOptiosModels("estudiante.php"); ?>
                </div>
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <h1 class="page-header">AGREGAR Y EDITAR ESTUDIANTES
                        <div>
                            <select id="selectGrado" class="btn btn-primary">
                                <?php
                                $grado = (empty($_GET["grado"])) ? "0" : $_GET["grado"];
                                CTool::printGradosSelect($grado);
                                ?>
                            </select>
                        </div>
                    </h1>          
                    <div class="table-responsive">
                        <?php
                        CEstudiante::printEstudiantes($grado);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="divFormEstu" title="Estudiante" style="display: none">
        </div>
        <div id="divImage" title="Cambiar imagen" style="display: none">
        </div>
        <div id="msn" title="Alerta" style="display: none">
        </div>
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../js/jquery/jquery-1.11.0.min.js"></script>
        <script src="../js/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="../js/jquery/jquery-ui.js"></script>
        <script src="../js/formulario.js" ></script>
        <script src="../js/jquery/jquery.validate.js" ></script>
        <script src="../js/jquery/jquery.dataTables.js"></script>
        <script src="../js/jquery/jquery.uploadify.js"></script>
        <script src="../js/estudiante.js"></script>
        <script src="../css/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
