<?php
include_once '../../controlador/CObservacion.php';
include_once '../../controlador/Clogin.php';
include_once '../../controlador/CCursos.php';
include_once '../../modelo/logica/Calendario.php';
include_once '../../modelo/persistencia/FormatoNotaDAO.php';
@session_start();
Clogin::redirect("InsertarNotas");
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="SHORTCUT ICON" href="../image/favicon.ico"/>
        <title>Sesion Profesor </title>
        <link href="../css/jquery.dataTables.css" rel="stylesheet"/>
        <link href="../css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/sgcmauxi.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/jquery-ui.css"/>
        <link href="../css/tabla.css" rel="stylesheet"/>
        <link href="../css/formulario.css" rel="stylesheet" />

    </head>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="RecuperarContrasena.php">SGC MARIA AUXILIADORA</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Imprimir <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="InsertarNotas.php">Insertar Notas</a>

                                <li><a href="InsertarObservacion.php">Insertar Observacion</a></li>                   
                            </ul>
                        </li>
                        <li><a href="RecuperarContrasena.php">Seguridad</a></li>
                        <p class="navbar-text">Bienvenido "<?php
                            $u = Clogin::getUser();
                            echo $u->getNombre() . " " . $u->getApellido()
                            ?>"</p>
                        <li><a  href="../../controlador/Clogin.php?metodo=logout">(cerrar sesion)</a></li>
                    </ul>

                </div>

            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">            
                        <div id="divBotonperiodo" class="btn-group">
                            <select id="Botonperiodo" class="btn btn-primary" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" >
                                <?php
                                $maxYear = BD::getMayor("periodo", "idperiodo");
                                $fn = FormatoNotaDAO::getFormatoNotaByYear($maxYear);
                                $periodo = (empty($_SESSION["periodo"]) ? $maxYear . "1" : $_SESSION["periodo"]);
                                for ($i = 0; $i < $fn->getNPeridos(); $i++) {
                                    echo '<option ' . ((($i + 1) == substr($periodo, 4)) ? "selected" : "") . ' value="' . $maxYear . ($i + 1) . '" >Periodo ' . ($i + 1) . '</option>';
                                }
                                ?>
                            </select>
                        </div> 
                    </ul>
                    <ul class="nav nav-sidebar">
                        <?php
                        $cursos = CursoDAO::getCursosByDirectorId($u->getId());
                        $idcurso = (empty($_SESSION["curso"])) ? $cursos[0]->getId() : $_SESSION["curso"];
                        CCursos::printCursosDirector($cursos, $idcurso);
                        ?> 
                    </ul>
                </div>
                <div  class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <h1 class="page-header">Periodo: <?php echo substr($periodo, 4); ?> Curso: <?php echo $idcurso ?></h1>
                    <div id="divTableObservacion" class="table-responsive">
                        <?php
                        CObservacion::printEstudiantesAndObservacions($idcurso, $periodo);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="divFormObservacion" title="Observacion" style="display: none" class="formWithTextArea">
            <?php
            CObservacion::printFormObservacion($profe = $u->getId());
            ?>
        </div>
        <div id="msn"></div>
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script type="text/javascript" src="../js/jquery/jquery-1.11.0.min.js" ></script>
        <script type="text/javascript" src="../js/jquery/jquery-migrate-1.2.1.min.js" ></script>
        <script type="text/javascript" src="../js/jquery/jquery-ui.js" ></script> 
        <script type="text/javascript" src="../css/bootstrap/js/bootstrap.min.js" ></script>
        <script type="text/javascript" src="../js/formulario.js" ></script>
        <script type="text/javascript" src="../js/jquery/jquery.dataTables.js" ></script>
        <script type="text/javascript" src="../js/jquery/jquery.validate.js" ></script>        
        <script type="text/javascript" src="../js/observacion.js" ></script>
    </body>
</html>
