<!DOCTYPE html>
<?php
@session_start();
$_SESSION["cookie"] = isset($_COOKIE["user"]) ? $_COOKIE["user"] : "";
include_once 'controlador/Clogin.php';
include_once 'modelo/dto/Usuario.php';
?>
<html lang="es">
    <head>
        <?php
        Clogin::redirect("index");
        ?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="SHORTCUT ICON" href="vista/image/favicon.ico"/>
        <title>Bienvenidos a su nueva plataforma</title>

        <!-- Bootstrap core CSS -->
        <link href="vista/css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="vista/css/bootstrap/css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="vista/css/sgcmauxi.css" rel="stylesheet"/>
        <link href="vista/css/formulario.css" rel="stylesheet"/>

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class= "container">
            <div class = "row"> 

                <div class="col-lg-1 "> 

                </div>

                <div class= "col-lg-3 color1"> 

                    <div class="login">

                        <h1><a href="index.php"></a></h1>
                        <h3 class="titulo">INGRESA A SGC</h3>

                        <div class= "colorindex">
                            <div  style = "padding-left: 10px;">
                                <form method="post" id="loginForm" action="controlador/Clogin.php?metodo=login" >
                                    <br>
                                    <p>
                                        <select class="form-control" name="tipo" id="selectTipo">
                                            <option value="<?php echo Usuario::ESTUDIANTE ?>">Estudiante</option>
                                            <option value="<?php echo Usuario::PROFESOR ?>">Profesor</option>
                                            <option value="<?php echo Usuario::SECRETARIO ?>">Secretario</option>
                                            <option value="<?php echo Usuario::COORDINADOR ?>">Coordinador</option>
                                            <option value="<?php echo Usuario::ADMIN ?>">Admin</option>
                                        </select>
                                    </p>
                                    <p>Nombre de Usuario: </p>
                                    <p><input id="ps" type="text" class ="form-control"name="login" value="" placeholder="Usuario " required="" ></p>
                                    <p>Contraseña: </p>
                                    <p><input type="password" class ="form-control"  name="password" value="" placeholder="Contraseña" required=""> </p>
                                    <p class="submit">                           
                                        <input id="login_enviar" type="submit" class="btn btn-primary" name="commit" value="Login" style="margin-left: 192px;">
                                    </p>
                                </form>
                            </div>

                            <div class="login-help">
                                <p>Olvido su contraseña? <a href="index.php">Clic aqui.</a>.</p>
                            </div>
                            <br>

                        </div>
                        <div id="respuesta">
                            <p class="text-danger text-center  " >
                                <?php
                                $s = (empty($_GET)) ? "" : $_GET["s"];
                                echo ($s == "bad") ? "El nombre de usuario o la contraseña son incorrectos" : "";
                                ?>
                            </p>
                        </div>


                        <h3 class="contactenos">CONTACTENOS</h3>
                        <div class= "colorindex" >
                            <br>
                            <div style = "padding-left: 10px;" >
                                <section>
                                    <address>
                                        <strong>I. E MARIA AUXILIADORA</strong><br>

                                        <abbr title="Telefono Fijo">Tel:</abbr> 674 4328
                                    </address>

                                    <address>
                                        <strong>Correo Elecronico</strong><br>
                                        <a href="mailto:#">soporte@sgcmauxi.co</a>
                                    </address>
                                    <address>
                                        <strong>Pagina Web</strong><br>
                                        <a href="/../../../http://www.iemauxicartagena.edu.co">iemauxicartagena.edu.co</a>
                                    </address>

                                </section>
                            </div>
                            <br>
                        </div>
                    </div>


                </div>
                <div class="col-lg-7 color1" class="container">
                    <br>

                    <section id="miSlide" class="carousel slide">
                        <ol class = "carousel-indicators">
                            <li data-target= "#miSlide" data-slide-to = "0" class="active"> </li>
                            <li data-target= "#miSlide" data-slide-to = "1"> </li>
                            <li data-target= "#miSlide" data-slide-to = "2"> </li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="vista/image/img1.JPG" class= "completo" >
                            </div>
                            <div class="item">
                                <img src="vista/image/img2.JPG" class= "completo">
                            </div>
                            <div class="item">
                                <img src="vista/image/img3.JPG" class= "completo">
                            </div>
                        </div>

                        <a href="#miSlide" class= "left carousel-control" data-slide="prev">
                            <span class ="glyphicon glyphicon-chevron-left"> </span></a>
                        <a href="#miSlide" class= "right carousel-control" data-slide="next">
                            <span class ="glyphicon glyphicon-chevron-right"> </span></a>

                    </section>
                    <br>

                    <h3 class="contactenos">PRESENTAMOS LA NUEVA PLATAFORMA SGC MAUXI</h3>
                    <div class="color2">
                        <img src="vista/image/imagebond.png"class="completo ">



                    </div>                

                </div>

                <div class="col-lg-1">
                </div>

            </div>
        </div>
        <script src="vista/js/jquery/jquery-1.11.0.min.js"></script>
        <script src="vista/js/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="vista/js/jquery/jquery.validate.js"></script>
        <script src="vista/js/login.js"></script>
        <script src="vista/css/bootstrap/js/bootstrap.min.js"></script>
        <script src="vista/css/bootstrap/js/main.js"></script>
    </body>
</html>