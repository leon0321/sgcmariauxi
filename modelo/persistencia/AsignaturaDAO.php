<?php

include_once 'BD.php';
include_once 'InterfaceDAO.php';
include_once realpath(dirname(__FILE__)) . '/../dto/Asignatura.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Asignatura
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class AsignaturaDAO implements InterfaceDAO {

    private static $asignaturaDAO;

    function __construct() {
        
    }

    public static function get() {
        if (AsignaturaDAO::$asignaturaDAO == NULL) {
            AsignaturaDAO::$asignaturaDAO = NEW AsignaturaDAO();
        }
        return AsignaturaDAO::$asignaturaDAO;
    }

    public function actualizar($objeto) {
        $sql = "UPDATE `" . BD::$dataBase . "`.`asignatura` SET `a_shortname` = '" . $objeto->getShortName() . "',`a_codigo` = '" . $objeto->getCodigo() . "', `a_nombre` = '" . $objeto->getNombre() . "', `a_descripcion` = '" . $objeto->getDescripcion() . "', `a_creditos` = '" . $objeto->getCreditos() . "', `a_intensidad` = '" . $objeto->getIntensidad() . "' WHERE `asignatura`.`idasignatura` = " . $objeto->getId() . ";";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public function eliminar($id) {
        $sql = "DELETE FROM `sig`.`asignatura` WHERE `asignatura`.`idasignatura` = " . $id . ";";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public function insertar($o) {
        $sql = "INSERT INTO `" . BD::$dataBase . "`.`asignatura` "
                . "(`idasignatura`, `a_codigo`, `area_id_a`, `a_nombre`, `a_descripcion`, `a_grado`, `a_creditos`,`a_intensidad`,`a_shortname`) VALUES "
                . "(NULL, '" . $o->getCodigo() . "', '" . $o->getIdArea() . "', '" . $o->getNombre() . "', '" . $o->getDescripcion() . "', '" . $o->getGrado() . "', '" . $o->getCreditos() . "', '" . $o->getIntensidad() . "', '" . $o->getShortName() . "');";

        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public function getAsignaturaByid($id) {
        $sql = "SELECT *  FROM `asignatura` WHERE `idasignatura` = " . $id;
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            $asign = new Asignatura($id, $row["a_codigo"], $row["a_nombre"], $row["a_descripcion"], $row["a_grado"], "", $row["area_id_a"], $row["a_intensidad"], $row["a_shortname"]);
            return $asign;
        }
        return null;
    }

    public static function getCountClaseByAsignatura($ida) {
        $sql = "SELECT count(*)  FROM `clase` WHERE `asignatura_id_c` = $ida";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            return $row["count(*)"];
        }
        return 0;
    }

    public static function getAsignaturasNoCursoByGrado($grado, $curso) {
        $sql = "SELECT  `asignatura`.* FROM `asignatura` WHERE `a_grado` = $grado AND NOT EXISTS (SELECT 1 FROM `clase` WHERE `clase`.`curso_id_c` = '$curso' AND  `clase`.`asignatura_id_c` = `idasignatura`);";
        $asign = array();
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            $asign[] = new Asignatura($row["idasignatura"], $row["a_codigo"], $row["a_nombre"], $row["a_descripcion"], $row["a_grado"], "", $row["area_id_a"], $row["a_intensidad"], $row["a_shortname"]);
        }
        return $asign;
    }

    public static function getAsignaturas($orden = "ORDER BY  `asignatura`.`a_grado` DESC ") {
        $sql = "SELECT * FROM `asignatura`" . $orden;
        $asign = array();
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            $asign[] = new Asignatura($row["idasignatura"], $row["a_codigo"], $row["a_nombre"], $row["a_descripcion"], $row["a_grado"], "", $row["area_id_a"], $row["a_intensidad"], $row["a_shortname"]);
        }
        return $asign;
    }

    public static function getNombreByClaseId($idclase) {
        $sql = "SELECT `asignatura`.`a_nombre`  FROM `asignatura`,`clase` 
            WHERE `idclase` = " . $idclase . " AND `idasignatura` = `asignatura_id_c`";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            return $row["a_nombre"];
        }
        return null;
    }

    public static function getAsignaturaByGrado($grado, $campo = "idasignatura", $orden = "ASC") {
        $sql = "SELECT *  FROM `asignatura` WHERE `a_grado` = $grado ORDER BY `asignatura`.`$campo` $orden";
        $asign = array();
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            $asign[] = new Asignatura($row["idasignatura"], $row["a_codigo"], $row["a_nombre"], $row["a_descripcion"], $row["a_grado"], "", $row["area_id_a"], $row["a_intensidad"], $row["a_shortname"]);
        }
        return $asign;
    }

    public static function getAsignaturaByCurso($curso, $campo = "idasignatura", $orden = "ASC") {
        $sql = "SELECT `asignatura`.* FROM `asignatura`, `clase`,`curso` WHERE `idcurso` = '$curso' AND `idcurso` = `curso_id_c` AND  `idasignatura` = `asignatura_id_c` ORDER BY `asignatura`.`$campo` $orden";
        $asign = array();
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            $asign[] = new Asignatura($row["idasignatura"], $row["a_codigo"], $row["a_nombre"], $row["a_descripcion"], $row["a_grado"], "", $row["area_id_a"], $row["a_intensidad"], $row["a_shortname"]);
        }
        return $asign;
    }

    public static function delete($ida) {
        $sql = "DELETE FROM `" . BD::$dataBase . "`.`asignatura` WHERE `asignatura`.`idasignatura` = $ida;";
        $n = AsignaturaDAO::getCountClaseByAsignatura($ida);
        if ($n > 0) {
            return BD::error("Esta asiganatura se encuentra asignada a $n " . (($n == 1 ) ? "clase" : "clases"));
        }        
        BD::open();
        BD::sentenceSQL($sql);
        return BD::error();
    }

}

?>
