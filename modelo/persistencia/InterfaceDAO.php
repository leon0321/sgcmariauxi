<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InterfaceDAO
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
interface InterfaceDAO {

    public function actualizar($objeto);

    public function insertar($objeto);

    public function eliminar($objeto);
}

?>
