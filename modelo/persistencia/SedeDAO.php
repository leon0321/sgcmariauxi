<?php

include_once 'InterfaceDAO.php';
include_once 'BD.php';
include_once realpath(dirname(__FILE__)) . '/../dto/Sede.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SedeDAO
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class SedeDAO {

    public static function getSedeById($id) {
        $s = BD::getRegistrosByField("sede", "idsede", $id)[0];
        return new Sede($id, $s["id_institucion_s"], $s["s_nombre_sede"], $s["s_latitud"], $s["s_longitud"], $s["s_nombre_lugar"], $s["s_direccion"], $s["s_pais"], $s["s_estado"], $s["s_ciudad"],$s["s_telefono"]);
    }

    public static function getSedesByInstitucion($id) {
        $s = BD::getRegistrosByField("sede", "id_institucion_s", $id);
        $sedes = array();
        for ($i = 0; $i < count($sedes); $i++) {
            $sedes[$i] = new Sede($s[$i]["idsede"], $s[$i]["id_institucion_s"], $s[$i]["s_nombre_sede"], $s[$i]["s_latitud"], $s[$i]["s_longitud"], $s[$i]["s_nombre_lugar"], $s[$i]["s_direccion"], $s[$i]["s_pais"], $s[$i]["s_estado"], $s[$i]["s_ciudad"],$s["s_telefono"]);
        }
        return $sedes;
    }

    public static function getSedePrincipalByInstitucion($id) {
        $s = BD::getRegistrosByField("sede", "idsede", $id . " AND `s_principal` = 1")[0];
        return new Sede($id, $s["id_institucion_s"], $s["s_nombre_sede"], $s["s_latitud"], $s["s_longitud"], $s["s_nombre_lugar"], $s["s_direccion"], $s["s_pais"], $s["s_estado"], $s["s_ciudad"],$s["s_telefono"]);
    }

//put your code here
}

?>
