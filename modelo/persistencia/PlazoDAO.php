<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PlazoDao
 *
 * @author leon
 */
include_once 'BD.php';
include_once realpath(dirname(__FILE__)) . '/../dto/Plazo.php';

class PlazoDAO {

    public static function getPlazosByYear($year) {
        $sql = "SELECT *  FROM `plazo_notas` WHERE `pn_idperiodo` = $year";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $plazos = array();
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $plazos[$i] = new Plazo($year, $r["pn_periodo"], $r["pn_inicio_plazo"], $r["pn_fin_plazo"]);
        }
        return $plazos;
    }

    public static function isPlazo($fecha, $year, $periodo) {
        $sql = "SELECT count(*)  FROM `plazo_notas` WHERE `pn_idperiodo` = $year AND `pn_periodo` LIKE '$periodo' AND `pn_inicio_plazo` <= '$fecha' AND `pn_fin_plazo` >= '$fecha'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            return $row["count(*)"];
        }
        return 0;
    }

    public static function getPlazosByIntervalo($fechaI, $fechaF) {
        $sql = "SELECT * FROM `plazo_notas` WHERE"
                . "((`pn_inicio_plazo` <= '$fechaI' AND `pn_fin_plazo` >= '$fechaI') OR "
                . "(`pn_inicio_plazo` <= '$fechaF' AND `pn_fin_plazo` >= '$fechaF'))"
                . " OR ((`pn_inicio_plazo` >= '$fechaI' AND `pn_fin_plazo` <= '$fechaF'))";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $plazos = array();
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $plazos[$i] = new Plazo($r["pn_idperiodo"], $r["pn_periodo"], $r["pn_inicio_plazo"], $r["pn_fin_plazo"]);
        }
        return $plazos;
    }

    public static function getPlazoById($year, $periodo) {
        $sql = "SELECT *  FROM `plazo_notas` WHERE `pn_idperiodo` = $year AND `pn_periodo` = '$periodo'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        if ($r = mysql_fetch_array($result)) {
            return new Plazo($r["pn_idperiodo"], $r["pn_periodo"], $r["pn_inicio_plazo"], $r["pn_fin_plazo"]);
        }
        return null;
    }

    public static function insertar(Plazo $plazo) {
        $sql = "INSERT INTO `" . BD::$dataBase . "`.`plazo_notas` (`pn_idperiodo`, `pn_periodo`, `pn_inicio_plazo`, `pn_fin_plazo`) "
                . "VALUES ('" . $plazo->getYear() . "', '" . $plazo->getPeriodo() . "', '" . $plazo->getPlazoIncial() . "', '" . $plazo->getPlazoFinal() . "');";
        BD::open();
        BD::sentenceSQL($sql);
        return BD::error();
    }

    public static function update(Plazo $plazo) {
        $sql = "UPDATE `" . BD::$dataBase . "`.`plazo_notas` SET `pn_inicio_plazo` = '" . $plazo->getPlazoIncial() . "', `pn_fin_plazo` = '" . $plazo->getPlazoFinal() . "' WHERE `plazo_notas`.`pn_idperiodo` = " . $plazo->getYear() . " AND `plazo_notas`.`pn_periodo` = '" . $plazo->getPeriodo() . "';";
        BD::open();
        BD::sentenceSQL($sql);
        if (BD::affectedRows() == 0) {
            return BD::error("No se guardaron los cambios");
        }
        return BD::error();
    }

}
