<?php

include_once realpath(dirname(__FILE__)) . '/../dto/FormatoNota.php';
include_once 'BD.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FormatoNotaDAO
 *
 * @author leon
 */
class FormatoNotaDAO {

    public static function getFormatoNotaById($year) {
        $sql = "SELECT *  FROM `formato_nota` WHERE `fn_ano` = $year ORDER BY `formato_nota`.`fn_periodo` ASC";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $por = array();
        $n_periodos = "";
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $por[$i] = $r["fn_porcentaje"];
            $n_periodos = $r["fn_n_periodo"];
        }
        if ($n_periodos == "") {
            return null;
        }
        return new FormatoNota($por, $year, $n_periodos);
    }

    public static function getFormatoNotaByYear($year) {
        $sql = "SELECT *  FROM `formato_nota` WHERE `fn_ano` = $year ORDER BY `formato_nota`.`fn_periodo` ASC";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $por = array();
        $n_periodos = "";
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $por[$i] = $r["fn_porcentaje"];
            $n_periodos = $r["fn_n_periodo"];
        }
        if ($n_periodos == "") {
            return null;
        }
        return new FormatoNota($por, $year, $n_periodos);
    }

    public static function updateProcentaje($porcentaje, $year, $periodo) {
        $sql = "UPDATE  `" . BD::$dataBase . "`.`formato_nota` SET  `fn_porcentaje` =  '$porcentaje' WHERE  `formato_nota`.`fn_ano` =$year AND  `formato_nota`.`fn_periodo` =$periodo;";
        BD::open();
        BD::sentenceSQL($sql);
        return BD::error();
    }

    public static function intertar($nperidos, $year) {
        $por = 1 / $nperidos;
        BD::open();
        for ($i = 0; $i < $nperidos; $i++) {
            $sql = "INSERT INTO `" . BD::$dataBase . "`.`formato_nota` (`fn_ano`, `fn_periodo`, `fn_porcentaje`, `fn_n_periodo`) VALUES ('$year', '" . ($i + 1) . "', '$por', '$nperidos');";
            BD::sentenceSQL($sql);
            if (BD::error() != "") {
                return BD::error();
            }
        }
    }

    public static function sumarPorcentaje($year) {
        $sql = "SELECT ROUND(SUM(`fn_porcentaje`),4)  FROM `formato_nota` WHERE `fn_ano` = $year";
        BD::open();
        $reasult = BD::sentenceSQL($sql);
        while ($r = mysql_fetch_array($reasult)) {
            return $r["ROUND(SUM(`fn_porcentaje`),4)"];
        }
        return 0;
    }

}

