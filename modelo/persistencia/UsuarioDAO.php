<?php

include_once 'BD.php';
include_once 'InterfaceDAO.php';
include_once 'SecretarioDAO.php';
include_once 'ProfesorDAO.php';
include_once 'CoordinadorDAO.php';
include_once 'EstudianteDAO.php';
include_once realpath(dirname(__FILE__)) . '/../dto/Usuario.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsuarioDAO
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class UsuarioDAO implements InterfaceDAO {

    private static $Instance;

    public static function get() {
        if (UsuarioDAO::$Instance == null) {
            UsuarioDAO::$Instance = new UsuarioDAO();
        }
        return UsuarioDAO::$Instance;
    }

    public function actualizar($objeto) {
        
    }

    public function eliminar($id) {
        $sql = "DELETE FROM `" . BD::$dataBase . "`.`usuario` WHERE `usuario`.`codigo` = '" . $id . "';";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public static function cambiarPassword($username, $pass, $newpass) {
        $sql = "UPDATE `" . BD::$dataBase . "`.`usuario` SET `password` = '$newpass' WHERE `username` = '$username' AND `password` = '$pass'";
        BD::open();
        BD::sentenceSQL($sql);
        $msn = BD::affectedRows();
        BD::close();
        return $msn;
    }

    public static function updateUsuario($u, $id) {
        if (UsuarioDAO::validUserUpdate($u, $id) != "") {
            return BD::error();
        }
        $sql = "UPDATE `" . BD::$dataBase . "`.`usuario` SET  `usuario`.`codigo` = '" . $u->getId() . "', `correo` = '" . $u->getCorreo() . "', `username` = '" . $u->getId() . "', `password` = '" . $u->getId() . "' WHERE `usuario`.`codigo` = '$id';";
        BD::sentenceSQL($sql);
        return BD::error();
    }

    public static function validEmail($email) {
        return BD::getFieldFind("usuario", "correo", "codigo", $email);
    }

    public static function validUsername($username) {
        return BD::getFieldFind("usuario", "username", "codigo", $username);
    }

    public static function validId($id) {
        return BD::getFieldFind("usuario", "codigo", "codigo", $id);
    }

    public static function validUserUpdate($u, $id) {
        $id2 = UsuarioDAO::validId($u->getId());
        if ($id2 != $id && $id2 != NULL) {
            return BD::error("El codigo \"" . $u->getId() . "\" se encuentra repetido");
        }
        $id3 = UsuarioDAO::validEmail($u->getCorreo());
        if ($id3 != $id && $id3 != NULL) {
            return BD::error("El correo \"" . $u->getCorreo() . "\" se encuentra repetido");
        }
        return "";
    }

    public function insertar($objeto) {
        BD::open();
        $sql1 = "INSERT INTO `" . BD::$dataBase . "`.`usuario` "
                . "(`codigo`, `correo`, `username`, `password`, `estado`) "
                . "VALUES ('" . $objeto->getId() . "', '" . $objeto->getCorreo() . "', '" . $objeto->getId() . "', '" . $objeto->getPassword() . "', 0);";

        $sql2 = "INSERT INTO `" . BD::$dataBase . "`.`tipos_usuario` "
                . "(`usuario_id`, `tipousuario_id`) "
                . "VALUES"
                . " ('" . $objeto->getId() . "', '" . $objeto->getTipo() . "');";

        BD::sentenceSQL($sql1);
        if (BD::error() != "") {
            return BD::error();
        }
        BD::sentenceSQL($sql2);
        if (BD::error() != "") {
            return BD::error();
        }
        return BD::error();
    }

    public static function getUsuarioById($id) {
        $sql = "SELECT *  FROM `usuario`,`tipos_usuario` WHERE `codigo` = '$id' AND `estado` = 0 AND `usuario_id` = `codigo`";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            return new Usuario($row["codigo"], null, null, null, $row["correo"], $row["username"], NULL, $row["tipousuario_id"]);
        }
        return null;
    }

    public function getUsuario($user, $pass, $tipo) {
        $sql = "SELECT *  FROM `usuario` WHERE `username` = '" . $user . "' AND `password` = '" . $pass . "' AND `estado` = 0";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            $usuario = new Usuario($row["codigo"], null, null, null, $row["correo"], $user, $pass, $tipo);
            return UsuarioDAO::getUser($usuario);
        }
        return null;
    }

    public static function getUsuarioByCookieId($id) {
        if ($id != "") {
            $usuario = UsuarioDAO::getUsuarioById($id);
            return UsuarioDAO::getUser($usuario);
        }
        return null;
    }

    public static function getUser(Usuario $usuario) {
        if ($usuario->getTipo() == Usuario::SECRETARIO) {
            return SecretarioDAO::getSecretarioById($usuario->getId());
        } else if ($usuario->getTipo() == Usuario::PROFESOR) {
            return ProfesorDAO::get()->getProfesorByid($usuario->getId());
        } else if ($usuario->getTipo() == Usuario::COORDINADOR) {
            return CoordinadorDAO::getCoordinadorByid($usuario->getId());
        } else if ($usuario->getTipo() == Usuario::ESTUDIANTE) {
            return EstudianteDAO::get()->getEstudiantById($usuario->getId());
        } else if ($usuario->getTipo() == Usuario::ADMIN) {
            $sql = "SELECT count(*)  FROM `tipos_usuario` WHERE `usuario_id` = '" . $usuario->getId() . "' AND `tipousuario_id` = 5";
            BD::open();
            $resul = BD::sentenceSQL($sql);
            $c = 0;
            while ($r = mysql_fetch_array($resul)) {
                $c = $r["count(*)"];
            }
            if ($c == 0) {
                return NULL;
            }
            return $usuario;
        }
    }

}

?>
