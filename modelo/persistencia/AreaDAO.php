<?php

include_once 'BD.php';
include_once 'InterfaceDAO.php';
include_once realpath(dirname(__FILE__)) . '/../dto/Area.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProgramaDAO
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class AreaDAO implements InterfaceDAO {

    private static $areaDAO;

    function __construct() {
        
    }

    public static function get() {
        if (AreaDAO::$areaDAO == NULL) {
            AreaDAO::$areaDAO = NEW AreaDAO();
        }
        return AreaDAO::$areaDAO;
    }

    public function actualizar($objeto) {
        $sql = "UPDATE  `sig`.`area` SET  `idarea` =  '" . $objeto->getCodigo() . "',`area_nombre` =  '" . $objeto->getNombre() . "' WHERE  `area`.`idarea` =  '" . $objeto->getCodigo() . "';";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public function eliminar($objeto) {
        $sql = "DELETE FROM `sig`.`area` WHERE `area`.`idarea` = '" . $objeto->getCodigo() . "';";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public function insertar($objeto) {
        $sql1 = "INSERT INTO `sig`.`area` (`idarea`, `area_nombre`) VALUES ('" . $objeto->getCodigo() . "', '" . $objeto->getNombre() . "');";
        BD::open();
        BD::sentenceSQL($sql1);
        BD::close();
        return BD::error();
    }

    public function getAreaByid($id) {
        $sql = "SELECT `area_nombre`FROM  `area` WHERE  `idarea` =  '" . $id . "'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            $area = new Area($id, $row["area_nombre"]);
            return $area;
        }
        return null;
    }

    public static function getAreas() {
        $sql = "SELECT * FROM `area`ORDER BY `area`.`area_nombre` ASC";
        $programas = array();
        BD::open();
        $result = BD::sentenceSQL($sql);
        for ($i = 0; $row = mysql_fetch_array($result); $i++) {
            $programas[$i] = new Area($row["idarea"], $row["area_nombre"]);
        }
        return $programas;
    }

//put your code here
}

?>
