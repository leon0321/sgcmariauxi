<?php

include_once 'BD.php';
include_once 'InterfaceDAO.php';
include_once realpath(dirname(__FILE__)) . '/../dto/HoraClase.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HoraClaseDAO
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class HoraClaseDAO implements InterfaceDAO {

    private static $horaClase;

    function __construct() {
        
    }

    public static function get() {
        if (HoraClaseDAO::$horaClase == NULL) {
            HoraClaseDAO::$horaClase = NEW HoraClaseDAO();
        }
        return HoraClaseDAO::$horaClase;
    }

    public function actualizar($objeto) {
        $objeto = new HoraClase($dia, $horaInicio, $horas, $salon);
        $sql = "UPDATE `".BD::$dataBase."`.`hora` 
                SET `clase_id_h` = '" . $objeto->getIdClase() . "', `h_dia` = '" . $objeto->getDia() . "', `h_horainicio` = '" . $objeto->getHoraInicio() . "', `h_numerohoras` = '" . $objeto->getHoras() . "', `h_salon` = '" . $objeto->getSalon() . "' 
                WHERE `hora`.`idhorario` = " . $objeto->getId() . ";";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public function eliminar($objeto) {
        $sql = "DELETE FROM `".BD::$dataBase."`.`hora` WHERE `hora`.`idhorario` = '" . $objeto->getId() . "';";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public function insertar($objeto) {
        $sql1 = "INSERT INTO `".BD::$dataBase."`.`hora` (`idhorario`, `clase_id_h`, `h_dia`, `h_horainicio`, `h_numerohoras`, `h_salon`) 
                 VALUES (NULL, '" . $objeto->getIdClase() . "', '" . $objeto->getDia() . "', '" . $objeto->getHoraInicio() . "', '" . $objeto->getHoras() . "', '" . $objeto->getSalon() . "');";
        BD::open();
        BD::sentenceSQL($sql1);
        BD::close();
        return BD::error();
    }

    public function getHoraClaseByid($id) {
        $sql = "SELECT *  FROM `hora` WHERE `idhorario` = " . $id;
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            $hora = new HoraClase($row["h_dia"], $row["h_horainicio"], $row["h_numerohoras"], $row["h_salon"]);
            $hora->setId($row["idhorario"]);
            $hora->setIdClase($row["clase_id_h"]);
            return $hora;
        }
        return null;
    }

    public function getHoraByClaseId($id) {
        $sql = "SELECT *  FROM `hora` WHERE `clase_id_h` = " . $id;
        BD::open();
        $horas = array();
        $result = BD::sentenceSQL($sql);
        for ($i = 0; $row = mysql_fetch_array($result); $i++) {
            $hora = new HoraClase($id, $row["clase_id_h"], $row["h_dia"], $row["h_horainicio"], $row["h_numerohoras"], $row["h_salon"]);
            $horas[$i] = $hora;
        }
        BD::closefree();
        if (BD::error() != "") {
            return null;
        }
        return $horas;
    }

}
?>
