<?php

include_once 'InterfaceDAO.php';
include_once realpath(dirname(__FILE__)) . '/../dto/Usuario.php';
include_once realpath(dirname(__FILE__)) . '/../dto/Profesor.php';
include_once 'BD.php';
include_once 'UsuarioDAO.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProfesorDAO
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class ProfesorDAO implements InterfaceDAO {

    private static $profesorDAO = NULL;

    private function __construct() {
        
    }

    public static function get() {
        if (ProfesorDAO::$profesorDAO == NULL) {
            ProfesorDAO::$profesorDAO = NEW ProfesorDAO();
        }
        return ProfesorDAO::$profesorDAO;
    }

    public function actualizar($objeto) {
        $sql1 = "UPDATE  `sig`.`usuario` SET  `correo` =  '" . $objeto->getCorreo() . "', `username` =  '" . $objeto->getLogin() . "', `password` =  '" . $objeto->getPassword() . "' WHERE  `usuario`.`codigo` =  '" . $objeto->getId() . "';";
        $sql2 = "UPDATE  `sig`.`profesor` SET `p_nivel_actual` =  '" . $objeto->getNivel() . "', 'p_nombre` =  '" . $objeto->getNombre() . "',`p_apellido` =  '" . $objeto->getApellido() . "',`p_telefono` =  '" . $objeto->getTelefono() . "' WHERE  `profesor`.`idprofesor` =  '" . $objeto->getId() . "';";
        BD::open();
        BD::sentenceSQL($sql1);
        if (BD::error() != "")
            return BD::error();
        BD::sentenceSQL($sql2);
        if (BD::error() != "")
            return BD::error();
        BD::close();
        return BD::error();
    }

    public static function actualizarP(Profesor $p, $id) {
        $sql2 = "UPDATE `" . BD::$dataBase . "`.`profesor` SET `p_nombre` = '" . $p->getNombre() . "', `p_apellido` = '" . $p->getApellido() . "', `p_telefono` = '" . $p->getTelefono() . "', `p_tipo` = '" . $p->getPtipo() . "', `p_nivel_actual` = '" . $p->getNivel() . "' WHERE `profesor`.`idprofesor` = '".$p->getId()."';";
        $cambios = 0;        
        if (UsuarioDAO::updateUsuario($p, $id)) {
            return BD::error();
        }
        
        $cambios += BD::affectedRows();
        BD::sentenceSQL($sql2);
        if (BD::error() != "") {
            echo $sql2;
            return BD::error();
        }
        $cambios += BD::affectedRows();
        if ($cambios == 0) {
            return BD::error("Los Cambios no fueron guardados");
        }
        BD::close();
        return BD::error();
    }

    public function eliminar($id) {
        UsuarioDAO::get()->eliminar($id);
        return BD::error();
    }

    public function insertar($objeto) {
        $sql2 = "INSERT INTO `" . BD::$dataBase . "`.`profesor` "
                . "(`idprofesor`, `p_correo`,`p_username`, `p_nombre`, `p_apellido`, `p_telefono`,`p_nivel_actual`)"
                . " VALUES "
                . "('" . $objeto->getId() . "', '" . $objeto->getCorreo() . "', '" . $objeto->getLogin() . "','" . $objeto->getNombre() . "', '" . $objeto->getApellido() . "', '" . $objeto->getTelefono() . "', '" . $objeto->getNivel() . "');";
        BD::open();
        UsuarioDAO::get()->insertar($objeto);
        if (BD::error() != "") {
            return BD::error();
        }
        BD::sentenceSQL($sql2);
        if (BD::error() != "") {
            return BD::error();
        }
        BD::close();
        return BD::error();
    }

    public function getProfesorByid($id) {
        $sql = "SELECT `password`, `profesor`.* FROM  `usuario`,`profesor` WHERE  `codigo` =  '" . $id . "' AND `idprofesor` = '" . $id . "'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        if ($row = mysql_fetch_array($result)) {
            $profe = new Profesor($row["idprofesor"], $row["p_nombre"], $row["p_apellido"], $row["p_telefono"], $row["p_correo"], $row["p_username"], $row["password"], Usuario::PROFESOR, $row["p_nivel_actual"], $row["p_tipo"]);
            //$estu = new Profesor($row["idprofesor"], $row["p_nombre"], $row["p_apellido"], $row["p_telefono"], $row["p_correo"], $row["p_username"], $row["password"], Usuario::PROFESOR, $row["p_tipo"]);
            return $profe;
        }
        return null;
    }

    public static function getProfesorByClaseid($idclase) {
        $sql = "SELECT `profesor`.*  FROM `clase`,`profesor` WHERE `idclase` = $idclase AND `profesor_id_c` = `idprofesor`";
        BD::open();
        $result = BD::sentenceSQL($sql);
        if ($row = mysql_fetch_array($result)) {
            $profe = new Profesor($row["idprofesor"], $row["p_nombre"], $row["p_apellido"], $row["p_telefono"], $row["p_correo"], NULL, NULL, Usuario::PROFESOR, $row["p_nivel_actual"], $row["p_tipo"]);
            return $profe;
        }
        return null;
    }

    public static function getProfesores($ptipo) {
        $sql = "SELECT * FROM  `profesor` WHERE  `p_tipo` LIKE  '%" . $ptipo . "%' ORDER BY  `profesor`.`p_apellido` ASC ";
        $profes = array();
        BD::open();
        $result = BD::sentenceSQL($sql);
        for ($i = 0; $row = mysql_fetch_array($result); $i++) {
            $profes[$i] = new Profesor($row["idprofesor"], $row["p_nombre"], $row["p_apellido"], $row["p_telefono"], $row["p_correo"], null, null, Usuario::PROFESOR, $row["p_nivel_actual"], $row["p_tipo"]);
        }
        return $profes;
    }

}

//echo "</br>" . ProfesorDAO::get()->getProfesorByid("111")->getPtipo();
//echo ProfesorDAO::get()->insertar(new Profesor("111", "111", "111", "111", "111", "111", "111", "111", "111"));
?>
