<?php

include_once 'BD.php';
realpath(dirname(__FILE__)) . '/../logica/Calendario.php';
realpath(dirname(__FILE__)) . '/../dto/Curso.php';
include_once 'FormatoNotaDAO.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MatriculaDAO
 *
 * @author leon
 */
class MatriculaDAO {

    public static function matricularClase($idclase, $idestudiante) {
        $year = Calendario::getYear();
        BD::open();
        for ($i = 0; $i < 4; $i++) {
            $sql = "INSERT INTO `" . BD::$dataBase . "`.`nota_periodo` "
                    . "(`clase_id`, `estudiante_id`, `m_periodo_id`, `nota_final`, `falta`, `trabajo_escrito`, `evaluacion_escrita`, `evaluacion_periodo`, `socializacion`, `consulta_bibli`, `taller_clase`, `pres_cuad`, `taller_casa`, `nota_formativa`) VALUES "
                    . "('$idclase', '$idestudiante', '$year" . ($i + 1) . "', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');";
            BD::sentenceSQL($sql);
        }
        BD::close();
        return BD::error();
    }

    /**
     * Genera el sql correspodiente a los insert de registros de la tabla nota_periodo, para uno o un grupo de estudiantes del mismo curso
     * @param type $idsClase ids de las clases
     * @param type $idsEstudiante ids del grupo de estudiantes
     * @param type $year1 año lectivo
     * @param type $nperiodo numero de periodos manejados en ese ano lectivo
     * @param type $$headsql Indica se el sql tendra cabecera o no si es igual a cero no tendra cabecera si es otro numero si tendra cabecera
     */
    public static function getSQLmatricularClasesEstudiantes($idsClase, $idsEstudiante, $year, $nperiodo = 4, $headsql = 0) {
        $sql = ($headsql == 0) ? "" : ("INSERT INTO `" . BD::$dataBase . "`.`nota_periodo` " . "(`clase_id`, `estudiante_id`, `m_periodo_id`) VALUES");
        for ($i = 0; $i < count($idsEstudiante); $i++) {
            for ($j = 0; $j < count($idsClase); $j++) {
                for ($k = 0; $k < $nperiodo; $k++) {
                    $sql = $sql . "('$idsClase[$j]', '$idsEstudiante[$i]', '$year" . ($k + 1) . "'),";
                }
            }
        }
        $sql = ($headsql == 0) ? $sql : substr_replace($sql, ";", -1);
        return $sql;
    }

    public static function matricularEstudiantesCurso($idsEstudiantes, Curso $curso) {
        $fn = FormatoNotaDAO::getFormatoNotaByYear($curso->getFecha());
        $sqlMatricula = MatriculaDAO::getSQLMatricularEstudiantesIdsToCurso(array($curso), $idsEstudiantes);
        $sqlNota = MatriculaDAO::getSQLmatricularClasesEstudiantes(ClaseDAO::getIdClaseByCursoId($curso->getId()), $idsEstudiantes, $curso->getFecha(), $fn->getNPeridos(), 1);
        BD::open();
        BD::sentenceSQL($sqlMatricula);
        if (BD::error() != "") {
            return BD::error();
        }
        BD::sentenceSQL($sqlNota);
        return BD::error();
    }

    public static function eliminarMatricula($idclase, $idestudiante) {
        $sql = "DELETE FROM `" . BD::$dataBase . "`.`matricula` WHERE `matricula`.`clase_id` = " . $idclase . " AND `matricula`.`estudiante_id` = '" . $idestudiante . "';";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public static function matricularCurso($idcurso, $idestu) {
        $sql = "INSERT INTO `" . BD::$dataBase . "`.`matricula` (`curso_id_m`, `estudiante_id_m`) VALUES ('$idcurso', '$idestu');";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }
    
 

    public static function getSQLMatricularEstudiantesToCurso($cursos, $estudiantes) {
        $nc = count($cursos);
        $ne = count($estudiantes);
        $sql = "INSERT INTO `" . BD::$dataBase . "`.`matricula` (`curso_id_m`, `estudiante_id_m`) VALUES";
        if ($nc == 0 || $ne == 0) {
            return "";
        }
        for ($i = 0; $i < count($estudiantes);) {
            for ($j = 0; $j < count($cursos); $j++) {
                if ($i < $ne) {
                    $sql = $sql . "('" . $cursos[$j]->getId() . "', '" . $estudiantes[$i]->getId() . "'),";
                    $i++;
                }
            }
        }
        $sql = substr_replace($sql, ";", -1);
        return $sql;
    }

    public static function getSQLMatricularEstudiantesIdsToCurso($cursos, $idsEstudiantes) {
        $nc = count($cursos);
        $ne = count($idsEstudiantes);
        $sql = "INSERT INTO `" . BD::$dataBase . "`.`matricula` (`curso_id_m`, `estudiante_id_m`) VALUES";
        if ($nc == 0 || $ne == 0) {
            return "";
        }
        for ($i = 0; $i < count($idsEstudiantes);) {
            for ($j = 0; $j < count($cursos); $j++) {
                if ($i < $ne) {
                    $sql = $sql . "('" . $cursos[$j]->getId() . "', '" . $idsEstudiantes[$i] . "'),";
                    $i++;
                }
            }
        }
        $sql = substr_replace($sql, ";", -1);
        return $sql;
    }

}
