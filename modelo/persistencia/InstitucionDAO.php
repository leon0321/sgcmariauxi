<?php

include_once realpath(dirname(__FILE__)) . '/../dto/Institucion.php';
include_once 'BD.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InstitucionDAO
 *
 * @author leon
 */
class InstitucionDAO {

    public static function getInstitucion($id = "1") {
        $d = BD::getRegistrosByField("institucion", "idinstitucion", $id)[0];
        return new Institucion($id, $d["i_nombre"], $d["i_vigencia"], $d["i_dane"], $d["i_nit"], $d["i_resolucion"], $d["i_version"], $d["i_codigo"], $d["i_eslogan"]);
    }

}
