<?php

include_once realpath(dirname(__FILE__)) . '/../dto/Asistencia.php';
include_once realpath(dirname(__FILE__)) . '/../logica/Calendario.php';
include_once 'BD.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AsistenciaDAO
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class AsistenciaDAO {

    private function __construct() {
        
    }

    public static function actualizar($a, $fecha) {
        $sql = "UPDATE `sig`.`asistencia` SET `as_estado` = '" . $a->getEstado() . "',`as_horas_dadas` = '" . $a->getHorasDictadas() . "', `as_fecha` = '" . $fecha . "', `as_tema` = '" . $a->getTema() . "', `as_descripcion` = '" . $a->getDescriocion() . "', `as_fecharegistro` = '" . $a->getFechaRegistro() . "' WHERE `asistencia`.`clase_id_a` = " . $a->getIdclase() . " AND `asistencia`.`as_fecha` = '" . $a->getFechaClase() . "';";
        BD::open();
        BD::sentenceSQL($sql);
        return BD::error();
    }

    public static function actualizarEstado($asist) {
        $sql = "UPDATE  `sig`.`asistencia` SET  `as_estado` =  '" . $asist->getEstado() . "' WHERE  `asistencia`.`clase_id_a` =" . $asist->getIdclase() . " AND  `asistencia`.`as_fecha` =  '" . $asist->getFechaClase() . "';";
        BD::open();
        BD::sentenceSQL($sql);
        return BD::error();
    }

    public function eliminar($asis) {
        
    }

    public static function insertAssistencias($listafechas, $idclase, $horas) {
        for ($i = 0; $i < count($listafechas); $i++) {
            $asis = new Asistencia($idclase, null, $listafechas[$i], null, null, null, null, $horas[$i]);
            $error = AsistenciaDAO::insertar($asis);
            if ($error != "") {
                return $error;
            }
        }
        return "";
    }

    public static function insertar($asis) {
        $sql = "INSERT INTO `sig`.`asistencia` 
            (`clase_id_a`, `as_horas_dadas`, `as_fecha`, `as_estado`, `as_tema`, `as_descripcion`, `as_fecharegistro`,`as_horas`)
            VALUES ('" . $asis->getIdclase() . "', '0', '" . $asis->getFechaClase() . "', '0', '----', '-----', '" . Calendario::getFechaActual() . "'," . $asis->getHoras() . ");";
        BD::open();
        BD::sentenceSQL($sql);
        return (BD::error() != "") ? $sql . "\n" . BD::error() : "";
    }

    public static function getAsistenciasByClaseId($idclase) {
        $sql = "SELECT *  FROM `asistencia` WHERE `clase_id_a` = " . $idclase . " ORDER BY `as_fecha` ASC";
        BD::open();
        $asis = array();
        $result = BD::sentenceSQL($sql);
        for ($i = 0; $row = mysql_fetch_array($result); $i++) {
            $asis[$i] = new Asistencia($idclase, $row["as_horas_dadas"], $row["as_fecha"], $row["as_fecharegistro"], $row["as_tema"], $row["as_descripcion"], $row["as_estado"], $row["as_horas"]);
        }
        return $asis;
    }

    public static function getAsistByClaseIdAndFecha($idclase, $fecha) {
        $sql = "SELECT *  FROM `asistencia` WHERE `clase_id_a` = " . $idclase . " AND `as_fecha` = '" . $fecha . "'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            return new Asistencia($idclase, $row["as_horas_dadas"], $row["as_fecha"], $row["as_fecharegistro"], $row["as_tema"], $row["as_descripcion"], $row["as_estado"], $row["as_horas"]);
        }
    }

    public static function getHorasEsperadas($idclase, $fechai, $fechaf) {
        $fechai = ($fechai == "") ? "" : "AND `as_fecha` >= '" . $fechai . "'";
        $fechaf = ($fechaf == "") ? "" : "AND `as_fecha` <= '" . $fechaf . "'";
        $sql = "SELECT SUM(`as_horas`)  FROM `asistencia` WHERE `clase_id_a` LIKE '%" . $idclase . "%' " . $fechai . " " . $fechaf;
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            return $row[0];
        }
    }

    public static function getHorasDictadas($idclase, $fechai, $fechaf) {        
        $fechai = ($fechai == "") ? "" : "AND `as_fecha` >= '" . $fechai . "'";
        $fechaf = ($fechaf == "") ? "" : "AND `as_fecha` <= '" . $fechaf . "'";
        $sql = "SELECT SUM(`as_horas_dadas`)  FROM `asistencia` WHERE `clase_id_a` LIKE '%" . $idclase . "%' ". $fechai . " " . $fechaf . "  AND `as_estado` = 3";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            return $row[0];
        }
    }

    public static function getHorasDictadasActual($idclase) {
        $sql = "SELECT SUM(`as_horas_dadas`) FROM `asistencia` WHERE `clase_id_a` = " . $idclase . " AND `as_estado` = 3 AND `as_fecha` < '" . Calendario::getFechaActual() . "'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            return $row[0];
        }
        return "0";
    }

    public static function getHorasEsperadasActual($idclase) {
        $sql = "SELECT SUM(`as_horas`) FROM `asistencia` WHERE `clase_id_a` = " . $idclase . " AND `as_fecha` < '" . Calendario::getFechaActual() . "'";
        BD::open();
        $row[0] = 0;
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            return $row[0];
        }
        return "0";
    }

}
?>
