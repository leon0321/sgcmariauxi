<?php

include_once 'BD.php';
include_once 'InterfaceDAO.php';
include_once realpath(dirname(__FILE__)) . '/../dto/Usuario.php';
include_once 'UsuarioDAO.php';
include_once realpath(dirname(__FILE__)) . '/../dto/Estudiante.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EstudianteDAO
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class EstudianteDAO implements InterfaceDAO {

    private static $EstudianteDAO = NULL;

    private function __construct() {
        
    }

    public static function get() {
        if (EstudianteDAO::$EstudianteDAO == NULL) {
            EstudianteDAO::$EstudianteDAO = NEW EstudianteDAO();
        }
        return EstudianteDAO::$EstudianteDAO;
    }

    public function actualizar($objeto) {
        $sql1 = "UPDATE  `sig`.`usuario` SET  `correo` =  '" . $objeto->getCorreo() . "' WHERE  `usuario`.`codigo` =  '" . $objeto->getId() . "';";
        $sql2 = "UPDATE  `sig`.`estudiante` SET  `e_nombre` =  '" . $objeto->getNombre() . "',`e_apellido` =  '" . $objeto->getApellido() . "',`e_telefono` =  '" . $objeto->getTelefono() . "' WHERE  `estudiante`.`idestudiante` =  '" . $objeto->getId() . "';";
        BD::open();
        BD::sentenceSQL($sql1);
        if (BD::error() != "")
            return BD::error();
        BD::sentenceSQL($sql2);
        if (BD::error() != "")
            return BD::error();
        BD::close();
        return BD::error();
    }

    public static function update(Estudiante $estu, $id) {
        $sql2 = "UPDATE  `" . BD::$dataBase . "`.`estudiante` SET  `e_nombre` =  '" . $estu->getNombre() . "',`e_apellido` =  '" . $estu->getApellido() . "',`e_telefono` =  '" . $estu->getTelefono() . "' WHERE  `estudiante`.`idestudiante` =  '" . $estu->getId() . "';";
        $cambios = 0;
        if (UsuarioDAO::updateUsuario($estu, $id)) {
            return BD::error();
        }
        $cambios += BD::affectedRows();
        BD::sentenceSQL($sql2);
        if (BD::error() != "") {
            return BD::error();
        }
        $cambios += BD::affectedRows();
        if ($cambios == 0) {
            return BD::error("Los Cambios no fueron guardados");
        }
        return BD::error();
    }

    /**
     * elimina el resgistro de estudiante con el ID indicado en la base de datod
     * @param type $id
     * @return type
     */
    public function eliminar($id) {
        UsuarioDAO::get()->eliminar($id);
        return BD::error();
    }

    /**
     * Inserta la informacion de un estudiante en la base de datos
     * @param type $objeto
     * @return type
     */
    public function insertar($objeto) {
        $sql = "INSERT INTO `" . BD::$dataBase . "`.`estudiante` "
                . "(`idestudiante`, `e_correo`, `e_username`, `e_nombre`, `e_apellido`, `e_telefono`, `e_grado_actual`) VALUES "
                . "('" . $objeto->getId() . "', '" . $objeto->getCorreo() . "', '" . $objeto->getId() . "', '" . $objeto->getNombre() . "', '" . $objeto->getApellido() . "', '" . $objeto->getTelefono() . "', '" . $objeto->getGrado() . "')";
        BD::open();
        if (UsuarioDAO::get()->insertar($objeto) != "") {
            return BD::error();
        }
        BD::sentenceSQL($sql);
        if (BD::error() != "") {
            return BD::error();
        }
        BD::close();
        return BD::error();
    }

    /**
     * Retorna un estudiante por el id
     * @param type $id
     * @return null|\Estudiante
     */
    public function getEstudianteByid($id) {
        $sql = "SELECT `password`, `estudiante`.* FROM  `usuario`,`estudiante` WHERE  `codigo` =  '" . $id . "' AND `idestudiante` = '" . $id . "'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        if ($r = mysql_fetch_array($result)) {
            $estu = new Estudiante($r["idestudiante"], $r["e_nombre"], $r["e_apellido"], $r["e_telefono"], $r["e_correo"], $r["e_username"], $r["password"], Usuario::ESTUDIANTE, $r["e_grado_actual"]);
            return $estu;
        }
        return null;
    }

    public static function getEstudiantById($id) {
        $sql = "SELECT `password`, `estudiante`.* FROM  `usuario`,`estudiante` WHERE  `codigo` =  '" . $id . "' AND `idestudiante` = '" . $id . "'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        if ($r = mysql_fetch_array($result)) {
            $estu = new Estudiante($r["idestudiante"], $r["e_nombre"], $r["e_apellido"], $r["e_telefono"], $r["e_correo"], $r["e_username"], $r["password"], Usuario::ESTUDIANTE, $r["e_grado_actual"]);
            return $estu;
        }
        return null;
    }
    
    public static function getGradoEstudianteById($id) {
        $sql = "SELECT `password`, `estudiante`.* FROM  `usuario`,`estudiante` WHERE  `codigo` =  '" . $id . "' AND `idestudiante` = '" . $id . "'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        if ($r = mysql_fetch_array($result)) {
             return $r["e_grado_actual"];
        }
        return null;
    }

    public static function getEstudianteLike($campo) {
        $campo = "%" . $campo . "%";
        $sql = "SELECT DISTINCT *  FROM `estudiante` WHERE `idestudiante` LIKE '" . $campo . "' OR `e_correo` LIKE '" . $campo . "' OR `e_apellido` LIKE '" . $campo . "' OR `e_grado_actual` LIKE '" . $campo . "' OR `e_telefono` LIKE '" . $campo . "';";
        $estudiantes = array();
        BD::open();
        $result = BD::sentenceSQL($sql);
        for ($i = 0; $row = mysql_fetch_array($result); $i++) {
            $estudiantes[$i] = new Estudiante($r["idestudiante"], $r["e_nombre"], $r["e_apellido"], $r["e_telefono"], $r["e_correo"], $r["e_username"], $r["password"], Usuario::ESTUDIANTE, $r["e_grado_actual"]);
        }
        return $estudiantes;
    }

    public static function getEstudiantesByClaseid($idclase) {
        $sql = "SELECT  DISTINCT `estudiante`.*  FROM `nota_periodo`,`estudiante` WHERE `clase_id` = " . $idclase . " AND `estudiante_id` = `idestudiante`ORDER BY `estudiante`.`e_apellido` ASC";
        $estudiantes = array();
        BD::open();
        $result = BD::sentenceSQL($sql);
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $estudiantes[$i] = new Estudiante($r["idestudiante"], $r["e_nombre"], $r["e_apellido"], $r["e_telefono"], $r["e_correo"], $r["e_username"], null, Usuario::ESTUDIANTE, $r["e_grado_actual"]);
        }
        return $estudiantes;
    }

    public static function matricular($idclase, $idestudiante) {
        $sql = "INSERT INTO `sig`.`matricula` (`clase_id`, `estudiante_id`) VALUES ('" . $idclase . "', '" . $idestudiante . "');";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public static function eliminarMatricula($idclase, $idestudiante) {
        $sql = "DELETE FROM `sig`.`matricula` WHERE `matricula`.`clase_id` = " . $idclase . " AND `matricula`.`estudiante_id` = '" . $idestudiante . "';";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public static function getEstudiantes() {
        $sql = "SELECT *  FROM `estudiante`";
        $estudiantes = array();
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($r = mysql_fetch_array($result)) {
            $estudiantes[] = new Estudiante($r["idestudiante"], $r["e_nombre"], $r["e_apellido"], $r["e_telefono"], $r["e_correo"], null, null, null, $r["e_grado_actual"]);
        }
        return $estudiantes;
    }

    public static function getEstudiantesByCurso($curso) {
        $sql = "SELECT `estudiante`.* FROM `estudiante`,`matricula` WHERE `idestudiante` = `estudiante_id_m` AND `curso_id_m` = '$curso' ORDER BY `estudiante`.`e_apellido` ASC";
        $estudiantes = array();
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($r = mysql_fetch_array($result)) {
            $estudiantes[] = new Estudiante($r["idestudiante"], $r["e_nombre"], $r["e_apellido"], $r["e_telefono"], $r["e_correo"], null, null, null, $r["e_grado_actual"]);
        }
        return $estudiantes;
    }

    public static function getIdsEstudiantesByCurso($curso) {
        $sql = "SELECT `idestudiante` FROM `estudiante`,`matricula` WHERE `idestudiante` = `estudiante_id_m` AND `curso_id_m` = '$curso' ORDER BY `estudiante`.`e_apellido` ASC";
        $ids = array();
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($r = mysql_fetch_array($result)) {
            $ids[] = $r["idestudiante"];
        }
        return $ids;
    }

    public static function getEstudiantesByGrado($grado) {
        $r = BD::getRegistrosByField("estudiante", "e_grado_actual", $grado);
        $estudiantes = array();
        for ($i = 0; $i < count($r); $i++) {
            $estudiantes[$i] = new Estudiante($r[$i]["idestudiante"], $r[$i]["e_nombre"], $r[$i]["e_apellido"], $r[$i]["e_telefono"], $r[$i]["e_correo"], null, null, null, $r[$i]["e_grado_actual"]);
        }
        return $estudiantes;
    }

    public static function getEstudiantesNoMatriculadoByGrado($grado, $year) {
        $sql = "SELECT * FROM `estudiante` WHERE NOT EXISTS (SELECT 1 FROM `matricula`,`curso` WHERE `idestudiante` = `estudiante_id_m` AND `idcurso` = `curso_id_m` AND `fecha` = $year) AND `e_grado_actual` = $grado;";
        $estudiantes = array();
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($r = mysql_fetch_array($result)) {
            $estudiantes[] = new Estudiante($r["idestudiante"], $r["e_nombre"], $r["e_apellido"], $r["e_telefono"], $r["e_correo"], null, null, null, $r["e_grado_actual"]);
        }
        return $estudiantes;
    }

    public static function updateGrado($idestu, $grado=null) {
        if ($grado == null){
            $s = 'e_grado_actual + 1';
        }else{
            $s = $grado;
        }
        $sql = "UPDATE `" . BD::$dataBase . "`.`estudiante` SET `e_grado_actual` = e_grado_actual + 1 WHERE `estudiante`.`idestudiante` = '$idestu';";
        BD::open();
        BD::sentenceSQL($sql);
        return BD::error();
    }

}

//$est = EstudianteDAO::getEstudiantesByGrado("11");
//for ($i = 0; $i < count($est); $i++) {
//    echo $est[$i]->getApellido() . "</br>";
//}
//echo "</br>" + EstudianteDAO::get()->getEstudianteByid("est")->getTelefono();
//echo EstudianteDAO::get()->insertar(new Estudiante("est1", "est1", "est1", "45454145", "e1st", "1est", "1est", Usuario::ESTUDIANTE, "11") );
?>
