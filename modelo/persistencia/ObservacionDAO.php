<?php

include_once 'BD.php';
include_once realpath(dirname(__FILE__)) . '/../dto/Observacion.php';

class ObservacionDAO {

    public static function getObservacionesByIdCurso($idcurso) {
        $sql = "SELECT *  FROM `observacion` WHERE `curso_id_ob` LIKE '$idcurso'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $observaciones = array();
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $observacion = new Observacion($r["curso_id_ob"], $r["estudiante_id_o"], $r["ob_fecha"], $r["profesor_id_o"], $r["ob_observacion"], $r["ob_periodo"], $r["comportamiento"], $r["accion_pedagogica"], $r["ob_faltas"]);
            $observaciones[$i] = $observacion;
        }
        return $observaciones;
    }

    public static function getObservacionesByEstudianteCurso($idcurso, $idestudiante) {
        $sql = "SELECT *  FROM `observacion` WHERE `estudiante_id_o` LIKE '$idestudiante' AND `curso_id_ob` LIKE '$idcurso'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $observaciones = array();
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $observacion = new Observacion($r["curso_id_ob"], $r["estudiante_id_o"], $r["ob_fecha"], $r["profesor_id_o"], $r["ob_observacion"], $r["ob_periodo"], $r["comportamiento"], $r["accion_pedagogica"], $r["ob_faltas"]);
            $observaciones[$i] = $observacion;
        }
        return $observaciones;
    }

    public static function getObservacionesByCursoAndPeriodo($curso, $periodo) {
        $sql = "SELECT *  FROM `observacion` WHERE `ob_periodo` = $periodo AND `curso_id_ob` = '$curso'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $observaciones = array();
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $observacion = new Observacion($r["curso_id_ob"], $r["estudiante_id_o"], $r["ob_fecha"], $r["profesor_id_o"], $r["ob_observacion"], $r["ob_periodo"], $r["comportamiento"], $r["accion_pedagogica"], $r["ob_faltas"]);
            $observaciones[$i] = $observacion;
        }
        return $observaciones;
    }

    public static function getOnservacionById($ide, $idp, $idcur, $periodo) {
        $sql = "SELECT *  FROM `observacion` WHERE `estudiante_id_o` = '$ide' AND `profesor_id_o` = '$idp' AND `ob_periodo` = $periodo AND `curso_id_ob` = '$idcur'";

        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($r = mysql_fetch_array($result)) {
            return new Observacion($r["curso_id_ob"], $r["estudiante_id_o"], $r["ob_fecha"], $r["profesor_id_o"], $r["ob_observacion"], $r["ob_periodo"], $r["comportamiento"], $r["accion_pedagogica"], $r["ob_faltas"]);
        }
        return NULL;
    }

    public static function insertar(Observacion $o) {
        $sql = "INSERT INTO `" . BD::$dataBase . "`.`observacion` "
                . "(`estudiante_id_o`, `profesor_id_o`, `ob_periodo`, `ob_observacion`, `ob_fecha`, `curso_id_ob`,`accion_pedagogica`,`comportamiento`,`ob_faltas`) VALUES"
                . " ('" . $o->getIdestudiante() . "', '" . $o->getIdprofe() . "', '" . $o->getPeriodo() . "', '" . $o->getDescripcion() . "', '" . $o->getFecha() . "', '" . $o->getIdcurso() . "', '" . $o->getAccionPedagogica() . "', '" . $o->getComportamiento() . "', '" . $o->getFaltas() . "');";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public static function update(Observacion $o) {
        $sql = "UPDATE `" . BD::$dataBase . "`.`observacion` SET `ob_faltas` = '" . $o->getFaltas() . "', `ob_observacion` = '" . $o->getDescripcion() . "', `accion_pedagogica` = '" . $o->getAccionPedagogica() . "', `comportamiento` = '" . $o->getComportamiento() . "' WHERE `observacion`.`estudiante_id_o` = '" . $o->getIdestudiante() . "' AND `observacion`.`profesor_id_o` = '" . $o->getIdprofe() . "' AND `observacion`.`ob_periodo` = " . $o->getPeriodo() . " AND `observacion`.`curso_id_ob` = '" . $o->getIdcurso() . "';";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

}

?>
