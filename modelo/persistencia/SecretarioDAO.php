<?php

include_once 'BD.php';
include_once realpath(dirname(__FILE__)) . '/../dto/Secretario.php';
include_once 'UsuarioDAO.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SupervisorDAO
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class SecretarioDAO {

    public static function eliminar($id) {
        return UsuarioDAO::get()->eliminar($id);
    }

    public static function insertar(Secretario $secretario) {
        $sql2 = "INSERT INTO `" . BD::$dataBase . "`.`secretario` "
                . "(`idsecretario`, `s_correo`, `s_username`, `s_nombre`, `s_apellido`, `s_telefono`) "
                . "VALUES "
                . "('" . $secretario->getId() . "', '" . $secretario->getCorreo() . "', '" . $secretario->getLogin() . "', '" . $secretario->getNombre() . "', '" . $secretario->getApellido() . "', '" . $secretario->getTelefono() . "');";

        BD::open();
        if (UsuarioDAO::get()->insertar($secretario) != "") {
            return BD::error();
        }
        BD::sentenceSQL($sql2);
        if (BD::error() != "") {
            return BD::error();
        }
        BD::close();
        return BD::error();
    }

    public static function getSecretarioById($id) {
        $sql = "SELECT `password`, `secretario`.* FROM  `usuario`,`secretario` WHERE  `codigo` =  '" . $id . "' AND `idsecretario` = '" . $id . "'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        if ($r = mysql_fetch_array($result)) {
            $secretario = new Secretario($r["idsecretario"], $r["s_nombre"], $r["s_apellido"], $r["s_telefono"], $r["s_correo"], $r["s_username"], $r["password"], Usuario::SECRETARIO);
            return $secretario;
        }
        return null;
    }

    public static function getSecretarios() {
        $sql = "SELECT * FROM  `secretario` ORDER BY  `secretario`.`s_apellido` ASC ";
        $secretarios = array();
        BD::open();
        $result = BD::sentenceSQL($sql);
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $secretarios[$i] = new Secretario($r["idsecretario"], $r["s_nombre"], $r["s_apellido"], $r["s_telefono"], $r["s_correo"], NULL, NULL, Usuario::SECRETARIO);
        }
        return $secretarios;
    }

    public static function actualizarSecretario(Secretario $p, $id) {
        $sql2 = "UPDATE `" . BD::$dataBase . "`.`secretario` SET `s_nombre` = '" . $p->getNombre() . "', `s_apellido` = '" . $p->getApellido() . "', `s_telefono` = '" . $p->getTelefono() . "' WHERE `secretario`.`idsecretario` = '" . $p->getId() . "';";
        $cambios = 0;
        if (UsuarioDAO::updateUsuario($p, $id)) {
            return BD::error();
        }
        $cambios += BD::affectedRows();
        BD::sentenceSQL($sql2);
        if (BD::error() != "") {
            return BD::error();
        }
        $cambios += BD::affectedRows();
        if ($cambios == 0) {
            return BD::error("Los Cambios no fueron guardados");
        }
        BD::close();
        return BD::error();
    }

}

?>