<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SolicitudDAO
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class SolicitudDAO {

    public static function actualizar($solicitud) {
        $sql = "UPDATE `".BD::$dataBase."`.`solicitud` SET `s_estado` = '" . $solicitud->getEstado() . "', `s_respuesta` = '" . $solicitud->getRespuesta() . "', `s_fecharespuesta` = '" . $solicitud->getFechaRespuesta() . "' WHERE `solicitud`.`idsolicitud` = " . $solicitud->getId() . ";";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public function eliminarById($objeto) {
        $sql = "DELETE FROM `".BD::$dataBase."`.`solicitud` WHERE `solicitud`.`idsolicitud` = 1" . $objeto->getId() . ";";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public static function insertar($s) {
        $a = $s->getAsistencia();
        $sql1 = "INSERT INTO `".BD::$dataBase."`.`solicitud`
            (`idsolicitud`, `asistencia_fecha_s`, `asistencia_clase_id_s`, `supervisor_id`, `profesor_id`, `s_tipo`, `s_asunto`, `s_descripcion`, `s_estado`, `s_respuesta`, `s_nuevafecha`, `s_fechaoriginal`, `s_fechasolicitud`, `s_fecharespuesta`,`asist_estadooriginal`) 
            VALUES (NULL, '" . $a->getFechaClase() . "', '" . $a->getIdclase() . "', '" . $s->getIdsupervisor() . "', '" . $s->getIdprofesor() . "', '" . $s->getTipo() . "', '" . $s->getAsunto() . "', '" . $s->getDescripcion() . "', '0', '...', '" . $s->getFechaNueva() . "', '" . $a->getFechaClase() . "', '" . Calendario::getFechaActual() . "', '" . Calendario::getFechaActual() . "'," . $a->getEstado() . ");";
        BD::open();
        BD::sentenceSQL($sql1);
        BD::close();
        return BD::error();
    }

    public static function getSolicitudes($id, $tipo) {
        $sql = "SELECT * FROM  `solicitud` WHERE  `" . $tipo . "_id` =  '" . $id . "' ORDER BY  `solicitud`.`s_fecharespuesta` DESC, `solicitud`.`s_estado` ASC";
        $solicitudes = array();
        BD::open();
        $result = BD::sentenceSQL($sql);
        for ($i = 0; $row = mysql_fetch_array($result); $i++) {
            $asistencia = AsistenciaDAO::getAsistByClaseIdAndFecha($row["asistencia_clase_id_s"], $row["asistencia_fecha_s"]);
            $solicitudes[$i] = new Solicitud($row["idsolicitud"], $asistencia, $row["s_tipo"], $row["s_asunto"], $row["s_descripcion"], $row["s_estado"], $row["s_respuesta"], $row["s_fechaoriginal"], $row["s_nuevafecha"], $row["s_fechasolicitud"], $row["s_fecharespuesta"], $row["profesor_id"], $row["supervisor_id"], $row["asist_estadooriginal"]);
        }
        return $solicitudes;
    }

    public static function getSolicitud($id) {
        $sql = "SELECT *  FROM `solicitud` WHERE `idsolicitud` = " . $id;
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            $asistencia = AsistenciaDAO::getAsistByClaseIdAndFecha($row["asistencia_clase_id_s"], $row["asistencia_fecha_s"]);
            $solicitud = new Solicitud($row["idsolicitud"], $asistencia, $row["s_tipo"], $row["s_asunto"], $row["s_descripcion"], $row["s_estado"], $row["s_respuesta"], $row["s_fechaoriginal"], $row["s_nuevafecha"], $row["s_fechasolicitud"], $row["s_fecharespuesta"], $row["profesor_id"], $row["supervisor_id"], $row["asist_estadooriginal"]);
            return $solicitud;
        }
    }

    public static function getInformeSolicitud($idp, $idasig, $ptipo, $pi, $pf, $fi, $ff) {
        $pi = ($pi != "") ? "AND `c_periodo` >= $pi" : "";
        $pf = ($pf != "") ? "AND `c_periodo` <= $pf" : "";
        $fi = ($fi != "")?"AND `s_fechasolicitud` >=  '$fi'":"";
        $ff = ($ff != "")?"AND `s_fechasolicitud` <=  '$ff'":"";
        $informe = array();
        $sql = "SELECT  `s_descripcion`,`p_tipo`,`p_nombre`,`p_apellido`, `a_nombre`, `c_periodo`, `sede`.`s_nombre`,`s_fechasolicitud` ,`s_fechaoriginal`, `s_nuevafecha`,`asist_estadooriginal` 
            FROM `solicitud`,`profesor`,`clase`,`asignatura`, `sede` 
            WHERE `profesor_id` LIKE '%$idp%'  AND `profesor_id` = `idprofesor` AND `p_tipo` LIKE '%$ptipo%' AND `asistencia_clase_id_s` = `idclase` AND `asignatura_id_c` = `idasignatura`  AND `idasignatura` LIKE '%$idasig%' AND `idsede` = `sede_id_c` $pi $pf $fi $ff
            ORDER BY `solicitud`.`s_fechasolicitud` ASC";
        BD::open();
        $result = BD::sentenceSQL($sql);
        for ($i = 0; $row = mysql_fetch_array($result); $i++) {
            $informe[$i] = $row;
        }
        return $informe;
    }

}

?>
