<?php

include_once realpath(dirname(__FILE__)) . '/../dto/NotaPeriodo.php';
include_once 'EstudianteDAO.php';
include_once 'BD.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NotaDAO
 *
 * @author leon
 */
class NotaDAO {

    public static function getNotaEstudiantePeriodo($idEstudiane, $idclase, $periodo, $ano) {
        $ano = "" . ($ano != '') ? $ano : "%";
        $periodo = "" . ($periodo != '') ? $periodo : "%";
        $idperiodo = "" . $ano . $periodo;
        $sql = "SELECT *  FROM `nota_periodo` WHERE `clase_id` = $idclase AND `estudiante_id` = '$idEstudiane' AND `m_periodo_id` LIKE '$idperiodo'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        if ($r = mysql_fetch_array($result)) {
            $notaPeriodo = new NotaPeriodo(EstudianteDAO::get()->getEstudianteByid($r["estudiante_id"]), $idclase, $r["m_periodo_id"], $r["nota_final"], $r["falta"], $r["taller_casa"], $r["taller_clase"], $r["trabajo_escrito"], $r["consulta_bibli"], $r["pres_cuad"], $r["evaluacion_periodo"], $r["evaluacion_escrita"], $r["socializacion"], $r["nota_formativa"]);
            return $notaPeriodo;
        }
        return null;
    }

    public static function getNotaPeriodoEstudiante($idEstudiane, $periodo) {
        $sql = "SELECT * FROM `nota_periodo` WHERE `estudiante_id` LIKE '$idEstudiane' AND `m_periodo_id` = $periodo";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $notas = array();
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $notas[$i] = new NotaPeriodo($r["estudiante_id"], $r["clase_id"], $r["m_periodo_id"], $r["nota_final"], $r["falta"], $r["taller_casa"], $r["taller_clase"], $r["trabajo_escrito"], $r["consulta_bibli"], $r["pres_cuad"], $r["evaluacion_periodo"], $r["evaluacion_escrita"], $r["socializacion"], $r["nota_formativa"]);
        }
        return $notas;
    }

    public static function getNotaPeriodoEstudianteCurso($idEstudiane, $periodo, $idcurso) {
        $sql = "SELECT * FROM `nota_periodo`, `clase` WHERE `estudiante_id` LIKE '$idEstudiane' AND `m_periodo_id` = $periodo AND  `curso_id_c` = '$idcurso'  AND `clase_id` = `idclase`";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $notas = array();
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $notas[$i] = new NotaPeriodo($r["estudiante_id"], $r["clase_id"], $r["m_periodo_id"], $r["nota_final"], $r["falta"], $r["taller_casa"], $r["taller_clase"], $r["trabajo_escrito"], $r["consulta_bibli"], $r["pres_cuad"], $r["evaluacion_periodo"], $r["evaluacion_escrita"], $r["socializacion"], $r["nota_formativa"]);
        }
        return $notas;
    }

    public static function getNotasEstudianteCurso($idestu, $idcurso) {
        $sql = "SELECT *  FROM `nota_periodo`,`clase` WHERE `estudiante_id` =  '$idestu' AND  `curso_id_c` = '$idcurso'  AND `clase_id` = `idclase`";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $notas = array();
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $notas[$i] = new NotaPeriodo($r["estudiante_id"], $r["clase_id"], $r["m_periodo_id"], $r["nota_final"], $r["falta"], $r["taller_casa"], $r["taller_clase"], $r["trabajo_escrito"], $r["consulta_bibli"], $r["pres_cuad"], $r["evaluacion_periodo"], $r["evaluacion_escrita"], $r["socializacion"], $r["nota_formativa"]);
        }
        return $notas;
    }

    public static function getNotaEstudiantePeriodo2($idEstudiane, $idclase, $periodo, $ano) {
        $ano = "" . ($ano != '') ? $ano : "%";
        $periodo = "" . ($periodo != '') ? $periodo : "%";
        $idperiodo = "" . $ano . $periodo;
        $sql = "SELECT *  FROM `nota_periodo` WHERE `clase_id` = $idclase AND `estudiante_id` = '$idEstudiane'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        if ($r = mysql_fetch_array($result)) {
            $notaPeriodo = new NotaPeriodo($r["estudiante_id"], $idclase, $r["m_periodo_id"], $r["nota_final"], $r["falta"], $r["taller_casa"], $r["taller_clase"], $r["trabajo_escrito"], $r["consulta_bibli"], $r["pres_cuad"], $r["evaluacion_periodo"], $r["evaluacion_escrita"], $r["socializacion"], $r["nota_formativa"]);
            return $notaPeriodo;
        }
        return null;
    }

    public static function getNotaByID($idEstudiane, $idclase, $idperiodo) {
        $sql = "SELECT *  FROM `nota_periodo` WHERE `clase_id` = $idclase AND `estudiante_id` = '$idEstudiane' AND `m_periodo_id` = $idperiodo";
        BD::open();
        $result = BD::sentenceSQL($sql);
        if ($r = mysql_fetch_array($result)) {
            $notaPeriodo = new NotaPeriodo($r["estudiante_id"], $idclase, $r["m_periodo_id"], $r["nota_final"], $r["falta"], $r["taller_casa"], $r["taller_clase"], $r["trabajo_escrito"], $r["consulta_bibli"], $r["pres_cuad"], $r["evaluacion_periodo"], $r["evaluacion_escrita"], $r["socializacion"], $r["nota_formativa"]);
            return $notaPeriodo;
        }
        return null;
    }

    public static function getNotasEstudianteClaseAno($idEstudiane, $idclase, $ano) {
        $sql = "SELECT *  FROM `nota_periodo` WHERE `clase_id` = $idclase AND `estudiante_id` = '$idEstudiane' AND `m_periodo_id` LIKE '$ano%'";
        $notas = array();
        BD::open();
        $result = BD::sentenceSQL($sql);
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $notaPeriodo = new NotaPeriodo($r["estudiante_id"], $idclase, $r["m_periodo_id"], $r["nota_final"], $r["falta"], $r["taller_casa"], $r["taller_clase"], $r["trabajo_escrito"], $r["consulta_bibli"], $r["pres_cuad"], $r["evaluacion_periodo"], $r["evaluacion_escrita"], $r["socializacion"], $r["nota_formativa"]);
            $notas[$i] = $notaPeriodo;
        }
        return $notas;
    }

    public static function getNotaFinalPeriodoEstudianteClase($clase, $periodo, $estudiante) {
        $sql = "SELECT  `nota_final` FROM `nota_periodo` WHERE `clase_id` = $clase AND `m_periodo_id` = $periodo AND `estudiante_id` = '$estudiante'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        if ($r = mysql_fetch_array($result)) {
            return $r["nota_final"];
        }
        return null;
    }

    public static function getNotasClasePerido($idclase, $periodo) {
        $sql = "SELECT *  FROM `nota_periodo` WHERE `clase_id` = $idclase AND `m_periodo_id` LIKE '%$periodo'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $notas = array();
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $notaPeriodo = new NotaPeriodo(EstudianteDAO::get()->getEstudianteByid($r["estudiante_id"]), $idclase, $r["m_periodo_id"], $r["nota_final"], $r["falta"], $r["taller_casa"], $r["taller_clase"], $r["trabajo_escrito"], $r["consulta_bibli"], $r["pres_cuad"], $r["evaluacion_periodo"], $r["evaluacion_escrita"], $r["socializacion"], $r["nota_formativa"]);
            $notas[$i] = $notaPeriodo;
        }
        return $notas;
    }

    public static function getNotasEstudianteClase($idEstudiante, $idClase) {
        $sql = "SELECT *  FROM `nota_periodo` WHERE `clase_id` = $idClase AND `estudiante_id` = '$idEstudiante'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $notas = array();
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $notaPeriodo = new NotaPeriodo($r["estudiante_id"], $r["clase_id"], $r["m_periodo_id"], $r["nota_final"], $r["falta"], $r["taller_casa"], $r["taller_clase"], $r["trabajo_escrito"], $r["consulta_bibli"], $r["pres_cuad"], $r["evaluacion_periodo"], $r["evaluacion_escrita"], $r["socializacion"], $r["nota_formativa"]);
            $notas[$i] = $notaPeriodo;
        }
        return $notas;
    }

    public static function actualizar(NotaPeriodo $n) {
        $sql = "UPDATE `" . BD::$dataBase . "`.`nota_periodo` SET  `nota_final` = '" . $n->getNotaFinal() . "', `falta` = '" . $n->getFaltas() . "', `trabajo_escrito` = '" . $n->getTrabajoEscrito() . "', `evaluacion_escrita` = '" . $n->getEvaluacionEscrita() . "', `evaluacion_periodo` = '" . $n->getEvaluacionPeriodo() . "', `socializacion` = '" . $n->getSocializacion() . "', `consulta_bibli` = '" . $n->getConsultaBili() . "', `taller_clase` = '" . $n->getTallerClase() . "', `pres_cuad` = '" . $n->getPresCuad() . "', `taller_casa` = '" . $n->getTallerCasa() . "', `nota_formativa` = '" . $n->getNotaFormativa() . "' WHERE `nota_periodo`.`clase_id` = " . $n->getIdclase() . " AND `nota_periodo`.`estudiante_id` = '" . $n->getIdEstu() . "' AND `nota_periodo`.`m_periodo_id` = " . $n->getPeriodo() . ";";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public static function actualizarCampos($values, $idestu, $idclase, $periodo) {
        $sql = "UPDATE `" . BD::$dataBase . "`.`nota_periodo` SET $values WHERE `nota_periodo`.`clase_id` = $idclase AND `nota_periodo`.`estudiante_id` = '$idestu' AND `nota_periodo`.`m_periodo_id` = $periodo;";
        BD::open();
        BD::sentenceSQL($sql);
        return BD::error();
    }

    public static function updateEstado($ideEstu, $year, $estado) {
        $sql = "UPDATE `" . BD::$dataBase . "`.`nota_periodo` SET `nota_estado` = '$estado' WHERE `estudiante_id` LIKE '$ideEstu' AND `m_periodo_id` LIKE '$year%'";
        BD::open();
        BD::sentenceSQL($sql);
        return BD::error();
    }

    public static function insertarObservacion($idestu, $iddirector, $observacion, $periodo) {
        $sql = "INSERT INTO `" . BD::$dataBase . "`.`observacion` (`estudiante_id_o`, `profesor_id_o`, `ob_observacion`, `ob_fecha`, `ob_periodo`) VALUES ('$idestu', '$iddirector', '$observacion', '" . Calendario::getFechaActual() . "', '$periodo');";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

}
