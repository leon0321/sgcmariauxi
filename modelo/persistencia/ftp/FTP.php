<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FTP
 *
 * @author leon
 */
class FTP {

//public static $ftp_server = "localhost";
//public static $ftp_user_name = "leon@sgcmauxi.co";
//public static $ftp_user_pass = "Leonardo0321";
//public static $photoUserFolder = "/userphoto/";
//public static $raiz = "//home/adminscg2014/public_html";

    public static $ftp_server = "localhost";
    public static $ftp_user_name = "nobody";
    public static $ftp_user_pass = "lampp";
    public static $photoUserFolder = "/sgcmariauxi/userphoto/";
    public static $raiz = "//opt/lampp/htdocs/";
    public static $conn_id;
    public static $login_result;

    public static function conect() {
        FTP::$conn_id = ftp_connect(FTP::$ftp_server);
        FTP::$login_result = ftp_login(FTP::$conn_id, FTP::$ftp_user_name, FTP::$ftp_user_pass);
        if ((!FTP::$conn_id) || (!FTP::$login_result)) {
            return "¡La conexión FTP ha fallado!";
        }
        return "";
    }

    public static function checkexists($filename, $folder = "") {
        if ($folder == "") {
            $folder = FTP::$raiz . FTP::$photoUserFolder;
        }
        return (file_exists($folder . $filename)) ? 1 : 0;
    }

    public static function upload($source_file, $destination_file) {
        if (file_exists($destination_file)) {
            return 0;
        }
        $upload = ftp_put(FTP::$conn_id, $destination_file, $source_file, FTP_BINARY);
        if (!$upload) {
            return "Error no se pudo subir el archivo";
        }
        ftp_close(FTP::$conn_id);
        return "";
    }

    public static function rename($dir, $currentname, $newname) {
        if ($dir == "") {
            $dir = FTP::$photoUserFolder;
        }
        return ftp_rename(FTP::$conn_id, $dir . $currentname, $dir . $newname);
    }

    public static function deleteFile($file, $path = "") {
        if ($path == "") {
            $path = FTP::$photoUserFolder;
        }
        $path = $path . $file;
        ftp_delete(FTP::$conn_id, $path);
    }

    public static function close() {
        ftp_close(FTP::$conn_id);
    }

    public static function solicitudes() {
        if (empty($_GET["metodo"])) {
            return;
        }
        FTP::conect();
        $r = $_GET["metodo"];
        if ($r == "upload") {
            $error = FTP::upload($_FILES["Filedata"]["tmp_name"], "/sgcmariauxi/vista/image/" . $_FILES["Filedata"]["name"]);
            if ($error == "") {
                echo "<div id='ok'>ok</div>";
            } else {
                echo "<div id='bad'>$error</div>";
            }
        } elseif ($r == "checkexists") {
            echo FTP::checkexists($_FILES["Filedata"]["name"]);
        }
    }

}

//echo FTP::checkexists("ACE79839AA.jpg", "/ftp/sgcmariauxi/userphoto/");
//FTP::solicitudes();
