<?php

include_once 'BD.php';
include_once realpath(dirname(__FILE__)) . '/../dto/Rector.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RectorDAO
 *
 * @author leon
 */
class RectorDAO {

    public static function getRectorById($idrector) {
        $sql = "SELECT *  FROM `rector` WHERE `idrector` = '$idrector'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($r = mysql_fetch_array($result)) {
            return new Rector($r["idrector"], $r["r_nombre"], $r["r_apellido"], $r["r_telefono"], $r["r_correo"], NULL, NULL, NULL, $r["r_sexo"]);
        }
    }

    public static function getRectores() {
        $sql = "SELECT * FROM `rector`";
        $rectores = array();
        BD::open();
        $result = BD::sentenceSQL($sql);
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $rectores[$i] = new Rector($r["idrector"], $r["r_nombre"], $r["r_apellido"], $r["r_telefono"], $r["r_correo"], NULL, NULL, NULL, $r["r_sexo"]);
        }
        return $rectores;
    }

    public static function getRectorByYear($year) {
        $sql = "SELECT `rector`.*  FROM `periodo`,`rector` WHERE `idperiodo` = $year AND `rector_id` = `idrector`";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($r = mysql_fetch_array($result)) {
            return new Rector($r["idrector"], $r["r_nombre"], $r["r_apellido"], $r["r_telefono"], $r["r_correo"], NULL, NULL, NULL, $r["r_sexo"]);
        }
    }

    public static function actualizarRector(Rector $r, $id) {
        $sql2 = "UPDATE `" . BD::$dataBase . "`.`rector` SET `r_nombre` = '" . $r->getNombre() . "', `r_apellido` = '" . $r->getApellido() . "', `r_telefono` = '" . $r->getTelefono() . "' WHERE `rector`.`idrector` = '" . $r->getId() . "';";
        $cambios = 0;
        if (UsuarioDAO::updateUsuario($r, $id)) {
            return BD::error();
        }
        $cambios += BD::affectedRows();
        BD::sentenceSQL($sql2);
        if (BD::error() != "") {
            return BD::error();
        }
        $cambios += BD::affectedRows();
        if ($cambios == 0) {
            return BD::error("Los Cambios no fueron guardados");
        }
        BD::close();
        return BD::error();
    }

    public static function insertar(Rector $r) {
        $sql = "INSERT INTO `" . BD::$dataBase . "`.`rector` "
                . "(`idrector`, `r_correo`, `r_username`, `r_nombre`, `r_apellido`, `r_telefono`) VALUES "
                . "('" . $r->getId() . "', '" . $r->getCorreo() . "', '" . $r->getLogin() . "', '" . $r->getNombre() . "', '" . $r->getApellido() . "', '" . $r->getTelefono() . "');";
        BD::open();
        UsuarioDAO::get()->insertar($r);
        if (BD::error() != "") {
            return BD::error();
        }
        BD::sentenceSQL($sql);
        if (BD::error() != "") {
            return BD::error();
        }
        BD::close();
        return BD::error();
    }

}
