<?php

class BD {

    private static $username = "root";
    private static $password = "";
    public static $dataBase = "plataforma11";
    private static $server = "localhost";
    private static $error = "";
    private static $link;
    private static $result;

    public static function open() {

        if (!(BD::$link = mysql_connect(BD::$server, BD::$username, BD::$password))) {
            echo 'Error conectando con el servidor';
            return false;
        }
        if (!mysql_select_db(BD::$dataBase, BD::$link)) {
            echo "Error selecionando la base de datos";
            return false;
        }
        return true;
    }

    public static function close() {
        mysql_close(BD::$link);
    }

    public static function closefree() {
        mysql_free_result(BD::$result);
        mysql_close(BD::$link);
    }

    public static function error($error = "") {
        BD::$error = ($error == "") ? BD::$error : $error;
        return BD::$error;
    }

    public static function sentenceSQL($sql) {
        BD::$result = mysql_query($sql, BD::$link);
        BD::$error = mysql_error();
        return BD::$result;
    }

    public static function getField($table, $field, $orden = "") {
        $sql = "SELECT `$field` FROM `$table` ORDER BY `$table`.`$field` DESC";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $values = array();
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $values[$i] = $r["$field"];
        }
        return $values;
    }

    public static function getFieldFind($table, $fieldQuery, $fieldData, $valueQuery) {
        $sql = "SELECT `$fieldData` FROM `$table` WHERE `$fieldQuery` = '$valueQuery'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($r = mysql_fetch_array($result)) {
            return $r["$fieldData"];
        }
        return NULL;
    }

    public static function getRegistroByField($table, $field, $value) {
        $sql = "SELECT *  FROM `$table` WHERE `$field` = $value";
        echo $sql . "<br>";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($r = mysql_fetch_array($result)) {
            return $r["$field"];
        }
        return BD::error();
    }

    public static function getRegistrosByField($table, $field, $value) {
        $sql = "SELECT *  FROM `$table` WHERE `$field` = $value";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $estu = array();
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $estu[$i] = $r;
        }
        return $estu;
    }

    public static function getMayor($table, $field) {
        $max = "MAX(`$field`)";
        $sql = "SELECT MAX(`$field`) FROM `$table`;";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($r = mysql_fetch_array($result)) {
            return $r["$max"];
        }
        return BD::error();
    }

    public static function getCount($field, $table, $value) {
        $sql = "SELECT count(`$field`) FROM `$table` WHERE `$field` = '$value'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($r = mysql_fetch_array($result)) {
            return $r["count(`$field`)"];
        }
        return BD::error();
    }

    public static function affectedRows() {
        return mysql_affected_rows();
    }

}
?>