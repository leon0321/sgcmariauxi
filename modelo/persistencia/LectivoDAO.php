<?php

include_once 'BD.php';
include_once 'FormatoNotaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../dto/Lectivo.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LectivoDAO
 *
 * @author leon
 */
class LectivoDAO {

    public static function getLectivo($year) {
        $sql = "SELECT *  FROM `periodo` WHERE `idperiodo` = $year";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($r = mysql_fetch_array($result)) {
            return new Lectivo($r["idperiodo"], $r["rector_id"], $r["p_estado"], $r["p_semanas"], $r["p_inicio"], $r["p_fin"]);
        }
        return NULL;
    }

    public static function getLectivos() {
        $sql = "SELECT * FROM `periodo` ORDER BY `periodo`.`idperiodo`  DESC";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $lectivos = array();
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $lectivos[$i] = new Lectivo($r["idperiodo"], $r["rector_id"], $r["p_estado"], $r["p_semanas"], $r["p_inicio"], $r["p_fin"]);
        }
        return $lectivos;
    }

    public static function insertar(Lectivo $lectivo, $nperiodos) {
        $sql = "INSERT INTO `" . BD::$dataBase . "`.`periodo` "
                . "(`idperiodo`, `p_inicio`, `p_fin`, `p_semanas`, `p_estado`, `rector_id`) VALUES"
                . " ('" . $lectivo->getId() . "', '" . $lectivo->getFechaInicio() . "', '" . $lectivo->getFechaFin() . "', '" . $lectivo->getSemanas() . "', '0', '" . $lectivo->getDirector() . "');";
        BD::open();
        BD::sentenceSQL($sql);
        if (BD::error() != "") {
            return BD::error();
        }
        
        $error = FormatoNotaDAO::intertar($nperiodos, $lectivo->getId());
        return $error;
    }

}
