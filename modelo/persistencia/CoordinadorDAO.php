<?php

include_once 'InterfaceDAO.php';
include_once realpath(dirname(__FILE__)) . '/../dto/Coordinador.php';
include_once realpath(dirname(__FILE__)) . '/../dto/Usuario.php';
include_once 'BD.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProfesorDAO
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class CoordinadorDAO {

    public static function actualizarCoord(Coordinador $p, $id) {
        $sql2 = "UPDATE `" . BD::$dataBase . "`.`coordinador` SET `coor_nombre` = '" . $p->getNombre() . "', `coor_apellido` = '" . $p->getApellido() . "', `coor_telefono` = '" . $p->getTelefono() . "' WHERE `coordinador`.`idcoordinador` = '" . $p->getId() . "';";
        $cambios = 0;
        if (UsuarioDAO::updateUsuario($p, $id)) {
            return BD::error();
        }
        $cambios += BD::affectedRows();
        BD::sentenceSQL($sql2);
        if (BD::error() != "") {
            return BD::error();
        }
        $cambios += BD::affectedRows();
        if ($cambios == 0) {
            return BD::error("Los Cambios no fueron guardados");
        }
        BD::close();
        return BD::error();
    }

    public static function eliminar($id) {
        return UsuarioDAO::get()->eliminar($id);
    }

    public static function insertar($o) {
        $sql = "INSERT INTO `" . BD::$dataBase . "`.`coordinador` "
                . "(`idcoordinador`, `coor_correo`, `coor_username`, `coor_nombre`, `coor_apellido`, `coor_telefono`, `area_id_j`) VALUES "
                . "('" . $o->getId() . "', '" . $o->getCorreo() . "', '" . $o->getLogin() . "', '" . $o->getNombre() . "', '" . $o->getApellido() . "', '" . $o->getTelefono() . "', '1');";
        BD::open();
        UsuarioDAO::get()->insertar($o);
        if (BD::error() != "") {
            return BD::error();
        }
        BD::sentenceSQL($sql);
        if (BD::error() != "") {
            return BD::error();
        }
        BD::close();
        return BD::error();
    }

    public static function getCoordinadorByid($id) {
        $sql = "SELECT `password`, `coordinador`.* FROM  `usuario`,`coordinador` WHERE  `codigo` =  '" . $id . "' AND `idcoordinador` = '" . $id . "'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        if ($row = mysql_fetch_array($result)) {
            $estu = CoordinadorDAO::castToCoord($row);
            return $estu;
        }
        return null;
    }

    public static function getCoordinadores() {
        $sql = "SELECT * FROM `coordinador`ORDER BY `coordinador`.`coor_apellido` ASC";
        $jefes = array();
        BD::open();
        $result = BD::sentenceSQL($sql);
        for ($i = 0; $row = mysql_fetch_array($result); $i++) {
            $jefes[$i] = CoordinadorDAO::castToCoord($row);
        }
        return $jefes;
    }

    public static function castToCoord(array $row) {
        return new Coordinador($row["idcoordinador"], $row["coor_nombre"], $row["coor_apellido"], $row["coor_telefono"], $row["coor_correo"], $row["coor_username"], NULL, Usuario::COORDINADOR, $row["area_id_j"]);
    }

}

//echo CoordinadorDAO::get()->getCoordinadorByid("coord")->getNombre();
//echo JefeProgramaDAO::get()->insertar(new JefePrograma("11111111111", "11111111111", "1111111111", "1111111111", "111111111111", "111111111111", "1111111111111", Usuario::JEFEPROGRAMA, "022"));
?>
