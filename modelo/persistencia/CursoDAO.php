<?php

include_once realpath(dirname(__FILE__)) . '/../dto/Curso.php';
include_once 'ProfesorDAO.php';
include_once 'BD.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CursoDao
 *
 * @author leon
 */
class CursoDAO {

    public static function actualizar(Curso $c) {
        $sql = "UPDATE `" . BD::$dataBase . "`.`curso` SET `director_curso` = '" . $c->getDirector() . "', `estado` = '" . $c->getEstado() . "' WHERE `curso`.`idcurso` = '3A2014'';";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public static function updateDirector($idcurso, $idprofe) {
        $sql = "UPDATE `" . BD::$dataBase . "`.`curso` SET `director_curso` = '" . $idprofe . "' WHERE `curso`.`idcurso` = '$idcurso';";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public static function eliminar($id) {
        $sql = "DELETE FROM `" . BD::$dataBase . "`.`curso` WHERE `curso`.`idcurso` = " . $id;
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public static function insertar(Curso $o) {
        $sql = "INSERT INTO `" . BD::$dataBase . "`.`curso` "
                . "(`idcurso`, `grado_id_cur`, `fecha`, `letra`, `nombre`, `director_curso`, `estado`) VALUES "
                . "('" . $o->getId() . "', '" . $o->getGrado() . "', '" . $o->getFecha() . "', '" . $o->getLetra() . "', '" . $o->getNombre() . "', '" . $o->getDirector() . "', '" . $o->getEstado() . "');";
        echo $sql;
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public static function getCursoByid($id) {
        $sql = "SELECT *  FROM `curso` WHERE `idcurso` = '$id'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($curso = mysql_fetch_array($result)) {

            $director = ProfesorDAO::get()->getProfesorById($curso["director_curso"]);
            $grado = $curso["grado_id_cur"];
            $letra = $curso["letra"];
            $nombre = $curso["nombre"];
            $fecha = $curso["fecha"];
            return new Curso($id, $director, $grado, $fecha, $letra, $nombre, "1");
        }
        return null;
    }

    public static function getCursosByEstudiante($idestudiante) {
        $sql = "SELECT `curso`.*  FROM `estudiante`,`matricula`,`curso` WHERE `idestudiante` = '$idestudiante' AND `estudiante_id_m` = `idestudiante` AND `curso_id_m` = `idcurso` ORDER BY   `fecha`, `grado_id_cur` DESC";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $cursos = array();
        for ($i = 0; $curso = mysql_fetch_array($result); $i++) {
            $director = $curso["director_curso"];
            $grado = $curso["grado_id_cur"];
            $letra = $curso["letra"];
            $nombre = $curso["nombre"];
            $fecha = $curso["fecha"];
            $id = $curso["idcurso"];
            $estado = $curso["estado"];
            $cursos[$i] = new Curso($id, $director, $grado, $fecha, $letra, $nombre, $estado);
        }
        return $cursos;
    }

    public static function getCursosByGradoId($idGrado, $year = "") {
        $f = ($year == "") ? "" : ("AND `fecha` = $year");
        $sql = "SELECT *  FROM `curso` WHERE `grado_id_cur` = $idGrado $f;";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $cursos = array();
        for ($i = 0; $curso = mysql_fetch_array($result); $i++) {
            $director = ProfesorDAO::get()->getProfesorById($curso["director_curso"]);
            $grado = $curso["grado_id_cur"];
            $letra = $curso["letra"];
            $nombre = $curso["nombre"];
            $fecha = $curso["fecha"];
            $id = $curso["idcurso"];
            $estado = $curso["estado"];
            $cursos[$i] = new Curso($id, $director, $grado, $fecha, $letra, $nombre, $estado);
        }
        return $cursos;
    }

    public static function getCursos($year = "") {
        //$sql = "SELECT * FROM `curso` ORDER BY `grado_id_cur` ASC , `letra` ASC";
        $where = ($year == "") ? "" : "WHERE `idcurso` LIKE '%$year'";
        $sql = "SELECT *  FROM `curso` $where ORDER BY `grado_id_cur` ASC , `letra` ASC";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $cursos = array();
        while ($curso = mysql_fetch_array($result)) {
            $director = ProfesorDAO::get()->getProfesorById($curso["director_curso"]);
            $grado = $curso["grado_id_cur"];
            $letra = $curso["letra"];
            $nombre = $curso["nombre"];
            $fecha = $curso["fecha"];
            $id = $curso["idcurso"];
            $estado = $curso["estado"];
            $cursos[] = new Curso($id, $director, $grado, $fecha, $letra, $nombre, $estado);
        }
        return $cursos;
    }

    public static function getCursosByYear($year) {
        //$sql = "SELECT * FROM `curso` ORDER BY `grado_id_cur` ASC , `letra` ASC";
        $where = "WHERE `fecha` = $year";
        $sql = "SELECT *  FROM `curso` $where ORDER BY `grado_id_cur` ASC , `letra` ASC";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $cursos = array();
        while ($curso = mysql_fetch_array($result)) {
            $director = ProfesorDAO::get()->getProfesorById($curso["director_curso"]);
            $grado = $curso["grado_id_cur"];
            $letra = $curso["letra"];
            $nombre = $curso["nombre"];
            $fecha = $curso["fecha"];
            $id = $curso["idcurso"];
            $estado = $curso["estado"];
            $cursos[] = new Curso($id, $director, $grado, $fecha, $letra, $nombre, $estado);
        }
        return $cursos;
    }

    public static function getCursosByDirectorId($idprofe) {
        $sql = "SELECT *  FROM `curso` WHERE `director_curso` = '$idprofe'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        $cursos = array();
        while ($curso = mysql_fetch_array($result)) {
            $director = ProfesorDAO::get()->getProfesorById($curso["director_curso"]);
            $grado = $curso["grado_id_cur"];
            $letra = $curso["letra"];
            $nombre = $curso["nombre"];
            $fecha = $curso["fecha"];
            $id = $curso["idcurso"];
            $estado = $curso["estado"];
            $cursos[] = new Curso($id, $director, $grado, $fecha, $letra, $nombre, $estado);
        }
        return $cursos;
    }

    public static function getCursoMaxIdByEstudiante($idestu) {
        $sql = "SELECT max(`curso_id_m`)  FROM `matricula` WHERE `estudiante_id_m` = '$idestu'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($r = mysql_fetch_array($result)) {
            return $r["max(`curso_id_m`)"];
        }
        return NULL;
    }

//put your code here
}

//$fecha = 2014;
//$grado = 5;
//$letra = "A";
//echo CursoDAO::insertar(new Curso( "" . $grado . $letra . $fecha, "111", "5", "1", "2014", "A", "11 - A", ""));
//echo CursoDAO::getCursoByid($grado . $letra . $fecha)->getGrado();
