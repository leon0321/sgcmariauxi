<?php

include_once 'BD.php';
include_once realpath(dirname(__FILE__)) . '/../dto/Valoracion.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ValoracionDAO
 *
 * @author leon
 */
class ValoracionDAO {

    public static function getValoracionesByYear($year) {
        $sql = "SELECT *  FROM `valoracion` WHERE `v_year` = $year ORDER BY `valoracion`.`v_orden` ASC";
        $valoraciones = array();
        BD::open();
        $result = BD::sentenceSQL($sql);
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            $valoraciones[$i] = new Valoracion($year, $r["v_inicial"], $r["v_final"], $r["v_orden"], $r["v_titulo"]);
        }
        return $valoraciones;
    }

    public static function getValoracionById($year, $orden) {
        $sql = "SELECT *  FROM `valoracion` WHERE `v_year` = $year AND `v_orden` = $orden";
        BD::open();
        $result = BD::sentenceSQL($sql);
        for ($i = 0; $r = mysql_fetch_array($result); $i++) {
            return new Valoracion($year, $r["v_inicial"], $r["v_final"], $r["v_orden"], $r["v_titulo"]);
        }
        return NULL;
    }

    public static function insertar(Valoracion $valoracion) {
        $sql = "INSERT INTO `" . BD::$dataBase . "`.`valoracion` "
                . "(`v_year`, `v_orden`, `v_inicial`, `v_final`, `v_titulo`) VALUES"
                . " ('" . $valoracion->getYear() . "', '" . $valoracion->getOrden() . "', '" . $valoracion->getInicial() . "', '" . $valoracion->getFinal() . "', '" . $valoracion->getTitulo() . "');";
        BD::open();
        BD::sentenceSQL($sql);
        return BD::error();
    }

    public static function update(Valoracion $valoracion, $orden) {
        $ordensql = ($valoracion->getOrden() == $orden) ? "" : "`v_orden` = '" . $valoracion->getOrden() . "',";
        $sql = "UPDATE `" . BD::$dataBase . "`.`valoracion` SET  $ordensql `v_inicial` = '" . $valoracion->getInicial() . "', `v_final` = '" . $valoracion->getFinal() . "', `v_titulo` = '" . $valoracion->getTitulo() . "' WHERE `valoracion`.`v_year` = " . $valoracion->getYear() . " AND `valoracion`.`v_orden` = $orden;";
        BD::open();
        BD::sentenceSQL($sql);
        return BD::error();
    }

    public static function delete($year, $orden) {
        $sql = "DELETE FROM `" . BD::$dataBase . "`.`valoracion` WHERE `valoracion`.`v_year` = $year AND `valoracion`.`v_orden` = $orden";
        BD::open();
        BD::sentenceSQL($sql);
        return BD::error();
    }

}
