<?php

include_once realpath(dirname(__FILE__)) . '/../dto/Clase.php';
include_once realpath(dirname(__FILE__)) . '/../dto/Profesor.php';
include_once realpath(dirname(__FILE__)) . '/../dto/HoraClase.php';
include_once 'BD.php';
include_once 'InterfaceDAO.php';
include_once 'ProfesorDAO.php';
include_once 'AreaDAO.php';
include_once 'SedeDAO.php';
include_once 'AsignaturaDAO.php';
include_once 'HoraClaseDAO.php';
include_once 'CursoDAO.php';


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ClaseDAO
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class ClaseDAO {

    public function actualizar($objeto) {
        
    }

    public static function eliminar($id) {
        $sql = "DELETE FROM `sig`.`clase` WHERE `clase`.`idclase` = " . $id;
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public static function insertar($clase) {
        $sql = "INSERT INTO `" . BD::$dataBase . "`.`clase` "
                . "(`idclase`, `sede_id_c`, `profesor_id_c`, `area_id_c`, `asignatura_id_c`, `c_estado`, `c_periodo`, `curso_id_c`) VALUES "
                . "(NULL, '" . $clase["sede"] . "', '" . $clase["profesor"] . "', '" . $clase["area"] . "', '" . $clase["asignatura"] . "', '" . $clase["estado"] . "', '" . $clase["periodo"] . "', '" . $clase["curso"] . "');";
        BD::open();
        BD::sentenceSQL($sql);
        BD::close();
        return BD::error();
    }

    public static function getSQLinsertarClases($clases, $headsql = 0) {
        $sql = ($headsql == 0) ? "" : "INSERT INTO `" . BD::$dataBase . "`.`clase` (`idclase`, `sede_id_c`, `profesor_id_c`, `area_id_c`, `asignatura_id_c`, `c_estado`, `c_periodo`, `curso_id_c`) VALUES";
        for ($i = 0; $i < count($clases); $i++) {
            $sql = $sql . "(NULL, '" . $clases[$i]->getSede() . "', '" . $clases[$i]->getProfesor() . "', '" . $clases[$i]->getArea() . "', '" . $clases[$i]->getAsignatura() . "', '" . $clases[$i]->getEstado() . "', '" . $clases[$i]->getPeriodo() . "', '" . $clases[$i]->getCurso() . "'),";
        }
        $sql = ($headsql == 0) ? $sql : substr_replace($sql, ";", -1);
        return $sql;
    }

    public static function insertarO(Clase $c) {
        $sql = "INSERT INTO `" . BD::$dataBase . "`.`clase` "
                . "(`idclase`, `sede_id_c`, `profesor_id_c`, `area_id_c`, `asignatura_id_c`, `c_estado`, `c_periodo`, `curso_id_c`) VALUES "
                . "(NULL, '" . $c->getSede() . "', '" . $c->getProfesor() . "', '" . $c->getArea() . "', '" . $c->getAsignatura() . "', '" . $c->getEstado() . "', '" . $c->getPeriodo() . "', '" . $c->getCurso() . "');";
        BD::open();
        BD::sentenceSQL($sql);
        return BD::error();
    }

    public static function getClasesByProfesorId($pro, $year = "") {
        $y = ($year == "") ? "" : "AND `c_periodo` = $year";
        $clases = array();
        $sql = "SELECT *  FROM `clase` WHERE `profesor_id_c` = '" . $pro . "' AND  `c_estado` = 0 $y";
        BD::open();
        $result = BD::sentenceSQL($sql);
        for ($i = 0; $row = mysql_fetch_array($result); $i++) {
            $id = $row["idclase"];
            $profesor = ($pro);
            $area = ($row["area_id_c"]);
            $sede = ($row["sede_id_c"]);
            $curso = ($row["curso_id_c"]);
            $asignatura = AsignaturaDAO::get()->getAsignaturaById($row["asignatura_id_c"]);
            $clases[$i] = new Clase($id, NULL, $profesor, NULL, $area, $sede, $asignatura, NULL, $row["c_estado"], $row["c_periodo"], $curso);
        }
        return $clases;
    }

    public static function getClasesBy($idprofe, $asign, $pi, $pf, $ptipo) {
        $clases = array();
        $pi = ($pi != "") ? "AND `c_periodo` >= '$pi'" : "";
        $pf = ($pf != "") ? "AND `c_periodo` <= '$pf'" : "";
        $sql = "SELECT  `clase`.* FROM `clase`,`profesor` WHERE `profesor_id_c` LIKE '%$idprofe%' AND `asignatura_id_c` LIKE '%$asign%' $pi $pf AND `profesor_id_c` = `idprofesor` AND `p_tipo` LIKE '%$ptipo%'";
        BD::open();
        $result = BD::sentenceSQL($sql);
        for ($i = 0; $row = mysql_fetch_array($result); $i++) {
            $id = $row["idclase"];
            $profesor = ProfesorDAO::get()->getProfesorById($row["profesor_id_c"]);
            $area = AreaDAO::get()->getAreaByid($row["area_id_c"]);
            $sede = SedeDAO::getSedeById($row["sede_id_c"]);
            $curso = CursoDAO::getCursoById($row["curso_id_c"]);
            $asignatura = AsignaturaDAO::get()->getAsignaturaById($row["asignatura_id_c"]);
            $clases[$i] = new Clase($id, NULL, $profesor, NULL, $area, $sede, $asignatura, NULL, $row["c_estado"], $row["c_periodo"], $curso);
        }
        return $clases;
    }

    public static function getClases() {
        $clases = array();
        $sql = "SELECT * FROM `clase` WHERE  `c_estado` = 0 ORDER BY `clase`.`curso_id_c` ASC";
        BD::open();
        $result = BD::sentenceSQL($sql);
        for ($i = 0; $row = mysql_fetch_array($result); $i++) {
            $id = $row["idclase"];
            $profesor = ProfesorDAO::get()->getProfesorById($row["profesor_id_c"]);
            $area = AreaDAO::get()->getAreaByid($row["area_id_c"]);
            $sede = SedeDAO::getSedeById($row["sede_id_c"]);
            $curso = CursoDAO::getCursoById($row["curso_id_c"]);
            $asignatura = AsignaturaDAO::get()->getAsignaturaById($row["asignatura_id_c"]);
            $clases[$i] = new Clase($id, NULL, $profesor, NULL, $area, $sede, $asignatura, NULL, $row["c_estado"], $row["c_periodo"], $curso);
        }
        return $clases;
    }

    public static function getClasesByYear($year) {
        $clases = array();
        $sql = "SELECT *  FROM `clase` WHERE `c_periodo` = $year AND `c_estado` = 0 ORDER BY `clase`.`curso_id_c` ASC";
        BD::open();
        $result = BD::sentenceSQL($sql);
        for ($i = 0; $row = mysql_fetch_array($result); $i++) {
            $id = $row["idclase"];
            $profesor = ProfesorDAO::get()->getProfesorById($row["profesor_id_c"]);
            $area = AreaDAO::get()->getAreaByid($row["area_id_c"]);
            $sede = SedeDAO::getSedeById($row["sede_id_c"]);
            $curso = CursoDAO::getCursoById($row["curso_id_c"]);
            $asignatura = AsignaturaDAO::get()->getAsignaturaById($row["asignatura_id_c"]);
            $clases[$i] = new Clase($id, NULL, $profesor, NULL, $area, $sede, $asignatura, NULL, $row["c_estado"], $row["c_periodo"], $curso);
        }
        return $clases;
    }

    public static function getClaseById($id) {
        $sql = "SELECT *  FROM `clase` WHERE `idclase` = " . $id;
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            $profesor = ProfesorDAO::get()->getProfesorById($row["profesor_id_c"]);
            $area = $row["area_id_c"];
            $sede = $row["sede_id_c"];
            $curso = CursoDAO::getCursoById($row["curso_id_c"]);
            $asignatura = AsignaturaDAO::get()->getAsignaturaById($row["asignatura_id_c"]);
            return new Clase($id, NULL, $profesor, NULL, $area, $sede, $asignatura, NULL, $row["c_estado"], $row["c_periodo"], $curso);
        }
        return null;
    }

    public static function getUltima() {
        $sql = "SELECT MAX(`idclase`) FROM `clase`;";
        BD::open();
        $result = BD::sentenceSQL($sql);
        while ($row = mysql_fetch_array($result)) {
            if (empty($row["MAX(`idclase`)"])) {
                return "";
            }
            return ClaseDAO::getClaseById($row["MAX(`idclase`)"]);
        }
        return null;
    }

    public static function getIdClaseByCursoId($idcurso, $orden = "") {
        $sql = "SELECT `idclase` FROM `clase` WHERE `curso_id_c` = '$idcurso' " . $orden;
        BD::open();
        $result = BD::sentenceSQL($sql);
        $ids = array();
        for ($i = 0; $row = mysql_fetch_array($result); $i++) {
            $ids[$i] = $row["idclase"];
        }
        return $ids;
    }

    public static function getClasesByCursoId($idcurso, $orden = "") {
        $sql = "SELECT * FROM `clase` WHERE `curso_id_c` = '$idcurso' " . $orden;
        BD::open();
        $result = BD::sentenceSQL($sql);
        $clases = array();
        for ($i = 0; $row = mysql_fetch_array($result); $i++) {
            $id = $row["idclase"];
            $profesor = ($row["profesor_id_c"]);
            $area = ($row["area_id_c"]);
            $sede = ($row["sede_id_c"]);
            $curso = ($row["curso_id_c"]);
            $asignatura = ($row["asignatura_id_c"]);
            $clases[$i] = new Clase($id, NULL, $profesor, NULL, $area, $sede, $asignatura, NULL, $row["c_estado"], $row["c_periodo"], $curso);
        }
        return $clases;
    }

    public static function actualizarClase($idclase, $idprofe) {
        $sql = "UPDATE `" . BD::$dataBase . "`.`clase` SET `profesor_id_c` = '$idprofe' WHERE `idclase` = $idclase";
        BD::open();
        BD::sentenceSQL($sql);
        return BD::error();
    }

    public static function getGradoByClaseId($idclase) {
        $sql = "SELECT `grado_id_cur`  FROM `curso`,`clase` WHERE `idcurso` =  `curso_id_c` AND  `idclase` = $idclase;";
        BD::open();
        $result = BD::sentenceSQL($sql);
        if ($row = mysql_fetch_array($result)) {
            return $row["grado_id_cur"];
        }
        return -1;
    }

}

//$ids = ClaseDAO::getIdClaseByCursoId($_GET["clase"]);
//for ($i = 0; $i < count($ids); $i++) {
//    echo  $ids[$i] . "<br>";
//}
?>
