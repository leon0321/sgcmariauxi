<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Asistencia
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class Asistencia {

    const ESPERA = "0";
    const ACTIVA = "1";
    const COMFIRMADA_PROFESOR = "2";
    const DICTADA = "3";
    const APLAZADA = "4";
    const NO_DICTADA = "5";
    const RECUPERAR = "6";
    const DESAPROBADA = "7";
    public static $estados = array("0","1", "2", "3", "4", "5","6");
    private $idclase;
    private $horasDictadas;
    private $fechaClase;
    private $fechaRegistro;
    private $tema;
    private $descriocion;
    private $estado;
    private $horas;

    function __construct($idclase, $horasDictadas, $fechaClase, $fechaRegistro, $tema, $descriocion, $estado, $horas) {
        $this->idclase = $idclase;
        $this->horasDictadas = $horasDictadas;
        $this->fechaClase = $fechaClase;
        $this->fechaRegistro = $fechaRegistro;
        $this->tema = $tema;
        $this->descriocion = $descriocion;
        $this->estado = $estado;
        $this->horas = $horas;
    }

    public function getHorasDictadas() {
        return $this->horasDictadas;
    }

    public function setHorasDictadas($horasDictadas) {
        $this->horasDictadas = $horasDictadas;
    }

    public function getFechaClase() {
        return $this->fechaClase;
    }

    public function setFechaClase($fechaClase) {
        $this->fechaClase = $fechaClase;
    }

    public function getFechaRegistro() {
        return $this->fechaRegistro;
    }

    public function setFechaRegistro($fechaRegistro) {
        $this->fechaRegistro = $fechaRegistro;
    }

    public function getTema() {
        return $this->tema;
    }

    public function setTema($tema) {
        $this->tema = $tema;
    }

    public function getDescriocion() {
        return $this->descriocion;
    }

    public function setDescripcion($descriocion) {
        $this->descriocion = $descriocion;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }

    public function getIdclase() {
        return $this->idclase;
    }

    public function setIdclase($idclase) {
        $this->idclase = $idclase;
    }

    public function getHoras() {
        return $this->horas;
    }

    public function setHoras($horas) {
        $this->horas = $horas;
    }

}

?>
