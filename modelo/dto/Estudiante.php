<?php

include_once 'Persona.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Estudiante
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class Estudiante extends Usuario {

    private $grado;

    public function __construct($id, $nombre, $apellido, $telefono, $correo, $login, $password, $tipo,$grado) {
        parent::__construct($id, $nombre, $apellido, $telefono, $correo, $login, $password, $tipo);
        $this->grado = $grado;
    }

    public function getGrado() {
        return $this->grado;
    }

    public function setGrado($grado) {
        $this->grado = $grado;
    }

}
?>
