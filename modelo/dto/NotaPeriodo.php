<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NotaPeriodo
 *
 * @author leon
 */
class NotaPeriodo {

    private $idEstu;
    private $idclase;
    private $periodo;
    private $notaFinal;
    private $faltas;
    private $tallerCasa;
    private $tallerClase;
    private $trabajoEscrito;
    private $consultaBili;
    private $presCuad;
    private $evaluacionPeriodo;
    private $evaluacionEscrita;
    private $socializacion;
    private $notaFormativa;
    public static $FACTOR_NE = 0.25;
    public static $FACTOR_RN = 0.75;

    function __construct($idEstu, $idClase, $periodo, $notaFinal, $faltas, $tallerCasa, $tallerClase, $trabajoEscrito, $consultaBili, $presCuad, $evaluacionPeriodo, $evaluacionEscrita, $socializacion, $notaFormativa) {
        $this->notaFinal = $notaFinal;
        $this->faltas = $faltas;
        $this->tallerCasa = $tallerCasa;
        $this->tallerClase = $tallerClase;
        $this->trabajoEscrito = $trabajoEscrito;
        $this->consultaBili = $consultaBili;
        $this->presCuad = $presCuad;
        $this->evaluacionPeriodo = $evaluacionPeriodo;
        $this->evaluacionEscrita = $evaluacionEscrita;
        $this->socializacion = $socializacion;
        $this->notaFormativa = $notaFormativa;
        $this->idEstu = $idEstu;
        $this->idclase = $idClase;
        $this->periodo = $periodo;
    }

    public function calcularNota() {
        $notas = array();
        $notas[0] = $this->consultaBili + 0;
        $notas[1] = $this->evaluacionEscrita + 0;
        $notas[2] = $this->presCuad + 0;
        $notas[3] = $this->socializacion + 0;
        $notas[4] = $this->tallerCasa + 0;
        $notas[5] = $this->tallerClase + 0;
        $notas[6] = $this->notaFormativa + 0;
        $notas[7] = $this->trabajoEscrito + 0;
        $this->notaFinal = NotaPeriodo::calculaNota($this->evaluacionPeriodo, $notas);
        return $this->notaFinal;
    }
    
    public function set($atrName,$value){
        if($atrName == "consultaBili"){
            $this->consultaBili = $value;
        }else if($atrName == "evaluacionEscrita"){
            $this->evaluacionEscrita = $value;
        }else if($atrName == "presCuad"){
            $this->presCuad = $value;
        }else if($atrName == "socializacion"){
            $this->socializacion = $value;
        }else if($atrName == "tallerCasa"){
            $this->tallerCasa = $value;
        }else if($atrName == "tallerClase"){
            $this->tallerClase = $value;
        }else if($atrName == "notaFormativa"){
            $this->notaFormativa = $value;
        }else if($atrName == "trabajoEscrito"){
            $this->trabajoEscrito = $value;
        }else if($atrName == "evaluacionPeriodo"){
            $this->evaluacionPeriodo = $value;
        }else if($atrName == "faltas"){
            $this->faltas = $value;
        }
        
    }

    public static function calculaNota($notaE, array $notas) {
        $factorNotaE = NotaPeriodo::$FACTOR_NE;
        $notaFinal = 0;
        $numeroNotasF = 0;
        $acum = 0.0;
        $numeroNotas = count($notas);
        $numeroNotasIngresadas = 0;
        for ($i = 0; $i < $numeroNotas; $i++) {
            if ($notas[$i] > 0) {
                $acum += $notas[$i];
                $numeroNotasIngresadas++;
            } else {
                $numeroNotasF++;
            }
        }

        if ($acum == 0) {
            $notaFinal = $notaE;
            return $notaFinal;
        }
        
        $notaFinal = ($acum/$numeroNotasIngresadas)*  NotaPeriodo::$FACTOR_RN + $notaE*NotaPeriodo::$FACTOR_NE;
//
//        $factorNotaE = $factorNotaE + $numeroNotasF * (0.75 / $numeroNotas);
//        $factorNotaA = 1 - $factorNotaE;
//        $promedio = $acum / ($numeroNotas - $numeroNotasF);
//        $notaFinal = $notaE * $factorNotaE + $promedio * $factorNotaA;
        return $notaFinal;
    }

    public function getIdEstu() {
        return $this->idEstu;
    }

    public function getIdclase() {
        return $this->idclase;
    }

    public function getPeriodo() {
        return $this->periodo;
    }

    public function getNotaFinal() {
        return $this->notaFinal;
    }

    public function getFaltas() {
        return $this->faltas;
    }

    public function getTallerCasa() {
        return $this->tallerCasa;
    }

    public function getTallerClase() {
        return $this->tallerClase;
    }

    public function getTrabajoEscrito() {
        return $this->trabajoEscrito;
    }

    public function getConsultaBili() {
        return $this->consultaBili;
    }

    public function getPresCuad() {
        return $this->presCuad;
    }

    public function getEvaluacionPeriodo() {
        return $this->evaluacionPeriodo;
    }

    public function getEvaluacionEscrita() {
        return $this->evaluacionEscrita;
    }

    public function getSocializacion() {
        return $this->socializacion;
    }

    public function getNotaFormativa() {
        return $this->notaFormativa;
    }

    public function setIdEstu($idEstu) {
        $this->idEstu = $idEstu;
    }

    public function setIdclase($idclase) {
        $this->idclase = $idclase;
    }

    public function setPeriodo($periodo) {
        $this->periodo = $periodo;
    }

    public function setNotaFinal($notaFinal) {
        $this->notaFinal = $notaFinal;
    }

    public function setFaltas($faltas) {
        $this->faltas = $faltas;
    }

    public function setTallerCasa($tallerCasa) {
        $this->tallerCasa = $tallerCasa;
    }

    public function setTallerClase($tallerClase) {
        $this->tallerClase = $tallerClase;
    }

    public function setTrabajoEscrito($trabajoEscrito) {
        $this->trabajoEscrito = $trabajoEscrito;
    }

    public function setConsultaBili($consultaBili) {
        $this->consultaBili = $consultaBili;
    }

    public function setPresCuad($presCuad) {
        $this->presCuad = $presCuad;
    }

    public function setEvaluacionPeriodo($evaluacionPeriodo) {
        $this->evaluacionPeriodo = $evaluacionPeriodo;
    }

    public function setEvaluacionEscrita($evaluacionEscrita) {
        $this->evaluacionEscrita = $evaluacionEscrita;
    }

    public function setSocializacion($socializacion) {
        $this->socializacion = $socializacion;
    }

    public function setNotaFormativa($notaFormativa) {
        $this->notaFormativa = $notaFormativa;
    }

}
