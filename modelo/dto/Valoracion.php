<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Valoracion
 *
 * @author leon
 */
class Valoracion {

    private $year;
    private $inicial;
    private $final;
    private $orden;
    private $titulo;
    private $n;

    function __construct($year, $inicial, $final, $orden, $titulo, $n = 4) {
        $this->year = $year;
        $this->inicial = $inicial;
        $this->final = $final;
        $this->orden = $orden;
        $this->titulo = $titulo;
        $this->n = $n;
    }

    public function getYear() {
        return $this->year;
    }

    public function getInicial() {
        return $this->inicial;
    }

    public function getFinal() {
        return $this->final;
    }

    public function getOrden() {
        return $this->orden;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getN() {
        return $this->n;
    }

    public function setYear($year) {
        $this->year = $year;
    }

    public function setInicial($inicial) {
        $this->inicial = $inicial;
    }

    public function setFinal($final) {
        $this->final = $final;
    }

    public function setOrden($orden) {
        $this->orden = $orden;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setN($n) {
        $this->n = $n;
    }

}
