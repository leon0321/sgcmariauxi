<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Solicitud
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class Solicitud {

    private $id;
    private $asistencia;
    private $tipo;
    private $asunto;
    private $descripcion;
    private $estado;
    private $respuesta;
    private $fechaOriginal;
    private $fechaNueva;
    private $fechaSolicitud;
    private $fechaRespuesta;
    private $idprofesor;
    private $idsupervisor;
    private $estadoOriginal;
    
    const ESPERA = "0";
    const APROBADO = "1";
    const RECHAZADA = "2";

    function __construct($id, $asistencia, $tipo, $asunto, $descripcion, $estado, $respuesta, $fechaOriginal, $fechaNueva, $fechaSolicitud, $fechaRespuesta, $idprofesor, $idsupervisor, $estadoOriginal) {
        $this->id = $id;
        $this->asistencia = $asistencia;
        $this->tipo = $tipo;
        $this->asunto = $asunto;
        $this->descripcion = $descripcion;
        $this->estado = $estado;
        $this->respuesta = $respuesta;
        $this->fechaOriginal = $fechaOriginal;
        $this->fechaNueva = $fechaNueva;
        $this->fechaSolicitud = $fechaSolicitud;
        $this->fechaRespuesta = $fechaRespuesta;
        $this->idprofesor = $idprofesor;
        $this->idsupervisor = $idsupervisor;
        $this->estadoOriginal = $estadoOriginal;
    }

    public function getAsistencia() {
        return $this->asistencia;
    }

    public function setAsistencia($asistencia) {
        $this->asistencia = $asistencia;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    public function getAsunto() {
        return $this->asunto;
    }

    public function setAsunto($asunto) {
        $this->asunto = $asunto;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }

    public function getRespuesta() {
        return $this->respuesta;
    }

    public function setRespuesta($respuesta) {
        $this->respuesta = $respuesta;
    }

    public function getFechaOriginal() {
        return $this->fechaOriginal;
    }

    public function setFechaOriginal($fechaOriginal) {
        $this->fechaOriginal = $fechaOriginal;
    }

    public function getFechaNueva() {
        return $this->fechaNueva;
    }

    public function setFechaNueva($fechaNueva) {
        $this->fechaNueva = $fechaNueva;
    }

    public function getFechaSolicitud() {
        return $this->fechaSolicitud;
    }

    public function setFechaSolicitud($fechaSolicitud) {
        $this->fechaSolicitud = $fechaSolicitud;
    }

    public function getFechaRespuesta() {
        return $this->fechaRespuesta;
    }

    public function setFechaRespuesta($fechaRespuesta) {
        $this->fechaRespuesta = $fechaRespuesta;
    }

    public function getIdprofesor() {
        return $this->idprofesor;
    }

    public function setIdprofesor($idprofesor) {
        $this->idprofesor = $idprofesor;
    }

    public function getIdsupervisor() {
        return $this->idsupervisor;
    }

    public function setIdsupervisor($idsupervisor) {
        $this->idsupervisor = $idsupervisor;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getEstadoOriginal() {
        return $this->estadoOriginal;
    }

    public function setEstadoOriginal($estadoOriginal) {
        $this->estadoOriginal = $estadoOriginal;
    }

}

?>
