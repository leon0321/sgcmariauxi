    <?php

    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of Sede
     *
     * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
     */
    class Sede {

        private $id;
        private $nombre;
        private $latitud;
        private $longitud;
        private $institucion;
        private $nombreLugar;
        private $direccion;
        private $pais;
        private $estado;
        private $ciudad;
        private $telefono;

        function __construct($id, $institucion, $nombre, $latitud, $longitud, $nombreLugar, $direccion, $pais, $estado, $ciudad, $telefono) {
            $this->id = $id;
            $this->nombre = $nombre;
            $this->latitud = $latitud;
            $this->longitud = $longitud;
            $this->institucion = $institucion;
            $this->nombreLugar = $nombreLugar;
            $this->direccion = $direccion;
            $this->pais = $pais;
            $this->estado = $estado;
            $this->ciudad = $ciudad;
            $this->telefono = $telefono;
        }

        public function getTelefono() {
            return $this->telefono;
        }

        public function setTelefono($telefono) {
            $this->telefono = $telefono;
        }

        public function getId() {
            return $this->id;
        }

        public function getNombre() {
            return $this->nombre;
        }

        public function getLatitud() {
            return $this->latitud;
        }

        public function getLongitud() {
            return $this->longitud;
        }

        public function getInstitucion() {
            return $this->institucion;
        }

        public function getNombreLugar() {
            return $this->nombreLugar;
        }

        public function getDireccion() {
            return $this->direccion;
        }

        public function getPais() {
            return $this->pais;
        }

        public function getEstado() {
            return $this->estado;
        }

        public function getCiudad() {
            return $this->ciudad;
        }

        public function setId($id) {
            $this->id = $id;
        }

        public function setNombre($nombre) {
            $this->nombre = $nombre;
        }

        public function setLatitud($latitud) {
            $this->latitud = $latitud;
        }

        public function setLongitud($longitud) {
            $this->longitud = $longitud;
        }

        public function setInstitucion($institucion) {
            $this->institucion = $institucion;
        }

        public function setNombreLugar($nombreLugar) {
            $this->nombreLugar = $nombreLugar;
        }

        public function setDireccion($direccion) {
            $this->direccion = $direccion;
        }

        public function setPais($pais) {
            $this->pais = $pais;
        }

        public function setEstado($estado) {
            $this->estado = $estado;
        }

        public function setCiudad($ciudad) {
            $this->ciudad = $ciudad;
        }

    }

    ?>
