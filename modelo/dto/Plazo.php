<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PlazoNota
 *
 * @author leon
 */
class Plazo {

    private $year;
    private $periodo;
    private $plazoIncial;
    private $plazoFinal;

    function __construct($year, $periodo, $plazoIncial, $plazoFinal) {
        $this->year = $year;
        $this->periodo = $periodo;
        $this->plazoIncial = $plazoIncial;
        $this->plazoFinal = $plazoFinal;
    }

    public function getYear() {
        return $this->year;
    }

    public function getPeriodo() {
        return $this->periodo;
    }

    public function getPlazoIncial() {
        return $this->plazoIncial;
    }

    public function getPlazoFinal() {
        return $this->plazoFinal;
    }

    public function setYear($year) {
        $this->year = $year;
    }

    public function setPeriodo($periodo) {
        $this->periodo = $periodo;
    }

    public function setPlazoIncial($plazoIncial) {
        $this->plazoIncial = $plazoIncial;
    }

    public function setPlazoFinal($plazoFinal) {
        $this->plazoFinal = $plazoFinal;
    }

}
