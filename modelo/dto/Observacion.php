<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Observacion
 *
 * @author EDGES
 */
class Observacion {

    private $idcurso;
    private $idestudiante;
    private $fecha;
    private $idprofe;
    private $descripcion;
    private $periodo;
    private $comportamiento;
    private $accion_pedagogica;
    private $faltas;

    function __construct($idcurso, $idestudiante, $fecha, $idprofe, $descripcion, $periodo, $comportamiento, $accion_pedagogica, $faltas = 0) {
        $this->idcurso = $idcurso;
        $this->idestudiante = $idestudiante;
        $this->fecha = $fecha;
        $this->idprofe = $idprofe;
        $this->descripcion = $descripcion;
        $this->periodo = $periodo;
        $this->accion_pedagogica = $accion_pedagogica;
        $this->comportamiento = $comportamiento;
        $this->faltas = $faltas;
    }

    public function getPeriodo() {
        return $this->periodo;
    }

    public function setPeriodo($periodo) {
        $this->periodo = $periodo;
    }

    public function getIdcurso() {
        return $this->idcurso;
    }

    public function setIdcurso($idcurso) {
        $this->idcurso = $idcurso;
    }

    public function getIdestudiante() {
        return $this->idestudiante;
    }

    public function setIdestudiante($idestudiante) {
        $this->idestudiante = $idestudiante;
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    public function getIdprofe() {
        return $this->idprofe;
    }

    public function setIdprofe($idprofe) {
        $this->idprofe = $idprofe;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function getComportamiento() {
        return $this->comportamiento;
    }

    public function setComportamiento($comportamiento) {
        $this->comportamiento = $comportamiento;
    }

    public function getAccionPedagogica() {
        return $this->accion_pedagogica;
    }

    public function setAccion_pedagogica($accion_pedagogica) {
        $this->accion_pedagogica = $accion_pedagogica;
    }

    public function getFaltas() {
        return $this->faltas;
    }

    public function setFaltas($faltas) {
        $this->faltas = $faltas;
    }

}

?>
