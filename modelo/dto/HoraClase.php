<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HoraClase
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class HoraClase {

    private $id;
    private $idClase;
    private $dia;
    private $horaInicio;
    private $horas;
    private $salon;

    function __construct($id, $idClase, $dia, $horaInicio, $horas, $salon) {
        $this->id = $id;
        $this->idClase = $idClase;
        $this->dia = $dia;
        $this->horaInicio = $horaInicio;
        $this->horas = $horas;
        $this->salon = $salon;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getIdClase() {
        return $this->idClase;
    }

    public function setIdClase($idClase) {
        $this->idClase = $idClase;
    }

    public function getDia() {
        return $this->dia;
    }

    public function setDia($dia) {
        $this->dia = $dia;
    }

    public function getHoraInicio() {
        return $this->horaInicio;
    }

    public function setHoraInicio($horaInicio) {
        $this->horaInicio = $horaInicio;
    }

    public function getHoras() {
        return $this->horas;
    }

    public function setHoras($horas) {
        $this->horas = $horas;
    }

    public function getSalon() {
        return $this->salon;
    }

    public function setSalon($salon) {
        $this->salon = $salon;
    }

}

?>
