<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Institucion
 *
 * @author leonardo ortega hernandez
 */
class Institucion {

    private $id;
    private $nombre;
    private $vigencia;
    private $dane;
    private $nit;
    private $resolucion;
    private $version;
    private $codigo;
    private $eslogan;

    function __construct($id, $nombre, $vigencia, $dane, $nit, $resolucion, $version, $codigo, $eslogan) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->vigencia = $vigencia;
        $this->dane = $dane;
        $this->nit = $nit;
        $this->resolucion = $resolucion;
        $this->version = $version;
        $this->codigo = $codigo;
        $this->eslogan = $eslogan;
    }

    public function getId() {
        return $this->id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getVigencia() {
        return $this->vigencia;
    }

    public function getDane() {
        return $this->dane;
    }

    public function getNit() {
        return $this->nit;
    }

    public function getResolucion() {
        return $this->resolucion;
    }

    public function getVersion() {
        return $this->version;
    }

    public function getCodigo() {
        return $this->codigo;
    }

    public function getEslogan() {
        return $this->eslogan;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setVigencia($vigencia) {
        $this->vigencia = $vigencia;
    }

    public function setDane($dane) {
        $this->dane = $dane;
    }

    public function setNit($nit) {
        $this->nit = $nit;
    }

    public function setResolucion($resolucion) {
        $this->resolucion = $resolucion;
    }

    public function setVersion($version) {
        $this->version = $version;
    }

    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    public function setEslogan($eslogan) {
        $this->eslogan = $eslogan;
    }

}
