<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Asignatura
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class Asignatura {

    private $id;
    private $codigo;
    private $nombre;
    private $descripcion;
    private $grado;
    private $creditos;
    private $idArea;
    private $intensidad;
    private $shortName;

    function __construct($id, $codigo, $nombre, $descripcion, $grado, $creditos, $area, $intensidad, $shortName = "") {
        $this->id = $id;
        $this->codigo = $codigo;
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
        $this->grado = $grado;
        $this->creditos = $creditos;
        $this->idArea = $area;
        $this->intensidad = $intensidad;
        $this->shortName = $shortName;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getIntensidad() {
        return $this->intensidad;
    }

    public function setIntensidad($intensidad) {
        $this->intensidad = $intensidad;
    }

    public function getCodigo() {
        return $this->codigo;
    }

    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function getGrado() {
        return $this->grado;
    }

    public function setGrado($semestre) {
        $this->grado = $semestre;
    }

    public function getCreditos() {
        return $this->creditos;
    }

    public function setCreditos($creditos) {
        $this->creditos = $creditos;
    }

    public function getIdArea() {
        return $this->idArea;
    }

    public function setIdArea($idProg) {
        $this->idArea = $idProg;
    }

    public function getShortName() {
        if($this->shortName == ""){
            return $this->nombre;
        }
        return $this->shortName;
    }

    public function setShortName($shortName) {
        $this->shortName = $shortName;
    }

}

?>
