<?php
include_once 'Usuario.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of JefePrograma
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class Coordinador extends Usuario {

    private $area;

    public function __construct($id, $nombre, $apellido, $telefono, $correo, $login, $password, $tipo,$area) {
        parent::__construct($id, $nombre, $apellido, $telefono, $correo, $login, $password, $tipo);
        $this->area = $area; 
    }

    public function getArea() {
        return $this->area;
    }

    public function setArea($area) {
        $this->area = $area;
    }

}

?>
