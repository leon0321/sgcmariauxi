<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Clase
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class Clase {

    private $id;
    private $horario;
    private $profesor;
    private $estudiantes;
    private $area;
    private $sede;
    private $curso;
    private $asignatura;
    private $asistencia;
    private $estado;
    private $periodo;

    function __construct($id, $horario, $profesor, $estudiantes, $area, $sede, $asignatura, $asistencia, $estado,$periodo,$curso) {
        $this->id = $id;
        $this->horario = $horario;
        $this->profesor = $profesor;
        $this->estudiantes = $estudiantes;
        $this->area = $area;
        $this->sede = $sede;
        $this->asignatura = $asignatura;
        $this->asistencia = $asistencia;
        $this->estado = $estado;
        $this->periodo = $periodo;
        $this->curso = $curso;
    }

    public function getId() {
        return $this->id;
    }

    public function getHorario() {
        return $this->horario;
    }

    public function getProfesor() {
        return $this->profesor;
    }

    public function getEstudiantes() {
        return $this->estudiantes;
    }

    public function getArea() {
        return $this->area;
    }

    public function getSede() {
        return $this->sede;
    }

    public function getCurso() {
        return $this->curso;
    }

    public function getAsignatura() {
        return $this->asignatura;
    }

    public function getAsistencia() {
        return $this->asistencia;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function getPeriodo() {
        return $this->periodo;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setHorario($horario) {
        $this->horario = $horario;
    }

    public function setProfesor($profesor) {
        $this->profesor = $profesor;
    }

    public function setEstudiantes($estudiantes) {
        $this->estudiantes = $estudiantes;
    }

    public function setArea($area) {
        $this->area = $area;
    }

    public function setSede($sede) {
        $this->sede = $sede;
    }

    public function setCurso($curso) {
        $this->curso = $curso;
    }

    public function setAsignatura($asignatura) {
        $this->asignatura = $asignatura;
    }

    public function setAsistencia($asistencia) {
        $this->asistencia = $asistencia;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }

    public function setPeriodo($periodo) {
        $this->periodo = $periodo;
    }

}

?>
