<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Lectivo
 *
 * @author leon
 */
class Lectivo {

    private $id;
    private $director;
    private $estado;
    private $semanas;
    private $fechaInicio;
    private $fechaFin;

    function __construct($id, $director, $estado, $semanas, $fechaInicio, $fechaFin) {
        $this->id = $id;
        $this->director = $director;
        $this->estado = $estado;
        $this->semanas = $semanas;
        $this->fechaInicio = $fechaInicio;
        $this->fechaFin = $fechaFin;
    }

    public function getId() {
        return $this->id;
    }

    public function getDirector() {
        return $this->director;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function getSemanas() {
        return $this->semanas;
    }

    public function getFechaInicio() {
        return $this->fechaInicio;
    }

    public function getFechaFin() {
        return $this->fechaFin;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setDirector($director) {
        $this->director = $director;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }

    public function setSemanas($semanas) {
        $this->semanas = $semanas;
    }

    public function setFechaInicio($fechaInicio) {
        $this->fechaInicio = $fechaInicio;
    }

    public function setFechaFin($fechaFin) {
        $this->fechaFin = $fechaFin;
    }

}
