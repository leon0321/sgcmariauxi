<?php

include_once 'Usuario.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Profesor
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class Profesor extends Usuario {

    private $nivel;
    private $ptipo;

    public function __construct($id, $nombre, $apellido, $telefono, $correo, $login, $password, $tipo, $nivel, $ptipo)
    {
        parent::__construct($id, $nombre, $apellido, $telefono, $correo, $login, $password, $tipo);
        $this->nivel = $nivel;
        $this->ptipo = $ptipo;
    }

    public function getPtipo()
    {
        return $this->ptipo;
    }

    public function setPtipo($ptipo)
    {
        $this->ptipo = $ptipo;
    }

    public function getNivel()
    {
        return $this->nivel;
    }

}

?>
