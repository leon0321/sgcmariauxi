<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Curso
 *
 * @author leon
 */
class Curso {

    private $id;
    private $director;
    private $grado;
    private $fecha;
    private $letra;
    private $nombre;
    private $estado;

    function __construct($id, $director, $grado, $fecha, $letra, $nombre, $estado) {
        $this->director = $director;
        $this->grado = $grado;
        $this->fecha = $fecha;
        $this->letra = $letra;
        $this->nombre = $nombre;
        $this->id = $id;
        $this->estado = $estado;
    }

    public function getDirector() {
        return $this->director;
    }

    public function getGrado() {
        return $this->grado;
    }

    public function getNivel() {
        
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function getLetra() {
        return $this->letra;
    }

    public function getNombre() {
        if ($this->grado == 0) {
            return "Precolar - " . $this->letra . " - " . $this->fecha;
        }
        return $this->nombre;
    }

    public function setDirector($director) {
        $this->director = $director;
    }

    public function setGrado($grado) {
        $this->grado = $grado;
    }

    public function setNivel($nivel) {
        
    }

    public function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    public function setLetra($letra) {
        $this->letra = $letra;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function getId() {
        return $this->id;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }

}
