<?php

include_once 'Persona.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuario
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class Usuario extends Persona {

    protected $login;
    protected $password;
    protected $tipo;

    const ESTUDIANTE = "1";
    const PROFESOR = "2";
    const SECRETARIO = "3";
    const COORDINADOR = "4";
    const ADMIN = "5";
    const RECTOR = "6";

    public function __construct($id, $nombre, $apellido, $telefono, $correo, $login, $password, $tipo, $sexo = 2) {
        parent::__construct($id, $nombre, $apellido, $telefono, $correo, $sexo);
        $this->login = $login;
        $this->password = $password;
        $this->tipo = $tipo;
    }

    public function getLogin() {
        return $this->login;
    }

    public function setLogin($login) {
        $this->login = $login;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }

}

?>
