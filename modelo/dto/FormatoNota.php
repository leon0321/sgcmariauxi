<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FormatoNota
 *
 * @author leon
 */
class FormatoNota {

    private $porcent = array();
    private $ano = "";
    private $nPeridos;

    function __construct($porcent, $ano, $nPeridos) {
        $this->porcent = $porcent;
        $this->ano = $ano;
        $this->nPeridos = $nPeridos;
    }

    public function getPorcent() {
        return $this->porcent;
    }

    public function getAno() {
        return $this->ano;
    }

    public function getNPeridos() {
        return $this->nPeridos;
    }

    public function setPorcent($porcent) {
        $this->porcent = $porcent;
    }

    public function setAno($ano) {
        $this->ano = $ano;
    }

    public function setNPeridos($nPeridos) {
        $this->nPeridos = $nPeridos;
    }

}
