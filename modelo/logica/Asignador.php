<?php

include_once '../dto/Curso.php';
include_once '../persistencia/AsignaturaDAO.php';
include_once '../persistencia/CursoDAO.php';
include_once '../persistencia/EstudianteDAO.php';
include_once '../persistencia/ClaseDAO.php';
include_once '../persistencia/MatriculaDAO.php';
include_once '../persistencia/FormatoNotaDAO.php';
include_once 'Calendario.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Asignador
 *
 * @author leon
 */
class Asignador {

    public static function relacionarClaseToCurso($idcurso, $idclase) {
        $idestudiantes = EstudianteDAO::getIdsEstudiantesByCurso($idcurso);
        $error = MatriculaDAO::matricularClase(array($idclase), $idestudiantes);
    }

    public static function crearClaseByCurso($idcurso, $sw = 0) {
        $curso = CursoDAO::getCursoByid($idcurso);
        $asig = AsignaturaDAO::getAsignaturaByGrado($curso->getGrado());
        $clases = ClaseDAO::getClasesByCursoId($idcurso);
        $asig = Asignador::filtrar($clases, $asig);
        $class = array();
        if (count($asig) > 0) {
            for ($i = 0; $i < count($asig); $i++) {
                $class[$i] = new Clase(NULL, NULL, $curso->getDirector()->getId(), NULL, $asig[$i]->getIdArea(), "1", $asig[$i]->getId(), NULL, "0", $curso->getFecha(), $curso->getId());
            }
            return ClaseDAO::getSQLinsertarClases($class, $sw);
        }
        return "";
    }

    public static function asignarEstudiantestoCursoByGrado($grado = "2", $year = "2013") {
        $cursos = CursoDAO::getCursosByGradoId($grado, $year);
        $estus = EstudianteDAO::getEstudiantesByGrado($grado);
        $sqlMa = MatriculaDAO::getSQLMatricularEstudiantesToCurso($cursos, $estus);
        $fn = FormatoNotaDAO::getFormatoNotaByYear($year);
        BD::open();
        BD::sentenceSQL($sqlMa);
        echo "<br>" . BD::error();

        $sqlClases = "INSERT INTO `" . BD::$dataBase . "`.`clase` (`idclase`, `sede_id_c`, `profesor_id_c`, `area_id_c`, `asignatura_id_c`, `c_estado`, `c_periodo`, `curso_id_c`) VALUES";
        $sqlNota = "INSERT INTO `" . BD::$dataBase . "`.`nota_periodo` " . "(`clase_id`, `estudiante_id`, `m_periodo_id`) VALUES";

        for ($i = 0; $i < count($cursos); $i++) {
            $sqlClases = $sqlClases . Asignador::crearClaseByCurso($cursos[$i]->getId());
        }

        $sqlClases = substr_replace($sqlClases, ";", -1);
        BD::sentenceSQL($sqlClases);
        echo "<br>" . BD::error();


        for ($i = 0; $i < count($cursos); $i++) {
            $sqlNota = $sqlNota . MatriculaDAO::getSQLmatricularClasesEstudiantes(ClaseDAO::getIdClaseByCursoId($cursos[$i]->getId()), EstudianteDAO::getIdsEstudiantesByCurso($cursos[$i]->getId()), $year, $fn->getNPeridos());
        }

        $sqlNota = substr_replace($sqlNota, ";", -1);
        BD::sentenceSQL($sqlNota);
        echo "<br>" . BD::error();
    }

    public static function cambiarEstudianteCurso($idcurso1, $idcurso2) {
        $cursoA = CursoDAO::getCursoByid($idcurso1);
        $cursoB = CursoDAO::getCursoByid($idcurso2);
        $fn = FormatoNotaDAO::getFormatoNotaByYear($cursoB->getFecha());
        $estus = EstudianteDAO::getEstudiantesByCurso($cursoA->getId(), 1);
        BD::open();
        $sqlMa = MatriculaDAO::getSQLMatricularEstudiantesToCurso(array($cursoB), $estus);
        BD::sentenceSQL($sqlMa);
        echo "<br>$sqlMa<br>" . BD::error();
        $sqlClases = Asignador::crearClaseByCurso($cursoB->getId(), 1);
        BD::sentenceSQL($sqlClases);
        echo "<br>$sqlClases<br>" . BD::error();
        $sqlNota = MatriculaDAO::getSQLmatricularClasesEstudiantes(ClaseDAO::getIdClaseByCursoId($cursoB->getId()), EstudianteDAO::getIdsEstudiantesByCurso($cursoB->getId()), $cursoB->getFecha(), $fn->getNPeridos(), 1);
        BD::sentenceSQL($sqlNota);
        echo "<br>$sqlNota<br>" . BD::error();
    }

    public static function asignarEstudianteToCurso($idestudiante, $idcurso) {
        $est = EstudianteDAO::get()->getEstudianteByid($idestudiante);
        $curso = CursoDAO::getCursoByid($idcurso);
        $fn = FormatoNotaDAO::getFormatoNotaByYear($curso->getFecha());
        BD::open();
        $sqlMa = MatriculaDAO::getSQLMatricularEstudiantesToCurso(array($curso), array($est));
        BD::sentenceSQL($sqlMa);
        echo "<br>$sqlMa<br>" . BD::error();
        $sqlClases = Asignador::crearClaseByCurso($curso->getId(), 1);
        BD::sentenceSQL($sqlClases);
        echo "<br>$sqlClases<br>" . BD::error();
        $sqlNota = MatriculaDAO::getSQLmatricularClasesEstudiantes(ClaseDAO::getIdClaseByCursoId($curso->getId()), array($est->getId()), $curso->getFecha(), $fn->getNPeridos(), 1);
        BD::sentenceSQL($sqlNota);
        echo "<br>$sqlNota<br>" . BD::error();
    }

    public static function filtrar($clases, $asignaruras) {
        $asig = array();
        $k = 0;
        $is = 0;
        for ($i = 0; $i < count($asignaruras); $i++) {
            for ($j = 0; $j < count($clases); $j++) {
                if ($clases[$j]->getAsignatura() == $asignaruras[$i]->getId()) {
                    $is = 1;
                }
            }
            if ($is == 0) {
                $asig[$k] = $asignaruras[$i];
                $k++;
            }
        }
        return $asig;
    }

}

Asignador::asignarEstudianteToCurso("aaaaaa1", "0A2014");
