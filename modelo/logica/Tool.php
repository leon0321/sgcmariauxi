<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tool
 *
 * @author leon
 */
class Tool {

    public static function calcularNotaAno($notas, $por, $grado = 1) {
        if ($grado == 0) {
            return Tool::calcularNotaAnoPrescolar($notas, $por);
        } else {
            return Tool::calcularNotaAnoNormal($notas, $por);
        }
    }

    public static function calcularNotaAnoNormal($notas, $por) {
        $acum = 0;
        $faltas = 0;
        for ($i = 0; $i < count($por); $i++) {
            $acum += $notas[$i]->getNotaFinal() * $por[$i];
            $faltas += $notas[$i]->getFaltas();
        }
        $result = array();
        $result["nota"] = $acum;
        $result["faltas"] = $faltas;
        return $result;
    }

    public static function calcularNotaAnoPrescolar($notas, $por) {
        $acum = 0;
        $faltas = 0;
        for ($i = 0; $i < count($por); $i++) {
            $faltas += $notas[$i]->getFaltas();
        }
        $result = array();
        $result["nota"] = $notas[count($por) - 1]->getNotaFinal();
        $result["faltas"] = $faltas;
        return $result;
    }

    public static function formatNota($nota, $grado = 1) {
        if($grado == 0){
            return Tool::getNameNota($nota);
        }  else {
            return number_format($nota, 1);
        }
    }

    public static function getNameNota($nota) {
       if ($nota >= 0 && $nota < 2.9) {
            return "B";
        } elseif ($nota >= 3 && $nota < 3.9) {
            return "BD";
        } elseif ($nota >= 4 && $nota < 4.7) {
            return "DA";
        } elseif ($nota >= 4.8 && $nota <= 5) {
            return "DS";
        }
    }

}
