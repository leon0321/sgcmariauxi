<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Calendario
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class Calendario {

    private static $dias = array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado');
    private static $meses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio','Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

    public static function get($fechas_inicio, $fecha_final) {
        date_default_timezone_set("GMT");
        $times = array();

        $final_time = strtotime($fecha_final);

        for ($i = 0; $i < count($fechas_inicio); $i++) {
            $times[$i] = strtotime($fechas_inicio[$i]);
        }

        $dia_time = 24 * 60 * 60;
        $semana_time = $dia_time * 7;

        $clase = 0;
        $sw = 1;
        $dates = array();
        for ($i = 0; $sw == 1; $i++) {
            for ($j = 0; $j < count($times); $j++) {
                if ($times[$j] <= $final_time) {
                    $dates[$clase] = date("y-m-d H:i:s", $times[$j]) . "";
                    $clase += 1;
                    $times[$j] += $semana_time;
                    $sw = 1;
                } else {
                    $sw = 0;
                }
            }
        }
        return $dates;
    }

    public static function getFechaActual() {
        date_default_timezone_set("America/Bogota");
        return date("Y-m-d H:i:s");
    }

    public static function getYear() {
        date_default_timezone_set("America/Bogota");
        return date("Y");
    }

    public static function format($format, $fecha) {
        $time = strtotime($fecha);
        return date($format, $time);
    }

    public static function formatDataBase($fecha) {
        return Calendario::format("Y-m-d H:i:s", $fecha);
    }

    public static function isVencida($horas, $fecha) {
        $time = strtotime($fecha) + $horas * 50 * 60;
        $timeActual = strtotime(Calendario::getFechaActual());
        return (($timeActual - $time) >= 86400) ? TRUE : FALSE;
    }

    public static function isActiva($horas, $fecha) {
        $time = strtotime($fecha) + $horas * 50 * 60;
        $timeActual = strtotime(Calendario::getFechaActual());
        return (($timeActual - $time) < 86400) ? TRUE : FALSE;
    }

    public static function diferent($horas, $fecha) {
        $time = strtotime($fecha) + $horas * 50 * 60;
        $timeActual = strtotime(Calendario::getFechaActual());
        return ($timeActual - $time);
    }

    public static function sumarT($format, $fecha, $time) {
        $time2 = strtotime($fecha) + $time;
        return date($format, $time2);
    }

    public static function isDentro($fechaI, $fechaF) {
        $fechaA = strtotime(Calendario::getFechaActual());
        if ($fechaA >= $fechaI && $fechaA <= $fechaF) {
            return true;
        }
        return false;
    }

    public static function printHorasClase($fecha, $horas) {
        echo Calendario::format("H:i", $fecha) . " - " . Calendario::sumarT("H:i", $fecha, ($horas * 60 * 50));
    }

    public static function formatToUser($fecha) {
        if ($fecha == "") {
            return "";
        }
        $t = strtotime($fecha);
        return Calendario::$dias[date('w', $t)] . " " . date('j', $t) . " " . Calendario::$meses[date('n', $t) - 1] . " " . date('Y', $t);
    }

}

?>
