<?php
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/JefeProgramaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ProgramaDAO.php';
include_once 'Clogin.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CEstudiante
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class CJefe {

    public static function printJefes() {
        if (!Clogin::isSesion()) {
            return;
        }
        $us = Clogin::getUser();
        if ($us->getTipo() != Usuario::ADMIN) {
            return;
        }
        $jefes = CoordinadorDAO::getCoordinadores();
        ?>
        <div class="tablaScroll">
            <table id="tclasesAdmin"  class="tablaAdmin">
                <caption><h2>Jefes de Programa</h2></caption> 
                 
                <thead>
                    <tr><th>Apellidos</th><th>Nombre</th><th>Codigo</th><th>Telefono</th><th>Correo </th><th>Username</th><th>Programa</th><th class="opciones">Opciones</th></tr>
                </thead>
                <tbody>
               
                    <?php
                    for ($i = 0; $i < count($jefes); $i++) {
                        ?>
                        <tr> 
                            <td><?php echo $jefes[$i]->getApellido(); ?></td>
                            <td><?php echo $jefes[$i]->getNombre(); ?></td>
                            <td><?php echo $jefes[$i]->getId(); ?></td>
                            <td><?php echo $jefes[$i]->getTelefono(); ?></td>
                            <td><?php echo $jefes[$i]->getCorreo(); ?></td>
                            <td><?php echo $jefes[$i]->getLogin(); ?></td>
                            <td><?php echo AreaDAO::get()->getAreaByid($jefes[$i]->getArea())->getNombre(); ?></td>
                            <td><input type="button" name="eliminar" onclick="elimJefe('<?php echo $jefes[$i]->getId(); ?>')" value="eliminar"/></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <br>
        </div>
        <?php
    }

    public static function printFormJefe() {
        ?>
        <div id="formuJefePrograma" class="formulario">
            <h2>Registro de Jefe de programa</h2>
            <form method="post" id="formularioJefePrograma" >
                <div>
                    <h3>Programa</h3>
                    <select name="programa">
                        <?php
                        $programas = AreaDAO::getAreas();
                        for ($i = 0; $i < count($programas); $i++) {
                            ?><option value="<?php echo $programas[$i]->getCodigo(); ?>"><?php echo $programas[$i]->getNombre() ?></option><?php
                        }
                        ?>
                    </select>
                </div>
                <div>
                    <h3>Nombre</h3>
                    <input name="nombre"type="text" />
                </div>
                <div>
                    <h3>Apellido</h3>
                    <input name="apellido" type="text" />
                </div>
                <div>
                    <h3>Codigo</h3>
                    <input name="codigo" type="text" />
                </div>
                <div>
                    <h3>Telefono</h3>
                    <input name="telefono" type="tel" />
                </div>
                <div>
                    <h3>Correo</h3>
                    <input name="correo" type="email" />
                </div>
                <p class="submit">
                    <input type="button" name="cancel" value="Cancelar" onclick="redirec('http://localhost/SIG/vista/jefe.php')">
                    <input id="jefe_guardar" type="button" name="commit" value="Guardar">
                </p>
            </form>
            <div id="respuesta">                
            </div>
        </div>
        <?php
    }

    public static function guadarJefe() {
        $jefe = new Coordinador($_POST["codigo"], $_POST["nombre"], $_POST["apellido"], $_POST["telefono"], $_POST["correo"], $_POST["correo"], $_POST["codigo"], Usuario::PROFESOR,$_POST["programa"]);
        $msn = CoordinadorDAO::get()->insertar($jefe);
        if ($msn != "") {
            echo "<p>" . $msn . "</p>";
            return;
        }
        echo "<p>ok</p>";
    }

    public static function eliminarJefe() {
        $id = $_POST["id"];
        echo "<h1>hola" . CoordinadorDAO::get()->eliminar($id) . "<h1>";
    }

    public static function requests() {
        if (empty($_GET["metodo"])) {
            return;
        }

        $r = $_GET["metodo"];

        if ($r == "printFormJefe") {
            CJefe::printFormJefe();
        } else if ($r == "guadarJefe") {
            CJefe::guadarJefe();
        } else if ($r == "eliminarJefe") {
            CJefe::eliminarJefe();
        }
    }

}

CJefe::requests();
?>
