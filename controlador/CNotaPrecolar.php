<?php
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/NotaDAO.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CNotaPrecolar
 *
 * @author leon
 */
class CNotaPrecolar {

    public static function printNotasPeriodoNoEdit($idclase, $periodo) {
        $text = $_POST["text"];
        $n = NotaDAO::getNotasClasePerido($idclase, $periodo);
        ?>
        <h1 class="page-header">Periodo: <?php echo $periodo; ?>  Curso : <?php echo $text ?></h1>          
        <div class="table-responsive">
            <table class="table" id="tnotaperiodo">
                <thead class="theadnota">
                    <tr>
                <a class="aclass"  href="clase=<?php echo $idclase; ?>&periodo=<?php echo $periodo; ?>" style="display: none"></a>
                <th>Apellido y Nombre</th>
                <th title="Codigo">Codigo</th>
                <th title="Faltas">Faltas</th>
                <th title="NOTA FINAL PERIODO">Nota final</th>
                </tr>
                </thead>
                <tbody id="tbodyNotas">
                    <?php
                    for ($i = 0; $i < count($n); $i++) {
                        ?>
                        <tr class="<?php echo ($i % 2 == 0) ? "" : "tr_impar"; ?>">
                            <td><?php echo $n[$i]->getIdEstu()->getApellido() . " " . $n[$i]->getIdEstu()->getNombre() ?></td>
                            <td class="notaFinal" >
                                <?php echo $n[$i]->getIdEstu()->getId(); ?>
                            </td>
                            <td class="notaFinal" >
                                <?php echo $n[$i]->getFaltas(); ?>
                            </td>
                            <td class="notaFinal" >
                                <?php echo CNotaPrecolar::getNameNota($n[$i]->getNotaFinal()); ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function printNotasPeriodoEdit($idclase, $periodo) {
        $text = $_POST["text"];
        $n = NotaDAO::getNotasClasePerido($idclase, $periodo);
        //Calendario::isDentro($fechaI, $fechaF);
        ?>
        <h1 class="page-header">Periodo: <?php echo $periodo; ?>  Curso : <?php echo $text ?></h1>          
        <div class="table-responsive">
            <table class="table" id="tnotaperiodo">
                <thead class="theadnota">
                    <tr>
                <a class="aclass"  href="clase=<?php echo $idclase; ?>&periodo=<?php echo $periodo; ?>" style="display: none"></a>
                <th>Apellido y Nombre</th>
                <th title="Codigo" >Codigo</th>
                <th title="Faltas en el periodo" >Faltas <br><a id="faltas">editar</a></th>
                <th title="NOTA FINAL PERIODO" >Nota Final<br><a id="notaFinal">guardar</a></th>
                </tr>
                </thead>
                <tbody id="tbodyNotas">
                    <?php
                    for ($i = 0; $i < count($n); $i++) {
                        $idestu = $n[$i]->getIdEstu()->getId();
                        ?>
                        <tr id="tr<?php echo $idestu ?>" class="<?php echo ($i % 2 == 0) ? "" : "tr_impar"; ?>">
                            <td><?php echo $n[$i]->getIdEstu()->getApellido() . " " . $n[$i]->getIdEstu()->getNombre(); ?></td>
                            <td><?php echo $n[$i]->getIdEstu()->getId(); ?></td>
                            <td class="faltas"  ><?php echo $n[$i]->getFaltas() ?></td>
                            <td class="notaFinal" >
                                <select>
                                    <?php CNotaPrecolar::printOptionNota(($n[$i]->getNotaFinal())); ?>
                                </select>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function getNameNota($nota) {
       if ($nota >= 0 && $nota < 2.9) {
            return "B";
        } elseif ($nota >= 3 && $nota < 3.9) {
            return "BD";
        } elseif ($nota >= 4 && $nota < 4.7) {
            return "DA";
        } elseif ($nota >= 4.8 && $nota <= 5) {
            return "DS";
        }
    }

    public static function printOptionNota($nota = -1) {
        ?>
        <option value="0" <?php echo ($nota == 0) ? "selected" : ""; ?> >Malo</option>
        <option value="1" <?php echo ($nota == 1) ? "selected" : ""; ?> >Medio Malo</option>
        <option value="2" <?php echo ($nota == 2) ? "selected" : ""; ?> >Regular</option>
        <option value="3" <?php echo ($nota == 3) ? "selected" : ""; ?> >Aceptable</option>
        <option value="4" <?php echo ($nota == 4) ? "selected" : ""; ?> >Bueno</option>
        <option value="5" <?php echo ($nota == 5) ? "selected" : ""; ?> >Exelente</option>
        <?php
    }

    public static function guardarNotasColumna($jsonNotas, $idclase, $periodo, $column) {
        $r = json_decode($jsonNotas, true);
        $campo = "";

        if ($column == "faltas") {
            $campo = "falta";
        } elseif ($column == "notaFinal") {
            $campo = "nota_final";
        }

        for ($i = 0; $i < count($r["notas"]); $i++) {
            $ne = $r["notas"][$i];
            echo "prescolar";
            $error = NotaDAO::actualizarCampos("`$campo` = '" . $ne["nota"] . "'", $ne["estu"], $idclase, $periodo);
            if ($error != "") {
                echo '<p id="bad" >' . $error . '</p>';
                return;
            }
        }
        echo "<p id='ok'>Sus Cambios han sido guardados</p>";
    }

}
