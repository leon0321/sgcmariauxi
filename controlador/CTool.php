<?php
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ValoracionDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ToolDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/CursoDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ftp/FTP.php';
include_once 'Clogin.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CTool
 *
 * @author leon
 */
class CTool {

    public static function printOptionValoracion($year, $orden = -1) {
        $vals = ValoracionDAO::getValoracionesByYear($year);
        echo '<select class="btn btn-primary selectValoracion noPrint" name="valoracion" id="select' . $year . '">';
        for ($i = 0; $i < count($vals); $i++) {
            $selected = ($orden == $vals[$i]->getOrden()) ? "selected" : "";
            echo '<option ' . $selected . ' value="' . $vals[$i]->getOrden() . '">' . $vals[$i]->getTitulo() . '  (' . number_format($vals[$i]->getInicial(), 1) . ', ' . number_format($vals[$i]->getFinal(), 1) . ']</option>';
        }
        echo "</select>";
    }

    public static function printGradosSelect($grado = "0") {
        $grados = ToolDAO::getGrados();
        ?>
        <option value = "all"  <?php echo ($grado == "all") ? "selected" : ''; ?> >Todos</option>
        <?php
        for ($i = 0; $i < count($grados); $i++) {
            ?>
            <option  value="<?php echo $grados[$i]["idgrado"]; ?>"  <?php echo ($grado == $grados[$i]["idgrado"]) ? "selected" : ''; ?>><?php echo $grados[$i]["g_nombre"] ?></option>
            <?php
        }
    }

    public static function printGradosLi($grado = -1) {
        $grados = ToolDAO::getGrados();
        for ($i = 0; $i < count($grados); $i++) {
            ?>
            <li  id="<?php echo $grados[$i]["idgrado"]; ?>" class="grado<?php echo ($grado == $grados[$i]["idgrado"]) ? " active" : ''; ?>" ><a> <?php echo $grados[$i]["g_nombre"] ?> </a></li>
            <?php
        }
    }

    public static function printSelectCursos($year) {
        $cursos = CursoDAO::getCursosByYear($year);
        echo '<option></option>';
        for ($i = 0; $i < count($cursos); $i++) {
            ?>
            <option id="<?php echo $cursos[$i]->getGrado(); ?>" value="<?php echo $cursos[$i]->getId(); ?>"><?php echo $cursos[$i]->getNombre(); ?></option>
            <?php
        }
    }

    public static function changeImage($source_file, $iduser) {
        $fileParts = pathinfo($_FILES['Filedata']['name']);
        $destination_file = FTP::$photoUserFolder . $iduser . "." . $fileParts['extension'];
        FTP::conect();
        $error = FTP::upload($source_file, $destination_file);
        if ($error == "") {
            return "<div id='ok'>ok</div>";
        } else {
            return "<div id='bad'>$error</div>";
        }
    }

    public static function showMenuAdmin() {
        ?>
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="Paneladministrativo.php">SGC MARIA AUXILIADORA</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a  class="dropdown-toggle" data-toggle="dropdown">
                                Configuraciones<b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="Configurar.php">Plazos</a></li>
                                <li><a href="Configurarporcentajes.php">Porcentajes</a></li>   
                                <li><a href="valoracion.php">Valoracion</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a  class="dropdown-toggle" data-toggle="dropdown">
                                Calificaion<b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="Actualizarnotas.php">Actualizar Notas</a></li>
                                <li><a href="InsertarObservacionAdmin.php">Actualizar Observaciones</a></li>   
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a  class="dropdown-toggle" data-toggle="dropdown">
                                Agregar y editar<b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="estudiante.php">Estudiantes</a></li>
                                <li><a href="profesor.php">Profesores</a></li>   
                                <li><a href="curso.php">Cursos</a></li>
                                <li><a href="asignatura.php">Asignaturas</a></li>
                                <li><a href="clase.php">Clases</a></li>   
                                <li><a href="lectivo.php">Años lectivos</a></li>
                                <li><a href="coordinador.php">Coordinadores</a></li>
                                <li><a href="rector.php">Rectores</a></li>   
                                <li><a href="secretario.php">Secretari@s</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a  class="dropdown-toggle" data-toggle="dropdown">
                                Promover<b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="Promocion.php">Promocion</a></li>
                                <li><a href="Promocionanticipada.php">Promocion anticipada</a></li>   
                                <li><a href="Matricula.php">Matricula</a></li>
                            </ul>
                        </li>
                        <li><a href="RecuperarContrasena.php">Seguridad</a></li>
                        <li><a  href="../../controlador/Clogin.php?metodo=logout">(cerrar sesion)</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <?php
    }

    public static function showOptiosModels($active="estudiante.php"){
        ?>
            <ul class="nav nav-sidebar">                        
                <li <?php echo ($active == "estudiante.php")?"class='active'":""; ?> ><a href="estudiante.php">Estudiantes</a></li>
                <li <?php echo ($active == "profesor.php")?"class='active'":""; ?> ><a href="profesor.php">Profesores</a></li>
                <li <?php echo ($active == "curso.php")?"class='active'":""; ?> ><a href="curso.php">Cursos</a></li>
                <li <?php echo ($active == "asignatura.php")?"class='active'":""; ?> ><a href="asignatura.php">Asiganaturas</a></li>
                <li <?php echo ($active == "clase.php")?"class='active'":""; ?> ><a href="clase.php">Clases</a></li>
                <li <?php echo ($active == "lectivo.php")?"class='active'":""; ?> ><a href="lectivo.php">Años lectivos</a></li>
                <li <?php echo ($active == "coordinador.php")?"class='active'":""; ?> ><a href="coordinador.php">Coordinadores</a></li>
                <li <?php echo ($active == "rector.php")?"class='active'":""; ?> ><a href="rector.php">Rectores</a></li>
                <li <?php echo ($active == "secretario.php")?"class='active'":""; ?> ><a href="secretario.php">Secretario</a></li>
            </ul>
        <?php
    }

    public static function solicitudes() {
        if (empty($_GET["metodo"])) {
            return;
        }

        $r = $_GET["metodo"];

        if ($r == "printSelectCursos") {
            CTool::printSelectCursos($_GET["year"]);
        } elseif ($r == "changeImage") {
            @session_start();
            if (Clogin::isSesion()) {
                $us = Clogin::getUser();
                echo CTool::changeImage($_FILES["Filedata"]["tmp_name"], $us->getId());
            }
        }
    }

}

CTool::solicitudes();
