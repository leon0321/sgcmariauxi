<?php
include_once realpath(dirname(__FILE__)) . '/../modelo/dto/FormatoNota.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/FormatoNotaDAO.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CPorcentaje
 *
 * @author leon
 */
class CPorcentaje {

    public static function prinFormPorcentaje(FormatoNota $fn, $periodo) {
        $save = false;
        if ($fn == null) {
            $fn = new FormatoNota(array(0, 0, 0, 0), "", 4);
            $save = true;
        }
        ?>
        <h1 class="page-header" style=" margin-left: 20px;">Periodo: <?php echo $periodo; ?>º </h1>
        <form id="formularioPorcentaje<?php echo $periodo; ?>" class="formPorcentaje" role="form">
            <div style="display: none">
                <input type="text" name="metodo" value="<?php echo ($save) ? "savePorcentaje" : "updatePorcentaje"; ?>"/>
                <input name="year" value="<?php echo $fn->getAno(); ?>"/>
                <input name="periodo" value="<?php echo $periodo; ?>"/>
            </div>
            <div>
                <p>PORCENTAJE <?php echo $periodo; ?>º CORTE:  </p>
                <input max="100" min="0" type="number" class="form-control" name="porcentaje" placeholder="VALOR" value="<?php echo $fn->getPorcent()[$periodo - 1] * 100; ?>">
                <div class="respuesta"></div>
            </div>
            <HR>
            <button id="<?php echo $periodo; ?>" type="submit" class="btn btn-primary" type="Enviar"style=" margin-left: 100px;">Guardar</button>
        </form>
        <?php
    }

    public static function updatePorcentaje($value, $year, $periodo) {
        $value = $value / 100;
        $fn = FormatoNotaDAO::getFormatoNotaById($year);
        if ($fn->getNPeridos() < $periodo) {
            return "<p id='bad'>El '$periodo' periodo  no esta definido</p>";
        }
        $error = FormatoNotaDAO::updateProcentaje($value, $year, $periodo);
        if ($error != "") {
            return "<p id='bad'>Ocurrio el error: ($error)</p>";
        }

        $total = FormatoNotaDAO::sumarPorcentaje($year);
        if ($total > 1) {
            return "<p id='bad'>la suma de los porcentajes(" . ($total * 100) . ") es mayor al 100%</p>";
        }
        if ($total < 1) {
            return "<p id='bad'>la suma de los porcentajes$(" . ($total * 100) . ") es menor al 100%</p>";
        }
        if (BD::affectedRows() == 0) {
            return "<p id='bad'>No se realizo cambios</p>";
        }
        return "<p='ok'>OK</p>";
    }

    public static function solicitudes() {
        if (empty($_GET["metodo"])) {
            return;
        }
        $r = $_GET["metodo"];
        if ($r == "updatePorcentaje") {
            echo CPorcentaje::updatePorcentaje($_POST["porcentaje"], $_POST["year"], $_POST["periodo"]);
        }
    }

}

CPorcentaje::solicitudes();
