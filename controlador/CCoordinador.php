<?php
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/CoordinadorDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/dto/Coordinador.php';
include_once 'Clogin.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CEstudiante
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class CCoordinador {

    public static function printCoordinadores() {
        $coords = CoordinadorDAO::getCoordinadores();
        ?>
        <table class="table" id="tablaProfesores">
            <thead>
                <tr>
                    <th>Nombre y Apellido</th>
                    <th>Codigo</th>
                    <th>Telefono</th>
                    <th>Correo</th>
                </tr>
            </thead>
            <tbody>
                <?php
                for ($i = 0; $i < count($coords); $i++) {
                    ?>
                    <tr id="trcoord<?php echo $coords[$i]->getId(); ?>" class="<?php echo ($i % 2 == 0) ? "" : "tr_impar"; ?>"> 
                        <td><a class="edit" id="<?php echo $coords[$i]->getId(); ?>" ><?php echo $coords[$i]->getApellido() . " " . $coords[$i]->getNombre(); ?></a></td>
                        <td><?php echo $coords[$i]->getId(); ?></td>
                        <td><?php echo $coords[$i]->getTelefono(); ?></td>
                        <td><?php echo $coords[$i]->getCorreo(); ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php
    }

    public static function printFormCoordinador($id = "") {
        $coord = new Coordinador("", "", "", "", "", "", "", "", "");
        if ($id != "") {
            $coord = CoordinadorDAO::getCoordinadorByid($id);
        }
        ?>
        <form method="post" id="formularioCoordinador" >
            <div>
                <label>Nombre</label>
                <input name="nombre"type="text" value="<?php echo $coord->getNombre(); ?>"/>
            </div>
            <div>
                <label>Apellido</label>
                <input name="apellido" type="text" value="<?php echo $coord->getApellido(); ?>"/>
            </div>
            <div>
                <label>Codigo</label>
                <input name="codigo" type="text" value="<?php echo $coord->getId(); ?>" />
                <input name="idactual" type="text" value="<?php echo $coord->getId(); ?>" style="display: none"/>
            </div>
            <div>
                <label>Telefono</label>
                <input name="telefono" type="tel" value="<?php echo $coord->getTelefono(); ?>"/>
            </div>
            <div>
                <label>Correo</label>
                <input name="correo" type="email" value="<?php echo $coord->getCorreo() ?>"/>
            </div>
        </form>
        <?php
    }

    public static function aditarCoordinador($idactual, $id, $nombre, $apellido, $telefono, $correo) {
        $coord = new Coordinador($id, $nombre, $apellido, $telefono, $correo, "", "", "", "");
        $msn = CoordinadorDAO::actualizarCoord($coord, $idactual);
        if ($msn != "") {
            echo '<p id="error" >' . $msn . '</p>';
            return;
        }
        ?>
        <td><a onclick="editProfe('<?php echo $coord->getId(); ?>')"><?php echo $coord->getApellido() . " " . $coord->getNombre(); ?></a></td>
        <td><?php echo $coord->getId(); ?></td>
        <td><?php echo $coord->getTelefono(); ?></td>
        <td><?php echo $coord->getCorreo(); ?></td>
        <?php
    }

    public static function guadarCoordinador() {
        $coord = new Coordinador($_POST["codigo"], $_POST["nombre"], $_POST["apellido"], $_POST["telefono"], $_POST["correo"], $_POST["codigo"], $_POST["codigo"], Usuario::COORDINADOR, "");
        $msn = CoordinadorDAO::insertar($coord);
        if ($msn != "") {
            echo "<p>" . $msn . "</p>";
            return;
        }
        echo "<p id='ok'>ok</p>";
    }

    public static function solicitudes() {
        if (empty($_GET["metodo"])) {
            return;
        }
        
        $r = $_GET["metodo"];
        if ($r == "printFormCoordinador") {
            $id = (empty($_GET["profe"])) ? "" : $_GET["profe"];
            CCoordinador::printFormCoordinador($id);
        } else if ($r == "guadarCoordinador") {
            CCoordinador::guadarCoordinador();
        } elseif ($r == "aditarCoordinador") {
            CCoordinador::aditarCoordinador($_POST["idactual"], $_POST["codigo"], $_POST["nombre"], $_POST["apellido"], $_POST["telefono"], $_POST["correo"], "");
        }
    }

}

CCoordinador::solicitudes();
