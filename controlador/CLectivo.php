<?php
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/LectivoDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/RectorDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/logica/Calendario.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CLectivo
 *
 * @author leon
 */
class CLectivo {

    public static function printLectivos() {
        $lectivos = LectivoDAO::getLectivos();
        ?>
        <table class="table" id="tablaProfesores">
            <thead>
                <tr>
                    <th>Año</th>
                    <th>Rector</th>
                    <th>Nº Cortes</th>
                    <th>Inicio</th>
                    <th>Fin</th>
                    <th>Estado</th>
                </tr>
            </thead>
            <tbody>
                <?php
                for ($i = 0; $i < count($lectivos); $i++) {
                    $rector = RectorDAO::getRectorById($lectivos[$i]->getDirector());
                    $fn = FormatoNotaDAO::getFormatoNotaByYear($lectivos[$i]->getId());
                    ?>
                    <tr id="tryear<?php echo $lectivos[$i]->getId(); ?>" class="<?php echo ($i % 2 == 0) ? "" : "tr_impar"; ?>"> 
                        <td><a id="<?php echo $lectivos[$i]->getId(); ?>" ><?php echo $lectivos[$i]->getId(); ?></a></td>
                        <td><?php echo $rector->getApellido() . " " . $rector->getNombre(); ?></td>
                        <td><?php echo $fn->getNPeridos(); ?></td>
                        <td><?php echo $lectivos[$i]->getFechaInicio(); ?></td>
                        <td><?php echo $lectivos[$i]->getFechaFin(); ?></td>
                        <td><?php echo $lectivos[$i]->getEstado(); ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php
    }

    public static function printFormLectivo($year) {
        $lectivo = NULL;
        if ($year != "") {
            $lectivo = LectivoDAO::getLectivo($year);
        }
        $rectores = RectorDAO::getRectores();
        if ($lectivo == NULL) {
            $yearMax = BD::getField("periodo", "idperiodo")[0] + 1;
            $lectivo = new Lectivo($yearMax, "", "", "", "", "");
        }
        ?>
        <form id="formularioLectivo">
            <div>
                <label>Año lectivo</label>
                <input type="number" max="2040" min="2000" name="year" value="<?php echo $lectivo->getId(); ?>"/><br>
            </div>
            <div>
                <label>Numero de periodos</label>
                <select name="nperiodos">
                    <option value="1" >1 periodo</option>
                    <option value="2" >2 periodos</option>
                    <option value="3" >3 periodos</option>
                    <option selected value="4" >4 periodos</option>
                    <option value="5" >5 periodos</option>
                    <option value="6" >6 periodos</option>
                    <option value="7" >7 periodos</option>
                    <option value="8" >8 periodos</option>
                    <option value="9" >9 periodos</option>
                    <option value="10" >10 periodos</option>
                </select>
            </div>
            <div>
                <label>Rector</label>
                <select name="rector">
                    <?php
                    for ($i = 0; $i < count($rectores); $i++) {
                        echo "<option " . (($lectivo->getDirector() == $rectores[$i]->getId()) ? "selected" : "") . " value='" . $rectores[$i]->getId() . "'>" . $rectores[$i]->getApellido() . " " . $rectores[$i]->getNombre() . "</option>";
                    }
                    ?>
                </select>
            </div>
            <div>
                <label>Fecha inicio</label>
                <input class="fecha" type="text" name="inicio" value="<?php echo $lectivo->getFechaInicio(); ?>"/>
            </div>
            <div>
                <label>Fecha Finalizacion</label>
                <input class="fecha" type="text" name="fin" value="<?php echo $lectivo->getFechaFin(); ?>"/>
            </div>
        </form>
        <?php
    }

    public static function guardarLectivo($year, $nperiodos, $fInicio, $fFin, $rector) {
        $lectivo = new Lectivo($year, $rector, 0, 20, Calendario::formatDataBase($fInicio), Calendario::formatDataBase($fFin));
        $error = LectivoDAO::insertar($lectivo, $nperiodos);
        if ($error != "") {
            return "<p id='bad'>$error<p>";
        }
        return "<p id='ok'>OK<p>";
    }

    public static function solicitudes() {
        if (empty($_GET["metodo"])) {
            return;
        }

        $s = $_GET["metodo"];

        if ($s == "guardarLectivo") {
            echo CLectivo::guardarLectivo($_POST["year"], $_POST["nperiodos"], $_POST["inicio"], $_POST["fin"], $_POST["rector"]);
        }
    }

}

CLectivo::solicitudes();
