<?php
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/NotaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/CursoDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ClaseDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/AsignaturaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ObservacionDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ProfesorDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/logica/Tool.php';
include_once 'Clogin.php';
include_once 'CBoletin.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CSessionEstudiante
 *
 * @author leon
 */
class CSessionEstudiante {

    public static function printNotasEstudiante($idestu, $idcurso, $periodo) {
        $curso = CursoDAO::getCursoByid($idcurso);
        $clases = ClaseDAO::getClasesByCursoId($idcurso, "ORDER BY `clase`.`asignatura_id_c` ASC");
        ?>
        <h1 class="page-header">Periodo: <?php echo $periodo; ?>  Curso : <?php echo $curso->getNombre(); ?></h1>          
        <div class="table-responsive">
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th>ASIGNATURA</th>
                        <th title="Faltas en el periodo" >Faltas</th>
                        <th title="Trabajo Escrito">Trab esc</th>
                        <th title="Evaluacion Escrita">Eval Esc</th>
                        <th title="Socializacion">Socia</th>
                        <th title="Consulta Biblioteca">Cons bibli</th>
                        <th title="Taller en Clase">Tall-clas</th>
                        <th title="Presentacion Cuaderno">pres-Cuad</th>
                        <th title="Tarea en Casa">Tar-Casa</th>
                        <th title="Nota Formativa">Nota-form</th>
                        <th title="Evalucaion Periodo">Eval-per</th>
                        <th title="NOTA FINAL PERIODO">Nota final</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    for ($i = 0; $i < count($clases); $i++) {
                        $nota = NotaDAO::getNotaEstudiantePeriodo($idestu, $clases[$i]->getId(), $periodo, "");
                        ?>
                        <tr>
                            <td><?php echo AsignaturaDAO::getNombreByClaseId($clases[$i]->getId()); ?></td>
                            <td><?php echo $nota->getFaltas(); ?></td>
                            <td><?php echo CSessionEstudiante::formatView(number_format($nota->getTrabajoEscrito(), 1)); ?></td>
                            <td><?php echo CSessionEstudiante::formatView(number_format($nota->getEvaluacionEscrita(), 1)); ?></td>
                            <td><?php echo CSessionEstudiante::formatView(number_format($nota->getSocializacion(), 1)); ?></td>
                            <td><?php echo CSessionEstudiante::formatView(number_format($nota->getConsultaBili(), 1)); ?></td>
                            <td><?php echo CSessionEstudiante::formatView(number_format($nota->getTallerClase(), 1)); ?></td>
                            <td><?php echo CSessionEstudiante::formatView(number_format($nota->getPresCuad(), 1)); ?></td>
                            <td><?php echo CSessionEstudiante::formatView(number_format($nota->getTallerCasa(), 1)); ?></td>
                            <td><?php echo CSessionEstudiante::formatView(number_format($nota->getNotaFormativa(), 1)); ?></td>
                            <td><?php echo CSessionEstudiante::formatView(number_format($nota->getEvaluacionPeriodo(), 1)); ?></td>
                            <td class= "danger"><?php echo number_format($nota->getNotaFinal(), 1); ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function formatView($nota){
        if($nota == "0.0"){
            return "";
        }
        return $nota;
    }

    public static function printBoletin($idestu, $idcurso, $periodo) {
        CBoletin::generarBoletinCurso($idcurso, $periodo, $idestu);
    }

    public static function printNotasGeneral($idestu, $idcurso) {
        $curso = CursoDAO::getCursoByid($idcurso);
        $asig = AsignaturaDAO::getAsignaturaByCurso($idcurso);
        $cl = ClaseDAO::getIdClaseByCursoId($idcurso, "ORDER BY `clase`.`asignatura_id_c` ASC");
        $fn = FormatoNotaDAO::getFormatoNotaByYear($curso->getFecha());
        ?>
        <h1 class="page-header">Año: <?php echo $curso->getFecha(); ?>  Curso : <?php echo $curso->getNombre(); ?>        </h1>          
        <div class="table-responsive">
            <table class="table  table-condensed">
                <thead>
                    <tr>
                        <th width="414" scope="col">Asignaturas</th>
                        <th width="40" scope="col">I.H</th>
                        <th width="40" scope="col">NF</th>
                        <?php
                        for ($m = 0; $m < $fn->getNPeridos(); $m++) {
                            echo "<th width='37' scope='col'>P" . ($m + 1) . "</th>";
                            echo "<th width='45' scope='col'>" . number_format($fn->getPorcent()[$m] *100, 0) . "%</th>";
                        }
                        ?>
                        <th width="52" scope="col">Acumulado</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    for ($i = 0; $i < count($asig); $i++) {
                        $notas = NotaDAO::getNotasEstudianteClaseAno($idestu, $cl[$i], $curso->getFecha());
                        $r = Tool::calcularNotaAno($notas, $fn->getPorcent(), $curso->getGrado());
                        $p = ProfesorDAO::getProfesorByClaseid($cl[$i]);
                        ?>
                        <tr>
                            <td title="Profesor: <?php echo $p->getApellido() . " " . $p->getNombre(); ?>"><?php echo $asig[$i]->getNombre(); ?></td>
                            <td><?php echo $asig[$i]->getIntensidad(); ?></td>
                            <td><?php echo $r["faltas"]; ?></td>
                            <?php
                            for ($l = 0; $l < $fn->getNPeridos(); $l++) {
                                $np = number_format($notas[$l]->getNotaFinal() * $fn->getPorcent()[$l], 1);
                                $n = number_format($notas[$l]->getNotaFinal(), 1);
                                echo '<td>' . (($n == "0.0") ? "" : Tool::formatNota($n, $curso->getGrado())) . '</td>
                                                    <td>' . (($np == "0.0") ? "" : Tool::formatNota($np, $curso->getGrado())) . '</td>';
                            }
                            ?>
                            <td><?php echo ($r["nota"] == 0) ? "" : Tool::formatNota($r["nota"], $curso->getGrado()); ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function printObservaciones($idestu, $idcurso, $periodo) {
        $curso = CursoDAO::getCursoByid($idcurso);
        $odservacion = ObservacionDAO::getOnservacionById($idestu, $curso->getDirector()->getId(), $idcurso, $periodo);
        if ($odservacion == NULL) {
            echo '<h1>No hay Oservaciones en este periodo</h1>';
            return;
        }
        ?>
        <table class="table parrafo">
            <thead>
                <tr>
                    <th>Director Curso</th>
                    <th title="Fecha de la observacion" >Fecha</th>
                    <th title="Faltas">Faltas</th>
                    <th title="Comportamiento">Comportamiento</th>
                    <th title="Descripcion">Descripcion</th>
                    <th title="Accion Pedagogica">Accion Pedagogica</th>
                </tr>
            </thead>
            <tbody id="tbodyObservacion">
                <tr> 
                    <td class="tdnombreprofe"><?php echo $curso->getDirector()->getNombre() . " " . $curso->getDirector()->getApellido() ?></td>
                    <td><?php echo $odservacion->getFecha(); ?></td>
                    <td><?php echo $odservacion->getFaltas(); ?></td>
                    <td><?php echo $odservacion->getComportamiento(); ?></td>
                    <td><?php echo $odservacion->getDescripcion(); ?></td>
                    <td><?php echo $odservacion->getAccionPedagogica(); ?></td>
                </tr>
            </tbody>
        </table>
        <?php
    }

    public static function solicitudes() {
        @session_start();
        if (!Clogin::isSesion()) {
            return;
        }
        $us = Clogin::getUser();
        if (empty($_GET["metodo"])) {
            return;
        }
        $r = $_GET["metodo"];
        if ($r == "printBoletin") {
            CSessionEstudiante::printBoletin($us->getId(), $_GET["curso"], $_GET["periodo"]);
        }
    }

}

CSessionEstudiante::solicitudes();
