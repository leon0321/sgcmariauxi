<?php
include_once '../modelo/dto/Solicitud.php';
include_once '../modelo/persistencia/SolicitudDAO.php';
include_once 'Clogin.php';
include_once 'CAsistencia.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CSolicitud
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class CSolicitud {

    public static function printSolicitudes() {
        $u = Clogin::getUser();
        $tipo = "supervisor";
        if ($u->getTipo() == Usuario::PROFESOR) {
            $tipo = "profesor";
        } else if ($u->getTipo() == Usuario::SECRETARIO) {
            $tipo = "supervisor";
        } else {
            return;
        }

        $s = SolicitudDAO::getSolicitudes($u->getId(), $tipo);
        ?>            
        <div id="blog_container">
            <?php
            for ($i = 0; $i < count($s); $i++) {
                $a = $s[$i]->getAsistencia();
                ?>
            <div class="solicitud">
                    <div class="blog"><h2><?php echo Calendario::format("M d", $s[$i]->getFechaSolicitud()); ?></h2><h3><?php echo Calendario::format("H:i", $s[$i]->getFechaSolicitud()); ?></h3></div>
                    <?php if ($s[$i]->getEstado() != Solicitud::ESPERA) { ?>
                        <div class="blog"><h2 class="<?php echo ($s[$i]->getEstado() == Solicitud::APROBADO) ? "verde" : "rojo"; ?>" ><?php echo Calendario::format("M d", $s[$i]->getFechaRespuesta()); ?></h2><h3><?php echo Calendario::format("H:i", $s[$i]->getFechaRespuesta()); ?></h3></div>
                    <?php } ?>
                        <h4><?php CAsistencia::printEstado($s[$i]->getEstadoOriginal()); ?><a id="<?php echo $s[$i]->getId() ?>" ><?php echo $s[$i]->getAsunto() ?></a></h4>
                    <p><?php echo $s[$i]->getDescripcion() ?></p>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
    }

    public static function printFormularioSolicitud() {
        $clase = CClases::getClase($_GET["idclase"]);
        $asis = AsistenciaDAO::getAsistByClaseIdAndFecha($clase->getId(), $_GET["fecha"]);
        $_SESSION["asistencia"] = serialize($asis);
        ?>
<div>
        <div id="formuSolicitud" class="formulario">
            <h2>Nueva Solicitud</h2>
            <form method="post" id="formularioSolicitud" >
                <div>
                    <h3>Clase</h3>
                    <textarea readonly="" name="clase"type="text" ><?php echo $clase->getAsignatura()->getNombre() ?></textarea>
                </div>
                <div>
                    <h3>Fecha - hora <?php CAsistencia::printEstado($asis->getEstado()) ?></h3>
                    <input readonly="" name="fechaclase" type="text"  value="<?php
                    echo Calendario::format("D d M Y", $asis->getFechaClase());
                    echo " [";
                    Calendario::printHorasClase($asis->getFechaClase(), $asis->getHoras());
                    echo "]";
                    ?>"/>
                </div>
                <div>
                    <h3>Asunto</h3>
                    <textarea name="asunto"type="text" ></textarea>
                </div>
                <div>
                    <h3>Descripcion</h3>
                    <textarea class="grand" name="descripcion" type="text" ></textarea>
                </div>
                <div>
                    <h3>Nueva fecha - hora</h3>
                    <input name="nuevafecha" type="date" />
                    <input name="nuevahora" type="time" />
                </div>
                <div class="submit">
                    <input type="button" name="cancel" value="Cancelar" onclick="redirec('http://localhost/SIG/vista/solicitud.php')">
                    <input id="solicitud_guardar" type="button" name="commit" value="Enviar">
                </div>
            </form>
            <div id="respuesta">                
            </div>
        </div>
    </div>
        <?php
    }

    public static function cargarSolicitud($id) {
        $us = Clogin::getUser();
        $sol = SolicitudDAO::getSolicitud($id);
        $asis = $sol->getAsistencia();
        $clase = CClases::getClase($asis->getIdClase());
        $_SESSION["asistencia"] = serialize($asis);
        $_SESSION["solicitud"] = serialize($sol);
        ?>
<div class="ayuda">
        <div id="formuSolicitud" class="formulario">
            <h2>Solicitud</h2>
            <form method="post" id="formularioSolicitud" >
                <div>
                    <h3>Clase</h3>
                    <textarea readonly="" name="clase"type="text" ><?php echo $clase->getAsignatura()->getNombre() ?></textarea>
                </div>
                <div>
                    <h3>Fecha - hora: <?php CAsistencia::printEstado($sol->getEstadoOriginal()) ?> </h3>
                    <input readonly="" name="fechaclase" type="text"  value="<?php
                    echo Calendario::format("D d M Y", $asis->getFechaClase());
                    echo " [";
                    Calendario::printHorasClase($sol->getEstadoOriginal(), $asis->getHoras());
                    echo "]";
                    ?>"/>
                </div>
                <div>
                    <h3>Asunto</h3>
                    <textarea readonly="" name="asunto"type="text" ><?php echo $sol->getAsunto() ?></textarea>
                </div>
                <div>
                    <h3>Descripcion</h3>
                    <textarea readonly="" class="grand" name="descripcion" type="text" ><?php echo $sol->getDescripcion() ?></textarea>
                </div>
                <div>
                    <h3>Nueva fecha - hora</h3>
                    <input readonly="" name="nuevafecha" type="date" value="<?php echo Calendario::format("Y-m-d", $asis->getFechaClase()) ?>"/>
                    <input readonly="" name="nuevahora" type="time" value="<?php echo Calendario::format("H:i", $asis->getFechaClase()) ?>"/>
                </div>
                <div>
                    <h3>Respuesta</h3>
                    <?php
                    $p = "readonly";
                    if ($us->getTipo() == Usuario::PROFESOR) {
                        ?><input readonly="" name="estado" type="text" value="<?php echo CSolicitud::estadoName($sol->getEstado()) ?>"/><?php
                    } else if ($sol->getEstado() != Solicitud::ESPERA) {
                        ?><input readonly="" name="estado" type="text" value="<?php echo CSolicitud::estadoName($sol->getEstado()) ?>"/><?php
                    } else {
                        $p = "";
                        ?>
                        <select name="estadoSolicitud">
                            <option value="1" >Aprobado</option>
                            <option value="2" >Desaprobado</option>
                        </select>
                        <?php
                    }
                    ?>  
                    <textarea <?php echo $p ?> class="grand" name="respuesta" type="text" ><?php echo $sol->getRespuesta() ?></textarea>
                </div>
                <div class="submit">
                    <input type="button" name="cancel" value="Cancelar" onclick="redirec('http://localhost/SIG/vista/solicitud.php')">
                    <?php if ($sol->getEstado() == Solicitud::ESPERA && $us->getTipo() != Usuario::PROFESOR) { ?>
                        <input id="solicitud_actualizar" type="button" name="commit" value="Enviar">
                    <?php } ?>
                </div>
            </form>
            <div id="respuesta">                
            </div>
        </div>
</div>
        <?php
    }

    public static function estadoName($estado) {
        if (Solicitud::ESPERA == $estado) {
            return "En espera";
        } else if (Solicitud::APROBADO == $estado) {
            return "Aprobada";
        } else if (Solicitud::RECHAZADA == $estado) {
            return "Rechazada";
        }
    }

    public static function guadarSolicitud() {
        $user = Clogin::getUser();
        $asis = CAsistencia::getAsist();
        $clase = CClases::getClaseF();
        if ($user->getTipo() == Usuario::PROFESOR) {
            $sol = new Solicitud(null, $asis, ($asis->getEstado() == Asistencia::NO_DICTADA) ? "1" : "0", $_POST["asunto"], $_POST["descripcion"], "0", "...", $asis->getFechaClase(), $_POST["nuevafecha"] . " " . $_POST["nuevahora"], null, null, $clase->getProfesor()->getId(), $clase->getSupervisor()->getId(), null);
        }
        SolicitudDAO::insertar($sol);
        if (BD::error() == "") {
            return "<h1>ok<h1>";
        } else {
            return "<h1>" . BD::error() . "</h1>";
        }
    }

    public static function actualizarSolicitud() {
        $sol = unserialize($_SESSION["solicitud"]);
        $asis = $sol->getAsistencia();
        $sol->setEstado($_POST["estadoSolicitud"]);
        $sol->setRespuesta($_POST["respuesta"]);
        $r = "";
        if ($sol->getEstado() == Solicitud::APROBADO) {

            if ($asis->getEStado() == Asistencia::NO_DICTADA) {
                $asis->setEstado(Asistencia::RECUPERAR);
            } else if ($asis->getEStado() == Asistencia::ESPERA) {
                $asis->setEstado(Asistencia::APLAZADA);
            }

            $r = AsistenciaDAO::actualizar($asis, $sol->getFechaNueva());
            if ($r != "") {
                return "<h1>" . $r . "</h1>";
            }
        }
        $sol->setFechaRespuesta(Calendario::getFechaActual());
        $r = SolicitudDAO::actualizar($sol);
        if ($r != "") {
            return "<h1>" . $r . "</h1>";
        }
        return "<h1>ok</h1>";
    }

    public static function requests() {
        if (empty($_GET["metodo"])) {
            return;
        }
        $r = $_GET["metodo"];

        if ($r == "opciones") {
            CSolicitud::opciones();
        } else if ($r == "printFormularioSolicitud") {
            CSolicitud::printFormularioSolicitud();
        } else if ($r == "guadarSolicitud") {
            echo CSolicitud::guadarSolicitud();
        } else if ($r == "cargarSolicitud") {
            CSolicitud::cargarSolicitud($_GET["id"]);
        } else if ($r == "actualizarSolicitud") {
            echo CSolicitud::actualizarSolicitud();
        }
    }

}

CSolicitud::requests();
?>
