<?php
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/SecretarioDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/dto/Secretario.php';
include_once 'Clogin.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CEstudiante
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class CSecretario {

    public static function printSecretarios() {
        $secretarios = SecretarioDAO::getSecretarios();
        ?>
        <table class="table" id="tablaSecretario">
            <thead>
                <tr>
                    <th>Nombre y Apellido</th>
                    <th>Codigo</th>
                    <th>Telefono</th>
                    <th>Correo</th>
                </tr>
            </thead>
            <tbody>
                <?php
                for ($i = 0; $i < count($secretarios); $i++) {
                    ?>
                    <tr id="trsecretario<?php echo $secretarios[$i]->getId(); ?>" class="<?php echo ($i % 2 == 0) ? "" : "tr_impar"; ?>"> 
                        <td><a class="edit" id="<?php echo $secretarios[$i]->getId(); ?>" ><?php echo $secretarios[$i]->getApellido() . " " . $secretarios[$i]->getNombre(); ?></a></td>
                        <td><?php echo $secretarios[$i]->getId(); ?></td>
                        <td><?php echo $secretarios[$i]->getTelefono(); ?></td>
                        <td><?php echo $secretarios[$i]->getCorreo(); ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php
    }

    public static function printFormSecretario($id = "") {
        $secretario = new Secretario("", "", "", "", "", "", "", "", "");
        if ($id != "") {
            $secretario = SecretarioDAO::getSecretarioByid($id);
        }
        ?>
        <form method="post" id="formularioSecretario" >
            <div>
                <label>Nombre</label>
                <input name="nombre"type="text" value="<?php echo $secretario->getNombre(); ?>"/>
            </div>
            <div>
                <label>Apellido</label>
                <input name="apellido" type="text" value="<?php echo $secretario->getApellido(); ?>"/>
            </div>
            <div>
                <label>Codigo</label>
                <input name="codigo" type="text" value="<?php echo $secretario->getId(); ?>" />
                <input name="idactual" type="text" value="<?php echo $secretario->getId(); ?>" style="display: none"/>
            </div>
            <div>
                <label>Telefono</label>
                <input name="telefono" type="tel" value="<?php echo $secretario->getTelefono(); ?>"/>
            </div>
            <div>
                <label>Correo</label>
                <input name="correo" type="email" value="<?php echo $secretario->getCorreo() ?>"/>
            </div>
        </form>
        <?php
    }

    public static function aditarSecretario($idactual, $id, $nombre, $apellido, $telefono, $correo) {
        $secretario = new Secretario($id, $nombre, $apellido, $telefono, $correo, "", "", "", "");
        $msn = SecretarioDAO::actualizarSecretario($secretario, $idactual);
        if ($msn != "") {
            echo '<p id="error" >' . $msn . '</p>';
            return;
        }
        echo '<p id="ok" >OK</p>';
    }

    public static function guadarSecretario() {
        $secretario = new Secretario($_POST["codigo"], $_POST["nombre"], $_POST["apellido"], $_POST["telefono"], $_POST["correo"], $_POST["codigo"], $_POST["codigo"], Usuario::SECRETARIO, "");
        $msn = SecretarioDAO::insertar($secretario);
        if ($msn != "") {
            echo "<p>" . $msn . "</p>";
            return;
        }
        echo "<p id='ok'>ok</p>";
    }

    public static function solicitudes() {
        if (empty($_GET["metodo"])) {
            return;
        }

        $r = $_GET["metodo"];
        if ($r == "printFormSecretario") {
            $id = (empty($_GET["secretario"])) ? "" : $_GET["secretario"];
            CSecretario::printFormSecretario($id);
        } else if ($r == "guadarSecretario") {
            CSecretario::guadarSecretario();
        } elseif ($r == "aditarSecretario") {
            CSecretario::aditarSecretario($_POST["idactual"], $_POST["codigo"], $_POST["nombre"], $_POST["apellido"], $_POST["telefono"], $_POST["correo"], "");
        }
    }

}

CSecretario::solicitudes();
