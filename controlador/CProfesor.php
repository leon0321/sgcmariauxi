<?php
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ProfesorDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/dto/Profesor.php';
include_once 'Clogin.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CEstudiante
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class CProfesor {

    public static function printProfesores() {
//        if (!Clogin::isSesion()) {
//            return;
//        }
//        $us = Clogin::getUser();
//        if ($us->getTipo() != Usuario::ADMIN) {
//            return;
//        }
        $profes = ProfesorDAO::getProfesores("");
        ?>
        <table class="table" id="tablaProfesores">
            <thead>
                <tr>
                    <th>Nombre y Apellido</th>
                    <th>Codigo</th>
                    <th>Telefono</th>
                    <th>Correo</th>
                    <th>Tipo</th>
                    <th>Nivel</th>
                </tr>
            </thead>
            <tbody>
                <?php
                for ($i = 0; $i < count($profes); $i++) {
                    ?>
                    <tr id="trprofe<?php echo $profes[$i]->getId(); ?>" class="<?php echo ($i % 2 == 0) ? "" : "tr_impar"; ?>"> 
                        <td><a onclick="editProfe('<?php echo $profes[$i]->getId(); ?>')"><?php echo $profes[$i]->getApellido() . " " . $profes[$i]->getNombre(); ?></a></td>
                        <td><?php echo $profes[$i]->getId(); ?></td>
                        <td><?php echo $profes[$i]->getTelefono(); ?></td>
                        <td><?php echo $profes[$i]->getCorreo(); ?></td>
                        <td><?php echo CProfesor::printTipo($profes[$i]->getPtipo()); ?></td>
                        <td><?php echo CProfesor::printNivel($profes[$i]->getNivel()); ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php
    }

    public static function printTipo($tipo) {
        if ($tipo == 0) {
            return "Director";
        } else {
            return "Normal";
        }
    }

    public static function printNivel($nivel) {
        if ($nivel == 1) {
            return "Primaria";
        } else {
            return "Segundaria";
        }
    }

    public static function printFormProfesor($id = "") {
        $p = new Profesor("", "", "", "", "", "", "", "", "", "");
        if ($id != "") {
            $p = ProfesorDAO::get()->getProfesorByid($id);
        }
        ?>
        <form method="post" id="formularioProfesor" >
            <div>
                <label>Nombre</label>
                <input name="nombre"type="text" value="<?php echo $p->getNombre(); ?>"/>
            </div>
            <div>
                <label>Apellido</label>
                <input name="apellido" type="text" value="<?php echo $p->getApellido(); ?>"/>
            </div>
            <div>
                <label>Codigo</label>
                <input name="codigo" type="text" value="<?php echo $p->getId(); ?>" />
                <input name="idactual" type="text" value="<?php echo $p->getId(); ?>" style="display: none"/>
            </div>
            <div>
                <label>Telefono</label>
                <input name="telefono" type="tel" value="<?php echo $p->getTelefono(); ?>"/>
            </div>
            <div>
                <label>Correo</label>
                <input name="correo" type="email" value="<?php echo $p->getCorreo() ?>"/>
            </div>
            <div>
                <label>Nivel</label>
                <select name="nivel">
                    <option <?php echo ($p->getNivel() == "1") ? "selected" : ""; ?> value="1" >Primaria</option>
                    <option <?php echo ($p->getNivel() == "2") ? "selected" : ""; ?> value="2" >Segundaria</option>
                </select>
            </div>
            <div>
                <label>Tipo Profesor</label>
                <select name="ptipo">
                    <option <?php echo ($p->getPtipo() == "0") ? "selected" : ""; ?> value="0" >Director</option>
                    <option <?php echo ($p->getPtipo() == "1") ? "selected" : ""; ?> value="1" >Normal</option>
                </select>
            </div>
        </form>
        <?php
    }

    public static function aditarProfesor($idactual, $id, $nombre, $apellido, $telefono, $correo, $nivel, $ptipo) {
        $p = new Profesor($id, $nombre, $apellido, $telefono, $correo, "", "", "", $nivel, $ptipo);
        $msn = ProfesorDAO::actualizarP($p, $idactual);
        if ($msn != "") {
            return '<p id="bad" >' . $msn . '</p>';
        }
        return '<p id="ok" >ok</p>';
    }

    public static function guadarProfesor() {
        $profe = new Profesor($_POST["codigo"], $_POST["nombre"], $_POST["apellido"], $_POST["telefono"], $_POST["correo"], $_POST["codigo"], $_POST["codigo"], Usuario::PROFESOR, $_POST["nivel"], $_POST["ptipo"]);
        $profe->setPtipo(($profe->getNivel() == 1) ? 0 : 1);
        $msn = ProfesorDAO::get()->insertar($profe);
        if ($msn != "") {
            echo "<p id='bad'>" . $msn . "</p>";
            return;
        }
        echo "<p id='ok'>ok</p>";
    }

    public static function eliminarProfesor() {
        $id = $_POST["id"];
        echo "<h1>hola" . ProfesorDAO::get()->eliminar($id) . "<h1>";
    }

    public static function requests() {
        if (empty($_GET["metodo"])) {
            return;
        }

        $r = $_GET["metodo"];

        if ($r == "printFormProfesor") {
            $id = (empty($_GET["profe"])) ? "" : $_GET["profe"];
            CProfesor::printFormProfesor($id);
        } else if ($r == "guadarProfesor") {
            CProfesor::guadarProfesor();
        } else if ($r == "eliminarProfesor") {
            CProfesor::eliminarProfesor();
        } elseif ($r == "aditarProfesor") {
            echo CProfesor::aditarProfesor($_POST["idactual"], $_POST["codigo"], $_POST["nombre"], $_POST["apellido"], $_POST["telefono"], $_POST["correo"], $_POST["nivel"], $_POST["ptipo"]);
        }
    }

}

CProfesor::requests();
?>
