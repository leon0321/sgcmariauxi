<?php
include_once 'CAsistencia.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/SolicitudDAO.php';
include_once 'CProfesor.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CInforme
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class CInforme {

    public static function printBandera($porcentaje) {
        $p = $porcentaje;
        if ($p <= 0.875) {
            CAsistencia::printEstado(5);
        } else if ($p > 0.875 && $p < 1) {
            CAsistencia::printEstado(0);
        } else if ($p >= 1) {
            CAsistencia::printEstado(1);
        }
    }

    public static function printFormBuscar() {
        ?>
        <br>
        <div>
            <form method="post" id="formularioBuscar" >
                <div>
                    <h3>Profesor</h3>
                    <select id="tipop" name="ptipo">
                        <option value="">Todos</option>
                        <option value="0">Planta</option>
                        <option value="1">Catedratico</option>
                    </select>
                    <br/>
                    <select id="profeSelect" name="profesor">
                        <?php CInforme::lisProfesores("") ?>
                    </select>
                </div>
                <div>
                    <h3>Asignatura</h3>
                    <input type="text" name="peridoi"><br/>
                    <select name="asignatura">
                        <option value="">Todos</option>
                        <?php
                        $asig = AsignaturaDAO::getAsignaturas();
                        for ($i = 0; $i < count($asig); $i++) {
                            ?><option value="<?php echo $asig[$i]->getId(); ?>"><?php echo $asig[$i]->getNombre(); ?></option><?php
                        }
                        ?>  
                    </select>
                </div>  
                <div>
                    <h3>Periodo</h3>
                    <input type="number" name="peridoi"><br/>
                    <input name="peridof" type="number">
                </div>
                <div>
                    <h3>Entre las fechas</h3>
                    <span>Inicial:</span><input type="date" name="fechai" ><br/>
                    <span>Final:</span><input name="fechaf" type="date" value="<?php echo date("Y-m-d"); ?>">
                </div>                
                <div class="submit">            
                    <input id="buscarinfo"type="button" name="cancel" value="Buscar" >
                </div>
                <br clear="all">
            </form>
            <div id="respuestaAsist">                
            </div>
        </div>
        <?php
    }

    public static function lisProfesores($ptipo) {
        $profes = ProfesorDAO::getProfesores($ptipo);
        ?> <option value = "">Todos</option> <?php
        for ($i = 0; $i < count($profes); $i++) {
            ?><option value="<?php echo $profes[$i]->getId(); ?>"><?php echo $profes[$i]->getApellido() . " " . $profes[$i]->getNombre(); ?></option><?php
        }
    }

    public static function printClasesInfoF($idprofe, $asig, $pi, $pf, $ti, $tf, $ptipo) {
        if (!Clogin::isSesion()) {
            return;
        }
        $us = Clogin::getUser();
        $clases = ClaseDAO::getClasesBy($idprofe, $asig, $pi, $pf, $ptipo);
        $tipo = $us->getTipo();
        ?>
        <div class="tablaScroll">
            <table class="tablaAdmin">
                <thead>
                    <tr><th>Asignatura</th><?php echo ($tipo != Usuario::PROFESOR) ? "<th>Profesor</th>" : ""; ?><th title="Horas Dictadas">HD</th><th title="Horas Esperadas">HE</th><th>Asistencia</th></tr>
                </thead>
                <tbody>
                    <?php
                    for ($i = 0; $i < count($clases); $i++) {
                        $a = $clases[$i]->getAsignatura();
                        $p = $clases[$i]->getProfesor();
                        $horas[0] = AsistenciaDAO::getHorasDictadas($clases[$i]->getId(), $ti, $tf);
                        $horas[1] = AsistenciaDAO::getHorasEsperadas($clases[$i]->getId(), $ti, $tf);
                        ?>
                        <tr> 
                            <td><a onclick="asistenInfo('<?php echo $clases[$i]->getId(); ?>')" ><?php echo "(" . $clases[$i]->getPeriodo() . ") " . $a->getNombre(); ?></a></td>
                            <?php if ($tipo != Usuario::PROFESOR) { ?>
                                <td><?php echo $p->getApellido() . " " . $p->getNombre() . " - " . $p->getId(); ?></td>
                            <?php } ?>
                            <td><?php echo ($horas[0] + 0); ?></td>
                            <td><?php echo $horas[1]; ?></td>
                            <td>
                                <?php
                                $por = ($horas[0] / $horas[1]);
                                CInforme::printBandera($por);
                                echo number_format($por * 100, 2) . "%";
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function printSolicitudInfo($idprofe, $asig, $pi, $pf, $ti, $tf, $ptipo) {
        $informe = SolicitudDAO::getInformeSolicitud($idprofe, $asig, $ptipo, $pi, $pf, $ti, $tf);
        ?>
        <div class="tablaScroll">
            <table class="tablaAdmin">
                <thead>
                <th>Motivo</th>
                <th>Solicitud</th>
                <th>Profesor</th>
                <th>Tipo</th>
                <th>Asignatura</th>
                <th>Sede</th>
                <th>FechaS</th>
                <th>FechaO</th>
                <th>FechaN</th>
                </thead>
                <tbody>
                    <?php
                    for ($i = 0; $i < count($informe); $i++) {
                        ?>
                        <tr>
                            <td title="<?php echo $informe[$i]["s_descripcion"]; ?>"><?php echo "..."; ?></td>
                            <td><?php echo CAsistencia::estadoName2($informe[$i]["asist_estadooriginal"]); ?> </td>
                            <td><?php echo $informe[$i]["p_apellido"] . " " . $informe[$i]["p_nombre"]; ?></td>
                            <td><?php echo CProfesor::printTipo($informe[$i]["p_tipo"]); ?> </td>
                            <td><?php echo $informe[$i]["a_nombre"]; ?> </td>
                            <td><?php echo $informe[$i]["s_nombre"]; ?> </td>
                            <td><?php echo Calendario::format("y-m-d", $informe[$i]["s_fechasolicitud"]); ?> </td>
                            <td><?php echo Calendario::format("y-m-d", $informe[$i]["s_fechaoriginal"]); ?> </td>
                            <td><?php echo Calendario::format("y-m-d", $informe[$i]["s_nuevafecha"]); ?> </td>

                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function requests() {
        if (empty($_GET["metodo"])) {
            return;
        }
        $r = $_GET["metodo"];
        if ($r == "printClasesInfoF") {
            CInforme::printClasesInfoF($_POST["profesor"], $_POST["asignatura"], $_POST["peridoi"], $_POST["peridof"], $_POST["fechai"], $_POST["fechaf"], $_POST["ptipo"]);
        } else if ($r == "lisProfesores") {
            CInforme::lisProfesores($_GET["ptipo"]);
        } else if ($r == "printSolicitudInfo") {
            CInforme::printSolicitudInfo($_POST["profesor"], $_POST["asignatura"], $_POST["peridoi"], $_POST["peridof"], $_POST["fechai"], $_POST["fechaf"], $_POST["ptipo"]);
        } else if ($r == "eliminarClase") {
            CClases::eliminarClase();
        }
    }

}

CInforme::requests();
