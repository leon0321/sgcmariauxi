<?php
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/RectorDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/dto/Rector.php';
include_once 'Clogin.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CEstudiante
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class CRector {

    public static function printRectores() {
        $rectors = RectorDAO::getRectores();
        ?>
        <table class="table" id="tablaProfesores">
            <thead>
                <tr>
                    <th>Nombre y Apellido</th>
                    <th>Codigo</th>
                    <th>Telefono</th>
                    <th>Correo</th>
                </tr>
            </thead>
            <tbody>
                <?php
                for ($i = 0; $i < count($rectors); $i++) {
                    ?>
                    <tr id="trrector<?php echo $rectors[$i]->getId(); ?>" class="<?php echo ($i % 2 == 0) ? "" : "tr_impar"; ?>"> 
                        <td><a class="edit" id="<?php echo $rectors[$i]->getId(); ?>" ><?php echo $rectors[$i]->getApellido() . " " . $rectors[$i]->getNombre(); ?></a></td>
                        <td><?php echo $rectors[$i]->getId(); ?></td>
                        <td><?php echo $rectors[$i]->getTelefono(); ?></td>
                        <td><?php echo $rectors[$i]->getCorreo(); ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php
    }

    public static function printFormRector($id = "") {
        $rector = new Rector("", "", "", "", "", "", "", "", "");
        if ($id != "") {
            $rector = RectorDAO::getRectorByid($id);
        }
        ?>
        <form method="post" id="formularioRector" >
            <div>
                <label>Nombre</label>
                <input name="nombre"type="text" value="<?php echo $rector->getNombre(); ?>"/>
            </div>
            <div>
                <label>Apellido</label>
                <input name="apellido" type="text" value="<?php echo $rector->getApellido(); ?>"/>
            </div>
            <div>
                <label>Codigo</label>
                <input name="codigo" type="text" value="<?php echo $rector->getId(); ?>" />
                <input name="idactual" type="text" value="<?php echo $rector->getId(); ?>" style="display: none"/>
            </div>
            <div>
                <label>Telefono</label>
                <input name="telefono" type="tel" value="<?php echo $rector->getTelefono(); ?>"/>
            </div>
            <div>
                <label>Correo</label>
                <input name="correo" type="email" value="<?php echo $rector->getCorreo() ?>"/>
            </div>
            <div>
                <label>Sexo</label>
                <select name="sexo">
                    <option value="1" <?php echo ($rector->getSexo() == "1") ? "selected" : ""; ?>>Masculino</option>
                    <option value="2" <?php echo ($rector->getSexo() == "2") ? "selected" : ""; ?>>Femenino</option>
                </select>
            </div>
        </form>
        <?php
    }

    public static function aditarRector($idactual, $id, $nombre, $apellido, $telefono, $correo, $sexo) {
        $rector = new Rector($id, $nombre, $apellido, $telefono, $correo, $id, $id, Rector::RECTOR, $sexo);
        $msn = RectorDAO::actualizarRector($rector, $idactual);
        if ($msn != "") {
            echo '<p id="error" >' . $msn . '</p>';
            return;
        }
        echo "<p id='ok'>ok</p>";
    }

    public static function guadarRector() {
        $rector = new Rector($_POST["codigo"], $_POST["nombre"], $_POST["apellido"], $_POST["telefono"], $_POST["correo"], $_POST["codigo"], $_POST["codigo"], Usuario::COORDINADOR, $_POST["sexo"]);
        $msn = RectorDAO::insertar($rector);
        if ($msn != "") {
            echo "<p>" . $msn . "</p>";
            return;
        }
        echo "<p id='ok'>ok</p>";
    }

    public static function solicitudes() {
        if (empty($_GET["metodo"])) {
            return;
        }

        $r = $_GET["metodo"];
        if ($r == "printFormRector") {
            $id = (empty($_GET["profe"])) ? "" : $_GET["profe"];
            CRector::printFormRector($id);
        } else if ($r == "guadarRector") {
            CRector::guadarRector();
        } elseif ($r == "aditarRector") {
            CRector::aditarRector($_POST["idactual"], $_POST["codigo"], $_POST["nombre"], $_POST["apellido"], $_POST["telefono"], $_POST["correo"], $_POST["sexo"]);
        }
    }

}
//hola
//hola
CRector::solicitudes();
