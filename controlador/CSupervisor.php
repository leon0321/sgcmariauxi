<?php
include_once '../modelo/persistencia/SupervisorDAO.php';
include_once '../modelo/dto/Supervisor.php';
include_once 'Clogin.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CEstudiante
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class CSupervisor {

    public static function printSupervisores() {
        if (!Clogin::isSesion()) {
            return;
        }
        $us = Clogin::getUser();
        if ($us->getTipo() != Usuario::ADMIN) {
            return;
        }
        $supervisores = SecretarioDAO::getSecretarios();
        ?>
        <div class="tablaScroll" >
            <table id="tclasesAdmin"  class="tablaAdmin">
                <caption><h2>Supervisores</h2></caption>                    
                <thead>
                    <tr><th>Apellidos</th><th>Nombre</th><th>Codigo</th><th>Telefono</th><th>Correo </th><th>Username</th><th class="opciones">Opciones</th></tr>
                </thead>
                <tbody>
                    <?php
                    for ($i = 0; $i < count($supervisores); $i++) {
                        ?>
                        <tr> 
                            <td><?php echo $supervisores[$i]->getApellido(); ?></td>
                            <td><?php echo $supervisores[$i]->getNombre(); ?></td>
                            <td><?php echo $supervisores[$i]->getId(); ?></td>
                            <td><?php echo $supervisores[$i]->getTelefono(); ?></td>
                            <td><?php echo $supervisores[$i]->getCorreo(); ?></td>
                            <td><?php echo $supervisores[$i]->getLogin(); ?></td>
                            <td><input type="button" name="eliminar" onclick="elimSupervisor('<?php echo $supervisores[$i]->getId(); ?>')" value="eliminar"/></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function printFormSupervisor() {
        ?>
        <div id="formuSupervisor" class="formulario">
            <h2>Formulario Supervisor</h2>
            <form method="post" id="formularioSupervisor" >
                <div>
                    <h3>Nombre</h3>
                    <input name="nombre"type="text" />
                </div>
                <div>
                    <h3>Apellido</h3>
                    <input name="apellido" type="text" />
                </div>
                <div>
                    <h3>Codigo</h3>
                    <input name="codigo" type="text" />
                </div>
                <div>
                    <h3>Telefono</h3>
                    <input name="telefono" type="tel" />
                </div>
                <div>
                    <h3>Correo</h3>
                    <input name="correo" type="email" />
                </div>
                <p class="submit">
                    <input type="button" name="cancel" value="Cancelar" onclick="redirec('http://localhost/SIG/vista/supervisor.php')">
                    <input id="supervisor_guardar" type="button" name="commit" value="Guardar">
                </p>
            </form>
            <div id="respuesta">                
            </div>
        </div>
        <?php
    }

    public static function guadarSupervisor() {
        $supervisor = new Secretario($_POST["codigo"], $_POST["nombre"], $_POST["apellido"], $_POST["telefono"], $_POST["correo"], $_POST["correo"], $_POST["codigo"], "",Usuario::SECRETARIO);
        $msn = SecretarioDAO::get()->insertar($supervisor);
        if ($msn != "") {
            echo "<p>" . $msn . "</p>";
            return;
        }
        echo "<p>ok</p>";
    }

    public static function eliminarSupervisor() {
        $id = $_POST["id"];
        echo "<h1>hola" . SecretarioDAO::get()->eliminar($id) . "<h1>";
    }

    public static function requests() {
        if (empty($_GET["metodo"])) {
            return;
        }

        $r = $_GET["metodo"];

        if ($r == "printFormSupervisor") {
            CSupervisor::printFormSupervisor();
        } else if ($r == "guadarSupervisor") {
            CSupervisor::guadarSupervisor();
        } else if ($r == "eliminarSupervisor") {
            CSupervisor::eliminarSupervisor();
        }
    }

}

CSupervisor::requests();
?>
