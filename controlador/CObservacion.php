<?php
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ObservacionDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ClaseDAO.php';
include_once 'Clogin.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/logica/Calendario.php';
@session_start();
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CObservacion
 *
 * @author EDGES
 */
class CObservacion {

    public static function printObservaciones($idcurso, $periodo = "") {
        // $o = ObservacionDAO::getObservacionesByIdCurso($idcurso);
        $o = ObservacionDAO::getObservacionesByCursoAndPeriodo($idcurso, $periodo);
        ?>
        <table class="table parrafo">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th title="Fecha de la observacion" >Fecha</th>
                    <th title="Periodo">Periodo</th>
                    <th title="Faltas">Faltas</th>
                    <th title="Comportamiento">Comportamiento</th>
                    <th title="Descripcion">Descripcion</th>
                    <th title="Accion Pedagogica">Accion Pedagogica</th>
                </tr>
            </thead>
            <tbody id="tbodyObservacion">
                <?php
                for ($i = 0; $i < count($o); $i++) {
                    $onclick = 'editObservacion(' . "'" . $o[$i]->getIdestudiante() . "'" . ',' . "'" . $o[$i]->getIdprofe() . "'" . ',' . "'" . $o[$i]->getIdcurso() . "'" . ',' . "'" . $o[$i]->getPeriodo() . "'" . ')';
                    $e = EstudianteDAO::get()->getEstudianteByid($o[$i]->getIdestudiante())
                    ?>
                    <tr id="tr<?php echo $e->getId() ?>" class="<?php echo ($i % 2 == 0) ? "" : "tr_impar"; ?>"> 
                        <td><a onclick="<?php echo $onclick; ?>"><?php echo $e->getApellido() . " " . $e->getNombre(); ?></a></td>
                        <td><?php echo $o[$i]->getFecha(); ?></td>
                        <td><?php echo $o[$i]->getPeriodo(); ?></td>
                        <td><?php echo $o[$i]->getFaltas(); ?></td>
                        <td><?php echo $o[$i]->getComportamiento(); ?></td>
                        <td><?php echo $o[$i]->getDescripcion(); ?></td>
                        <td><?php echo $o[$i]->getAccionPedagogica(); ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php
    }

    public static function printEstudiantesAndObservacions($idcurso, $periodo = "") {
        // $o = ObservacionDAO::getObservacionesByIdCurso($idcurso);
        $estudiantes = EstudianteDAO::getEstudiantesByCurso($idcurso);
        $curso = CursoDAO::getCursoByid($idcurso);
        $o = ObservacionDAO::getObservacionesByCursoAndPeriodo($idcurso, $periodo);
        $_SESSION["curso"] = $idcurso;
        $_SESSION["periodo"] = $periodo;
        ?>
        <table class="table parrafo">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th title="Fecha de la observacion" >Fecha</th>
                    <th title="Periodo">Periodo</th>
                    <th title="Faltas">Faltas</th>
                    <th title="Comportamiento">Comportamiento</th>
                    <th title="Descripcion">Descripcion</th>
                    <th title="Accion Pedagogica">Accion Pedagogica</th>
                </tr>
            </thead>
            <tbody id="tbodyObservacion">
                <?php
                for ($i = 0; $i < count($estudiantes); $i++) {
                    $o = ObservacionDAO::getOnservacionById($estudiantes[$i]->getId(), $curso->getDirector()->getId(), $curso->getId(), $periodo);
                    if ($o == NULL) {
                        $o = new Observacion("", "", "", "", "", "", "", "", "");
                    }
                    ?>
                    <tr  id="tr<?php echo $estudiantes[$i]->getId(); ?>" class="<?php echo ($i % 2 == 0) ? "" : "tr_impar"; ?>"> 
                        <td><a id="a<?php echo $estudiantes[$i]->getId(); ?>" class="edit"><?php echo $estudiantes[$i]->getApellido() . " " . $estudiantes[$i]->getNombre(); ?></a></td>
                        <td><?php echo $o->getFecha(); ?></td>
                        <td><?php echo $o->getPeriodo(); ?></td>
                        <td><?php echo $o->getFaltas(); ?></td>
                        <td><?php echo $o->getComportamiento(); ?></td>
                        <td><?php echo $o->getDescripcion(); ?></td>
                        <td><?php echo $o->getAccionPedagogica(); ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php
    }

    public static function printObservacionesSecret($idcurso, $periodo = "") {
        if ($idcurso == "" || $periodo == "") {
            return;
        }
        $o = ObservacionDAO::getObservacionesByCursoAndPeriodo($idcurso, $periodo);
        $c = CursoDAO::getCursoByid($idcurso);
        ?>
        <h1 class="page-header">Periodo: <?php echo $periodo; ?> Curso: <?php echo $c->getNombre(); ?> 
            <a target="_blank" href="<?php echo Clogin::getHost(); ?>/controlador/CBoletin.php?curso=<?php echo $idcurso; ?>&periodo=<?php echo $periodo; ?>&metodo=printFicha&estu=" type="button" class="btn btn-primary">Imprimir Curso</a>
        </h1>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th title="">Observacion</th>
                        <th title="">Accion Pedagogica</th>
                        <th title="" >Comportamiento</th>
                        <th title="" >Faltas</th>
                        <th>...</th>
                    </tr>
                </thead>
                <tbody id="tbodyObservacion">
                    <?php
                    for ($i = 0; $i < count($o); $i++) {
                        $onclick = 'editObservacion(' . "'" . $o[$i]->getIdestudiante() . "'" . ',' . "'" . $o[$i]->getIdprofe() . "'" . ',' . "'" . $o[$i]->getIdcurso() . "'" . ',' . "'" . $o[$i]->getPeriodo() . "'" . ')';
                        $e = EstudianteDAO::get()->getEstudianteByid($o[$i]->getIdestudiante())
                        ?>
                        <tr id="tr<?php echo $e->getId(); ?>"class="<?php echo ($i % 2 == 0) ? "" : "tr_impar"; ?>"> 
                            <td><a onclick="<?php echo $onclick; ?>"><?php echo $e->getApellido() . " " . $e->getNombre(); ?></a></td>
                            <td class="parrafo" ><?php echo $o[$i]->getDescripcion(); ?></td>
                            <td class="parrafo" ><?php echo $o[$i]->getAccionPedagogica(); ?></td>
                            <td><?php echo $o[$i]->getComportamiento(); ?></td>
                            <td><?php echo $o[$i]->getFaltas(); ?></td>
                            <td>
                                <a target="_blank" href="<?php echo Clogin::getHost(); ?>/controlador/CBoletin.php?curso=<?php echo $idcurso; ?>&periodo=<?php echo $periodo; ?>&metodo=printFicha&estu=<?php echo $o[$i]->getIdestudiante(); ?>"  class="btn btn-primary" >Imprimir</a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function printFormObservacionSelectEstudiante($idcurso) {
        $idclase = ClaseDAO::getIdClaseByCursoId($idcurso);
        $est = EstudianteDAO::getEstudiantesByClaseid($idclase[0]);
        ?>
        <?php
        for ($i = 0; $i < count($est); $i++) {
            ?>
            <option value="<?php echo $est[$i]->getId() ?>" >  <?php echo $est[$i]->getApellido() . " " . $est[$i]->getNombre() ?> </option>
            <?php
        }
        ?>
        <?php
    }

    public static function printFormObservacion($profe = "", $idcur = "", $ide = "", $periodo = "") {
        $op = "";
        $e = new Estudiante("", "", "", "", "", "", "", "", "");
        if ($ide == "") {
            $o = new Observacion("", "", "", "", "", "", "", "");
        } else {
            $o = ObservacionDAO::getOnservacionById($ide, $profe, $idcur, $periodo);
            if ($o == NULL) {
                $o = new Observacion("", "", "", "", "", "", "", "");
            }
            $e = EstudianteDAO::get()->getEstudianteByid($ide);
            $op = '<option value="' . $e->getId() . '">' . $e->getApellido() . " " . $e->getNombre() . '<option>';
        }
        ?>
        <form id="formObservacion">
            <input type="text" name="curso" value="<?php echo $idcur; ?>"id="inputCursoObservacion" >
            <input type="text" name="profe" value="<?php echo $profe; ?>" style="display: none"/>
            <input class="operiodo" type="number" name="periodo" value="<?php echo ($o->getPeriodo() == "") ? $periodo : $o->getPeriodo(); ?>" style="display: none"/> 
            <p>
                <input class="operiodo" type="number" name="period" value="<?php echo ($o->getPeriodo() == "") ? $periodo : $o->getPeriodo(); ?>" disabled=""/>
                <input name="comp"  placeholder="Comportamiento" value="<?php echo $o->getComportamiento(); ?>"  />
                <input name="faltas"  placeholder="Faltas" value="<?php echo $o->getFaltas(); ?>"  />
                <select id="selectEstu" name="estu" ><?php echo $op; ?></select>
            </p>
            <p>
                <textarea class="parrafo" name="descripcion" placeholder="Descripcion de la observacion"><?php echo $o->getDescripcion() ?></textarea>
            </p>
            <p>
                <textarea name="accion" placeholder="Accion Correctiva"><?php echo $o->getAccionPedagogica(); ?></textarea>
            </p>
        </form>
        <?php
    }

    public static function saveObservacion($idestudiante, $idcurso, $idprofe, $periodo, $descripcion, $comportamiento, $accionCorectiva, $faltas = 0) {
        $o = new Observacion($idcurso, $idestudiante, Calendario::getFechaActual(), $idprofe, $descripcion, $periodo, $comportamiento, $accionCorectiva, $faltas);
        $ob = ObservacionDAO::getOnservacionById($o->getIdestudiante(), $o->getIdprofe(), $o->getIdcurso(), $o->getPeriodo());
        if ($ob == NULL) {
            $error = ObservacionDAO::insertar($o);
        } else {
            $error = ObservacionDAO::update($o);
        }
        return "<p id='bad'>$error</p>";
    }

    public static function solicitudes() {
        if (empty($_GET["metodo"])) {
            return;
        }
        $r = $_GET["metodo"];
        if ($r == "printObservaciones") {
            $_SESSION["idcurso"] = $_GET["idcurso"];
            CObservacion::printEstudiantesAndObservacions($_GET["idcurso"], $_GET["periodo"]);
        } elseif ($r == "printFormObservacionSelectEstudiante") {
            CObservacion::printFormObservacionSelectEstudiante($_SESSION["idcurso"]);
        } elseif ($r == "saveObservacion") {
            echo CObservacion::saveObservacion($_POST["estu"], $_POST["curso"], $_POST["profe"], $_POST["periodo"], $_POST["descripcion"], $_POST["comp"], $_POST["accion"], $_POST["faltas"]);
        } else if ($r == "printFormObservacion") {
            CObservacion::printFormObservacion(Clogin::getUser()->getId(), $_GET["idcur"], $_GET["ide"], $_GET["periodo"]);
        } else if ($r == "printFormObservacionAdmin") {
            CObservacion::printFormObservacion($_GET["idprofesor"], $_GET["idcur"], $_GET["ide"], $_GET["periodo"]);
        }
    }

}

CObservacion::solicitudes();
?>