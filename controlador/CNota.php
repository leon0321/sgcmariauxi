<?php
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/NotaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/logica/Calendario.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ClaseDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/FormatoNotaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/PlazoDAO.php';
include_once 'CAsignatura.php';
include_once 'CNotaPrecolar.php';
include_once 'CTool.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cnotas
 *
 * @author leonprintNotasPeriodo
 */
class CNota {

    public static function printNotasPeriodo($idclase, $periodo) {
        $_SESSION["clase"] = $idclase;
        $_SESSION["periodo"] = $periodo;
        $c = PlazoDAO::isPlazo(Calendario::getFechaActual(), Calendario::getYear(), substr($periodo, 4));
        $clase = ClaseDAO::getClaseById($idclase);
        if ($c == 0) {
            if ($clase->getCurso()->getGrado() == 0) {
                CNotaPrecolar::printNotasPeriodoNoEdit($idclase, $periodo);
            } else {
                CNota::printNotasPeriodoNoEdit($idclase, $periodo);
            }
        } else {
            if ($clase->getCurso()->getGrado() == 0) {
                CNotaPrecolar::printNotasPeriodoEdit($idclase, $periodo);
            } else {
                CNota::printNotasPeriodoEdit($idclase, $periodo);
            }
        }
    }

    public static function printNotasPeriodoEdit($idclase, $periodo) {
        $n = NotaDAO::getNotasClasePerido($idclase, $periodo);
        $clase = ClaseDAO::getClaseById($idclase);
        ?>
        <h1 class="page-header">Periodo: <?php echo substr($periodo, 4); ?>  Curso : <?php echo $clase->getCurso()->getId() . " - " . $clase->getAsignatura()->getNombre(); ?></h1>          
        <div class="table-responsive">
            <table class="table" id="tnotaperiodo">
                <thead class="theadnota">
                    <tr>
                <a class="aclass"  href="clase=<?php echo $idclase; ?>&periodo=<?php echo $periodo; ?>" style="display: none"></a>
                <th>Apellido y Nombre</th>
                <th title="Faltas en el periodo" >Faltas <br><a id="faltas">editar</a></th>
                <th title="Trabajo Escrito">Trab esc<br><a id="trabajoEscrito">editar</a></th>
                <th title="Evaluacion Escrita">Eval Esc<br><a id="evaluacionEscrita">editar</a></th>
                <th title="Socializacion">Socia<br><a id="socializacion">editar</a></th>
                <th title="Consulta Biblioteca">Cons bibli<br><a id="consultaBili">editar</a></th>
                <th title="Taller en Clase">Tall-clas<br><a id="tallerClase">editar</a></th>
                <th title="Presentacion Cuaderno">pres-Cuad<br><a id="presCuad">editar</a></th>
                <th title="Tarea en Casa">Tar-Casa<br><a id="tallerCasa">editar</a></th>
                <th title="Nota Formativa">Nota-form<br><a id="notaFormativa">editar</a></th>
                <th title="Evalucaion Periodo">Eval-per<br><a id="evaluacionPeriodo">editar</a></th>
                <th title="NOTA FINAL PERIODO">Nota inal</th>
                </tr>
                </thead>
                <tbody id="tbodyNotas">
                    <?php
                    for ($i = 0; $i < count($n); $i++) {
                        $idestu = $n[$i]->getIdEstu()->getId();
                        ?>
                        <tr id="tr<?php echo $idestu ?>" class="<?php echo ($i % 2 == 0) ? "" : "tr_impar"; ?>">
                            <td class="nombre" ><?php echo $n[$i]->getIdEstu()->getApellido() . " " . $n[$i]->getIdEstu()->getNombre() . " " ?><a id="a<?php echo $idestu; ?>"class="editar">(Editar)</a></td>
                            <td class="faltas"  ><?php echo $n[$i]->getFaltas() ?></td>
                            <td class="trabajoEscrito" ><?php echo ($n[$i]->getTrabajoEscrito() == 0) ? "" : number_format($n[$i]->getTrabajoEscrito(), 1); ?></td>
                            <td class="evaluacionEscrita" ><?php echo ($n[$i]->getEvaluacionEscrita() == 0) ? "" : number_format($n[$i]->getEvaluacionEscrita(), 1); ?></td>
                            <td class="socializacion"  ><?php echo ($n[$i]->getSocializacion() == 0) ? "" : number_format($n[$i]->getSocializacion(), 1); ?></td>
                            <td class="consultaBili" ><?php echo ($n[$i]->getConsultaBili() == 0) ? "" : number_format($n[$i]->getConsultaBili(), 1); ?></td>
                            <td class="tallerClase" ><?php echo ($n[$i]->getTallerClase() == 0) ? "" : number_format($n[$i]->getTallerClase(), 1); ?></td>
                            <td class="presCuad" ><?php echo ($n[$i]->getPresCuad() == 0) ? "" : number_format($n[$i]->getPresCuad(), 1); ?></td>
                            <td class="tallerCasa"><?php echo ($n[$i]->getTallerCasa() == 0) ? "" : number_format($n[$i]->getTallerCasa(), 1); ?></td>
                            <td class="notaFormativa" ><?php echo ($n[$i]->getNotaFormativa() == 0) ? "" : number_format($n[$i]->getNotaFormativa(), 1); ?></td>
                            <td class="evaluacionPeriodo" ><?php echo ($n[$i]->getEvaluacionPeriodo() == 0) ? "" : number_format($n[$i]->getEvaluacionPeriodo(), 1); ?></td>
                            <td class="notaFinal" ><?php echo number_format($n[$i]->getNotaFinal(), 1); ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function printNotasPeriodoNoEdit($idclase, $periodo, $empty = 0) {
        $n = NotaDAO::getNotasClasePerido($idclase, $periodo);
        $clase = ClaseDAO::getClaseById($idclase);
        ?>
        <h1 class="page-header">Periodo: <?php echo substr($periodo, 4); ?>  Curso : <?php echo $clase->getCurso()->getId() . " - " . $clase->getAsignatura()->getNombre(); ?> 
            <a target="_blank" href="<?php echo Clogin::getHost(); ?>/controlador/CBoletin.php?metodo=printPlantillaClase&clase=<?php echo $idclase; ?>&periodo=<?php echo $periodo; ?>" type="button" class="btn btn-primary">Imprimir PLantilla</a>
        </h1>          
        <div class="table-responsive">
            <table class="table tableImprimir" id="tnotaperiodo">
                <thead class="theadnota">
                    <tr>
                <a class="aclass"  href="clase=<?php echo $idclase; ?>&periodo=<?php echo $periodo; ?>" style="display: none"></a>
                <th>Apellido y Nombre</th>
                <th title="Faltas en el periodo" >Faltas</th>
                <th title="Trabajo Escrito">Trab esc</th>
                <th title="Evaluacion Escrita">Eval Esc</th>
                <th title="Socializacion">Socia</th>
                <th title="Consulta Biblioteca">Cons bibl</th>
                <th title="Taller en Clase">Tall-clas</th>
                <th title="Presentacion Cuaderno">pres-Cuad</th>
                <th title="Tarea en Casa">Tar-Casa</th>
                <th title="Nota Formativa">Nota-form</th>
                <th title="Evalucaion Periodo">Eval-per</th>
                <th title="NOTA FINAL PERIODO">Nota inal</th>
                </tr>
                </thead>
                <tbody id="tbodyNotas">
                    <?php
                    for ($i = 0; $i < count($n); $i++) {
                        $idestu = $n[$i]->getIdEstu()->getId();
                        ?>
                        <tr class="<?php echo ($i % 2 == 0) ? "" : "tr_impar"; ?>">
                            <td class="nombre"><?php echo $n[$i]->getIdEstu()->getApellido() . " " . $n[$i]->getIdEstu()->getNombre() ?></td>
                            <td class="faltas"  ><?php echo ($empty == 0) ? $n[$i]->getFaltas() : "" ?></td>
                            <td class="trabajoEscrito" ><?php echo ($empty == 0) ? ($n[$i]->getTrabajoEscrito() == 0) ? "" : number_format($n[$i]->getTrabajoEscrito(), 1)  : ""; ?></td>
                            <td class="evaluacionEscrita" ><?php echo ($empty == 0) ? ($n[$i]->getEvaluacionEscrita() == 0) ? "" : number_format($n[$i]->getEvaluacionEscrita(), 1)  : ""; ?></td>
                            <td class="socializacion"  ><?php echo ($empty == 0) ? ($n[$i]->getSocializacion() == 0) ? "" : number_format($n[$i]->getSocializacion(), 1)  : ""; ?></td>
                            <td class="consultaBili" ><?php echo ($empty == 0) ? ($n[$i]->getConsultaBili() == 0) ? "" : number_format($n[$i]->getConsultaBili(), 1)  : ""; ?></td>
                            <td class="tallerClase" ><?php echo ($empty == 0) ? ($n[$i]->getTallerClase() == 0) ? "" : number_format($n[$i]->getTallerClase(), 1)  : ""; ?></td>
                            <td class="presCuad" ><?php echo ($empty == 0) ? ($n[$i]->getPresCuad() == 0) ? "" : number_format($n[$i]->getPresCuad(), 1)  : ""; ?></td>
                            <td class="tallerCasa"><?php echo ($empty == 0) ? ($n[$i]->getTallerCasa() == 0) ? "" : number_format($n[$i]->getTallerCasa(), 1)  : ""; ?></td>
                            <td class="notaFormativa" ><?php echo ($empty == 0) ? ($n[$i]->getNotaFormativa() == 0) ? "" : number_format($n[$i]->getNotaFormativa(), 1)  : ""; ?></td>
                            <td class="evaluacionPeriodo" ><?php echo ($empty == 0) ? ($n[$i]->getEvaluacionPeriodo() == 0) ? "" : number_format($n[$i]->getEvaluacionPeriodo(), 1)  : ""; ?></td>
                            <td class="notaFinal" ><?php echo ($empty == 0) ? number_format($n[$i]->getNotaFinal(), 1) : ""; ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function printFormNotaPeriodo($idEstudiane, $idclase, $periodo) {
        $n = NotaDAO::getNotaEstudiantePeriodo($idEstudiane, $idclase, $periodo, "");
        ?>
        <input style="display: none" type="text" name="idestu" value="<?php echo $idEstudiane ?>"/>
        <input style="display: none" type="text" name="idclase" value="<?php echo $idclase ?>"/>
        <input style="display: none" type="text" name="periodo" value="<?php echo $periodo ?>"/>
        <td><?php echo $n->getIdEstu()->getNombre() . " " . $n->getIdEstu()->getApellido() . " " ?><a class="botonGuardarNota" id="<?php echo $idEstudiane ?>">(Guardar)</a></td>
        <td><input type="number" name="f" value="<?php echo $n->getFaltas() ?>"/></td>
        <td><input type="number" name="te" value="<?php echo $n->getTrabajoEscrito() ?>"/></td>
        <td><input type="number" name="ee" value="<?php echo $n->getEvaluacionEscrita() ?>"/></td>
        <td><input type="number" name="s" value="<?php echo $n->getSocializacion() ?>"/></td>
        <td><input type="number" name="cb" value="<?php echo $n->getConsultaBili() ?>"/></td>
        <td><input type="number" name="tcla" value="<?php echo $n->getTallerClase() ?>"/></td>
        <td><input type="number" name="pc" value="<?php echo $n->getPresCuad() ?>"/></td>
        <td><input type="number" name="tca" value="<?php echo $n->getTallerCasa() ?>"/></td>
        <td><input type="number" name="nf" value="<?php echo $n->getNotaFormativa() ?>"/></td>
        <td><input type="number" name="ep" value="<?php echo $n->getEvaluacionPeriodo() ?>"/></td>
        <td><?php echo $n->getNotaFinal() ?></td>
        <?php
    }

    public static function printFormularioNotaPeriodo($idEstudiane, $periodo, $idcurso) {
        $n = NotaDAO::getNotaPeriodoEstudianteCurso($idEstudiane, $periodo, $idcurso);
        $estu = EstudianteDAO::get()->getEstudianteByid($n[0]->getIdEstu());
        if ($n == NULL) {
            $n = new NotaPeriodo("", "", "", "", "", "", "", "", "", "", "", "", "", "");
        }
        ?>
        <form id="formularioNotaPeriodo" style="background-color: white">
            <p><b id="nombre"><?php echo $estu->getApellido() . " " . $estu->getNombre(); ?></b></p>
            <input style="display: none" type="text" name="idestu" value="<?php echo $idEstudiane ?>"/>
            <input style="display: none" type="text" name="periodo" value="<?php echo $periodo ?>"/>
            <table class="notasfinales">
                <?php for ($i = 0; $i < count($n); $i++) { ?>
                    <tr>
                        <td><span><?php echo ClaseDAO::getClaseById($n[$i]->getIdclase())->getAsignatura()->getNombre(); ?></span></td>
                        <td><input type="number"  id="<?php echo $n[$i]->getIdclase() ?>" name="nf" value="<?php echo number_format($n[$i]->getNotaFinal(), 1) ?>"/></td>
                    </tr>
                <?php } ?>
            </table>
            <button type="submit" style="display: none">Guardar</button>
        </form>
        <?php
    }

    public static function printNotasEstudiantesCursoToAdmin($idcurso = "", $periodo = "") {
        if ($idcurso == "" || $periodo == "") {
            return;
        }
        $c = CursoDAO::getCursoByid($idcurso);
        $a = AsignaturaDAO::getAsignaturaByCurso($idcurso);
        $es = EstudianteDAO::getEstudiantesByCurso($idcurso);
        $cl = ClaseDAO::getIdClaseByCursoId($idcurso, "ORDER BY `clase`.`asignatura_id_c` ASC");
        ?>
        <h1 class="page-header">Periodo: <?php echo $periodo; ?> Curso: <?php echo $c->getNombre(); ?> 
        </h1>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>..</th>
                        <th>Nombre</th>
                        <?php
                        for ($i = 0; $i < count($a); $i++) {
                            ?>
                            <th><?php echo $a[$i]->getShortName() ?></th>
                            <?php
                        }
                        ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    for ($j = 0; $j < count($es); $j++) {
                        echo '<tr id="tr' . $es[$j]->getId() . '" class="' . (($j % 2 == 0) ? "" : "tr_impar") . '">';
                        echo '<td><a class="btn btn-primary editarnota" id="' . $es[$j]->getId() . '" >Editar</a></td>';
                        echo '<td>' . $es[$j]->getApellido() . $es[$j]->getNombre() . '</td>'
                        . '<div class="divnotas">';
                        for ($i = 0; $i < count($cl); $i++) {
                            ?>
                        <td><?php echo number_format(NotaDAO::getNotaFinalPeriodoEstudianteClase($cl[$i], $periodo, $es[$j]->getId()), 1) ?></td>
                        <?php
                    }
                    echo '</div>'
                    . '</tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function printNotasEstudiantesCurso($idcurso = "", $periodo = "") {
        if ($idcurso == "" || $periodo == "") {
            return;
        }
        $c = CursoDAO::getCursoByid($idcurso);
        $a = AsignaturaDAO::getAsignaturaByCurso($idcurso);
        $es = EstudianteDAO::getEstudiantesByCurso($idcurso);
        $cl = ClaseDAO::getIdClaseByCursoId($idcurso, "ORDER BY `clase`.`asignatura_id_c` ASC");
        ?>
        <h1 class="page-header">Periodo: <?php echo $periodo; ?> Curso: <?php echo $c->getNombre(); ?> 
            <a target="_blank" href="<?php echo Clogin::getHost(); ?>/controlador/CBoletin.php?curso=<?php echo $idcurso; ?>&periodo=<?php echo $periodo; ?>&metodo=generarBoletinCurso" type="button" class="btn btn-primary">Imprimir Curso</a>
        </h1>
        <div class="table-responsive">
            <table class="table table-striped tableImprimir">
                <thead>
                    <tr>
                        <th class="noPrint">..</th>
                        <th>Nombre</th>
                        <?php
                        for ($i = 0; $i < count($a); $i++) {
                            ?>
                            <th><?php echo $a[$i]->getShortName() ?></th>
                            <?php
                        }
                        ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    for ($j = 0; $j < count($es); $j++) {
                        echo '<tr>';
                        echo '<td class="noPrint"><a class="btn btn-primary" href="' . Clogin::getHost() . '/controlador/CBoletin.php?estu=' . $es[$j]->getId() . '&curso=' . $c->getId() . '&periodo=' . $periodo . '&metodo=generarBoletinCurso" target="_blank">Imprimir</a></td>';
                        echo '<td class="n">' . $es[$j]->getApellido() . " " . $es[$j]->getNombre() . '</td>';
                        for ($i = 0; $i < count($cl); $i++) {
                            ?>
                        <td><?php echo number_format(NotaDAO::getNotaFinalPeriodoEstudianteClase($cl[$i], $periodo, $es[$j]->getId()), 1) ?></td>
                        <?php
                    }
                    echo '</tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function printCosolidadoCursoPerido($idcurso = "", $periodo = "", $orden = 1) {
        if ($idcurso == "" || $periodo == "") {
            return;
        }
        $c = CursoDAO::getCursoByid($idcurso);
        $a = AsignaturaDAO::getAsignaturaByCurso($idcurso);
        $es = EstudianteDAO::getEstudiantesByCurso($idcurso);
        $cantidadEstu = count($es);
        $cantidadEstuPerdieron = 0;
        $cantidadMateriasEstus = array();
        $valoracion = ValoracionDAO::getValoracionById($c->getFecha(), $orden);

        $cl = ClaseDAO::getIdClaseByCursoId($idcurso, "ORDER BY `clase`.`asignatura_id_c` ASC");
        ?>
        <h1 class="page-header">Periodo: <?php echo $periodo; ?> Curso: <?php echo $c->getNombre(); ?> 
            <a target="_blank" href="<?php echo Clogin::getHost(); ?>/controlador/CBoletin.php?metodo=printConsolidado&curso=<?php echo $idcurso; ?>&periodo=<?php echo $periodo; ?>&orden=<?php echo $orden; ?>" type="button" class="btn btn-primary">Imprimir Curso</a>
            <?php CTool::printOptionValoracion($c->getFecha(), $orden); ?>
        </h1>
        <div class="table-responsive">
            <table  class="table tableImprimir">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <?php
                        for ($i = 0; $i < count($a); $i++) {
                            $cantidadMateriasEstus[$i] = 0;
                            ?>
                            <th><?php echo $a[$i]->getShortName(); ?></th>
                            <?php
                        }
                        ?>
                        <th>Nº</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    for ($j = 0; $j < count($es); $j++) {
                        $notasPeriodo = array();
                        $perdio = 0;
                        for ($i = 0; $i < count($cl); $i++) {
                            $notasPeriodo[$i] = NotaDAO::getNotaFinalPeriodoEstudianteClase($cl[$i], $periodo, $es[$j]->getId());
                            if ($notasPeriodo[$i] >= $valoracion->getInicial() && $notasPeriodo[$i] < $valoracion->getFinal()) {
                                $perdio++;
                                $cantidadMateriasEstus[$i] ++;
                            } else {
                                $notasPeriodo[$i] = -1;
                            }
                        }
                        if ($perdio > 0) {
                            $cantidadEstuPerdieron++;
                            echo '<tr' . ($i % 2 == 0) ? "" : "tr_impar" . '>';
                            echo '<td class="n">' . $es[$j]->getApellido() . $es[$j]->getNombre() . '</td>';
                            for ($i = 0; $i < count($cl); $i++) {
                                echo "<td>" . (($notasPeriodo[$i] == -1) ? "" : number_format($notasPeriodo[$i], 1)) . "</td>";
                            }

                            echo '<td>' . $perdio . '</td>';
                            echo '</tr>';
                        }
                    }
                    ?>
                <tfoot>
                    <tr>
                        <th>Total: <b><?php echo $cantidadEstuPerdieron . "/" . $cantidadEstu; ?></b></th>
                        <?php
                        for ($i = 0; $i < count($cantidadMateriasEstus); $i++) {
                            echo "<th>" . $cantidadMateriasEstus[$i] . "#<br>" . number_format((($cantidadMateriasEstus[$i] / $cantidadEstu) * 100), 1) . "%</th>";
                        }
                        ?>
                        <th>...</th>
                    </tr>
                </tfoot>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function guadarNotaPerido() {
        $n = new NotaPeriodo($_POST["idestu"], $_POST["idclase"], $_POST["periodo"], 0, $_POST["faltas"], $_POST["tallerCasa"], $_POST["tallerClase"], $_POST["trabajoEscrito"], $_POST["consultaBili"], $_POST["presCuad"], $_POST["evaluacionPeriodo"], $_POST["evaluacionEscrita"], $_POST["socializacion"], $_POST["notaFormativa"]);
        $n->calcularNota();
        $error = NotaDAO::actualizar($n);
        if ($error == "") {
            $idestu = $n->getIdEstu();
            $est = EstudianteDAO::get()->getEstudianteByid($n->getIdEstu());
            return "<p id='ok'>" . number_format($n->getNotaFinal(), 1) . "</p>";
        } else {
            return "<p id='bad'>" . $error . "</p>";
        }
    }

    public static function guadarObservacion($idestu, $idprofe, $observacion, $periodo) {
        NotaDAO::insertarObservacion($idestu, $idprofe, $observacion, $periodo);
    }

    public static function guardarNotasColumna($jsonNotas, $idclase, $periodo, $column) {
        $r = json_decode($jsonNotas, true);
        if (ClaseDAO::getGradoByClaseId($idclase) != 0) {
            for ($i = 0; $i < count($r["notas"]); $i++) {
                $ne = $r["notas"][$i];
                $nota = NotaDAO::getNotaByID($ne["estu"], $idclase, $periodo);
                echo "periodo = " . $nota->getPeriodo() . "  idclase = " . $nota->getIdclase() . " periodoEntrante = " . $periodo . "\n";
                $nota->set($column, $ne["nota"]);
                $nota->calcularNota();
                $error = NotaDAO::actualizar($nota);
                if ($error != "") {
                    echo '<p id="bad" >' . $error . '</p>';
                    return;
                }
            }
            echo "<p id='ok'>Sus Cambios han sido guardados</p>";
        } else {
            CNotaPrecolar::guardarNotasColumna($jsonNotas, $idclase, $periodo, $column);
        }
    }

    public static function guardarNotasFinalesEstu($jsonNotas, $estu, $periodo) {
        $r = json_decode($jsonNotas, true);
        $error = "";
        $n = count($r["notas"]);
        for ($i = 0; $i < $n; $i++) {
            $ne = $r["notas"][$i];
            $error = NotaDAO::actualizarCampos("`nota_final` = '" . $ne["nota"] . "'", $estu, $ne["clase"], $periodo);
            if ($error != "") {
                echo "<p id='bad' >$error</p>";
                return;
            }
        }
        echo "<p id='ok'>ok</p>";
    }

    public static function solicitudes() {
        if (empty($_GET["metodo"])) {
            return;
        }
        $s = $_GET["metodo"];

        if ($s == "printNotasPeriodo") {
            CNota::printNotasPeriodo($_POST["idclase"], $_POST["periodo"]);
        } else if ($s == "printFormNotaPeriodo") {
            CNota::printFormNotaPeriodo($_GET["idestu"], $_GET["idclase"], $_GET["periodo"]);
        } else if ($s == "printFormularioNotaPeriodo") {
            CNota::printFormularioNotaPeriodo($_POST["idestu"], $_POST["ano"] . $_POST["periodo"], $_POST["curso"]);
        } else if ($s == "guadarNotaPerido") {
            echo CNota::guadarNotaPerido();
        } else if ($s == "guadarObservacion") {
            CNota::guadarObservacion($_POST["idestu"], $_POST["idprofe"], $_POST["observacion"], $_POST["periodo"]);
        } else if ($s == "guardarNotasColumna") {
            CNota::guardarNotasColumna($_POST["notas"], $_POST["clase"], $_POST["periodo"], $_POST["column"]);
        } else if ($s == "guardarNotasFinalesEstu") {
            CNota::guardarNotasFinalesEstu($_POST["notas"], $_POST["estu"], $_POST["periodo"]);
        }
    }

}

CNota::solicitudes();
