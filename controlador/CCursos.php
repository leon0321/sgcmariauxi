<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CCursos
 *
 * @author leon
 */
include_once 'Clogin.php';
include_once 'CTool.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/logica/Calendario.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/CursoDAO.php';

class CCursos {

    public static function printCursosDirector($cursos, $idcurso) {
        for ($i = 0; $i < count($cursos); $i++) {
            ?>
            <li id="<?php echo $cursos[$i]->getId() ?>" class="curso <?php echo ($cursos[$i]->getId() == $idcurso) ? "active" : ""; ?>" ><a> <?php echo $cursos[$i]->getNombre() ?> </a></li>
            <?php
        }
    }

    public static function printCursosSecretario($id = "", $year = "") {

        $c = CursoDAO::getCursosByYear($year);
        for ($i = 0; $i < count($c); $i++) {
            ?>
            <li value="<?php echo $c[$i]->getDirector()->getId()?>" id="<?php echo $c[$i]->getId() ?>" class="curso<?php echo ($id == $c[$i]->getId()) ? " active" : ''; ?>" ><a> <?php echo $c[$i]->getNombre() ?> </a></li>
            <?php
        }
    }

    public static function printCursos($year) {
        if ($year == "all") {
            $cursos = CursoDAO::getCursos();
        } else {
            $cursos = CursoDAO::getCursosByYear($year);
        }
        $profe = ProfesorDAO::getProfesores("");
        $select = '';
        for ($i = 0; $i < count($profe); $i++) {
            $select = $select . '<option value="' . $profe[$i]->getId() . '">' . $profe[$i]->getApellido() . ' ' . $profe[$i]->getNombre() . '</option>';
        }
        $select = '<select id="selectDirector" name="profe">' . $select . '</select>';
        ?>
        <div title="Cambiar director de curso"id="divCambiarDirector" style="display: none">
            <form class="onlyselect">
                <?php echo $select; ?> 
            </form>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>Curso</th>
                    <th>Año</th>
                    <th>Codido</th>
                    <th>Grado</th>
                    <th>Director</th>
                    <th>Cambiar Director</th>
                </tr>
            </thead>
            <tbody>
                <?php
                for ($i = 0; $i < count($cursos); $i++) {
                    ?>
                    <tr id="trcurso<?php echo $cursos[$i]->getId(); ?>" class="<?php echo ($i % 2 == 0) ? "" : "tr_impar"; ?>"> 
                        <td onclick="editCurso(<?php echo "'" . $cursos[$i]->getId() . "'"; ?>)" ><a><?php echo $cursos[$i]->getNombre() ?></a></td>
                        <td><?php echo $cursos[$i]->getFecha(); ?></td>
                        <td><?php echo $cursos[$i]->getId(); ?></td>
                        <td><?php echo $cursos[$i]->getGrado(); ?></td>
                        <td class="nombreDirector"><?php echo $cursos[$i]->getDirector()->getApellido() . " " . $cursos[$i]->getDirector()->getNombre(); ?></td>
                        <td>
                            <button class="buttonCambiarDirector" id="<?php echo $cursos[$i]->getId(); ?>" >Cambiar</button>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php
    }

    public static function cambiarDirector($idcurso, $iddirector) {
        $msn = CursoDAO::updateDirector($idcurso, $iddirector);
        if ($msn != "") {
            echo '<p id="error" >' . $msn . '</p>';
            return;
        }
        $d = ProfesorDAO::get()->getProfesorByid($iddirector);
        echo $d->getApellido() . " " . $d->getNombre();
    }

    public static function printFormCurso() {
        $p = ProfesorDAO::getProfesores("");
        ?>
        <form method="post" id="formularioCurso" >
            <div>
                <label>Letra</label>
                <input name="letra"type="text" />
            </div>
            <div>
                <label>Grado</label>
                <select name="grado">
                    <?php
                    $grados = ToolDAO::getGrados();
                    for ($i = 0; $i < count($grados); $i++) {
                        ?> 
                        <option value="<?php echo $grados[$i]["idgrado"] ?>" ><?php echo $grados[$i]["g_nombre"] ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div>
                <label>Director</label>
                <select name="director">
                    <?php
                    for ($i = 0; $i < count($p); $i++) {
                        ?>
                        <option value="<?php echo $p[$i]->getId(); ?>"><?php echo $p[$i]->getApellido() . " " . $p[$i]->getNombre(); ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div>
                <label>Año</label>
                <select name="fecha">
                    <?php
                    $years = BD::getField("periodo", "idperiodo");
                    for ($i = 0; $i < count($years); $i++) {
                        echo '<option value="' . $years[$i] . '">' . $years[$i] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </form>
        <?php
    }

    public static function guadarCurso($director, $grado, $fecha, $letra) {
        if ($fecha == "") {
            return "<p id='bad'>Debe ingresar Año</p>";
        }
        $c = new Curso($grado . $letra . $fecha, $director, $grado, $fecha, $letra, $grado . " - " . $letra, 0);
        $msn = CursoDAO::insertar($c);
        if ($msn != "") {
            return "<p id='bad'>" . $msn . "</p>";
        }
        return "<p id='ok'>ok</p>";
    }

    public static function solicitudes() {
        if (empty($_GET["metodo"])) {
            return;
        }
        $r = $_GET["metodo"];
        if ($r == "printCursosDirector") {
            CCursos::printCursosDirector();
        } else if ($r == "printCursos") {
            CCursos::printCursos();
        } else if ($r == "guadarCurso") {
            echo CCursos::guadarCurso($_POST["director"], $_POST["grado"], $_POST["fecha"], $_POST["letra"]);
        } elseif ($r == "cambiarDirector") {
            CCursos::cambiarDirector($_POST["curso"], $_POST["profe"]);
        }
    }

}

CCursos::solicitudes();
