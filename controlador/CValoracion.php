<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cvaloracion
 *
 * @author leon
 */
include_once realpath(dirname(__FILE__)) . '/../modelo/dto/Valoracion.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ValoracionDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/logica/Calendario.php';

class CValoracion {

    public static function printFormValoracion($year, $orden) {
        $valoracion = ValoracionDAO::getValoracionById($year, $orden);
        $save = false;
        if ($valoracion == null) {
            $valoracion = new Valoracion("", "", "", "", "");
            $save = true;
        }
        ?>
        <h1 class="page-header" style=" margin-left: 20px;">Periodo: <?php echo $orden; ?>º </h1>
        <form id="formularioPeriodo<?php echo $orden; ?>" class="formperiodo" role="form">
            <div style="display: none">
                <input type="text" name="metodo" value="<?php echo ($save) ? "saveValoracion" : "updateValoracion"; ?>"/>
                <input name="year" value="<?php echo $year; ?>"/>
                <input name="periodo" value="<?php echo $orden; ?>"/>
            </div>
            <div id="Botonmes" class="btn-group">
                <input value="<?php echo Calendario::formatToUser($valoracion->getValoracionIncial()); ?>" type="text"  name="fechaInical" placeholder="Fecha Inicial" title="Fecha Inicial"/>
            </div>
            <div id="Botondia" class="btn-group">
                <input value="<?php echo Calendario::formatToUser($valoracion->getValoracionFinal()); ?>" type="text" name="fechafinal" placeholder="Fecha Final" title="Fecha Final"/>
                <div class="respuesta"></div>
            </div>
            <HR>
            <button id="<?php echo $orden; ?>" type="submit" class="btn btn-primary" type="Enviar"style=" margin-left: 100px;">Guardar</button>
        </form>
        <?php
    }

    public static function printValoraciones($year) {
        $valoraciones = ValoracionDAO::getValoracionesByYear($year);
        for ($i = 0; $i < count($valoraciones); $i++) {
            ?>
            <div class="divformhorizontal">
                <form class='formhorizontal'id='<?php echo $valoraciones[$i]->getOrden(); ?>' role="form">
                    <div style="display: none">
                        <input name="year" value="<?php echo $year; ?>">
                        <input name="ordenp" value="<?php echo $valoraciones[$i]->getOrden(); ?>">
                        <input name="metodo" value="updateValoracion">
                    </div>
                    <div>
                        <label>Orden</label><input name="orden" class="form-control" type='number' value='<?php echo $valoraciones[$i]->getOrden(); ?>'/>
                    </div>
                    <div>
                        <label>Inicial</label><input name="inicial" class="form-control" type='number' value='<?php echo $valoraciones[$i]->getInicial(); ?>'/>
                    </div>
                    <div>
                        <label>Final</label><input name="final" class="form-control" type='number' value='<?php echo $valoraciones[$i]->getFinal(); ?>'/>
                    </div>
                    <div>
                        <label>Titulo</label><input name="titulo" class="form-control titu" type='text' value='<?php echo $valoraciones[$i]->getTitulo(); ?>'/>
                    </div>
                    <div>
                        <button id="button<? echo $valoraciones[$i]->getOrden(); ?>" class="btn btn-primary" type="submit">Guardar</button>
                    </div>
                    <div>
                        <button id="delete<? echo $valoraciones[$i]->getOrden(); ?>" class="btn daletebutton" type="button"></button>
                    </div>
                    <div>
                        <p class="respuesta"></p>
                    </div>
                </form>

            </div>
            <?php
        }
    }

    public static function saveValoracion($year, $orden, $inicial, $final, $titulo) {
        $valoracion = new Valoracion($year, $inicial, $final, $orden, $titulo);
        $error = ValoracionDAO::insertar($valoracion);
        if ($error != "") {
            return "<p id='bad'>$error</p>";
        }
        return "<p id='ok'>ok</p>";
    }

    public static function deleteValoracion($year, $orden) {
        $error = ValoracionDAO::delete($year, $orden);
        if ($error != "") {
            return "<p id='bad'>$error</p>";
        }
        return "<p id='ok'>ok</p>";
    }

    public static function updateValoracion($year, $orden, $inicial, $final, $titulo, $ordenp) {
        $valoracion = new Valoracion($year, $inicial, $final, $orden, $titulo);
        $error = ValoracionDAO::update($valoracion, $ordenp);
        if ($error != "") {
            return "<p id='bad'>$error</p>";
        }
        return "<p id='ok'>ok</p>";
    }

    public static function validarIntervaloValoracion(Valoracion $valoracion, $m) {
        $valoraciones = ValoracionDAO::getValoracionesByIntervalo($valoracion->getValoracionIncial(), $valoracion->getValoracionFinal());
        $n = count($valoraciones);
        if ($m == "updateValoracion" && $n >= 2) {
            return ($valoracion->getPeriodo() == $valoraciones[0]->getPeriodo()) ? $valoraciones[1] : $valoraciones[0];
        } elseif ($m == "saveValoracion" && $n >= 1) {
            return $valoraciones[0];
        } elseif ($n == 0) {
            return NULL;
        }
        return NULL;
    }

    public static function validarIntervalo($fechai, $fechaf) {
        $fi = strtotime($fechai);
        $ff = strtotime($fechaf);
        if ($fi >= $ff) {
            return "El inicio del valoracion es mayor o igual que el fin del valoracion";
        }
        return "";
    }

    public static function solictudes() {
        if (empty($_GET["metodo"])) {
            return;
        }
        $r = $_GET["metodo"];
        if ($r == "saveValoracion") {
            echo CValoracion::saveValoracion($_POST["year"], $_POST["orden"], $_POST["inicial"], $_POST["final"], $_POST["titulo"]);
        } elseif ($r == "updateValoracion") {
            echo CValoracion::updateValoracion($_POST["year"], $_POST["orden"], $_POST["inicial"], $_POST["final"], $_POST["titulo"], $_POST["ordenp"]);
        } elseif ($r == "deleteValoracion") {
            echo CValoracion::deleteValoracion($_POST["year"], $_POST["orden"]);
        }
    }

}

CValoracion::solictudes();
