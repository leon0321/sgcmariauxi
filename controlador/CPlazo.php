<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cplazo
 *
 * @author leon
 */
include_once realpath(dirname(__FILE__)) . '/../modelo/dto/Plazo.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/PlazoDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/logica/Calendario.php';

class CPlazo {

    public static function printFormPlazo($year, $periodo) {
        $plazo = PlazoDAO::getPlazoById($year, $periodo);
        $save = false;
        if ($plazo == null) {
            $plazo = new Plazo("", "", "", "");
            $save = true;
        }
        ?>
        <h1 class="page-header" style=" margin-left: 20px;">Periodo: <?php echo $periodo; ?>º </h1>
        <form id="formularioPeriodo<?php echo $periodo; ?>" class="formperiodo" role="form">
            <div style="display: none">
                <input type="text" name="metodo" value="<?php echo ($save) ? "savePlazo" : "updatePlazo"; ?>"/>
                <input name="year" value="<?php echo $year; ?>"/>
                <input name="periodo" value="<?php echo $periodo; ?>"/>
            </div>
            <div id="Botonmes" class="btn-group">
                <input value="<?php echo Calendario::formatToUser($plazo->getPlazoIncial()); ?>" type="text"  name="fechaInical" placeholder="Fecha Inicial" title="Fecha Inicial"/>
            </div>
            <div id="Botondia" class="btn-group">
                <input value="<?php echo Calendario::formatToUser($plazo->getPlazoFinal()); ?>" type="text" name="fechafinal" placeholder="Fecha Final" title="Fecha Final"/>
                <div class="respuesta"></div>
            </div>
            <HR>
            <button id="<?php echo $periodo; ?>" type="submit" class="btn btn-primary" type="Enviar"style=" margin-left: 100px;">Guardar</button>
        </form>
        <?php
    }

    public static function savePlazo($year, $periodo, $inicioPlazo, $finPlazo) {
        $plazo = new Plazo($year, $periodo, Calendario::formatDataBase($inicioPlazo), Calendario::formatDataBase($finPlazo));
        $p = CPlazo::validarIntervaloPlazo($plazo, "savePlazo");
        if ($p != NULL) {
            echo "<p id='error'>El intervalo de tiempo que esta intendo ingresar esta en conflicto con el intervalo del periodo " . $p->getYear() . " - " . $p->getPeriodo() . "</p>";
        } else {
            $error = PlazoDAO::insertar($plazo);
            if ($error == "") {
                echo '<p id="ok">Ok</p>';
            } else {
                echo '<p id="error">' . $error . '</p>';
            }
        }
    }

    public static function updatePlazo($year, $periodo, $inicioPlazo, $finPlazo) {
        $msn = CPlazo::validarIntervalo($inicioPlazo, $finPlazo);
        if ($msn != "") {
            echo "<p>$msn<p>";
            return;
        }
        $plazo = new Plazo($year, $periodo, Calendario::formatDataBase($inicioPlazo), Calendario::formatDataBase($finPlazo));
        $p = CPlazo::validarIntervaloPlazo($plazo, "updatePlazo");
        if ($p != NULL) {
            echo "<p id='error'>El intervalo de tiempo que esta intendo ingresar esta en conflicto con el intervalo del periodo " . $p->getYear() . " - " . $p->getPeriodo() . "</p>";
            return;
        }
        $error = PlazoDAO::update($plazo);
        if ($error == "") {
            echo '<p id="ok">Ok</p>';
        } else {
            echo '<p id="error">' . $error . '</p>';
        }
    }

    public static function validarIntervaloPlazo(Plazo $plazo, $m) {
        $plazos = PlazoDAO::getPlazosByIntervalo($plazo->getPlazoIncial(), $plazo->getPlazoFinal());
        $n = count($plazos);
        if ($m == "updatePlazo" && $n >= 2) {
            return ($plazo->getPeriodo() == $plazos[0]->getPeriodo()) ? $plazos[1] : $plazos[0];
        } elseif ($m == "savePlazo" && $n >= 1) {
            return $plazos[0];
        } elseif ($n == 0) {
            return NULL;
        }
        return NULL;
    }

    public static function validarIntervalo($fechai, $fechaf) {
        $fi = strtotime($fechai);
        $ff = strtotime($fechaf);
        if ($fi >= $ff) {
            return "El inicio del plazo es mayor o igual que el fin del plazo";
        }
        return "";
    }

    public static function solictudes() {
        if (empty($_GET["metodo"])) {
            return;
        }
        $r = $_GET["metodo"];
        if ($r == "savePlazo") {
            CPlazo::savePlazo($_POST["year"], $_POST["periodo"], $_POST["fechaInical"], $_POST["fechafinal"]);
        } elseif ($r == "updatePlazo") {
            CPlazo::updatePlazo($_POST["year"], $_POST["periodo"], $_POST["fechaInical"], $_POST["fechafinal"]);
        }
    }

}

CPlazo::solictudes();
