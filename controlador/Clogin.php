<?php
include_once realpath(dirname(__FILE__)) . '/../modelo/dto/Coordinador.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/dto/Usuario.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/UsuarioDAO.php';
@session_start();
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Clogin
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class Clogin {

    const HOST = "http://localhost/sgcmariauxi";

    public static function iniciarSesion() {
        $ud = new UsuarioDAO();
        $us = $ud->getUsuario($_POST["login"], $_POST["password"], $_POST["tipo"]);
        if ($us == null) {
            Clogin::redireccionar("/index.php?s=bad");
        } else {
            $_SESSION["user"] = serialize($us);
            $_SESSION["url"] = Clogin::HOST;
            Clogin::saveCookie($us);
            Clogin::redireccionar("/index.php?s=ok");
        }
    }

    public static function getHost() {
        if (empty($_SESSION["url"])) {
            return Clogin::HOST;
        } else {
            return $_SESSION["url"];
        }
    }

    public static function startSessionCookie() {
//        if (!Clogin::isSesion()) {
//            if (isset($_COOKIE["user"])) {
//                $us = UsuarioDAO::getUsuarioByCookieId($_COOKIE["user"]);
//                if ($us != NULL) {
//                    $_SESSION["user"] = serialize($us);
//                    return $us->getId();
//                }
//            }else{
//                return "no cookie";
//            }
//        } else {
//            return "true";
//        }
//        return "false";
    }

    public static function saveCookie($us) {
        $expire = time() + 60 * 60 * 24 * 30;
        setcookie("user", $us->getId(), $expire, '/');
    }

    public static function sesion() {
        if (Clogin::isSesion()) {
            $us = Clogin::getUser();
            ?>
            <li ><a href="#" ><?php echo $us->getNombre() . " " . $us->getApellido() ?></a>
                <ul>
                    <li><a onclick="cerrarsesion()" href="#" >Cerrar Sesion</a></li>
                </ul>
            </li>
            <span id="tipousuario"><?php echo Clogin::tipoUsuario($us->getTipo()); ?></span>
            <?php
        } else {
            ?><a href="http://localhost/SIG/vista/login.php">Iniciar Sesion</a><?php
        }
    }

    public static function redirect($location = "") {
        if (Clogin::getHost() != Clogin::HOST) {
            Clogin::redireccionar("/");
        }
        if (Clogin::isSesion()) {
            $us = Clogin::getUser();
            $t = $us->getTipo();
            if ($t == Usuario::PROFESOR && $location != "InsertarNotas") {
                Clogin::redireccionar("/vista/html/InsertarNotas.php");
            } elseif ($t == Usuario::SECRETARIO && $location != "ImprimirBoletines") {
                Clogin::redireccionar("/vista/html/ImprimirBoletines.php");
            } elseif ($t == Usuario::COORDINADOR && $location != "consolidado") {
                Clogin::redireccionar("/vista/html/consolidado.php");
            } elseif ($t == Usuario::ADMIN && $location != "admin") {
                Clogin::redireccionar("/vista/html/Paneladministrativo.php");
            } elseif ($t == Usuario::ESTUDIANTE && $location != "estudiante") {
                Clogin::redireccionar("/vista/html/sesionestudiante.php");
            }
        } else if ($location != "index") {
            Clogin::redireccionar("/");
        }
    }

    public static function redireccionar($url) {
        ?>
        <script>
            location.href = "<?php echo Clogin::getHost() . $url; ?>";
        </script>
        <?php
    }

    public static function tipoUsuario($tipo) {
        if (Usuario::ADMIN == $tipo) {
            return "Admin";
        } else if (Usuario::COORDINADOR == $tipo) {
            return "JefePrograma";
        } else if (Usuario::SECRETARIO == $tipo) {
            return "JefeDepartamento";
        } else if (Usuario::PROFESOR == $tipo) {
            return "Profesor";
        } else if (Usuario::ESTUDIANTE == $tipo) {
            return "Estudiante";
        }
    }

    public static function opcionSession($id) {
        if (Clogin::isSesion()) {
            ?> 
            <li id="aclases" <?php Clogin::select("aclases", $id) ?>><a href="http://localhost/SIG/vista/clases.php">Clases</a></li>
            <?php if (Clogin::getUser()->getTipo() != Usuario::ADMIN) { ?>
                <li id="asol" <?php Clogin::select("asol", $id) ?> ><a href="http://localhost/SIG/vista/solicitud.php">Solicitudes</a></li>
            <?php } ?>
            <li id="ainfo" <?php Clogin::select("ainfo", $id) ?> ><a href="http://localhost/SIG/vista/informe.php">Informes</a></li>
            <?php
        }
    }

    public static function select($id1, $id2) {
        if ($id1 == $id2) {
            echo "class=\"selected\"";
        }
    }

    public static function isSesion() {
        if (!empty($_SESSION["user"])) {
            return true;
        } else {
            return false;
        }
    }

    public static function getUser() {
        return unserialize($_SESSION["user"]);
    }

    public static function cerrar() {
        setcookie("user", "", time() - 3600, '/');
        session_start();
        session_unset();
        session_destroy();
        echo 'cerrado';
    }
    
    public static function logout() {
        setcookie("user", "", time() - 3600, '/');
        session_start();
        session_unset();
        session_destroy();
        Clogin::redireccionar("/index.php");
    }

    public static function cambiarContrasena($username, $currentPassword, $newPassword) {
        $u = Clogin::getUser();
        if ($username != $u->getId()) {
            echo '<p id="error">Sesion incorrecta<p>';
            return;
        }
        $afects = UsuarioDAO::cambiarPassword($username, $currentPassword, $newPassword);
        if ($afects == 1) {
            echo "<p id=\"ok\">Contrasena Cambiada exitosamente</p>";
        } else if ($afects == 0) {
            echo "<p id=\"error\" >verifique los datos ingresados</p>";
        }
    }

    public static function solicitudes() {
        if (empty($_GET["metodo"])) {
            return;
        }

        $s = $_GET["metodo"];

        if ($s == "login") {
            Clogin::iniciarSesion();
        } else if ($s == "cerrar") {
            Clogin::cerrar();
        } else if ($s == "logout") {
            Clogin::logout();
        } else if ($s == "cambiarContrasena") {
            Clogin::cambiarContrasena($_POST["username"], $_POST["pass"], $_POST["newpass1"]);
        }
    }

    public static function Opcionesadmin() {
        if (!Clogin::isSesion()) {
            return;
        }if (Clogin::getUser()->getTipo() != Usuario::ADMIN) {
            return;
        }
        ?>
        <li><a href="#">Opciones</a>
            <ul>
                <li><a href="http://localhost/SIG/vista/profesor.php" >Profesores</a></li>
                <li><a href="#">Programas</a></li>
                <li><a href="http://localhost/SIG/vista/jefe.php">Jefes de Programas</a></li>
                <li><a href="#">Sedes</a></li>
                <li><a href="http://localhost/SIG/vista/estudiante.php">Estudiantes</a></li>
                <li><a href="http://localhost/SIG/vista/asignatura.php" >Asignatura</a></li>
                <li><a href="http://localhost/SIG/vista/supervisor.php">Supervisor</a></li>
                <li><a href="http://localhost/SIG/vista/clase.php">Clases</a>                                   
                    <ul>
                        <li><a class="nuevaClase" >Nueva</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <?php
    }

}

echo Clogin::startSessionCookie();
Clogin::solicitudes();
?>
