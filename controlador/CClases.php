<?php
include_once 'Clogin.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ClaseDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/AsistenciaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/AsignaturaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/EstudianteDAO.php';
include_once 'CInforme.php';
include_once 'CTool.php';

class CClases {

    public static function printClases($maxYear) {
        if (!Clogin::isSesion()) {
            return;
        }
        $us = Clogin::getUser();
        $clases = array();
        $tipo = "";
        if ($us->getTipo() == Usuario::PROFESOR) {
            $clases = ClaseDAO::getClasesByProfesorId($us->getId(), $maxYear);
            $tipo = Usuario::PROFESOR;
        } else if ($us->getTipo() == Usuario::SECRETARIO) {
            $clases = ClaseDAO::getClasesBySupervisor($us->getId());
            $tipo = Usuario::SECRETARIO;
        } else if ($us->getTipo() == Usuario::ADMIN) {
            $tipo = Usuario::ADMIN;
            $clases = ClaseDAO::getClases();
        }

        for ($i = 0; $i < count($clases); $i++) {
            $a = $clases[$i]->getAsignatura();
            $c = $clases[$i]->getCurso();
            ?>
            <li id="liClase<?php echo $clases[$i]->getId(); ?>" onclick="notas(<?php echo $clases[$i]->getId() ?>,<?php echo "'" . $c->getNombre() . " - " . $a->getNombre() . "'"; ?>)" ><a><?php echo $c->getNombre() . " - " . $a->getNombre() ?> </a></li>
            <?php
        }
    }

    public static function printClasesLi($clases, $idclase = "") {
        if ($idclase == "") {
            $idclase = $clases[0]->getId();
        }
        for ($i = 0; $i < count($clases); $i++) {
            $a = $clases[$i]->getAsignatura();
            $c = $clases[$i]->getCurso();
            ?>
            <li id="liClase<?php echo $clases[$i]->getId(); ?>" <?php echo ($clases[$i]->getId() == $idclase) ? "class='active'" : ""; ?>  ><a><?php echo $clases[$i]->getCurso()  . " - " . $a->getNombre() ?> </a></li>
            <?php
        }
    }

    public static function cambiarProfe($idclase, $idprofe) {
        $msn = ClaseDAO::actualizarClase($idclase, $idprofe);
        if ($msn != "") {
            echo "<p>" . $msn . "</p>";
            return;
        }
        echo "<p>ok</p>";
    }

    public static function printClasesInfo() {
        if (!Clogin::isSesion()) {
            return;
        }
        $us = Clogin::getUser();
        $clases = array();
        $tipo = "";
        if ($us->getTipo() == Usuario::PROFESOR) {
            $clases = ClaseDAO::getClasesByProfesorId($us->getId());
            $tipo = Usuario::PROFESOR;
        } else if ($us->getTipo() == Usuario::SECRETARIO) {
            $clases = ClaseDAO::getClasesBySupervisor($us->getId());
            $tipo = Usuario::SECRETARIO;
        } else if ($us->getTipo() == Usuario::ADMIN) {
            $tipo = Usuario::ADMIN;
            $clases = ClaseDAO::getClases();
        }
        ?>
        <div class="tablaScroll">
            <table class="tablaAdmin">
                <thead>
                    <tr><th>Asignatura</th><?php echo ($tipo != Usuario::PROFESOR) ? "<th>Profesor</th>" : ""; ?><th title="Horas Dictadas">HD</th><th title="Horas Esperadas">HE</th><th>Asistencia</th></tr>
                </thead>
                <tbody>
                    <?php
                    for ($i = 0; $i < count($clases); $i++) {
                        $a = $clases[$i]->getAsignatura();
                        $p = $clases[$i]->getProfesor();
                        $horas[0] = AsistenciaDAO::getHorasDictadasActual($clases[$i]->getId());
                        $horas[1] = AsistenciaDAO::getHorasEsperadasActual($clases[$i]->getId());
                        ?>
                        <tr> 
                            <td><?php echo "(" . $clases[$i]->getPeriodo() . ") " . $a->getNombre(); ?></td>
                            <?php if ($tipo != Usuario::PROFESOR) {
                                ?>
                                <td><?php echo $p->getApellido() . " " . $p->getNombre() . " - " . $p->getId(); ?></td>
                            <?php } ?>
                            <td><?php echo ($horas[0] + 0); ?></td>
                            <td><?php echo $horas[1]; ?></td>
                            <td>
                                <?php
                                $por = ($horas[0] / $horas[1]);
                                CInforme::printBandera($por);
                                echo number_format($por * 100, 2) . "%";
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function printClasesToAdmin($year) {

        if ($year == "all") {
            $clases = ClaseDAO::getClases();
        } else {
            $clases = ClaseDAO::getClasesByYear($year);
        }
        $profe = ProfesorDAO::getProfesores("");
        $select = '';
        for ($i = 0; $i < count($profe); $i++) {
            $select = $select . '<option value="' . $profe[$i]->getId() . '">' . $profe[$i]->getApellido() . ' ' . $profe[$i]->getNombre() . '</option>';
        }
        $select = '<select name="profe">' . $select . '</select>';
        ?>
        <div title="Cambiar Profesor"  style="display: none" id="divCambiarProfe">
            <form id="formularioCambiarProfe" class="onlyselect">
                <?php echo $select ?>
            </form>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>Grado</th>
                    <th>Curso</th>
                    <th>Año</th>
                    <th>Asignatura</th>
                    <th>Profesor</th>
                    <th>Cambiar Profesor</th>
                </tr>
            </thead>
            <tbody>
                <?php
                for ($i = 0; $i < count($clases); $i++) {
                    $a = $clases[$i]->getAsignatura();
                    ?>
                    <tr class="<?php echo ($i % 2 == 0) ? "" : "tr_impar"; ?>" id="tr<?php echo $clases[$i]->getId(); ?>"> 
                        <td><?php echo $clases[$i]->getCurso()->getGrado(); ?></td>
                        <td><?php echo $clases[$i]->getCurso()->getId(); ?></td>
                        <td><?php echo $clases[$i]->getPeriodo(); ?></td>
                        <td><?php echo $clases[$i]->getAsignatura()->getNombre(); ?></td>
                        <td class="nombreprofe"><?php echo $clases[$i]->getProfesor()->getApellido() . " " . $clases[$i]->getProfesor()->getNombre() ?></td>
                        <td> 
                            <button class="buttonCambiarProfe" id="<?php echo $clases[$i]->getId(); ?>" >Cambiar</button>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php
    }

    public static function printTablaEstudiantes($idclase) {
        $estudiantes = EstudianteDAO::getEstudiantesByClaseid($idclase);
        ?>
        <div id="contTablaEst1">
            <table id="tablaEstudiante">
                <tfoot>
                    Estudiantes
                <thead>
                    <tr><th>Nombres</th><th>Telefono</th><th>Correo</th><th>Codigo</th></tr>
                </thead>
                </tfoot>
                <tbody>
                    <?php
                    for ($i = 0; $i < count($estudiantes); $i++) {
                        ?>
                        <tr> 
                            <td><?php echo $estudiantes[$i]->getNombre() . " " . $estudiantes[$i]->getApellido(); ?></td>
                            <td><?php echo $estudiantes[$i]->getTelefono() ?></td>
                            <td><?php echo $estudiantes[$i]->getCorreo() ?></td>
                            <td><?php echo $estudiantes[$i]->getId() ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function printTablaAsistencia($idclase) {
        $clase = CClases::getClase($idclase);
        $asist = AsistenciaDAO::getAsistenciasByClaseId($clase->getId());
        ?>
        <table  id="tablaAsistencia" style="overflow-y:scroll" >
            <tfoot>
                Tabla Asitencia <?php echo $clase->getAsignatura()->getNombre() ?>
            <thead>
                <tr><th>Fecha de Clase</th><th>Hora</th><th>Horas D</th><th>Estado</th><th>Tema</th><th>Horas E</th></tr>
            </thead>
        </tfoot>
        <tbody>
            <?php
            for ($i = 0; $i < count($asist); $i++) {
                ?>
                <tr onclick="pfa('<?php echo $asist[$i]->getFechaClase() ?>', <?php echo $idclase ?>)" > 
                    <td><?php echo Calendario::format("D d M Y", $asist[$i]->getFechaClase()) ?></td>
                    <td><?php echo Calendario::format("H:i", $asist[$i]->getFechaClase()) . " - " . Calendario::sumarT("H:i", $asist[$i]->getFechaClase(), ($asist[$i]->getHoras() * 60 * 50)); ?></td>
                    <td><?php echo $asist[$i]->getHorasDictadas() ?></td>
                    <td><?php echo $asist[$i]->getEstado() ?></td>
                    <td><?php echo $asist[$i]->getTema() ?></td>
                    <td><?php echo $asist[$i]->getHoras() ?></td>
                </tr>
                <?php
            }
            ?>
        </tbody>
        </table>
        <?php
    }

    public static function printForm() {
        ?>
        <div id="divFormuClase" class="formularioClase" title="Formulario  Clase" style="display: none">
            <form method="post" id="formularioClase" >
                <div>
                    <label>Año</label>
                    <select id="selectYear" name="periodo">
                        <option></option>
                        <?php
                        $years = BD::getField("periodo", "idperiodo");
                        for ($i = 0; $i < count($years); $i++) {
                            echo '<option value="' . $years[$i] . '">' . $years[$i] . '</option>';
                        }
                        ?>
                    </select>  
                </div>
                <div>
                    <label>Curso</label>
                    <select id="selectCurso" name="curso">
                    </select>
                </div>
                <div>
                    <label>Asignatura</label>
                    <select id="selectAsignatura" name="asignatura">
                    </select>
                </div>
                <div>
                    <label>Profesor</label>
                    <select name="profesor">
                        <?php
                        $profes = ProfesorDAO::getProfesores();
                        for ($i = 0; $i < count($profes); $i++) {
                            ?><option  value="<?php echo $profes[$i]->getId(); ?>"><?php echo $profes[$i]->getApellido() . " " . $profes[$i]->getNombre(); ?></option><?php
                        }
                        ?>  
                    </select>
                </div>
                <div>
                    <label>Estado</label>
                    <select name="estado">
                        <option value="0">Activa</option>
                        <option value="1">Inactiva</option>   
                    </select>
                </div>
            </form>
        </div>
        <?php
    }

    public static function getClase($id) {
        if (empty($_SESSION["clase"])) {
            $clase = ClaseDAO::getClaseById($id);
            $_SESSION["clase"] = serialize($clase);
            return $clase;
        }
        $clase = unserialize($_SESSION["clase"]);
        if ($clase->getId() != $id) {
            $clase = ClaseDAO::getClaseById($id);
            $_SESSION["clase"] = serialize($clase);
            return $clase;
        }
        return $clase;
    }

    public static function getClaseF() {
        return unserialize($_SESSION["clase"]);
    }

    public static function isClase() {
        if (!empty($_SESSION["user"])) {
            return true;
        } else {
            return false;
        }
    }

    public static function guardarClase2($curso, $profesor, $estado, $periodo, $asignatura, $area) {
        $clase = new Clase(NULL, NULL, $profesor, NULL, $area, "1", $asignatura, NULL, $estado, $periodo, $curso);
        $msn = ClaseDAO::insertarO($clase);
        if ($msn != "") {
            echo "<p>" . $msn . "</p>";
            return;
        }
        $idclase = ClaseDAO::getUltima();

        echo "<p>ok</p>";
    }

    public static function guadarClase() {
        $msn = ClaseDAO::insertar($_POST);
        if ($msn != "") {
            echo "<h1>" . $msn . "</h1><h2>hola1</h2>";
            return;
        }

        $clase = ClaseDAO::getUltima();
        $i = 0;
        $campos = array();
        foreach ($_POST as $value) {
            if ($i > 7) {
                $campos[] = $value;
            }
            $i++;
        }

        $horas = array();
        for ($i = 0; $i < count($campos); $i = $i + 4) {
            $fechass[] = $campos[$i + 1] . "  " . $campos[$i + 2];
            $horas[] = $campos[$i + 3];
            $msn = HoraClaseDAO::get()->insertar(new HoraClase(NULL, $clase->getId(), $campos[$i + 1], $campos[$i + 2], $campos[$i + 3], $campos[$i]));

            if ($msn != "") {
                ClaseDAO::eliminar($clase->getId());
                echo "<h1>" . $msn . "</h1>";
                return;
            }
        }
        $nh = count($horas);
        $fechass = Calendario::get($fechass, $_POST["fechafinal"]);
        $nf = count($fechass);

        echo "nclases: " . $nf . "\n";
        echo "horas: " . $nh . "\n";
        echo "divicion: " . $nf / $nh . "\n";

        $horass = array();
        for ($i = 0; $i < $nf / $nh; $i++) {
            for ($j = 0; $j < $nh; $j++) {
                echo $horas[$j] . "\n";
                $horass[] = $horas[$j];
            }
        }

        $msn = AsistenciaDAO::insertAssistencias($fechass, $clase->getId(), $horass);
        if ($msn != "") {
            ClaseDAO::eliminar($clase->getId());
            echo "<h1>" . $msn . "</h1>";
            return;
        }
        echo "<h1>ok</h1>";
    }

    public static function eliminarClase() {
        $id = $_POST["id"];
        ClaseDAO::eliminar($id);
    }

    public static function requests() {
        if (empty($_GET["metodo"])) {
            return;
        }
        $r = $_GET["metodo"];
        if ($r == "printTablaEstudiantes") {
            if (Clogin::getUser()->getTipo() != Usuario::PROFESOR) {
                CClases::printTablaEstudiantes($_POST["idclase"]);
            }
        } else if ($r == "printForm") {
            CClases::printForm();
        } else if ($r == "guadarClase") {
            CClases::guardarClase2($_POST["curso"], $_POST["profesor"], $_POST["estado"], $_POST["periodo"], $_POST["asignatura"], $_POST["area"]);
        } else if ($r == "eliminarClase") {
            CClases::eliminarClase();
        } else if ($r == "cambiarProfe") {
            CClases::cambiarProfe($_POST["clase"], $_POST["profe"]);
        }
    }

}

CClases::requests();
?>
