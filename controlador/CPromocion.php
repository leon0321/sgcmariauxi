<?php
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/CursoDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/AsignaturaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/EstudianteDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ClaseDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/MatriculaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/NotaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/FormatoNotaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ToolDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/logica/Tool.php';


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CPromocion
 *
 * @author Leonardo ortega hernandez
 */
class CPromocion {

    public static function printNotasEstudiantes($idcurso = "", $year = "") {
        if ($idcurso == "" || $year == "") {
            return;
        }
        $c = CursoDAO::getCursoByid($idcurso);
        $a = AsignaturaDAO::getAsignaturaByCurso($idcurso);
        $es = EstudianteDAO::getEstudiantesByCurso($idcurso);
        $cl = ClaseDAO::getIdClaseByCursoId($idcurso, "ORDER BY `clase`.`asignatura_id_c` ASC");
        $formatoNota = FormatoNotaDAO::getFormatoNotaByYear($year);
        ?>
        <h1 class="page-header">Año: <?php echo $year; ?> Curso: <?php echo $c->getNombre(); ?>
            <button type="button" class="btn btn-primary buttonPromover" id="<?php echo $idcurso ?>">Promover Seleccionados</button>
        </h1>
        <div class="table-responsive">
            <table class="table" >
                <thead>
                    <tr>
                        <th>Promo</th>
                        <th>Nombre</th>
                        <?php
                        for ($i = 0; $i < count($a); $i++) {
                            ?>
                            <th><?php echo $a[$i]->getShortName() ?></th>
                            <?php
                        }
                        ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    for ($j = 0; $j < count($es); $j++) {
                        echo '<tr id="tr' . $es[$j]->getId() . '" >';
                        echo '<td><input type="checkbox" id="input' . $es[$j]->getId() . '" /></td>';
                        echo '<td class="nombre">' . $es[$j]->getApellido() . $es[$j]->getNombre() . '</td>'
                        . '<div class="divnotas">';
                        for ($i = 0; $i < count($cl); $i++) {
                            $notas = NotaDAO::getNotasEstudianteClase($es[$j]->getId(), $cl[$i]);
                            $notaFinalAno = Tool::calcularNotaAno($notas, $formatoNota->getPorcent());
                            ?>
                        <td><?php echo number_format($notaFinalAno["nota"], 1); ?></td>
                        <?php
                    }
                    echo '</div>'
                    . "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function promoverGrado($grado, $json_estudiantes) {
        $json = json_decode($json_estudiantes, true);
        $m ='';
        $ok = '';
        $bad = '';
        for ($i = 0; $i < count($json["estudiantes"]); $i++) {
            $idestu = $json["estudiantes"][$i]["id"];
            $estudiante = EstudianteDAO::getEstudiantById($idestu);
            if ($grado > 11){
                return "<p id='bad'>No existe grado superior al cual promover</p>";
            }
            if ($grado == ($estudiante->getGrado() + 1)){
                EstudianteDAO::updateGrado($idestu, $grado);
                $ok .= "<tr class='green'><td>" . $estudiante->getApellido() . " " . $estudiante->getNombre() . "</td></tr>";
            }else{
                $bad .= "<tr class='red'><td>" . $estudiante->getApellido() . " " . $estudiante->getNombre() . "</td></tr>";
            }            
            if (BD::error() != "") {
                return "<p id='bad' >" . BD::error() . "</p>";
            }
        }
        if ($ok != ''){
            $m .= "<p class='black' >Estudiantes promodivos al grado " . $grado . "º.</b><table class'table'><tbody>" . $ok ."</tbody></table>";
        }
        if ($bad != ''){
            $m .= "<p class='black' >Estudiantes no promodivos al grado " . $grado . "º porque ya se encuentran en un grado igual o  mayor</b><table class'table'><tbody>" . $bad ."</tbody></table>";
        }
        return "<div id='ok' class='black'>".$m."</div>";
    }

    public static function promoverAnt($idcurso, $jsonEstudiantes) {
        $r = json_decode($jsonEstudiantes, true);
        $curso = CursoDAO::getCursoByid($idcurso);
        if ($curso == NULL) {
            return "<p id='bad'>No existe curso superior para promover<p>";
        }
        $idclases = ClaseDAO::getIdClaseByCursoId($curso->getId());
        if (count($idclases) == 0) {
            return "<p id='bad'>El Curso Selecconado no posee clases definidas<p>";
        }

        $fn = FormatoNotaDAO::getFormatoNotaByYear($curso->getFecha());
        for ($i = 0; $i < count($r["estudiantes"]); $i++) {
            $idestu = $r["estudiantes"][$i];
            $error = MatriculaDAO::matricularCurso($idcurso, $idestu["id"]);
            if ($error != "") {
                return "<p id='bad'>$error</p>";
            }
            EstudianteDAO::updateGrado($idestu["id"], $curso->getGrado());
            $sqlNota = MatriculaDAO::getSQLmatricularClasesEstudiantes($idclases, array($idestu["id"]), $curso->getFecha(), $fn->getNPeridos(), 1);
            BD::open();
            BD::sentenceSQL($sqlNota);
            if (BD::error() != "") {
                return "<p id='bad'>" . BD::error() . "</p>";
            }
        }
        return "<p id='ok' >ok</p>";
    }

    public static function printCursosDisponibles($idcurso) {
        $curso = CursoDAO::getCursoByid($idcurso);
        $cursos = CursoDAO::getCursosByGradoId(($curso->getGrado() + 1), $curso->getFecha());
        ?>
        <form id="formularioCursosDisponibles">
            <div>
                <h3>Estudiantes Selecionados</h3>
                <div class="estudiantesSeleccionados"></div>
                <h3>Seleccione el curso al cual sera promovido</h3>
            </div>
            <span>Curso disponibles para promocion</span><br>
            <select name="curso">
                <?php
                for ($i = 0; $i < count($cursos); $i++) {
                    echo "<option value='" . $cursos[$i]->getId() . "'>" . $cursos[$i]->getId() . " - " . $cursos[$i]->getDirector()->getApellido() . "</option>";
                }
                ?>
            </select>
        </form>
        <?php
    }

    public static function printGradoSuperior($idcurso) {
        $curso = CursoDAO::getCursoByid($idcurso);
        $grado = $curso->getGrado();
        $grados = ToolDAO::getGrados();
        for ($i = 0; $i < count($grados); $i++) {
            if (($grado + 1) == $grados[$i]["idgrado"]) {
                return "<p class='msn' id='ok' >" . ($grado + 1) . "</p>";
            }
        }
        return "<p class='msn' id='bad' >No Existe un grado superior al cual promover</p>";
    }

    public static function solicitudes() {
        if (empty($_GET["metodo"])) {
            return;
        }
        $r = $_GET["metodo"];
        if ($r == "promoverGrado") {
            echo CPromocion::promoverGrado($_POST["grado"], $_POST["jsonestudiantes"]);
        } elseif ($r == "printCursosDisponibles") {
            CPromocion::printCursosDisponibles($_POST["curso"]);
        } elseif ($r == "promoverAnt") {
            echo CPromocion::promoverAnt($_POST["curso"], $_POST["estu"]);
        } elseif ($r == "printGradoSuperior") {
            echo CPromocion::printGradoSuperior($_GET["curso"]);
        }
    }

}

CPromocion::solicitudes();
