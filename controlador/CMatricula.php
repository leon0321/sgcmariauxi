<?php
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/EstudianteDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ToolDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/CursoDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ClaseDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/FormatoNotaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/MatriculaDAO.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CMatricula
 *
 * @author leon
 */
class CMatricula {

    public static function matricular(Curso $curso, $idclases, FormatoNota $fn, Estudiante $estudiante) {
        if ($curso == NULL) {
            return "<p class='msn' id='bad'>No es posible matricular porque no existe el curso $idcurso</p>";
        }

        if (count($idclases) == 0) {
            return "<p class='msn' id='bad'>El Curso Selecconado no posee clases definidas</p>";
        }

        $error = MatriculaDAO::matricularCurso($curso->getId(), $estudiante->getId());
        if ($error != "") {
            return "<p class='msn' id='bad'>No fue posible matricular al estudiante " . $estudiante->getApellido() . " " . $estudiante->getNombre() . ""
                    . "Es posible que ya se encuntre matriculado en el curso selecionado ($error)</p>";
        }

        $sqlNota = MatriculaDAO::getSQLmatricularClasesEstudiantes($idclases, array($estudiante->getId()), $curso->getFecha(), $fn->getNPeridos(), 1);
        BD::open();
        BD::sentenceSQL($sqlNota);
        if (BD::error() != "") {
            return BD::error("<p class='msn' id='bad'>No se pudo crear las notas por defecto</p>");
        }
        return "<p class='msn' id='ok'>Estudiante Matriculado Exitosamente</p>";
    }

    public static function guadarEstudianteAndMatricular(Estudiante $estudiante, Curso $curso) {
        if ($curso == NULL) {
            return "<p class='msn' id='bad'>No es posible matricular porque no existe el curso $idcurso</p>";
        }

        $idclases = ClaseDAO::getIdClaseByCursoId($curso->getId());
        if (count($idclases) == 0) {
            return "<p class='msn' id='bad'>El Curso Selecconado no posee clases definidas</p>";
        }

        $msn = EstudianteDAO::get()->insertar($estudiante);
        if ($msn != "") {
            return "<p class='msn' id='bad'>" . $msn . "</p>";
        }

        $fn = FormatoNotaDAO::getFormatoNotaByYear($curso->getFecha());
        $error = MatriculaDAO::matricularCurso($curso->getId(), $estudiante->getId());
        if ($error != "") {
            return "<p class='msn' id='bad'>No fue posible matricular al estudiante " . $estudiante->getApellido() . " " . $estudiante->getNombre() . "Es posible que ya se encuntre matriculado en el curso selecionado ($error)</p>";
        }

        $sqlNota = MatriculaDAO::getSQLmatricularClasesEstudiantes($idclases, array($estudiante->getId()), $curso->getFecha(), $fn->getNPeridos(), 1);
        BD::open();
        BD::sentenceSQL($sqlNota);
        if (BD::error() != "") {
            return BD::error("<p class='msn' id='bad'>No se pudo crear las notas por defecto</p>");
        }
        return "<p class='msn' id='ok'>Estudiante Matriculado Exitosamente</p>";
    }

    public static function MatricularEstudiantes($jsonEstudiantes, $idcurso) {
        $r = json_decode($jsonEstudiantes, true);
        $curso = CursoDAO::getCursoByid($idcurso);
        if ($curso == NULL) {
            return "<p id='bad'>No existe curso superior para promover<p>";
        }

        $idclases = ClaseDAO::getIdClaseByCursoId($curso->getId());
        if (count($idclases) == 0) {
            return "<p id='bad'>El Curso Selecconado no posee clases definidas<p>";
        }

        $fn = FormatoNotaDAO::getFormatoNotaByYear($curso->getFecha());
        for ($i = 0; $i < count($r["estudiantes"]); $i++) {
            $idestu = $r["estudiantes"][$i];
            $error = MatriculaDAO::matricularCurso($idcurso, $idestu["id"]);
            if ($error != "") {
                return "<p id='bad'>$error</p>";
            }
            EstudianteDAO::updateGrado($idestu["id"], $curso->getGrado());
            $sqlNota = MatriculaDAO::getSQLmatricularClasesEstudiantes($idclases, array($idestu["id"]), $curso->getFecha(), $fn->getNPeridos(), 1);
            BD::open();
            BD::sentenceSQL($sqlNota);
            if (BD::error() != "") {
                return "<p id='bad'>" . BD::error() . "</p>";
            }
        }
        return "<p id='ok' >ok</p>";
    }

    public static function printCursosDisponibles($grado, $year) {
        $cursos = CursoDAO::getCursosByGradoId($grado, $year);
        ?>
        <form id="formularioCursosDisponibles">
            <div>
                <h3>Estudiantes Selecionados</h3>
                <div class="estudiantesSeleccionados"></div>
                <h3>Seleccione el curso al sera matriculado</h3>
            </div>
            <span>Curso disponibles para matricula</span><br>
            <select name="curso">
                <?php
                for ($i = 0; $i < count($cursos); $i++) {
                    echo "<option value='" . $cursos[$i]->getId() . "'>" . $cursos[$i]->getId() . " - " . $cursos[$i]->getDirector()->getApellido() . "</option>";
                }
                if(count($cursos) == 0){
                    echo "<p id='bad'>No existen cursos disponibles</p>";
                }
                ?>
            </select>
        </form>
        <?php
    }

    public static function solicitudes() {
        if (empty($_GET["metodo"])) {
            return;
        }
        $r = $_GET["metodo"];
        if ($r == "printCursosDisponibles") {
            CMatricula::printCursosDisponibles($_POST["grado"], $_POST["year"]);
        } elseif ($r == "MatricularEstudiantes") {
            echo CMatricula::MatricularEstudiantes($_POST["estu"], $_POST["curso"]);
        }
    }

}

CMatricula::solicitudes();
