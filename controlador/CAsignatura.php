<?php
include_once 'Clogin.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/AsignaturaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/AreaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ToolDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/dto/Area.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CEstudiante
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class CAsignatura {

    public static function printAsignaturas() {
        $orden = "";
        $fila = "";
        $em = empty($_GET);
        if (empty($_GET)) {
            $orden = "DESC";
            $fila = "a_nombre";
        } else {
            $orden = $_GET["orden"];
            $fila = $_GET["fila"];
        }

        $as = AsignaturaDAO::getAsignaturas("ORDER BY  `asignatura`.`$fila` $orden");
        ?>
        <table class="table">
            <thead>
                <tr>
                    <th>Asinatura</th>
                    <th>Corto</th>
                    <th>Codigo</th>
                    <th>Area</th>
                    <th>Grado</th>
                    <th>Intensidad</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <?php
            for ($i = 0; $i < count($as); $i++) {
                ?>
                <tr id="tra<?php echo $as[$i]->getId(); ?>" class="<?php echo ($i % 2 == 0) ? "" : "tr_impar"; ?>"> 
                    <td><a class="nombreAsig" onclick="editAsignatura(<?php echo "'" . $as[$i]->getId() . "'"; ?>)"><?php echo ($as[$i]->getNombre() == "") ? "..." : $as[$i]->getNombre(); ?></a></td>
                    <td><?php echo $as[$i]->getShortName(); ?></td>
                    <td><?php echo $as[$i]->getCodigo(); ?></td>
                    <td><?php echo AreaDAO::get()->getAreaByid($as[$i]->getIdArea())->getNombre(); ?></td>
                    <td><?php echo $as[$i]->getGrado() . '°'; ?></td>
                    <td><?php echo $as[$i]->getIntensidad(); ?></td>
                    <td><button title="Eliminar Asignatura"id="delete<? echo $as[$i]->getId(); ?>" class="btn daletebutton" type="button"></button></td>
                </tr>
                <?php
            }
            ?>
        </tbody>
        </table>
        <?php
    }

    public static function printTr(Asignatura $a) {
        ?>
        <td><a onclick = "editAsignatura(<?php echo "'" . $a->getId() . "'"; ?>)"><?php echo $a->getNombre();
        ?></a></td>
        <td><?php echo $a->getShortName(); ?></td>
        <td><?php echo $a->getCodigo() ?></td>
        <td><?php echo AreaDAO::get()->getAreaByid($a->getIdArea())->getNombre(); ?></td>
        <td><?php echo $a->getGrado() . '°'; ?></td>
        <td><?php echo $a->getIntensidad(); ?></td>
        <?php
    }

    public static function printAsignaturasOptionByGrado($grado) {
        $a = AsignaturaDAO::getAsignaturaByGrado($grado);
        for ($i = 0; $i < count($a); $i++) {
            ?>
            <option class="<?php echo $a[$i]->getIdArea(); ?>" value="<?php echo $a[$i]->getId() ?>"><?php echo $a[$i]->getNombre() ?></option>
            <?php
        }
    }

    public static function deleteAsignatura($ida) {
        $error = AsignaturaDAO::delete($ida);
        if ($error == "") {
            return "<p id='ok'>ok</p>";
        }
        return "<p id='bad'>$error</p>";
    }

    public static function printFormAsignatura($ida = "") {
        $a = new Asignatura("", "", "", "", "", "", "", "");
        if ($ida != "") {
            $a = AsignaturaDAO::get()->getAsignaturaByid($ida);
        }
        ?>
        <form method="post" id="formularioAsignatura" >
            <input name="ida" value="<?php echo $ida; ?>" style="display: none"/>
            <table  >
                <tr>
                    <td><span>Area</span></td>
                    <td>
                        <select name="area">
                            <?php
                            $area = AreaDAO::getAreas();
                            for ($i = 0; $i < count($area); $i++) {
                                ?><option  <?php echo ($a->getIdArea() == $area[$i]->getCodigo()) ? "selected" : "" ?> value="<?php echo $area[$i]->getCodigo(); ?>"><?php echo $area[$i]->getNombre() ?></option><?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><span>Nombre</span></td>
                    <td><input name="nombre" type="text"  value="<?php echo $a->getNombre(); ?>"/></td>
                </tr>
                <tr>
                    <td><span>Nombre Corto</span></td>
                    <td><input name="shortname" type="text"  value="<?php echo $a->getShortName(); ?>"/></td>
                </tr>
                <tr>
                    <td><span>Grado</span></td>
                    <td>
                        <?php
                        if ($ida == "") {
                            $grados = ToolDAO::getGrados();
                            echo '<select name="grado">';
                            for ($i = 0; $i < count($grados); $i++) {
                                echo '<option value="' . $grados[$i]["idgrado"] . '" >' . $grados[$i]["g_nombre"] . '</option>';
                            }
                            echo '</select>';
                        } else {
                            ?>
                            <p><?php echo $a->getGrado(); ?>°</p>
                            <input  name="grado" value="<?php echo $a->getGrado(); ?>" style="display: none"/><?php
                        }
                        ?>
                    </td>
                </tr>
                <tr>                    
                    <td><span>Codigo</span></td></td>
                    <td><input name="codigo" type="text" value="<?php echo $a->getCodigo(); ?>"/></td>
                </tr> 
                <tr>
                    <td><span>Intensidad</span></td>
                    <td><input name="intensidad" type="number" value="<?php echo $a->getIntensidad(); ?>"/></td>
                </tr>
            </table>
            <span>Descripcion</span><br/>
            <textarea name="descripcion" ><?php echo $a->getDescripcion(); ?></textarea>
        </form>
        <?php
    }

    public static function guadarAsignatura($codigo, $nombre, $descripcion, $grado, $area, $intensidad, $shortname = "") {
        $a = new Asignatura(null, $codigo, $nombre, $descripcion, $grado, 0, $area, $intensidad, $shortname);
        $msn = AsignaturaDAO::get()->insertar($a);
        if ($msn != "") {
            echo "<p>" . $msn . "</p>";
            return;
        }
        echo "<p>ok</p>";
    }

    public static function printSelectAsignaturasDisponibles($grado, $curso) {
        $a = AsignaturaDAO::getAsignaturaByGrado($grado);
        for ($i = 0; $i < count($a); $i++) {
            ?>
            <option  class="<?php echo $a[$i]->getIdArea(); ?>"value="<?php echo $a[$i]->getId(); ?>"><?php echo $a[$i]->getNombre(); ?></option>
            <?php
        }
    }

    public static function editarAsignatura($id, $codigo, $nombre, $descripcion, $grado, $area, $intensidad, $shortname = "") {
        $a = new Asignatura($id, $codigo, $nombre, $descripcion, $grado, 0, $area, $intensidad, $shortname);
        $msn = AsignaturaDAO::get()->actualizar($a);
        if ($msn != "") {
            echo '<p id="error" >' . $msn . "</p>";
            return;
        }
        CAsignatura::printTr($a);
    }

    public static function eliminarAsignatura() {
        $id = $_POST["id"];
        echo "<h1>hola" . AsignaturaDAO::get()->eliminar($id) . "<h1>";
    }

    public static function requests() {
        if (empty($_GET["metodo"])) {
            return;
        }

        $r = $_GET["metodo"];

        if ($r == "printFormAsignatura") {
            $ida = (empty($_GET["ida"])) ? "" : $_GET["ida"];
            CAsignatura::printFormAsignatura($ida);
        } else if ($r == "guadarAsignatura") {
            CAsignatura::guadarAsignatura($_POST["codigo"], $_POST["nombre"], $_POST["descripcion"], $_POST["grado"], $_POST["area"], $_POST["intensidad"], $_POST["shortname"]);
        } else if ($r == "eliminarAsignatura") {
            CAsignatura::eliminarAsignatura();
        } else if ($r == "printAsignaturasOptionByGrado") {
            CAsignatura::printAsignaturasOptionByGrado($_GET["grado"]);
        } elseif ($r == "editarAsignatura") {
            CAsignatura::editarAsignatura($_POST["ida"], $_POST["codigo"], $_POST["nombre"], $_POST["descripcion"], $_POST["grado"], $_POST["area"], $_POST["intensidad"], $_POST["shortname"]);
        } elseif ($r == "printSelectAsignaturasDisponibles") {
            CAsignatura::printSelectAsignaturasDisponibles($_GET["grado"], $_GET["curso"]);
        } elseif ($r == "deleteAsignatura") {
            echo CAsignatura::deleteAsignatura($_POST["ida"]);
        }
    }

}

CAsignatura::requests();
?>
