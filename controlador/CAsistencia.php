<?php
include_once 'CClases.php';
include_once 'Clogin.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CAsistencia
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class CAsistencia {

    public static function printFormAsis($asis) {
        $_SESSION["asistencia"] = serialize($asis);
        $us = Clogin::getUser();
        $p = ($us->getTipo() == Usuario::PROFESOR) ? "readonly" : "";
        $readonly = "readonly";
        if ($asis->getEstado() != Asistencia::ACTIVA && $us->getTipo() == Usuario::PROFESOR) {
            $readonly = "readonly";
        } else {
            $readonly = "";
        }
        ?>
        <div class="formulario">
            <form method="post" id="formularioAsis" >
                <p>fecha<br><input type="date" name="fecha" value="<?php echo Calendario::format("Y-m-d", $asis->getFechaClase()) ?>" placeholder="Fecha" <?php echo $p ?>> </p>
                <p>hora<br><input type="time" name="hora" value="<?php echo Calendario::format("H:i", $asis->getFechaClase()) ?>" placeholder="Hora" <?php echo $p ?>> </p>
                <div>
                    <p>Estado</p>
                    <select name="estado">
                        <?php
                        if ($us->getTipo() == Usuario::PROFESOR) {
                            if ($readonly == "") {
                                ?><option value="<?php echo 1; ?>" ><?php echo CAsistencia::estadoName(1); ?></option><?php
                                ?><option value="<?php echo 2; ?>" ><?php echo CAsistencia::estadoName(2); ?></option><?php
                            } else if ($readonly == "readonly") {
                                ?><option value="<?php echo 2; ?>" ><?php echo CAsistencia::estadoName($asis->getEstado()); ?></option><?php
                                }
                            } else {
                                for ($i = 0; $i < count(Asistencia::$estados); $i++) {
                                    ?><option  <?php echo ($asis->getEstado() == $i) ? "selected" : ""; ?> value="<?php echo $i; ?>" ><?php echo CAsistencia::estadoName($i); ?></option><?php
                                }
                            }
                            ?>
                    </select>
                </div>
                <p>horas dictadas<br><input min="0"  max="<?php echo $asis->getHoras(); ?>"type="number" name="horasd" value="<?php echo $asis->getHorasDictadas() ?>" placeholder="horas dicatdas" <?php echo $readonly ?>></p>
                <p>tema dado<br><textarea name="tema" title="hola" placeholder="Tema"  <?php echo $readonly; ?> ><?php echo $asis->getTema() ?></textarea></p>
                <p>descripcion<br><textarea name="descripcion" title="hola" placeholder="Descripcion" <?php echo $readonly; ?> ><?php echo $asis->getDescriocion() ?></textarea></p>
                <p class="submit">
                    <input id="cancelAsist"type="button" name="cancel" value="Cancelar" >
                    <?php if ($readonly == "") { ?><input id="asis_guardar"type="button" name="commit" value="Guardar"> <?php } ?>
                </p>
            </form>
            <div id="respuestaAsist">                
            </div>
        </div>
        <?php
    }

    public static function printTablaAsistencia($idclase) {
        $clase = CClases::getClase($idclase);
        $asist = AsistenciaDAO::getAsistenciasByClaseId($clase->getId());
        ?>
        <div id="tt">
            <table  id="tablaAsistencia">
                <br>
                <h1>Tabla De Asistencia <?php echo $clase->getAsignatura()->getNombre() ?></h1>
                <thead>
                    <tr><th>Fecha de Clase</th><th>Hora</th><th>Horas D</th><th>Horas E</th><th>Tema</th><th>Estado</th></tr>
                </thead>
                <tbody>
                    <?php
                    $tipo = Clogin::getUser()->getTipo();
                    for ($i = 0; $i < count($asist); $i++) {
                        CAsistencia::actualizarEstado($asist[$i]);
                        CAsistencia::printtr($asist[$i], $tipo);
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function printtr($asist, $tipo) {
        ?>
        <tr id="ok" onclick="pfa('<?php echo $asist->getFechaClase() ?>', <?php echo $asist->getIdclase() ?>)" >
            <td><?php echo Calendario::format("D d M Y", $asist->getFechaClase()) ?></td>
            <td><?php Calendario::printHorasClase($asist->getFechaClase(), $asist->getHoras()) ?></td>
            <td><?php echo $asist->getHorasDictadas(); ?></td>
            <td><?php echo $asist->getTema(); ?></td>
            <td><?php echo $asist->getHoras(); ?></td>
            <td><?php
                CAsistencia::printEstado($asist->getEstado());
                if ($tipo == Usuario::PROFESOR) {
                    CAsistencia::opcionSolicitud($asist);
                }
                ?>
            </td>
        </tr>
        <?php
    }

    public static function printtd($asist,$tipo) {
        ?>
            <td><?php echo Calendario::format("D d M Y", $asist->getFechaClase()) ?></td>
            <td><?php Calendario::printHorasClase($asist->getFechaClase(), $asist->getHoras()) ?></td>
            <td><?php echo $asist->getHorasDictadas(); ?></td>
            <td><?php echo $asist->getTema(); ?></td>
            <td><?php echo $asist->getHoras(); ?></td>
            <td><?php
                CAsistencia::printEstado($asist->getEstado());
                if ($tipo == Usuario::PROFESOR) {
                    CAsistencia::opcionSolicitud($asist);
                }
                ?>
            </td>
        <?php
    }

    public static function opcionSolicitud($asist) {
        if ($asist->getEstado() == Asistencia::ESPERA || $asist->getEstado() == Asistencia::NO_DICTADA) {
            ?><a class="nuevaSolicitud"  onclick="nuevaS('<?php echo $asist->getFechaClase() ?>', '<?php echo $asist->getIdclase() ?>')" >Solicitud</a><?php
        }
    }

    public static function printEstado($estado) {
        ?>
        <img height="10" src="../vista/images/estados/<?php echo $estado; ?>.png" />
        <?php
    }

    public static function estadoName($estado) {
        if (Asistencia::ESPERA == $estado) {
            return "Espera";
        } else if (Asistencia::ACTIVA == $estado) {
            return "Activa";
        } else if (Asistencia::COMFIRMADA_PROFESOR == $estado) {
            return "Comfirmada Profesor";
        } else if (Asistencia::NO_DICTADA == $estado) {
            return "No diactada";
        } else if (Asistencia::DICTADA == $estado) {
            return "Dictada";
        } else if (Asistencia::RECUPERAR == $estado) {
            return "Recuperar";
        } else if (Asistencia::APLAZADA == $estado) {
            return "Aplazada";
        }
    }
    
    public static function estadoName2($estado) {
        if (Asistencia::ESPERA == $estado) {
            return "Aplazar";
        } else if (Asistencia::NO_DICTADA == $estado) {
            return "Recuperar";
        }
    }

    public static function actualizarEstado($asist) {
        $estado = $asist->getEstado();
        if (Asistencia::ESPERA == $estado || Asistencia::ACTIVA == $estado || Asistencia::APLAZADA == $estado || Asistencia::RECUPERAR == $estado) {
            $d = Calendario::diferent($asist->getHoras(), $asist->getFechaClase());
            if ($d < 0) {
//                $asist->setEstado(Asistencia::ESPERA);
            } else if ($d >= 0 && $d <= 86400) {
                $asist->setEstado(Asistencia::ACTIVA);
            } else if ($d > 86400) {
                $asist->setEstado(Asistencia::NO_DICTADA);
            }
            if ($asist->getEstado() != $estado) {
                AsistenciaDAO::actualizarEstado($asist);
            }
        }
    }

    public static function getAsist() {
        return unserialize($_SESSION["asistencia"]);
    }

    public static function guardarAsist() {
        $user = Clogin::getUser();
        $asist = CAsistencia::getAsist();
        $fecha = null;
        if ($user->getTipo() != Usuario::PROFESOR) {
            $fecha = $_POST["fecha"] . " " . $_POST["hora"];
        } else {
            $fecha = $asist->getFechaClase();
        }
        $asist->setEstado($_POST["estado"]);
        $asist->setDescripcion($_POST["descripcion"]);
        $asist->setTema($_POST["tema"]);
        $asist->setHorasDictadas($_POST["horasd"]);
        $asist->setFechaRegistro(Calendario::getFechaActual());
        AsistenciaDAO::actualizar($asist, $fecha);
        if (BD::error() == "") {
            CAsistencia::printtd($asist, $user->getTipo());
        } else {
            echo "<h2 id=\"error\">" . BD::error() . "</h2>";
        }
    }

    public static function p() {
        echo $_POST["descripcion"];
    }

    public static function requests() {
        if (empty($_GET["metodo"])) {
            return;
        }
        $r = $_GET["metodo"];
        @session_start();
        if ($r == "printTablaAsistencia") {
            CAsistencia::printTablaAsistencia($_POST["idclase"]);
        } else if ($r == "printFormAsis") {
            $clase = CClases::getClase($_POST["idclase"]);
            $asis = AsistenciaDAO::getAsistByClaseIdAndFecha($clase->getId(), $_POST["afecha"]);
            CAsistencia::printFormAsis($asis);
        } else if ($r == "guardarAsist") {
            CAsistencia::guardarAsist();
        }
    }

}

CAsistencia::requests();
?>
