<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CBoletin
 *
 * @author leon
 */
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/EstudianteDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/CursoDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/AsignaturaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ClaseDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/NotaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ObservacionDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/FormatoNotaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/SedeDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/RectorDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/InstitucionDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/logica/Tool.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/dto/Institucion.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/dto/Sede.php';
include_once 'CNota.php';

class CBoletin {

    public static function generarBoletinCurso($idcurso, $periodo, $idestu = "") {
        $es = array();
        if ($idestu == "") {
            $es = EstudianteDAO::getEstudiantesByCurso($idcurso);
        } else {
            $es[] = EstudianteDAO::get()->getEstudianteByid($idestu);
        }
        $institucion = InstitucionDAO::getInstitucion();
        $c = CursoDAO::getCursoByid($idcurso);
        $a = AsignaturaDAO::getAsignaturaByCurso($idcurso);
        $cl = ClaseDAO::getIdClaseByCursoId($idcurso, "ORDER BY `clase`.`asignatura_id_c` ASC");
        $sede = SedeDAO::getSedePrincipalByInstitucion(ClaseDAO::getClaseById($cl[0])->getSede());
        $fn = FormatoNotaDAO::getFormatoNotaByYear(substr($periodo, 0, 4));
        $p = $c->getDirector();
        $rector = RectorDAO::getRectorByYear($c->getFecha());
        ?>
        <html lang="es">
            <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <title>Boletin</title>
                <link href="../vista/css/boletin.css" rel="stylesheet"/>
                <link rel="SHORTCUT ICON" href="../vista/image/favicon.ico"/>
            </head>

            <body>
                <?php
                for ($k = 0; $k < count($es); $k++) {
                    ?>
                    <div class="<?php echo ($k == (count($es) - 1)) ? "" : "salto"; ?>">
                        <div>
                            <table width="900"  border="1" cellpadding="0" cellspacing="0" class="Contener">
                                <tr>
                                    <td width="101" height="107" ><img src="../vista/image/logo.jpg" width="100" height="97" /></td>
                                    <td width="600" class = "Titulo"> <center><b><?php echo $institucion->getNombre(); ?> Boletín Informativo</b>
                                </center>
                                </td>
                                <td width="191"><table class="Superior"  width="171" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="287" ><table width="194"  border="1" cellpadding="0" cellspacing="0" >
                                                    <tr>
                                                        <td width="180" height="31">Vigencia    <?php echo $institucion->getVigencia(); ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="22"><p>Versión <?php echo $institucion->getVersion(); ?> </p></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="27">Código <?php echo $institucion->getCodigo(); ?> </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                                </tr>
                            </table>
                            <div>
                                <table  width="900" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="1208"> <center> <i>  Dir. <?php echo $sede->getDireccion(); ?>  Tel. N° <?php echo $sede->getTelefono(); ?>- Resolucion N° <?php echo $institucion->getResolucion(); ?> </i> </center> </td>
                                    </tr>
                                </table> 
                                <table width="901" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="221"><table width="204" border="1" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="123">Año Lectivo</td>
                                                    <td width="61"><?php echo $c->getFecha(); ?></td>
                                                </tr>
                                            </table></td>
                                        <td width="473"> <center> 
                                        <div align="center"> <i>DANE : <?php echo $institucion->getDane(); ?> * NIT. <?php echo $institucion->getNit(); ?><br />
                                                &quot;<?php echo $institucion->getEslogan(); ?>&quot;
                                            </i></div>  </center> </td> 

                                    <td width="207"></td>
                                    </tr>
                                </table>
                                <table class="DatosEs" width="900" border="1" cellspacing="0" cellpadding="0">

                                    <tr>
                                        <td width="494"><center> Estudiante</center></td>
                                    <td width="126"><center>Código</center></td>
                                    <td width="137">Grado &quot;<?php echo $c->getGrado(); ?>&quot;</td>
                                    <td width="131">Jornada: Ordinaria</td>
                                    </tr>
                                    <tr>
                                        <td><center><?php echo $es[$k]->getApellido() . " " . $es[$k]->getNombre(); ?></center></td>
                                    <td><?php echo $es[$k]->getId(); ?></td>
                                    <td>Grupo &quot;<?php echo $c->getNombre(); ?>&quot;</td>
                                    <td>Nivel: &quot;<?php echo ($c->getGrado() < 6) ? "Primaria" : "Segundaria"; ?>&quot;</td>
                                    </tr>

                                </table>

                                <div class="TablaNotas">
                                    <table width="900" border="1" cellspacing="0" cellpadding="0">
                                        <thead>
                                            <tr>
                                                <th>*** REGISTRO ACADEMICO DE NOTAS***</th>
                                                <th>I.H</th>
                                                <th>NF</th>
                                                <?php
                                                for ($m = 0; $m < $fn->getNPeridos(); $m++) {
                                                    echo "<th>P " . ($m + 1) . "</th>";
                                                    if ($c->getGrado() > 0) {
                                                        echo "<th>" . number_format($fn->getPorcent()[$m] * 100, 0) . "%</th>";
                                                    }
                                                }
                                                ?>
                                                <th width="52" scope="col">Acum</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            for ($i = 0; $i < count($a); $i++) {
                                                $notas = NotaDAO::getNotasEstudianteClaseAno($es[$k]->getId(), $cl[$i], substr($periodo, 0, 4));
                                                $r = Tool::calcularNotaAno($notas, $fn->getPorcent(), $c->getGrado());
                                                ?>
                                                <tr>
                                                    <td class="nombreclase"><?php echo $a[$i]->getNombre() ?></td>
                                                    <td><?php echo $a[$i]->getIntensidad() ?></td>
                                                    <td><?php echo $r["faltas"]; ?></td>
                                                    <?php
                                                    for ($l = 0; $l < count($notas); $l++) {
                                                        $np = number_format($notas[$l]->getNotaFinal() * $fn->getPorcent()[$l], 1);
                                                        $n = number_format($notas[$l]->getNotaFinal(), 1);
                                                        echo '<td>' . (($n == "0.0") ? "" : Tool::formatNota($n, $c->getGrado())) . '</td>';
                                                        if ($c->getGrado() > 0) {
                                                            echo '<td>' . (($np == "0.0") ? "" : Tool::formatNota($np, $c->getGrado())) . '</td>';
                                                        }
                                                    }
                                                    ?>
                                                    <td><?php echo ($r["nota"] == 0) ? "" : Tool::formatNota($r["nota"], $c->getGrado()); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div>
                            <table width="900" border="1" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th scope="col">Escala de Valoracion De Desempeños: 0 a 2.9 = Bajo(B) - 3.0 a 3.9 =Basico (DB) - 4.0 a 4.7 = Alto(DA) - 4.8 a 5.0 = Superior (DS)</th>
                                </tr>
                            </table>
                            <div class="AnotacionesEséciales">
                                <p><em><u><strong>Anotaciones especiales:</strong></u></em><em><strong>Comportamiento: 
                                            <?
                                            $o = ObservacionDAO::getOnservacionById($es[$k]->getId(), $p->getId(), $idcurso, $periodo);
                                            if ($o == NULL) {
                                                $o = new Observacion("", "", "", "", "", "", "", "");
                                            }
                                            ?>
                                            <input type="text" name="NotaComportamiento" id="NotaComportamiento" value="<?php echo $o->getComportamiento(); ?>"/>
                                            <span><b>Faltas por dia: </b></span><input type="text" name="Faltas" id="NotaComportamiento" value="<?php echo $o->getFaltas(); ?>"/>

                                            </label>

                                        </strong>      </em>      </p>
                                <p class="parrafo">
                                    <?php
                                    echo $o->getDescripcion();
                                    ?>
                                </p>
                                </br>
                                </br>
                                </br>
                                </br>
                                </br>
                                </br>
                                </br>
                                </br>
                            </div>
                        </div>
                        <div class="Fimas">
                            <table width="900" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th width="450" scope="col"><p></p>
                                        <p><u><em><?php echo $rector->getApellido() . " " . $rector->getNombre(); ?></em></u><br />
                                            <br>Rectora</p></th>
                                    <th width="450" scope="col">  <p></p>
                                        <p><u>                       </u> <br />
                                            <?php echo (($p->getApellido() . " " . $p->getNombre())); ?><br>Director(a) de grupo</p></th>
                                </tr>
                            </table>
                            <p></p>
                        </div>
                        <p></p>
                    </div>
                    <?php
                }
                ?>
            </body>
            <script>
                window.print();
            </script>
        </html>
        <?php
    }

    public static function printFicha($curso, $periodo, $estu = "") {
        $c = CursoDAO::getCursoByid($curso);
        $p = $c->getDirector();
        if ($estu == "") {
            $o = ObservacionDAO::getObservacionesByCursoAndPeriodo($curso, $periodo);
        } else {
            $o = array();
            $o[0] = ObservacionDAO::getOnservacionById($estu, $p->getId(), $curso, $periodo);
        }
        ?>
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Ficha de Acompañamiento</title>
                <link href="../vista/css/boletin.css" rel="stylesheet"/>
                <link rel="SHORTCUT ICON" href="../vista/image/favicon.ico"/>
            </head>

            <body>
                <?php
                $n = count($o);
                for ($k = 0; $k < $n; $k++) {
                    $e = EstudianteDAO::getEstudiantById($o[$k]->getIdestudiante());
                    ?>
                    <div class="<?php echo ($k == ($n - 1)) ? "" : "salto"; ?>">
                        <table width="900"  border="1" cellpadding="0" cellspacing="0" class="Contener">
                            <tr>
                                <td width="101" height="107" ><img src="../vista/image/logo.jpg" width="100" height="110" /></td>
                                <td width="600" class = "Titulo"><center>
                                <b> Institución Educativa María Auxiliadora Ficha de Acompañamiento</b>
                            </center></td>
                            <td width="191"><table class="Superior"  width="171" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="287" ><table width="194"  border="1" cellpadding="0" cellspacing="0" >
                                                <tr>
                                                    <td width="180" height="31">Vigencia    08/04 2014</td>
                                                </tr>
                                                <tr>
                                                    <td height="22"><p>Versión  2</p></td>
                                                </tr>
                                                <tr>
                                                    <td height="27">Código F-A-18</td>
                                                </tr>
                                            </table></td>
                                    </tr>
                                </table></td>
                            </tr>
                        </table>
                        <div>
                            <table width="900" border="0" cellspacing="0" cellpadding="0">
                                <tr style="height: 33px">
                                    <th width="513">Estudiante: &quot;<?php echo $e->getApellido() . " " . $e->getNombre() ?>&quot; </th>
                                    <th width="144"><p>Grado: &quot;<?php echo $c->getGrado(); ?>&quot;</p></th>
                                    <th width="206">Periodo: &quot;<?php echo $periodo ?>&quot;</th>
                                    <th width="110">Nota: &quot;<?php echo $o[$k]->getComportamiento(); ?>&quot;</th>
                                </tr>
                            </table>
                            <table width="900" border="1" cellpadding="0" cellspacing="0" class="Cuadrodeinfo">
                                <tr>
                                    <th width="60" scope="col">Fecha</th>
                                    <th width="832" scope="col">Descripción General Del Proceso</th>
                                </tr>
                                <tr>
                                    <td style="min-width: 145"><p><?php echo $o[$k]->getFecha(); ?>
                                        </p>
                                        <p></p>
                                        <p></p>
                                        <p><br />
                                            <br />
                                            <br />
                                            <br />
                                        </p>
                                        <p><br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                        </p></td>
                                    <td valign="top">
                                        <p class="parrafo o"><?php echo $o[$k]->getDescripcion(); ?></p>
                                        <p class="o"><strong>Accion Pedagogica:</strong></p>
                                        <p class="parrafo o"><?php echo $o[$k]->getAccionPedagogica(); ?></p></td>
                                </tr>
                            </table>
                            <p><br />
                                <br />
                                <br />
                            </p>
                            <table width="900" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th class="nombre" width="297" scope="col"><u>                       </u> <br />
                                        <?php echo $p->getApellido() . " " . $p->getNombre(); ?><br>Director de grupo</th>
                                    <th width="300" scope="col"><u>                        </u> <br />
                                        <br>Familiar</th>
                                    <th class="nombre" width="303" scope="col"> <u>                       </u><br />
                                        <?php echo $e->getApellido() . " " . $e->getNombre(); ?><br>Firma de la niña</th>
                                </tr>
                            </table>
                            <p> </p>
                            <p></p>
                            <p></p>
                            <p></p>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </body>
        </html>
        <script>
            window.print();
        </script>
        <?
    }

    public static function printConsolidado($idcurso, $periodo, $orden) {
        ?>
        <html lang="es">
            <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <link rel="SHORTCUT ICON" href="../image/favicon.ico">
                <title>Consolidado</title>
                <link href="../vista/css/boletin.css" rel="stylesheet"/>
                <link rel="SHORTCUT ICON" href="../vista/image/favicon.ico"/>
            </head>
            <body>
                <div id="divConsolidado">
                    <?php
                    CNota::printCosolidadoCursoPerido($idcurso, $periodo, $orden);
                    ?>
                </div>
            </body>
        </html>
        <script>
            window.print();
        </script>
        <?php
    }

    public static function printPlantillaCurso($idcurso = "", $periodo = "") {
        ?>
        <html lang="es">
            <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <link rel="SHORTCUT ICON" href="../image/favicon.ico">
                <title>Consolidado</title>
                <link href="../vista/css/boletin.css" rel="stylesheet"/>
                <link rel="SHORTCUT ICON" href="../vista/image/favicon.ico"/>
            </head>
            <body>
                <div id="divConsolidado">
                    <?php
                    CNota::printNotasEstudiantesCurso($idcurso, $periodo);
                    ?>
                </div>
            </body>
            <script>
                window.print();
            </script>
        </html>
        <?php
    }

    public static function printPlantillaClase($idclase, $periodo, $empty = 1) {
        ?>
        <html lang="es">
            <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <link rel="SHORTCUT ICON" href="../image/favicon.ico">
                <title>Plantilla Clase</title>
                <link href="../vista/css/boletin.css" rel="stylesheet" type="text/css"/>
                <link rel="SHORTCUT ICON" href="../vista/image/favicon.ico"/>
            </head>
            <body>
                <div id="divConsolidado">
                    <?php
                    CNota::printNotasPeriodoNoEdit($idclase, $periodo, 1);
                    ?>
                </div>
            </body>
            <script>
                window.print();
            </script>
        </html>
        <?php
    }
    
    public static function requests() {
        if (empty($_GET["metodo"])) {
            return;
        }
        $r = $_GET["metodo"];
        if ($r == "generarBoletinCurso") {
            CBoletin::generarBoletinCurso($_GET["curso"], $_GET["periodo"], (empty($_GET["estu"])) ? "" : $_GET["estu"]);
        } elseif ($r == "printFicha") {
            $estu = (empty($_GET["estu"])) ? "" : $_GET["estu"];
            CBoletin::printFicha($_GET["curso"], $_GET["periodo"], $estu);
        } elseif ($r == "printConsolidado") {
            CBoletin::printConsolidado($_GET["curso"], $_GET["periodo"], $_GET["orden"]);
        } elseif ($r == "printPlantillaCurso") {
            CBoletin::printPlantillaCurso($_GET["curso"], $_GET["periodo"]);
        } elseif ($r == "printPlantillaClase") {
            CBoletin::printPlantillaClase($_GET["clase"], $_GET["periodo"]);
        }
    }

}

CBoletin::requests();