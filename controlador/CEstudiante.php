<?php
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/EstudianteDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ToolDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/CursoDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ClaseDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/FormatoNotaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/MatriculaDAO.php';
include_once realpath(dirname(__FILE__)) . '/../modelo/persistencia/ftp/FTP.php';
include_once 'Clogin.php';
include_once 'CTool.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CEstudiante
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
class CEstudiante {

    public static function printEstudiantes($grado = "all") {
        if ($grado == "all") {
            $est = EstudianteDAO::getEstudiantes();
        } else {
            $est = EstudianteDAO::getEstudiantesByGrado($grado);
        }
        ?>
        <table class="table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Codigo</th>
                    <th>Grado</th>
                    <th>Telefono</th>
                    <th>Correo</th>
                </tr>
            </thead>
            <tbody>
                <?php
                for ($i = 0; $i < count($est); $i++) {
                    ?>
                    <tr id="trestu<?php echo $est[$i]->getId(); ?>" class="<?php echo ($i % 2 == 0) ? "" : "tr_impar"; ?>"> 
                        <td class="name" ><a><?php echo $est[$i]->getApellido() . " " . $est[$i]->getNombre(); ?></a></td>
                        <td><?php echo $est[$i]->getId(); ?></td>
                        <td><?php echo $est[$i]->getGrado(); ?></td>
                        <td><?php echo $est[$i]->getTelefono(); ?></td>
                        <td><?php echo $est[$i]->getCorreo(); ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php
    }

    public static function printEstudiantesNoMatriculados($grado, $year) {
        $est = EstudianteDAO::getEstudiantesNoMatriculadoByGrado($grado, $year);
        ?>
        <h1 class="page-header">No Matriculados Año: <?php echo $year; ?> Grado: <?php echo $grado; ?>º
            <button type="button" class="btn btn-primary buttonMatricular" id="<?php echo $grado; ?>">Matricular Seleccionados</button>
        </h1>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Matricular</th>
                        <th>Nombre</th>
                        <th>Codigo</th>
                        <th>Grado</th>
                        <th>Telefono</th>
                        <th>Correo</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    for ($i = 0; $i < count($est); $i++) {
                        ?>
                        <tr id="tr<?php echo $est[$i]->getId(); ?>" class="<?php echo ($i % 2 == 0) ? "" : "tr_impar"; ?>"> 
                            <td><input type="checkbox" id="input<?php echo $est[$i]->getId(); ?>"/></td>
                            <td class="nombre" ><a><?php echo $est[$i]->getApellido() . " " . $est[$i]->getNombre(); ?></a></td>
                            <td><?php echo $est[$i]->getId(); ?></td>
                            <td><?php echo $est[$i]->getGrado(); ?></td>
                            <td><?php echo $est[$i]->getTelefono(); ?></td>
                            <td><?php echo $est[$i]->getCorreo(); ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function printEstudiantesSelect() {
        session_start();
        if (!Clogin::isSesion()) {
            return;
        }
        $us = Clogin::getUser();
        if ($us->getTipo() != Usuario::ADMIN) {
            return;
        }
        $est = EstudianteDAO::getEstudianteLike($_GET["campo"]);
        for ($i = 0; $i < count($est); $i++) {
            echo $est[$i]->getNombre() . "";
            ?>
            <option value="<?php echo $est[$i]->getId(); ?>"><?php echo $est[$i]->getId() . " - " . $est[$i]->getApellido() . " " . $est[$i]->getNombre(); ?></option>>
            <?php
        }
    }

    public static function printEstudiantesClase($id) {
        if (!Clogin::isSesion()) {
            return;
        }
        $us = Clogin::getUser();
        if ($us->getTipo() != Usuario::ADMIN) {
            return;
        }

        $est = EstudianteDAO::getEstudiantesByClaseid($id);
        ?>
        <div id="tableEstu">
            <table id="tclasesAdmin"  class="tablaAdmin">
                <caption><h2>Estudiantes</h2></caption>                    
                <thead>
                    <tr><th>Apellidos</th><th>Nombre</th><th>Codigo</th><th>Telefono</th><th>Correo </th><th class="opciones">Opciones</th></tr>
                </thead>
                <tbody>
                    <?php
                    for ($i = 0; $i < count($est); $i++) {
                        ?>
                        <tr> 
                            <td><?php echo $est[$i]->getApellido() ?></td>
                            <td><?php echo $est[$i]->getNombre() ?></td>
                            <td><?php echo $est[$i]->getId() ?></td>
                            <td><?php echo $est[$i]->getTelefono() ?></td>
                            <td><?php echo $est[$i]->getCorreo() ?></td>
                            <td><input type="button" name="eliminar" onclick="elimEstuClase('<?php echo $id; ?>', '<?php echo $est[$i]->getId(); ?>')" value="eliminar"/></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    public static function printFormEstudiante($id = "", $grado = "0") {
        if($grado == "all")$grado = "0";
        $estu = new Estudiante("", "", "", "", "", "", "", "", $grado);
        if ($id != "") {
            $estu = EstudianteDAO::get()->getEstudianteByid($id);
        }
        ?>
        <div  title="Nuevo Estudiante" id="formuEstudiante" class="formulario">
            <form method="post" id="formularioEstudiante" >
                <div class="img<?php echo ($id == "") ? " none" : "" ?>">
                    <img id="img" class="" src="<?php echo Clogin::getHost() ?>/userphoto/<?php echo $id ?>.jpg?date=<?php echo time() ?>" />
                </div>
                <div>
                    <label>Nombres</label>
                    <input name="nombre"type="text"  value="<?php echo $estu->getNombre(); ?>"/>
                </div>
                <div>
                    <label>Apellidos</label>
                    <input name="apellido" type="text" value="<?php echo $estu->getApellido(); ?>"/>
                </div>
                <div>
                    <label>Codigo</label>
                    <input name="codigo" type="text" value="<?php echo $estu->getId(); ?>"/>
                    <input name="codigoActual" type="text" value="<?php echo $estu->getId(); ?>" style="display: none"/>
                </div>
                <div>
                    <label>Telefono</label>
                    <input name="telefono" type="tel" value="<?php echo $estu->getTelefono(); ?>"/>
                </div>
                <div>
                    <label>Correo</label>
                    <input name="correo" type="email" value="<?php echo $estu->getCorreo(); ?>"/>
                </div>
                <div>
                    <label><?php echo ($id != "") ? "Grado Actual" : "Grado ingreso"; ?></label>
                    <select class="grado" name="grado" >
                        <?php
                        $grados = ToolDAO::getGrados();
                        for ($i = 0; $i < count($grados); $i++) {
                            ?> 
                            <option value="<?php echo $grados[$i]["idgrado"] ?>" <?php echo (($estu->getGrado() == $grados[$i]["idgrado"]) ? "selected" : ""); ?> ><?php echo $grados[$i]["g_nombre"] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div <?php echo ($id != "") ? "style=\"display: none\"" : ""; ?>>
                    <label title="">Año Lectivo</label>
                    <select class="year" name="year">
                        <?php
                        $years = BD::getField("periodo", "idperiodo");
                        for ($i = 0; $i < count($years); $i++) {
                            echo '<option value="' . $years[$i] . '">' . $years[$i] . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="curso" <?php echo ($id != "") ? "style=\"display: none\"" : ""; ?>>
                    <label title="Ultimo curso dictado">Curso</label>
                    <select name="curso">
                        <?php CEstudiante::printOPtionCurso($grado, $years[0]); ?>
                    </select>
                </div>
                <input class='none' type='text' name='photoname' value="<?php echo $estu->getId() ?>"/>
            </form>
            <div id="respuesta">                
            </div>
        </div>
        <?php
    }

    public static function printOPtionCurso($grado, $year = "") {
        $cursos = CursoDAO::getCursosByGradoId($grado, $year);
        for ($i = 0; $i < count($cursos); $i++) {
            echo "<option value='" . $cursos[$i]->getId() . "'>" . $cursos[$i]->getNombre() . "</option>";
        }
    }

    public static function matricular($idclase, $idestudiante) {
        $error = EstudianteDAO::matricular($idclase, $idestudiante);
        if ($error != "") {
            return "<h1>" . $error . "</h1>";
        }
        return "<h1>ok</h1>";
    }

    public static function changePhoto($source_file, $idestu) {
        return CTool::changeImage($source_file, $idestu);
    }

    public static function guadarEstudiante() {
        $idcurso = $_POST["curso"];
        $curso = CursoDAO::getCursoByid($idcurso);
        if ($curso == NULL) {
            return "<p id='bad'>No es posible matricular porque no existe el curso $idcurso<p>";
        }

        $idclases = ClaseDAO::getIdClaseByCursoId($curso->getId());
        if (count($idclases) == 0) {
            return "<p id='bad'>El Curso Selecconado no posee clases definidas<p>";
        }

        $est = new Estudiante($_POST["codigo"], $_POST["nombre"], $_POST["apellido"], $_POST["telefono"], $_POST["correo"], $_POST["codigo"], $_POST["codigo"], Usuario::ESTUDIANTE, $_POST["grado"]);
        $msn = EstudianteDAO::get()->insertar($est);
        if ($msn != "") {
            return "<p id='bad'>" . $msn . "</p>";
        }

        $fn = FormatoNotaDAO::getFormatoNotaByYear($curso->getFecha());
        $error = MatriculaDAO::matricularCurso($curso->getId(), $est->getId());
        if ($error != "") {
            return "<p id='bad'>$error</p>";
        }

        $sqlNota = MatriculaDAO::getSQLmatricularClasesEstudiantes($idclases, array($est->getId()), $curso->getFecha(), $fn->getNPeridos(), 1);
        BD::open();
        BD::sentenceSQL($sqlNota);
        if (BD::error() != "") {
            return "<p id='bad'>" . BD::error() . "</p>";
        }
        if (empty($_POST["photoname"])) {
            return "<p class='msn' id='ok'>Registro y matricula exitosa</p>";
        }
        $photoname = $_POST["photoname"];
        if (($photoname == "")) {
            return "<p class='msn' id='ok'>Registro y matricula exitosa</p>";
        }
        if ($photoname != $est->getId()) {
            $ext = $_POST["photoextension"];
            FTP::conect();
            $r = FTP::rename("", $photoname . "." . $ext, $est->getId() . "." . $ext);
            FTP::close();
            if (!$r) {
                return "<p class='msn' id='ok'>Registro y matricula exitosa,pero hubo problemas con la imagen</p>";
            }
        }

        return "<p class='msn' id='ok'>Registro y matricula exitosa</p>";
    }

    public static function updateEstudiante() {
        $est = new Estudiante($_POST["codigo"], $_POST["nombre"], $_POST["apellido"], $_POST["telefono"], $_POST["correo"], $_POST["codigo"], $_POST["codigo"], Usuario::ESTUDIANTE, $_POST["grado"]);
        $msn = EstudianteDAO::update($est, $_POST["id"]);
        if ($msn != "") {
            echo "<p id='error'>" . $msn . "</p>";
            return;
        }

        $idactual = $_POST["id"];
        if ($idactual != $est->getId()) {
            $ext = "jpg";
            FTP::conect();
            $EXIST = FTP::checkexists($idactual . "." . $ext);
            if ($EXIST == 1) {
                $r = FTP::rename("", $idactual . "." . $ext, $est->getId() . "." . $ext);
                FTP::close();
                return "<p id='ok'>NOMBREREANME: " . $_POST["id"] . " TO " . $est->getId() . "</p>";
            } else {
                FTP::close();
                return "<p id='ok' class='nono'>No entro" . ($EXIST == TRUE) ? "1" : "0" . " " . $_POST["id"] . "." . $ext . "</p>";
            }
            FTP::close();
        }

        return "<p id='ok'>ok</p>";
    }

    public static function eliminarMatricula($idclase, $idestudiante) {
        echo "<h1>hola" . EstudianteDAO::eliminarMatricula($idclase, $idestudiante) . "<h1>";
    }

    public static function eliminarEstudiante() {
        $id = $_POST["id"];
        echo "<h1>hola" . EstudianteDAO::get()->eliminar($id) . "<h1>";
    }

    public static function requests() {
        if (empty($_GET["metodo"])) {
            return;
        }

        $r = $_GET["metodo"];
        if ($r == "printFormEstudiante") {
            $estu = (empty($_POST["estu"])) ? "" : $_POST["estu"];
            $grado = (empty($_POST["grado"]) ? "0" : $_POST["grado"]);
            CEstudiante::printFormEstudiante($estu, $grado);
        } else if ($r == "guadarEstudiante") {
            echo CEstudiante::guadarEstudiante();
        } else if ($r == "eliminarEstudiante") {
            CEstudiante::eliminarEstudiante();
        } else if ($r == "printEstudiantesSelect") {
            CEstudiante::printEstudiantesSelect();
        } else if ($r == "printEstudiantesClase") {
            session_start();
            CEstudiante::printEstudiantesClase($_GET["idclase"]);
        } else if ($r == "matricular") {
            echo CEstudiante::matricular($_POST["idclase"], $_POST["idestudiante"]);
        } else if ($r == "eliminarMatricula") {
            echo CEstudiante::eliminarMatricula($_POST["idclase"], $_POST["id"]);
        } else if ($r == "updateEstudiante") {
            echo CEstudiante::updateEstudiante();
        } else if ($r == "printOPtionCurso") {
            CEstudiante::printOPtionCurso($_GET["grado"], $_GET["year"]);
        } elseif ($r == "printEstudiantesNoMatriculados") {
            CEstudiante::printEstudiantesNoMatriculados($_GET["grado"]);
        } elseif ($r == "changePhoto") {
            echo CEstudiante::changePhoto($_FILES["Filedata"]["tmp_name"], $_GET["id"]);
        }
    }

}

CEstudiante::requests();
?>
